var app=new Vue({
    el: '#aplicacion',
    data:{
        respuesta_controlador : 0,
        // botones principales de la pantalla de registro paciente
        nuevo : 'activo',
        editar : 'inactivo',
        imprimir : 'inactivo',
        limpiar : 'inactivo',
        grabar_actualizar : '',
        guardar : true,
        verificar_titular : [],
        verificar_titular_cedula : '',

        // btotones de busquedad de registro paciente
        buscar_nombre : 'activo',
        buscar_titular_nombre : 'inactivo',

        //boton del modal de la creacion de usuarios
        nuevo_titular : 'activo',

        // vsibilidad de los campos del formulario registro pacientes
        visibilidad_IB: false,
        visibilidadIB: false,
        visibilidad_IB2: false,//
        visibilidad_T1 : false,
        visibilidad_T2 : false,
        visibilidad_T3 : false,
        visibilidad_ID :false,
        visibilidad_ID2 :false,
        visibilidad_S: false,
        visibilidad_S2: false,
        visibilidad_S3: false,
        visibilidad_IA : false,
        visibilidad_IA2 : false,
        visibilidad_IB3 : false,
        visibilidad_paciente : false,
        limpiar_ : false,
        visibilidad_coberturas : false,
        // fn visibilidad

        in_cedula : '', // variable para la busqueda por cedula
        coberturas: false,
        busqueda :false, // para que no se recargue el combo de ciudades

        discapacidad : '',
        otro_seguro : 0,

        apellido: '', // para realizar las busquedas por apellido

        // combos donde se va a cargar la informacion consultada de la base
        listaAfiliados : [],
        listaAfiliadosCedula: [],
        listaInfoAdicional : [],
        listaInfoTitular : [],
        cmbidentificacion : [],
        cmbSangre : [],
        cmbNacionalidad : [],
        cmbParentesco : [],
        cmbGenero: [],
        cmbEstadoCivil : [],
        cmbEtnia : [],
        cmbCiudad : [],
        cmbParroquia : [],
        cmbProvincia : [],
        cmbSeguro : [],
        cmbOtroSeguro : [],
        cmbBeneficiario : [],
        cmbDocumentosIess : [],
        cmbIngresoCalificacion : [],
        ConsultarPacienteCliente : [],

        // cambos del formulario
        id : '',
        cedula: '',
        tipo_identificacion : 0,
        primer_nombre : '',
        segundo_nombre : '',
        apellido_paterno : '',
        apellido_materno : '',
        genero : "M",
        fecha_nacimiento : '',
        edad :0,
        lugar_nacimiento : '',
        ocupacion : '',
        estado_civil : 0,
        tipo_sangre : 0,
        nacionalidad : 0,
        pais : 0,
        provincia : 0,
        ciudad : 0,
        parroquia : 0,
        direccion : '',
        telefono : '',
        celular : '',
        otro : '',
        observacion : '',
        lugar_trabajo : '',
        tipo_parentesco : '',
        tipo_beneficiario : '',
        cedula_titular : '',
        tipo_seguro : 0,
        des_campo1 : '',
        des_campo2 : '',
        des_campo3 : '',
        usuario_ingreso : '',
        fecha_ingreso : '',
        usuario_modificacion : '',
        fecha_modificacion : '',
        pcname : '',
        status : '',
        status_discapacidad : '',
        status_discapacidad2 : '',
        carnet_conadis : '',
        carnet_conadis2 : '',
        status_otro_seguro : '',
        tipo_seguro_iess : '',
        descripcion_otro_seguro : '',
        etnico : 0,

        // informacion adicional
        id_a : '',
        paciente : '',
        nombres_padre : '',
        nombres_madre : '',
        conyugue : '',
        des_campo1_a : '',
        des_campo2_a : '',
        des_campo3_a : '',
        usuario_ingreso_a : '',
        fecha_ingreso_a : '',
        usuario_modificacion_a : '',
        pcname_a : '',
        status_a : '',
        cedula_seg_progenitor : '',
        fecha_cedulacion : '',
        fecha_fallecimiento : '',
        instruccion : '',
        cobertura_compartida : '',
        prepagada : false,
        issfa : false,
        isspol : false,
        iess : false,

        // información del titular o afiliado que va en el form
        apellidos :'',
        nombres : '',
        observacionTitular : '',

        // variables temporales para distintas funcionalidades
        TempPais : 1,
        TempProvincia :1,
        TempCiudad :1,
        TempParroquia :1,
        TempTipoSeguro : 0, //pasar como campos ocultos en el update
        TempTitular : '', //pasar como campos ocultos en el update
        TempTipoBeneficiario : '', // prueba para elegir el tipo de beneficiario

        // combo para guardar todos los errores del formulario
        errores : [],

        CedulaPueba : '0923956916',
        CedulaPueba2 :'0951556745',

        // información del titular traida de la base de datos
        ListaDatosTitular : [],
        Titularid : '',
        Titularcedula: '',
        Titulartipo_identificacion : 0,
        Titularprimer_nombre : '',
        Titularsegundo_nombre : '',
        Titularapellido_paterno : '',
        Titularapellido_materno : '',
        Titulargenero : '',
        Titularfecha_nacimiento : '',
        Titularlugar_nacimiento : '',
        Titularocupacion : '',
        Titularestado_civil : '',
        Titulartipo_sangre : '',
        Titularnacionalidad : '',
        Titularpais : '',
        Titularprovincia : '',
        Titularciudad : 0,
        Titularparroquia : 0,
        Titulardireccion : '',
        Titulartelefono : '',
        Titularcelular : '',
        Titularotro : '',
        Titularobservacion : '',
        Titularlugar_trabajo : '',
        Titulartipo_parentesco : '',
        Titulartipo_beneficiario : '',
        Titularcedula_titular : '',
        Titulartipo_seguro :0,
        Titulardes_campo1 : '',
        Titulardes_campo2 : '',
        Titulardes_campo3 : '',
        Titularusuario_ingreso : '',
        Titularfecha_ingreso : '',
        Titularusuario_modificacion : '',
        Titularfecha_modificacion : '',
        Titularpcname : '',
        Titularstatus : '',
        Titularstatus_discapacidad : '',
        Titularcarnet_conadis : '',
        Titularstatus_otro_seguro : '',
        Titulartipo_seguro_iess : '',
        Titulardescripcion_otro_seguro : '',
        Titularetnico : 1,
        Titularedad : 0,

        // document.getElementById("myForm").submit();
        // Cuentas por cobrar
        razonSocial : "",
        conacto : "",
        direccionDomicilio: "",
        direccionComercial: "",
        telefonos: "",
        fax: "",
        telex: "",
        casilla: "",
        email: "",
        paisC: "",
        provinciaC: "",
        ciudadC : "",
        zona: "",
        ruc: "",
        tipoIdentificacionC: "",
        tipoId : "",
        listaPrecioId : "",
        venCodigo : "",
        recCodigo : "",
        codigodeCuenta : "",
        cuentaAuxiliar : "",
        departamentoId: "",
        plazo : "",
        cupo : "",
        descuento : "",
        comentario : "",
        pagaIva : "",
        tipoPago : "",
        conribuyenteEspecial : "",
        retFuente : "",
        retIva : "",
        numLetra : "",
        empresa : "",
        codigoAuxiliar : "",
        codigoAuxiliar2 : "",
        refLabEmpresa : "",
        refLabActividad : "",
        refLabCargo : "",
        refLabAntiguedad : "",
        refLabDireccion : "",
        lugarDeNacimiento : "",
        fechaDeNacimiento : "",
        edadC : "",
        estadoCivil : "",
        conyuge : "",
        cargasFamiliares : "",
        activo : "",
        adiCup : "",
        adiFec : "",
        empleado : ""
    },

    created: function () {
        // tomamos la variable de entrada
        //this.respuesta_controlador = $("#Respuesta_controlador").val();
        // definimos los mensajes
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "3000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        // es la primera que se ejecuta y se encarga de llenar los combos

        axios.get('tipoidentificacion').then(response => {
            this.cmbidentificacion = response.data
    }).
        catch(function (error) {
            console.log(error);
        });


        var urlProvincia = 'provincia/1';
        axios.get(urlProvincia).then(response => {
            this.cmbProvincia = response.data
    }).
        catch(function (error) {
            console.log(error);
        });


        var urlCiudad = 'ciudad/1';
        axios.get(urlCiudad).then(response => {
            this.cmbCiudad = response.data
    }).
        catch(function (error) {
            console.log(error);
        });
        var urlParroquia = 'parroquia/1';
        axios.get(urlParroquia).then(response => {
            this.cmbParroquia = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        var urlBeneficiario = 'parentesco/1';
        axios.get(urlBeneficiario).then(response => {
            this.cmbBeneficiario = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('otroseguro').then(response => {
            this.cmbOtroSeguro = response.data
    }).
        catch(function (error) {
            console.log(error);
        });
        axios.get('seguro').then(response => {
            this.cmbSeguro = response.data
    }).
        catch(function (error) {
            console.log(error);
        });
        axios.get('etnia').then(response => {
            this.cmbEtnia = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('nacionalidad').then(response => {
            this.cmbNacionalidad = response.data
    }).
        catch(function (error) {
            console.log(error);
        });


        axios.get('estadoCivil').then(response => {
            this.cmbEstadoCivil = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('sangre').then(response => {
            this.cmbSangre = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('genero').then(response => {
            this.cmbGenero = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('parentescoPaciente').then(response => {
            this.cmbParentesco = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('documentosiess').then(response => {
            this.cmbDocumentosIess = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        axios.get('tipoingreso').then(response => {
            this.cmbIngresoCalificacion = response.data
    }).
        catch(function (error) {
            console.log(error);
        });

        // var url = 'consultarpacienteid/'+this.respuesta_controlador+'';
        // axios.get(url).then(response => {
        //      this.listaAfiliadosCedula = response.data;
        //     var cedula = '';
        //  cedula = this.listaAfiliadosCedula[0].cedula;
    // });

            //
            //axios.get('tipoingreso').then(response => {
            //  this.cmbIngresoCalificacion  = response.data
            //})

            this.visibilidad_IB = true;
            this.visibilidad_IB2= true;
            this.visibilidad_IB3= false;//
            this.visibilidad_T1 = true;
            this.visibilidad_T3 = true;
            this.visibilidad_T2 = true;
            this.visibilidad_ID =true;
            this.visibilidad_ID2 =true;
            this.visibilidad_S = true;
            this.visibilidad_S2 = true;
            this.visibilidad_IA = true;
            this.visibilidad_IA2 = true;
            this.visibilidad_paciente = true;


    },

    methods: {

        /*------------------controles principales-----------------------------------*/
            // botones de la parte superior del formulario

            Editar : function () {
                this.busqueda = false;
                this.ciudad = this.TempCiudad;

                if(this.des_campo1 === "R") {
                    this.visibilidad_ID = false; //editable
                    this.visibilidad_S = false; //editable
                    this.visibilidad_S2 = false; //editable
                    this.visibilidad_T2 = false;
                    this.visibilidad_T3 = false;
                    this.visibilidad_T1 = false;

                    this.visibilidad_IA2 = false;
                    this.visibilidad_IB = true;
                    if(this.tipo_sangre === 0) {
                        this.visibilidad_IB2 = false;
                    }
                } else {
                    this.visibilidad_T1 =false;
                    this.visibilidad_T2 =true;
                    if(this.tipo_parentesco === "AA"){
                        this.visibilidad_T3 = true;
                    } else {
                        this.visibilidad_T3 = false;
                    }

                    this.visibilidad_IB = false;
                    this.visibilidad_IB2= false; // editable
                    this.visibilidad_ID = false; //editable
                    this.visibilidad_S = false; //editable
                    this.visibilidad_S2 = false; //editable
                    this.visibilidad_IA = false;
                    this.visibilidad_IA2 = false; //editable
                    this.visibilidad_paciente = false;

                }


                this.limpiar = 'activo';
                this.grabar_actualizar = "Acualizar";
                this.editar = 'inactivo';
                this.guardar = true;
            },
            Nuevo: function () {
                this.busqueda = false;
                this.Limpiar();
                this.guardar = true;
                this.nuevo = 'inactivo';
                this.limpiar = 'activo';
                this.buscar_nombre = 'inactivo';
                this.buscar_titular_nombre = 'activo';
                this.nuevo_titular = 'inactivo';
                this.grabar_actualizar = "Nuevo";
                this.visibilidad_IB = false;
                this.visibilidad_IB2= false;
                this.visibilidad_IB3= false;//
                this.visibilidad_T1 = false;
                this.visibilidad_T2 = false;
                this.visibilidad_T3 = false;
                this.visibilidad_ID =false;
                this.visibilidad_ID2 =false;
                this.visibilidad_S = false;
                this.visibilidad_S2 = false;
                this.visibilidad_IA = false;
                this.visibilidad_IA2 = false;
                this.visibilidad_paciente = false;
            },
            LimpiarInfoPaciente : function () { // este metodo se utiliza cuando se esta creando un nuevo titular
                this.edad = '',
                    this.in_cedula = '',
                    this.apellido= '',
                    this.id  ='',
                    this.cedula=  '',
                    this.tipo_identificacion = '',
                    this.primer_nombre = '',
                    this.segundo_nombre= '',
                    this.apellido_paterno = '',
                    this.apellido_materno = '',
                    this.genero = '',
                    this.fecha_nacimiento = '',
                    this.lugar_nacimiento = '',
                    this.ocupacion = '',
                    this.estado_civil = '',
                    this.tipo_sangre = '',
                    this.nacionalidad = '',
                    this.pais = '',
                    this.provincia = '',
                    this.ciudad = '',
                    this.parroquia = '',
                    this.direccion = '',
                    this.telefono = '',
                    this.celular = '',
                    this.otro = '',
                    this.observacion = '',
                    this.lugar_trabajo = '',
                    this.tipo_seguro = '',
                    this.des_campo1 = '',
                    this.des_campo2 = '',
                    this.des_campo3 = '',
                    this.usuario_ingreso = '',
                    this.fecha_ingreso = '',
                    this.usuario_modificacion = '',
                    this.fecha_modificacion = '',
                    this.pcname = '',
                    this.status = '',
                    this.status_discapacidad = '',
                    this.carnet_conadis = '',
                    this.status_otro_seguro = '',
                    this.tipo_seguro_iess = '',
                    this.descripcion_otro_seguro = '',
                    this.etnico = '',
                    this.id_a = '',
                    this.paciente = '',
                    this.nombres_padre = '',
                    this.nombres_madre = '',
                    this.conyugue = '',
                    this.des_campo1_a = '',
                    this.des_campo2_a = '',
                    this.des_campo3_a = '',
                    this.usuario_ingreso_a = '',
                    this.fecha_ingreso_a= '',
                    this.usuario_modificacion_a = '',
                    this.pcname_a = '',
                    this.status_a = '',
                    this.cedula_seg_progenitor = '',
                    this.fecha_cedulacion = '',
                    this.fecha_fallecimiento = '',
                    this.instruccion = '',
                    this.cobertura_compartida = '',
                    this.prepagada = '',
                    this.issfa = '',
                    this.isspol = '',
                    this.iess = ''
            },
            Limpiar: function () {

                // dejar botones en su estado inicial
                this.nuevo = 'activo';
                this.editar = 'inactivo';
                this.imprimir = 'inactivo';
                this.limpiar = 'inactivo';
                this.buscar_nombre= 'activo';
                this.buscar_titular_nombre = 'inactivo';
                this.nuevo_titular = 'activo';

                // vaciar combos
                /*
                this.listaAfiliados = [];
                this.listaAfiliadosCedula= [];
                this.listaInfoAdicional = [];
                this.listaInfoTitular = [];
                this.cmbidentificacion = [];
                this.cmbSangre = [];
                this.cmbNacionalidad = [];
                this.cmbParentesco = [];
                this.cmbGenero= [];
                this.cmbEstadoCivil = [];
                this.cmbEtnia = [];
                this.cmbCiudad = [];
                this.cmbParroquia = [];
                this.cmbProvincia = [];
                this.cmbSeguro = [];
                this.cmbOtroSeguro = [];
                this.cmbBeneficiario = [];
                this.cmbDocumentosIess = [];
                this.cmbIngresoCalificacion = [];
            */

                //borrar información de los campos
                this.edad = '';
                this.in_cedula = '';
                this.apellido= '';
                this.id  ='';
                this.cedula=  '';
                this.tipo_identificacion = '';
                this.primer_nombre = '';
                this.segundo_nombre= '';
                this.apellido_paterno = '';
                this.apellido_materno = '';
                this.genero = '';
                this.fecha_nacimiento = '';
                this.lugar_nacimiento = '';
                this.ocupacion = '';
                this.estado_civil = '';
                this.tipo_sangre = '';
                this.nacionalidad = '';
                this.pais = '';
                this.provincia = '';
                this.ciudad = '';
                this.parroquia = '';
                this.direccion = '';
                this.telefono = '';
                this.celular = '';
                this.otro = '';
                this.observacion = '';
                this.lugar_trabajo = '';
                this.tipo_parentesco = '';
                this.tipo_beneficiario = '';
                this.cedula_titular = '';
                this.tipo_seguro = '';
                this.des_campo1 = '';
                this.des_campo2 = '';
                this.des_campo3 = '';
                this.usuario_ingreso = '';
                this.fecha_ingreso = '';
                this.usuario_modificacion = '';
                this.fecha_modificacion = '';
                this.pcname = '';
                this.status = '';
                this.status_discapacidad = '';
                this.carnet_conadis = '';
                this.status_otro_seguro = '';
                this.tipo_seguro_iess = '';
                this.descripcion_otro_seguro = '';
                this.etnico = '';


                this.id_a = '';
                this.paciente = '';
                this.nombres_padre = '';
                this.nombres_madre = '';
                this.conyugue = '';
                this.des_campo1_a = '';
                this.visibilidad_IB = true;

                this.des_campo2_a = '';
                this.des_campo3_a = '';
                this.usuario_ingreso_a = '';
                this.fecha_ingreso_a= '';
                this.usuario_modificacion_a = '';
                this.pcname_a = '';
                this.status_a = '';
                this.cedula_seg_progenitor = '';
                this.fecha_cedulacion = '';
                this.fecha_fallecimiento = '';
                this.instruccion = '';
                this.cobertura_compartida = '';
                this.prepagada = '';
                this.issfa = '';
                this.isspol = '';
                this.iess = '';


                // información del titular o afiliado
                this.apellidos ='';
                this.nombres = '';
                this.observacionTitular = '';

                // Cargar combos
                // this.CargarCombosAll();

                // variables temporales
                this.TempProvincia =0;
                this.TempCiudad =0;
                this.TempParroquia =0;
                this.visibilidad_IB2= true;
                this.visibilidad_IB3= false;//
                this.visibilidad_T1 = false;
                this.visibilidad_T2 = true;
                this.visibilidad_T3 = false;
                this.visibilidad_ID =true;
                this.visibilidad_ID2 =true;
                this.visibilidad_S = true;
                this.visibilidad_S2 = true;
                this.visibilidad_IA = true;
                this.visibilidad_IA2 = true;


            },

        /*------------------//controles principales-----------------------------------*/



        /*------------------ Carga de información-----------------------------------*/
            // llena la información dentro del formulario

            cargarCombos : function (pais,provincia,ciudad) {
                // cargaa la info de las ciudades en las consultas
                var urlProvincia = 'provincia/'+pais+'';
                    axios.get(urlProvincia).then(response => {
                        this.cmbProvincia = response.data
                    })


                    var urlCiudad = 'ciudad/'+provincia+'';
                    axios.get(urlCiudad).then(response => {
                        this.cmbCiudad = response.data
                    })


                var urlParroquia = 'parroquia/'+ciudad+'';
                axios.get(urlParroquia).then(response => {
                    this.cmbParroquia = response.data
                })

                this.pais= pais;
                // llama al metodo onchaged
                this.provincia = provincia;
                //this.ciudad = ciudad;

            },
            InformacionTitular : function (data) {

                var urlTitular = 'registropacientes/TitularAfiliadoPorCedula/'+data[0].cedula_titular+'';
                axios.get(urlTitular).then(response => {
                    this.listaInfoTitular = response.data;
                this.apellidos =""+this.listaInfoTitular[0].apellido_paterno+" " + this.listaAfiliadosCedula[0].apellido_materno+""  ;
                if(this.listaAfiliadosCedula[0].segundo_nombre === null){
                    this.nombres =""+this.listaInfoTitular[0].primer_nombre+"";

                }else {
                    this.nombres =""+this.listaInfoTitular[0].primer_nombre+" " + this.listaAfiliadosCedula[0].segundo_nombre+""  ;

                }

                this.observacionTitular = this.listaInfoTitular[0].observacion;
                this.cedula_titular = data[0].cedula_titular;
                })
            },
            ConsultarTitularCedula : function() {


                if(this.cedula_titular.length === 10 || this.cedula_titular.length === 13) {

                    var urlUsers = 'registropacientes/TitularAfiliadoPorCedula/'+this.cedula_titular+'';
                    axios.get(urlUsers).then(response => {
                        this.ListaDatosTitular  = response.data;

                    if(this.ListaDatosTitular === "") {
                        alert("El titular no existe en la base de datos por favor realizar su registro");
                    } else{
                        if(this.ListaDatosTitular[0].tipo_parentesco !== "AA") {
                            toastr.error("Este paciente no se puede registrar como titular");
                        } else {
                            this.Titularapellido_paterno = this.ListaDatosTitular[0].apellido_paterno;
                            this.Titularid = this.ListaDatosTitular[0].id;
                            this.Titulartipo_identificacion = this.ListaDatosTitular[0].tipo_identificacion;
                            this.Titularprimer_nombre = this.ListaDatosTitular[0].primer_nombre;
                            this.Titularsegundo_nombre = this.ListaDatosTitular[0].segundo_nombre;
                            this.Titularapellido_paterno = this.ListaDatosTitular[0].apellido_paterno;
                            this.Titularapellido_materno = this.ListaDatosTitular[0].apellido_materno;
                            this.Titulargenero = this.ListaDatosTitular[0].genero;
                            this.Titularfecha_nacimiento = new Date(this.ListaDatosTitular[0].fecha_nacimiento);
                            // convertir la fecha que viene de la base en un tipo legible para el formulario ademas la edad
                            this.FormarFechaEdad(response.data);
                            //
                            this.Titularlugar_nacimiento = this.ListaDatosTitular[0].lugar_nacimiento;
                            this.Titularocupacion = this.ListaDatosTitular[0].ocupacion;
                            this.Titularestado_civil= this.ListaDatosTitular[0].estado_civil;
                            this.Titulartipo_sangre= this.ListaDatosTitular[0].tipo_sangre;
                            this.Titularnacionalidad =this.ListaDatosTitular[0].nacionalidad;
                            this.Titularpais = this.ListaDatosTitular[0].pais;
                            this.Titularprovincia = this.ListaDatosTitular[0].provincia;
                            this.Titularciudad = this.ListaDatosTitular[0].ciudad;
                            this.Titulardireccion = this.ListaDatosTitular[0].direccion;
                            this.Titulartelefono = this.ListaDatosTitular[0].telefono;
                            this.Titularcelular = this.ListaDatosTitular[0].celular;
                            this.Titularotro = this.ListaDatosTitular[0].otro;
                            this.Titularobservacion = this.ListaDatosTitular[0].observacion;
                            this.Titularlugar_trabajo  = this.ListaDatosTitular[0].lugar_trabajo;
                            this.Titulartipo_parentesco  = this.ListaDatosTitular[0].tipo_parentesco;
                            this.Titulartipo_beneficiario  = this.ListaDatosTitular[0].tipo_beneficiario;
                            this.Titularcedula_titular  = this.ListaDatosTitular[0].cedula_titular;
                            this.Titulartipo_seguro  = this.ListaDatosTitular[0].tipo_seguro;
                            this.Titulardes_campo1  = this.ListaDatosTitular[0].des_campo1;
                            this.Titulardes_campo2  = this.ListaDatosTitular[0].des_campo2;
                            this.Titulardes_campo3  = this.ListaDatosTitular[0].des_campo3;
                            this.Titularusuario_ingreso = this.ListaDatosTitular[0].usuario_ingreso;
                            this.Titularfecha_ingreso  = this.ListaDatosTitular[0].fecha_ingreso;
                            this.Titularusuario_modificacion  = this.ListaDatosTitular[0].usuario_modificacion;
                            this.Titularfecha_modificacion  = this.ListaDatosTitular[0].fecha_modificacion;
                            this.Titularpcname  = this.ListaDatosTitular[0].pcname;
                            this.Titularstatus  = this.ListaDatosTitular[0].status;
                            this.Titularstatus_discapacidad  = this.ListaDatosTitular[0].status_discapacidad;
                            this.Titularcarnet_conadis  = this.ListaDatosTitular[0].carnet_conadis;
                            this.Titularstatus_otro_seguro  = this.ListaDatosTitular[0].status_otro_seguro;
                            this.Titulartipo_seguro_iess  = this.ListaDatosTitular[0].tipo_seguro_iess;
                            this.Titulardescripcion_otro_seguro  = this.ListaDatosTitular[0].descripcion_otro_seguro;
                            this.Titularetnico  = this.ListaDatosTitular[0].etnico;
                            this.Titularparroquia  = this.ListaDatosTitular[0].parroquia;
                            //this.cedula_titular = this.Titularcedula;
                            this.apellidos = this.Titularapellido_paterno + ' ' + this.Titularapellido_materno;
                            this.nombres = this.Titularprimer_nombre + ' ' + this.Titularsegundo_nombre;
                            this.observacion = this.Titularobservacion;
                        }

                    }

                })

                }
                else {
                    alert("ingrese un numero de cedula de 10 0 13 digitos");
                }


            },
            pasarDatosCedulaConsulta : function() {
                // se pasan los datos al formulario
                var numero = 0;

                var urlUsers = 'registropacientes/TitularAfiliadoPorCedulaAll/'+this.in_cedula+'';
                axios.get(urlUsers).then(response => {
                        this.listaAfiliadosCedula = response.data;
                    this.numero = this.listaAfiliadosCedula.length;
                    if (this.numero === 0) {
                        toastr.error("No existen datos asociados a ese tipo de identificación.")
                    }
                    else {
                            // le indicamos al sistema que se va a realizar una busqueda para que al momento de cambiar
                            // la ciudad y se active el metodo onchanged sepa que valor se debe tomar.
                            this.busqueda = true;

                            this.id = this.listaAfiliadosCedula[0].id;
                            this.tipo_identificacion = this.listaAfiliadosCedula[0].tipo_identificacion;
                            this.primer_nombre = this.listaAfiliadosCedula[0].primer_nombre;
                            this.segundo_nombre = this.listaAfiliadosCedula[0].segundo_nombre;
                            this.apellido_paterno = this.listaAfiliadosCedula[0].apellido_paterno;
                            this.apellido_materno = this.listaAfiliadosCedula[0].apellido_materno;
                            this.genero = this.listaAfiliadosCedula[0].genero;
                            this.fecha_nacimiento = new Date(this.listaAfiliadosCedula[0].fecha_nacimiento);
                            // convertir la fecha que viene de la base en un tipo legible para el formulario ademas la edad
                            this.FormarFechaEdad2(response.data);
                            //
                            this.lugar_nacimiento = this.listaAfiliadosCedula[0].lugar_nacimiento;
                            this.ocupacion = this.listaAfiliadosCedula[0].ocupacion;
                            this.estado_civil= this.listaAfiliadosCedula[0].estado_civil;
                            this.tipo_sangre= this.listaAfiliadosCedula[0].tipo_sangre;
                            this.nacionalidad =this.listaAfiliadosCedula[0].nacionalidad;

                            this.TempPais = this.listaAfiliadosCedula[0].pais;
                            this.TempProvincia = this.listaAfiliadosCedula[0].provincia;
                            this.TempCiudad = this.listaAfiliadosCedula[0].ciudad;
                            this.parroquia  = this.listaAfiliadosCedula[0].parroquia;

                                var urlProvincia = 'provincia/'+this.TempPais+'';
                                axios.get(urlProvincia).then(response => {
                                    this.cmbProvincia = response.data
                                })


                                var urlCiudad = 'ciudad/'+this.TempProvincia+'';
                                axios.get(urlCiudad).then(response => {
                                    this.cmbCiudad = response.data
                                })


                                var urlParroquia = 'parroquia/'+this.TempCiudad+'';
                                axios.get(urlParroquia).then(response => {
                                    this.cmbParroquia = response.data
                                })


                        this.provincia = this.listaAfiliadosCedula[0].provincia;
                        this.ciudad = this.listaAfiliadosCedula[0].ciudad;
                        this.parroquia  = this.listaAfiliadosCedula[0].parroquia;



                        //this.cargarCombos(this.TempPais,this.TempProvincia,this.TempCiudad);

                            this.direccion = this.listaAfiliadosCedula[0].direccion;
                            this.telefono = this.listaAfiliadosCedula[0].telefono;
                            this.celular = this.listaAfiliadosCedula[0].celular;
                            this.otro = this.listaAfiliadosCedula[0].otro;
                            this.observacionTitular= this.listaAfiliadosCedula[0].observacion;
                            this.lugar_trabajo  = this.listaAfiliadosCedula[0].lugar_trabajo;
                            this.tipo_seguro  = this.listaAfiliadosCedula[0].tipo_seguro;
                            this.TempTipoSeguro = this.listaAfiliadosCedula[0].tipo_seguro; // campos de consulta para cambio
                            this.des_campo1  = this.listaAfiliadosCedula[0].des_campo1;
                            this.des_campo2  = this.listaAfiliadosCedula[0].des_campo2;
                            this.des_campo3  = this.listaAfiliadosCedula[0].des_campo3;
                            this.usuario_ingreso = this.listaAfiliadosCedula[0].usuario_ingreso;
                            this.fecha_ingreso  = this.listaAfiliadosCedula[0].fecha_ingreso;
                            this.usuario_modificacion  = this.listaAfiliadosCedula[0].usuario_modificacion;
                            this.fecha_modificacion  = this.listaAfiliadosCedula[0].fecha_modificacion;
                            this.pcname  = this.listaAfiliadosCedula[0].pcname;
                            this.status  = this.listaAfiliadosCedula[0].status;
                            this.status_discapacidad  = this.listaAfiliadosCedula[0].status_discapacidad;
                            this.carnet_conadis  = this.listaAfiliadosCedula[0].carnet_conadis;
                            this.status_otro_seguro  = this.listaAfiliadosCedula[0].status_otro_seguro;
                            this.tipo_seguro_iess  = this.listaAfiliadosCedula[0].tipo_seguro_iess;
                            this.descripcion_otro_seguro  = this.listaAfiliadosCedula[0].descripcion_otro_seguro;
                            this.etnico  = this.listaAfiliadosCedula[0].etnico;

                            this.informacionAdicional(response.data);

                            //this.cargarCombos(this.TempPais,this.TempProvincia,this.TempCiudad);
                            this.BuscarPacienteClientes();
                            this.tipo_parentesco  = this.listaAfiliadosCedula[0].tipo_parentesco;
                            var urlBeneficiario = 'parentesco/' + this.tipo_parentesco + '';
                                axios.get(urlBeneficiario).then(response => {
                                    this.cmbBeneficiario = response.data
                                })
                            //this.Parentesco_beneficiario();
                            this.tipo_beneficiario = this.listaAfiliadosCedula[0].tipo_beneficiario;
                            this.cedula_titular  = this.listaAfiliadosCedula[0].cedula_titular;
                            this.TempTitular = this.listaAfiliadosCedula[0].cedula_titular;
                            this.TempTipoBeneficiario = this.listaAfiliadosCedula[0].tipo_beneficiario;


                                if(this.tipo_parentesco === "AA") {
                                    if(this.segundo_nombre === null){
                                        this.nombres = this.primer_nombre+"";
                                    }else{
                                        this.nombres = this.primer_nombre+" "+this.segundo_nombre+"";
                                    }

                                    this.apellidos= this.apellido_paterno+" "+this.apellido_materno;
                                    this.cedula_titular = this.in_cedula;
                                    this.observacion = this.Titularobservacion;
                                }else {
                                    alert("entre a buscar la info del titular");
                                    //this.ConsultarTitularCedula();
                                    this.InformacionTitular(response.data);

                                }

                            // activación de botones
                            this.imprimir = 'activo';
                            this.editar = 'activo';
                            this.nuevo = 'inactivo';
                            this.nuevo_titular = 'inactivo';
                            this.busqueda = true; // para que no se activen los combos
                            this.limpiar = 'activo';



                    }
                })

                
            },
        /*------------------//Carga de información-----------------------------------*/


        /*------------------ consultas de información-----------------------------------*/
            getUsers: function() {
                this.apellido = this.apellido.trim();
                var urlUsers = 'registropacientes/TitularAfiliadoPorApellido/'+this.apellido+'';
                axios.get(urlUsers).then(response => {
                    this.listaAfiliados  = response.data
                })
            },
            CargarCombosAll: function () {
                // carga la información de todos los combos
                axios.get('tipoidentificacion').then(response => {
                    this.cmbidentificacion  = response.data
            })

                var urlProvincia = 'provincia/1';
                axios.get(urlProvincia).then(response => {
                    this.cmbProvincia = response.data
            })


                var urlCiudad = 'ciudad/1';
                axios.get(urlCiudad).then(response => {
                    this.cmbCiudad = response.data
            })
                var urlParroquia = 'parroquia/1';
                axios.get(urlParroquia).then(response => {
                    this.cmbParroquia = response.data
            })

                var urlBeneficiario = 'parentesco/1';
                axios.get(urlBeneficiario).then(response => {
                    this.cmbBeneficiario = response.data
            })

                axios.get('otroseguro').then(response => {
                    this.cmbOtroSeguro  = response.data
            })
                axios.get('seguro').then(response => {
                    this.cmbSeguro  = response.data
            })
                axios.get('etnia').then(response => {
                    this.cmbEtnia  = response.data
            })
                axios.get('nacionalidad').then(response => {
                    this.cmbNacionalidad  = response.data
            })


                axios.get('estadoCivil').then(response => {
                    this.cmbEstadoCivil  = response.data
            })

                axios.get('sangre').then(response => {
                    this.cmbSangre = response.data
            })

                axios.get('genero').then(response => {
                    this. cmbGenero  = response.data
            })

                axios.get('parentescoPaciente').then(response => {
                    this.cmbParentesco  = response.data
            })

                axios.get('documentosiess').then(response => {
                    this.cmbDocumentosIess  = response.data
            })


            },
            consultarCedula : function() {

                this.in_cedula = this.in_cedula.trim();
                if(this.tipo_identificacion === 0) {
                    toastr.error("Seleccione el tipo de identificación.")
                } else if (this.tipo_identificacion === 1 ) {

                    if(this.in_cedula.length === 10) {

                        if(isNaN(this.in_cedula)) {
                            toastr.error("El numero de cedula no es valido. Ingrese solo numeros");

                        } else {
                            /*
                                var cad = this.in_cedula.trim();
                                var total = 0;
                                var longitud = cad.length;
                                var longcheck = longitud - 1;
                                if (cad !== "" && longitud === 10) {
                                    for (i = 0; i < longcheck; i++) {
                                        if (i % 2 === 0) {
                                            var aux = cad.charAt(i) * 2;
                                            if (aux > 9) aux -= 9;
                                            total += aux;
                                        } else {
                                            total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
                                        }
                                    }

                                    total = total % 10 ? 10 - total % 10 : 0;

                                    // verifica si el ultimo numero de tu cedula es igual al resultado obtenido
                                        //0952013225 el ultimo numero es 5  por lo tanto eel total debe ser = 5
                                    if (cad.charAt(longitud - 1) == total) {
                                        this.pasarDatosCedulaConsulta();
                                    } else {
                                        toastr.error("Cedula Invalida");
                                    }
                                } */
                            this.pasarDatosCedulaConsulta();
                         }

                    } else {
                        toastr.error("El numero de cedula debe contar con 10 digitos");
                    }
                } else if(this.tipo_identificacion === 2 ){

                    if(this.in_cedula.length === 13) {
                        if(isNaN(this.in_cedula)){
                            toastr.error("El numero de ruc no es valido. Ingrese solo numeros");
                        } else{
                            this.pasarDatosCedulaConsulta();
                        }
                    }else {
                        toastr.error("El numero de ruc debe contar con 13 digitos");
                    }
                } else if(this.tipo_identificacion === 4 ){

                    if(this.in_cedula.length === 7) {
                        if(isNaN(this.in_cedula)){
                            toastr.error("La partida de nacimiento no es valida. Ingrese solo numeros");
                        } else{
                            this.pasarDatosCedulaConsulta();
                        }
                    }else {
                        toastr.error("El numero de partida de nacimiento solo tiene 7 digitos");
                    }
                }  else if(this.tipo_identificacion === 3 ) {

                   // expresion regular
                    var rex = "/^[0-9a-zA-Z]+$/";

                }
                this.tipo_beneficiario = this.TempTipoBeneficiario;

            },
            PasarDatos: function (cedula,titular) {

                if(titular === 'si') {
                    this.cedula_titular = cedula;
                    this.ConsultarTitularCedula();
                    $('#buscar_titular_nombre_modal').modal('toggle');


                } else  {
                    this.in_cedula = cedula;
                    if(this.in_cedula.length == 10)
                    {
                        this.tipo_identificacion = 1
                    } else {
                        this.tipo_identificacion=0
                    }
                    $('#buscar_paciente_modal').modal('toggle');
                    this.consultarCedula();
                }
            },
            pasarDatosTitular: function (cedula,parentesco,beneficiario) {

                this.cedula_titular = cedula;
                if(parentesco == "AA" ) {

                    alert(this.in_cedula.length);
                    this.tipo_parentesco = parentesco;
                    this.Parentesco_beneficiario();
                    this.tipo_beneficiario = beneficiario;
                    this.ConsultarTitularCedula();
                    $('#buscar_titular_nombre_modal').modal('toggle');

                } else {

                }
            },
            informacionAdicional : function (data) {
                var urlInfo = 'registropacientes/informacionadicional/'+this.id+'';
                axios.get(urlInfo).then(response => {
                    this.listaInfoAdicional = response.data;
                    this.nombres_padre = this.listaInfoAdicional[0].nombres_padre;
                    this.nombres_madre = this.listaInfoAdicional[0].nombres_madre;
                    this.conyugue = this.listaInfoAdicional[0].conyugue;
                    // seguros
                    this.coberturas =  this.listaInfoAdicional[0].cobertura_compartida === "S" ? true : false;
                    this.issfa =  this.listaInfoAdicional[0].issfa === "S" ? true : false;
                    this.isspol =  this.listaInfoAdicional[0].isspol=== "S" ? true : false;
                    this.iess =  this.listaInfoAdicional[0].iess === "S" ? true : false;
                    if(this.listaInfoAdicional[0].prepagada !== null){
                        this.prepagada = true;
                        this.otro_seguro = this.listaInfoAdicional[0].prepagada;
                    }

                })
            },
            verificarTitular : function() {
                // se encarga de verificar si un titular se encuetra registrado antes de realizar el ingreso del paciente
                this.verificar_titular_cedula = this.verificar_titular_cedula.trim();
                if(isNaN(this.verificar_titular_cedula)) {
                    toastr.error("El numero de identificación no es valido. Ingrese solo numeros");
                } else {
                    var urlUsers = 'registropacientes/TitularAfiliadoPorCedulaAll/'+this.verificar_titular_cedula+'';
                    axios.get(urlUsers).then(response => {
                        this.verificar_titular= response.data;
                            if(this.verificar_titular.length !== 0){
                                toastr.success("El titular se encuentra registrado");
                            } else {
                                toastr.error("El titular no existe en la base de datos");
                                toastr.warning("Procesa a realizar el ingreso del titular");
                            }
                    })
                }

                },
        /*------------------//consultas de información-----------------------------------*/



        /*------------------ Metodos onchanged-----------------------------------*/
            CambioProvincia:function() {

                var urlCiudad = 'ciudad/'+this.provincia+"";
                axios.get(urlCiudad).then(response => {
                    this.cmbCiudad = response.data
                })

               // this.TempCiudad= this.ciudad;

            },
            CambioCiudad:function() {


                if(this.busqueda === false) {
                    var urlParroquia = 'parroquia/'+this.TempCiudad+"";
                    axios.get(urlParroquia).then(response => {
                        this.cmbParroquia = response.data
                    })
                } else {
                    //this.TempCiudad = ciudad;
                    //var urlParroquia = 'parroquia/'+this.TempCiudad+"";
                    //axios.get(urlParroquia).then(response => {
                      //  this.cmbParroquia = response.data
                    //})
                }

            },
            Parentesco_beneficiario:function() {
                // al seleccionar el parentesco se carga el tipo de beneficiario

                this.in_cedula = this.in_cedula.trim();
                this.primer_nombre = this.primer_nombre.trim();
                //this.segundo_nombre = this.segundo_nombre.trim();
                this.apellido_paterno = this.apellido_paterno.trim();
                this.apellido_materno = this.apellido_materno.trim();

                if(this.tipo_parentesco === "AA") {
                    if (this.primer_nombre === "" || this.apellido_paterno === "" || this.apellido_materno === "" || this.in_cedula === "") {
                        toastr.error("ingrese la informacion basica del paciente antes de continuar.");
                    } else {

                        var urlBeneficiario = 'parentesco/' + this.tipo_parentesco + '';
                        axios.get(urlBeneficiario).then(response => {
                            this.cmbBeneficiario = response.data

                    })

                        this.cedula_titular = this.in_cedula;
                        this.nombres = this.primer_nombre + " " + this.segundo_nombre;
                        this.apellidos = this.apellido_paterno + " " + this.apellido_materno;
                        this.visibilidad_T2 = true;
                        this.visibilidad_T3 = true;
                    }
                } else {
                    this.cedula_titular = "";
                    this.nombres = "";
                    this.apellidos = "";
                    this.visibilidad_T2 = true;
                    this.visibilidad_T3 = false;

                    var urlBeneficiario = 'parentesco/' + this.tipo_parentesco + '';
                    axios.get(urlBeneficiario).then(response => {
                        this.cmbBeneficiario = response.data;

                    })
                }
            },
            CambioTipoSeguro : function() {
                // depende del tipo de seguri se muestran el resto de coberturas
                if(this.tipo_seguro == 1 || this.tipo_seguro == 4 || this.tipo_seguro==53){
                    this.visibilidad_coberturas = true;
                } else {
                    this.visibilidad_coberturas = false;
                }
            },
        /*------------------//Metodos onchanged-----------------------------------*/


        /*------------------ Calculos/Procesamientos -----------------------------------*/
            FormarFechaEdad : function (data) {
                var fecha_nacimiento = new Date(data[0].fecha_nacimiento);
                var dia = fecha_nacimiento.getDate();
                var mes = fecha_nacimiento.getMonth();
                mes = mes + 1;
                if (mes < 10) {
                    mes = String("0" + mes);
                }
                if(dia < 10) {
                    dia = String("0" + dia);
                }
                var year = fecha_nacimiento.getFullYear();
                var Fecha = String(year + "-" + mes + "-" + dia);
                var hoy = new Date();
                var cumpleanos = new Date(fecha_nacimiento);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }

                if(this.tipo_beneficiario == "AA") {

                    this.fecha_nacimiento = Fecha;
                    this.edad = edad;
                } else {
                    this.Titularfecha_nacimiento = Fecha;
                    this.Titularedad = edad;
                }
            },
            // este se utiliza para la fecha del metodo consultar cedula
            FormarFechaEdad2 : function (data) {
                var fecha_nacimiento = new Date(data[0].fecha_nacimiento);
                var dia = fecha_nacimiento.getDate();
                var mes = fecha_nacimiento.getMonth();
                mes = mes + 1;
                if (mes < 10) {
                    mes = String("0" + mes);
                }
                if(dia < 10) {
                    dia = String("0" + dia);
                }
                var year = fecha_nacimiento.getFullYear();
                var Fecha = String(year + "-" + mes + "-" + dia);
                var hoy = new Date();
                var cumpleanos = new Date(fecha_nacimiento);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }

                this.fecha_nacimiento = Fecha;
                this.edad = edad;

            },
            calcular_edad : function () {
                //this.edad = this.fecha_nacimiento - getdate()
                //var today = new Date();
                var hoy = new Date();
                var cumpleanos = new Date(this.fecha_nacimiento);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }

                this.edad = edad;
            },
            setCampos : function() {
            this.EspaciosBlanco(); // quita los espacios en blanco de toda la info ingresada
        },
        /*------------------//Calculos -----------------------------------*/



        /*------------------------ agregar/actualizar un nuevo paciente---------------------------  */
        //
            AgregarPaciente : function () {
            this.EspaciosBlanco(); // quita los espacios en blanco de toda la info ingresada
            this.ValidarCombosP();// valida que la información dentro de los combos sea correcta
            this.ValidarCampos(); // valida que la información de los campos de texto sea correcta
            this.validar_cedula(); // valida el numero de cedula/pasaporte/ruc/no cedulado/
            this.validarNombres(); // valida con expresiones regulares la información ingresada por teclado



            if( this.errores.length === 0) {
                //envia el formulario por medio de html-php
               document.getElementById('form1').submit()
                //toastr.success("Guardando...");
            } else {
                var num = this.errores.length;
                for(i=0; i<num;i++) {
                    toastr.error(this.errores[i]);
                }
            }
            this.errores = [];
        },
        /*------------------------ // agregar/actualizar un nuevo paciente---------------------------  */



        /*------------------------  Validaciones  ---------------------------  */

            EspaciosBlanco : function () {
            // quitar espacios en blanco y poner letras en mayuscula
            this.in_cedula = this.in_cedula.trim();
            this.primer_nombre = this.primer_nombre.trim();
            if(this.segundo_nombre !== null){
                this.segundo_nombre= this.segundo_nombre.trim();
                this.segundo_nombre= this.segundo_nombre.toUpperCase();
            }

            this.apellido_paterno = this.apellido_paterno.trim();
            this.apellido_materno = this.apellido_materno.trim();
            this.lugar_nacimiento = this.lugar_nacimiento.trim();
            this.ocupacion = this.ocupacion.trim();
            this.direccion = this.direccion.trim();
            this.telefono = this.telefono.trim();
            this.celular = this.celular.trim();
            if(this.otro !== null ){
                this.otro = this.otro.trim();
            }
            if(this.otro !== '' ){
                    this.otro = this.otro.trim();
            }
            this.observacion = this.observacion.trim();
            this.observacionTitular  = this.observacionTitular.trim();
            this.lugar_trabajo = this.lugar_trabajo.trim();
            this.cedula_titular = this.cedula_titular.trim();
            if(this.carnet_conadis === null || this.carnet_conadis === undefined ) {this.carnet_conadis = null} else{ this.carnet_conadis = this.carnet_conadis.trim()}
            this.nombres_padre = this.nombres_padre.trim();
            this.nombres_madre = this.nombres_madre.trim();
            this.conyuge = this.conyuge.trim();
            if(this.cedula_seg_progenitor  === null || this.cedula_seg_progenitor  === undefined ) {this.cedula_seg_progenitor  = null} else{ this.cedula_seg_progenitor  = this.cedula_seg_progenitor .trim()}
            // convertir a MAYUSCULA
            this.primer_nombre = this.primer_nombre.toUpperCase();

            this.apellido_paterno = this.apellido_paterno.toUpperCase();
            this.apellido_materno = this.apellido_materno.toUpperCase();
            this.nombres_padre = this.nombres_padre.toUpperCase();
            this.nombres_madre = this.nombres_madre.toUpperCase();
            this.conyuge = this.conyuge.toUpperCase();
            this.observacion = this.observacion.toUpperCase();
            this.observacionTitular  = this.observacionTitular.toUpperCase();
            this.lugar_trabajo = this.lugar_trabajo.toUpperCase();
            this.lugar_nacimiento = this.lugar_nacimiento.toUpperCase();
            this.ocupacion = this.ocupacion.toUpperCase();
            this.direccion = this.direccion.toUpperCase();

        },

            validar_cedula : function () {

                this.in_cedula = this.in_cedula.trim();
                if(this.tipo_identificacion === 0) {
                    this.errores.push("Seleccione el tipo de identificación.");
                } else if (this.tipo_identificacion === 1 ) {

                    if(this.in_cedula.length === 10) {

                        if(isNaN(this.in_cedula)) {
                            this.errores.push("El numero de cedula no es valido. Ingrese solo numeros");
                        }

                    } else {
                        this.errores.push("El numero de cedula debe contar con 10 digitos");
                    }
                } else if(this.tipo_identificacion === 2 ){

                    if(this.in_cedula.length === 13) {
                        if(isNaN(this.in_cedula)){
                            this.errores.push("El numero de ruc no es valido. Ingrese solo numeros");
                        }
                    }else {
                        this.errores.push("El numero de ruc debe contar con 13 digitos");
                    }
                } else if(this.tipo_identificacion === 4 ){

                    if(this.in_cedula.length === 7) {
                        if(isNaN(this.in_cedula)){
                            this.errores.push("La partida de nacimiento no es valida. Ingrese solo numeros");
                        }
                    }else {
                        this.errores.push("El numero de partida de nacimiento solo tiene 7 digitos");
                    }
                }  else if(this.tipo_identificacion === 3 ) {

                    // expresion regular
                    var rex = "/^[0-9a-zA-Z]+$/";
                }
            },

            digitoVerificador :function() {

        }, // borrar

            ValidarCombosP :function () {
            // validar que todos los combos hayan sido seleccionados.
            if(this.tipo_identificacion === 0) {
                this.errores.push("seleccione el tipo de identificación");
            }

            if(this.genero !== "M" && this.genero !== "F" && this.genero !== "I") {
                this.errores.push("Seleccione el genero");
            }

            if(this.estado_civil === 0) {
                this.errores.push("Seleccione el estado civil");
            }

            if(this.nacionalidad === 0) {
                this.errores.push("Seleccione la nacionalidad");
            }

            if(this.etnia === 0) {
                this.errores.push("Seleccione la etnia");
            }

            if(this.provincia === 0) {
                this.errores.push("Seleccione la provincia");
            }

            if(this.ciudad === 0 && this.TempCiudad === 0) {
                this.errores.push("Seleccione la ciudad");
            }

            if(this.parroquia === 0) {
                this.errores.push("Seleccione la parroquia");
            }

            if(this.tipo_seguro ===0) {
                this.errores.push("Seleccione el tipo de seguro");
            }

            if(this.tipo_sangre === 0) {
                this.errores.push("Seleccione el tipo de sangre");
            }

            if(this.status_discapacidad2 === "S") {
                if(this.carnet_conadis2 === "") {
                    this.errores.push("agregue el carnet de conadis ");
                }
            }

            if(this.status_discapacidad === "S") {
                if(this.carnet_conadis === "") {
                    this.errores.push("agregue el carnet de conadis ");
                }
            }

            if(this.tipo_seguro === 1) {
                if(this.tipo_seguro_iess === 0) {
                    this.errores.push("Agregue el tipo de seguro iess");
                }
            }

            if(this.tipo_seguro === 0) {
                    this.errores.push("Seleccione el tipo de seguro");
            }

            if(this.coberturas === true) {
                if(this.tipo_seguro === 1) {
                    if (this.issfa === false && this.isspol === false && this.prepagada === false) {
                        this.errores.push("Seleccione algun tipo de cobertura");
                    }
                } else if(this.tipo_seguro === 4) {
                    if (this.issfa === false && this.isspol === false && this.prepagada === false && this.iess === false) {
                        this.errores.push("Seleccione algun tipo de cobertura");
                    }
                }
            }

            if(this.prepagada === true && this.otro_seguro === 0) {
                this.errores.push("seleccione el otro tipo de seguro")
            }
        },

            ValidarCampos :function () {
            // que los campos no se encuentren vacios.

            if(this.apellido_paterno === "" && this.apellido_materno === "") {
                this.errores.push("Ingrese ambos apellidos");
            }
            if(this.primer_nombre === "") {
                this.errores.push("Ingrese el primer nombre");
            }
            if (this.fecha_nacimiento === "") {
                this.errores.push("seleccione su fecha de nacimiento");
            }
            if(this.edad < 0 || this.edad >120 ){
                this.errores.push("ingrese una fecha de nacimiento validad");
            }
            if(this.edad >=0 && this.edad < 18  ) {
                if(this.cedula_seg_progenitor === ""){
                    this.errores.push("ingrese la cedula del segundo progenitor");
                }else {
                    // validar si solo ingresa letras

                }
            }

                if(this.observacionTitular=== "") {
                    this.errores.push("ingrese la observación del titular");
                }

            if(this.lugar_nacimiento === "") {
                this.errores.push("ingrese su lugar de nacimiento");
            }

            if(this.ocupacion === "") {
                this.errores.push("ingrese su ocupacion");
            }
            if(this.dirección === "") {
                this.errores.push("ingrese su dirección domiciliaria");
            }

            if(this.celular === "" && this.telefono === "" &&  this.otro === "") {
                this.errores.push("ingrese el celular,telefono o otro numero de contacto");
            }

                if(this.celular !== "") {
                    if(isNaN(this.celular)) {
                        this.errores.push("Ingrese solo numeros en el campo celular [0990916465]");
                    }
                    if(this.celular.length !==10){
                        this.errores.push("El numero de celular solo debe contener 10 digitos [0990916465]");
                    }
                }
                if(this.telefono !== "") {
                    if(isNaN(this.telefono)) {
                        this.errores.push("Ingrese solo numeros en el campo telefono [043873498]");
                    }
                    if(this.telefono.length !==9){
                        this.errores.push("El numero de telefono debe de contener 7 digitos [043873498]");
                    }
                }
                if(this.otro !== "") {
                    if(isNaN(this.otro)) {
                        this.errores.push("Ingrese solo numeros en el campo otro [0990916465]");
                    }
                }

            if(this.nombres_padre === "" || this.nombres_madre === "") {
                this.errores.push("Ingrese los nombres del padre del paciente");
            }
        },

            validarNombres : function() {
                var patt =  /^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/; // para los nombres del paciente
                var patt2 =  /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/; // para los nombres de los padres
                var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s0-9]+$/; //para las direcciones

                if(patt.test(this.primer_nombre) == false)
                {
                    this.errores.push("El primer nombre no puede contener numeros ni caracteres especiales");
                }

                if(this.segundo_nombre !== null) {

                    if(patt.test(this.segundo_nombre) == false)
                    {
                        this.errores.push("El segundo nombre no puede contener numeros/espacios/caracteres especiales");
                    }
                }
                if(patt.test(this.apellido_paterno) == false)
                {
                    this.errores.push("El apellido paterno  no puede contener numeros/espacios/caracteres especiales");
                }

                if(patt.test(this.apellido_materno) == false)
                {
                    this.errores.push("El apellido materno  no puede contener numeros/espacios/caracteres especiales");
                }

                if(patt2.test(this.nombres_padre) == false)
                {
                    this.errores.push("Los nombres del padre no puede contener numeros/caracteres especiales");
                }

                if(patt2.test(this.nombres_madre) == false)
                {
                    this.errores.push("Los nombres de la madre no puede contener numeros/caracteres especiales");
                }

                if(this.conyugue !== null) {

                    if(patt2.test(this.conyugue) == false)
                    {
                        this.errores.push("El nombre de el/la conyuge no puede contener numeros/caracteres especiales");
                    }
                }

                if(patt3.test(this.direccion) == false)
                {
                    this.errores.push("La dirección no puede contener caracteres especiales");
                }

                if(patt3.test(this.observacionTitular) == false)
                {
                    this.errores.push("La observación del titular no puede contener caracteres especiales");
                }

                if(patt3.test(this.lugar_trabajo) == false)
                {
                    this.errores.push("El lugar de trabajo no puede contener caracteres especiales");
                }

            },

        /*------------------------ // Validaciones  ---------------------------  */



        /*------------------------ Cuentas por cobrar  ---------------------------  */
            BuscarPacienteClientes : function (nombre,cedula) {
            //se va a consultar por nombre y por cedula. dependiendo si trae o no algo nombre,cedula
            // btn grabar habilitado y oper = I
            var urlTitular = 'registropacientes/TitularAfiliadoPorCedula/'+nombre+'';
            axios.get(urlTitular).then(response => {
                this.ConsultarPacienteCliente = response.data;

        })
        },
        /*------------------------ //Cuentas por cobrar  ---------------------------  */

    }


});



