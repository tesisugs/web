var app = new Vue ({
   el:'#asistenciaMedica',
   data : {

       errores : [],
       id2 : '',

       atencion: 0,
       cedula : '' ,
       nombre : '',
       paciente : '',
       responsable: '',
       afinidad : '',
       direccion : '',
       telefono:'',
       seguro : '',
       firmante:'',
       acompanante : '',
       doctor: '',
       observacion : '',
       parentesco : '',
       ejemplo : '',
       cedula_acompanate : '',
       nombre_acompanante : '',
       derivacion : false,

       cmbmedico : [],
       cmbparentesco: [],
       cmbSeguro : [],
       cmbpaciente : [],
       pacientesAdmision : [],

       //
       id:'',
       fecha_nacimiento : '',
       celular : '',
       otro : '',
       observacion2: '',
       primer_nombre: '',
       segundo_nombre : '',
       apelldio_paterno : '',
       apellido_materno : '',
       hospital:0,

   },

    created : function() {
        axios.get('seguro').then(response => {
            this.cmbSeguro  = response.data
        })

        axios.get('consultarmedico').then(response => {
            this.cmbmedico  = response.data
        })

        axios.get('consultarparentesco2').then(response => {
            this.cmbparentesco  = response.data
        })

        var url = 'consultarpacientesadmision';
        axios.get(url).then(response => {
            this.pacientesAdmision  = response.data;
        })

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "600",
            "hideDuration": "3000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $.get("cmbderivacionhospital", function (datos) {
            $.each(datos, function (key, value) {
                $("#hospital").append("<option value=" + value.id + ">" + value.descripcion + "</option>");
            });

        });

    },

    methods : {

        consultarPacientes : function() {
          var url = 'consultarpacientesadmision';
            axios.get(url).then(response => {
                this.pacientesAdmision  = response.data;
            })
        },

        ConsultarBeneficiario : function () {
            var url = 'consultarbeneficiario/'+this.cedula+'';
            axios.get(url).then(response => {
                this.cmbpaciente  = response.data;
            })
        },
        ConsultarCedula : function() {
                        var urlUsers = 'registropacientes/TitularAfiliadoPorCedula/'+this.cedula+'';
                        axios.get(urlUsers).then(response => {
                            this.listaAfiliadosCedula  = response.data;
                        this.id = this.listaAfiliadosCedula[0].id;

                        this.primer_nombre = this.listaAfiliadosCedula[0].primer_nombre;
                        this.segundo_nombre = this.listaAfiliadosCedula[0].segundo_nombre;
                        this.apellido_paterno = this.listaAfiliadosCedula[0].apellido_paterno;
                        this.apellido_materno = this.listaAfiliadosCedula[0].apellido_materno;

                        this.nombre = this.primer_nombre + ' '+this.segundo_nombre+' '+this.apellido_paterno+' '+this.apellido_materno+'';
                        this.responsable = this.primer_nombre + ' '+this.segundo_nombre+' '+this.apellido_paterno+' '+this.apellido_materno+'';
                        this.fecha_nacimiento = new Date(this.listaAfiliadosCedula[0].fecha_nacimiento);
                        this.direccion = this.listaAfiliadosCedula[0].direccion;
                        this.telefono = this.listaAfiliadosCedula[0].telefono;
                        this.seguro  = this.listaAfiliadosCedula[0].tipo_seguro;

                        this.celular = this.listaAfiliadosCedula[0].celular;
                        this.otro = this.listaAfiliadosCedula[0].otro;
                        this.observacion2 = this.listaAfiliadosCedula[0].observacion;
                        this.ConsultarBeneficiario(response.data);
                    })
          },

        validarInsert : function () {
          this.espaciosBlancos();
          this.validarCampos();
            // mensajes de alerta
            if( this.errores.length === 0) {
                //document.getElementById('registroTitular').submit()
                this.guardar();
                //var formulario = document.getElementById("form1");
                //formulario.submit();
            } else {
                var num = this.errores.length;
                for(i=0; i<num;i++) {
                    toastr.error(this.errores[i]);
                }
            }
            this.errores = [];

        },

          guardar : function() {
                var temp1 = 0;
                var temp2 = 0;

              var parametros = {
                  "_token": "{{ csrf_token() }}",
                  //--
                  "afiliado" : this.id,
                  "paciente" : this.paciente,
                  "responsable": this.responsable,
                  "observacion" : this.observacion,
                  "doctor": this.doctor,
                  "seguro" : this.seguro,
                  //"fuente_informacion" : "fuente_informacion",
                  "derivacion" : this.derivacion,
                  "hospital" : this.hospital,
                  "dcedul_a" : this.cedula_acompanate,
                  "dnombre_a" : this.cedula_acompanate,
                  "parentesco" : this.parentesco,

              };
              $.ajax({
                  data : parametros,
                  url : "asistenciamedica/storepacientes",
                  type : "post",
                  async : false,
                  success : function(response){
                      toastr.success('Registro ingresado con exito.', 'Exito', {timeOut: 5000});
                  },
                  error : function (response,jqXHR) {
                      toastr.error('Error al momento de ingresar el registro.', 'Error', {timeOut: 5000});

                  }
              });

          },

          validarCampos : function () {

              if (this.firmante !== 'si' && this.firmante !== 'no' && this.firmante !== 'in') {
                  this.errores.push("Seleccione si es 'Firmante','No firmante' o acompañante");
              } else {
                  if (this.firmante === 'no') {
                      if (this.parentesco === '' || this.parentesco === null || this.parentesco === undefined)
                          this.errores.push("Seleccione el tipo de parentesco");

                  } else if (this.firmante === 'in') {
                      if (this.parentesco === '' || this.parentesco === null || this.parentesco === undefined) {
                          this.errores.push("Seleccione el tipo de parentesco");
                      } else {
                          if (this.cedula_acompanate === '' || this.nombre_acompanante === '') {
                              this.errores.push("Introduzca  los datos del acompañante");
                          }
                      }

                  }

              }
              if(this.cedula === '')
                  this.errores.push("Introduzca un numero de cedula");
              if(this.nombre === '')
                  this.errores.push("Introduzca el nombre del pacient");
              if(this.paciente === '')
                  this.errores.push("Seleccione el paciente");
              if(this.responsable === ''){
                  this.errores.push("Introduzca el nombre del responsable");
              }
              if(this.direccion === ''){
                  this.errores.push("Introduzca la dirección");
              }
              if(this.telefono === ''){
                  this.errores.push("Introduzca un numero de telefono");
              }
              if(this.seguro === ''){
                  this.errores.push("Seleccione el tipo de seguro");
              }
              if(this.doctor === 0 || this.doctor === ''){
                  this.errores.push("Seleccione al medico");
              }


          },

          espaciosBlancos : function () {
              this.cedula = this.cedula.trim();
              this.responsable = this.responsable.trim();
              this.afinidad  = this.afinidad.trim();
              this.direccion = this.direccion.trim();
              this.telefono = this.telefono.trim();

          }
        
    }

});