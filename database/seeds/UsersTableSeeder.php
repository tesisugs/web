<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Creamos usuario
        /*
        $user = new User();
        $user->nombres="Bryan Estiven";
        $user->apellidos="Silva Mercado";
        $user->name = "bryan";
        $user->estado = 1;
        $user->password = bcrypt('admin');
        $user->save();
       */
        $user = new User();
        $user->nombres="Prueba Prueba";
        $user->apellidos="Prueba Prueba";
        $user->name = "new";
        $user->estado = 1;
        $user->password = bcrypt('admision');
        $user->save();
    }
}
