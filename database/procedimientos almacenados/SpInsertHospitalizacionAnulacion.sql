Delimiter //
create PROCEDURE SpInsertHospitalizacionAnulacion (
in _id	int	,
in _id_hospitalizacion	int	,
in _paciente	int	,
in _observacion	nvarchar(20)	,
in _usuario_ingreso	int	,
in _fecha_modificacion datetime	
)
begin
 INSERT INTO tbHospitalizacionAnulacion ()
				VALUES (_id_hospitalizacion				,
						_paciente		,
						_observacion	,
						curdate()	,
						'.'	,
						'.',	
						'0'	,
						_usuario_ingreso	,
						_usuario_ingreso		,
						_fecha_modificacion	);
						select id from tbHospitalizacionAnulacion  order by id desc limit 1;

end 
// delimiter //