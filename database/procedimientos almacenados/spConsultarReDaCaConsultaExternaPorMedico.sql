
delimiter //
create procedure spConsultarReDaCaConsultaExternaPorMedico
(in doctor int)

-- exec SpConsultarReDaCaConsultaExternaPorMedico 10
begin
declare _fechaini date; 
declare _fechafin date;
set _fechaini := CONVERT(curdate(),date);
-- Considerar que se esta generando mas de un diagnostico por cada consulta medica, tbConsultaExternaDiagnosticos.
/*
SELECT a.paciente 'HISTORIA CLINICA',
       A.fecha_registro_paciente 'FECHA CONSULTA',
       B.cedula 'CEDULA',
       B.genero 'GENERO',
       B.apellido_paterno 'APELLIDO PATERNO',
       B.apellido_materno 'APELLIDO MATERNO',
	   B.primer_nombre 'PRIMER NOMBRE',
	   B.segundo_nombre 'SEGUNDO NOMBRE',
	   A.numero_asistencia 'NUMERO ASISTENCIA',
       B.fecha_nacimiento 'FECHA NACIMIENTO',
      CONVERT(VARCHAR(2),  floor(cast(datediff(day, B.fecha_nacimiento, A.fecha_registro_paciente) as float)/365))+'AÑOS'  + '-'
       + CONVERT (VARCHAR(2),floor((cast(datediff(day, B.fecha_nacimiento, A.fecha_registro_paciente) as float)/365-(floor(cast(datediff(day, B.fecha_nacimiento, A.fecha_registro_paciente) as float)/365)))*12)) + 'MESES'
       
       as 'EDAD_PACIENTE',
      -- A.medico 'MEDICO',
       X.apellidos 'APELLIDOS MEDICO',
      X.nombres 'NOMBRES MEDICO' ,
  --     A.tipo_ingreso 'TIPO INGRESO',
       G.descripcion 'SEGURO',
       C.principal ' CIE10 PRINCIPAL',
       D.descripcion 'DESCRIPCION',
       C.asociado ' CIE10 ASOCIADO',
       E.descripcion 'DESCRIPCION',
       C.asociado2 'CIE10 ASOCIADO 2',
       F.descripcion 'DESCRIPCION' ,
       (SELECT tb1.descripcion FROM tbDependencias tb1 where tb1.codigo = a.dependencias) 'DEPENDENCIA'
            --,A.principal,A.asociado_1,A.asociado_2
FROM tbIngresoPacienteConsultaExterna A
      INNER JOIN tbPaciente B ON A.paciente = B.ID
      INNER JOIN tbMedico X ON X.id = A.medico
      LEFT JOIN tbConsultaExternaDiagnosticos C ON C.consulta_externa = A.id
      LEFT JOIN tbDiagnostico D ON D.codigo = C.principal
      LEFT JOIN tbDiagnostico E ON E.codigo = C.asociado
      LEFT JOIN tbDiagnostico F ON F.codigo = C.asociado2
      INNER JOIN tbTipoIngreso G ON G.codigo = A.tipo_ingreso
WHERE A.fecha_registro_paciente BETWEEN @fechaini AND @fechafin
AND A.status = 1
order by 2,5,6
*/
SELECT a.paciente 'HISTORIA CLINICA',
       A.fecha_registro_paciente 'FECHA CONSULTA',
       B.cedula 'CEDULA',
       B.genero 'GENERO',
       B.apellido_paterno 'APELLIDO PATERNO',
       B.apellido_materno 'APELLIDO MATERNO',
	   B.primer_nombre 'PRIMER NOMBRE',
	   B.segundo_nombre 'SEGUNDO NOMBRE',
	   A.numero_asistencia 'NUMERO ASISTENCIA',
       B.fecha_nacimiento 'FECHA NACIMIENTO',
       concat(CONVERT( floor(cast(datediff( B.fecha_nacimiento, A.fecha_registro_paciente) as signed )/365), CHAR(2)),
				'AÑOS',
				'-',
                CONVERT (floor((cast(datediff( B.fecha_nacimiento, A.fecha_registro_paciente) as signed)/365-(floor(cast(datediff( B.fecha_nacimiento, A.fecha_registro_paciente) as signed )/365)))*12) ,CHAR(2)),
                "AÑOS",
                "-") as 'EDAD_PACIENTE',
       X.apellidos 'APELLIDOS MEDICO',
      X.nombres 'NOMBRES MEDICO' ,
       G.descripcion 'SEGURO',
  
		ifnull((select  c.principal from tbConsultaExternaDiagnosticos c where c.consulta_externa = a.id limit 1), 'NO EVOLUCIONO') 'CIE10 PRINCIPAL',
         ifnull( ( select D.descripcion  from tbDiagnostico D WHERE d.codigo = (select  c.principal from tbConsultaExternaDiagnosticos c where c.consulta_externa = a.id limit 1) ),'NO EVOLUCIONO' ) 'DIAGNOSTICO PRINCIPAL',
         ifnull((select  c.asociado from tbConsultaExternaDiagnosticos c where c.consulta_externa = a.id limit 1), 'NO EVOLUCIONO') 'CIE10 ASOCIADO1',
		 ifnull( ( select D.descripcion  from tbDiagnostico D WHERE d.codigo = (select c.asociado from tbConsultaExternaDiagnosticos c where c.consulta_externa = a.id limit 1) ),'NO EVOLUCIONO' ) 'DIAGNOSTICO ASOCIADO1',
         ifnull((select c.asociado2 from tbConsultaExternaDiagnosticos c where c.consulta_externa = a.id limit 1), 'NO EVOLUCIONO') 'CIE10 ASOCIADO2',
		 ifnull( ( select D.descripcion  from tbDiagnostico D WHERE d.codigo = (select  c.asociado2 from tbConsultaExternaDiagnosticos c where c.consulta_externa = a.id limit 1) ),'NO EVOLUCIONO' ) 'DIAGNOSTICO ASOCIADO2',
       (SELECT tb1.descripcion FROM tbDependencias tb1 where tb1.codigo = a.dependencias) 'DEPENDENCIA'
          
FROM tbIngresoPacienteConsultaExterna A
      INNER JOIN tbPaciente B ON A.paciente = B.ID
      INNER JOIN tbMedico X ON X.id = A.medico
      INNER JOIN tbTipoIngreso G ON G.codigo = A.tipo_ingreso
WHERE A.fecha_registro_paciente = @fechaini
      and  a.medico = _doctor
	  AND A.status = 1
      order by 2,5,6;
end
// delimiter //