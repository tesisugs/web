
delimiter //
create procedure SpInsertVisor
-- select * from tbVisorConsulta
(
in _codigo                    int,
in _medico               varchar(30),
in _consultorio               nvarchar(30),
in _turno						int
)
begin
insert into tbVisorConsulta 
   values(_medico,
           _consultorio,
            _turno,
            curdate(),1);
select *from tbVisorConsulta order by id limit 1 ;
end
// delimiter //