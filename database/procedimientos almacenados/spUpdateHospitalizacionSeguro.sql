create PROCEDURE spUpdateHospitalizacionSeguro (
in _id int ,
in _Fecha_Hora_Ingreso datetime,
in _usuario_modificacion int ,
in _pcname NVARCHAR(50),
in _status int,
in _seguroTraspaso int
)
UPDATE	tbHospitalizacion
		SET		Varios_Fecha=now() 		
				,fecha_modificacion =curdate()
				,usuario_modificacion =_usuario_modificacion 
				,pcname=_pcname
				,Procedencias=_seguroTraspaso 
		WHERE	id = _id
       and _status in (1,3)
       -- and Procedencias <> 1
