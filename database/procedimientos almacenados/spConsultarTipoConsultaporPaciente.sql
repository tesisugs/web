CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarTipoConsultaporPaciente`(
in _paciente varchar(50),
in _tipo int
)
select COUNT(*) as 'numero_asistencia' from tbConsultaExternaTipoConsulta
where paciente = _paciente and tipo_consulta = _tipo