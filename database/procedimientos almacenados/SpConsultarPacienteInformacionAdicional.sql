create procedure SpConsultarPacienteInformacionAdicional(
in _paciente int
)
-- exec SpConsultarPacienteInformacionAdicional 45675
select	id,
		paciente,
		nombres_padre,
		nombres_madre,
		conyugue,
		des_campo1,
		des_campo2,
		des_campo3,
		usuario_ingreso,
		fecha_ingreso,
		usuario_modificacion,
		pcname,
		status,
		cedula_seg_progenitor,
		fecha_cedulacion,
		fecha_fallecimiento,
		instruccion,
		cobertura_compartida,
		prepagada,
		issfa,
		isspol,
		iess
from tbPacienteInformacionAdicional
where paciente = _paciente
  -- and tipo_identificacion in (1,2)


-- select * from tbPaciente where cedula='0917292765' and status=1

  