

create PROCEDURE SpDeleteAnulacionRegistroHospitalario (
in _id int,
in _usuario_modificacion int ,
in _observacion_eliminada nvarchar(50),
-- @fecha_modificacion datetime,
in _pcname nvarchar(50),
in _status int
)
UPDATE	tbHospitalizacion  
          set		usuario_modificacion =_usuario_modificacion,
          observacion_eliminada = _observacion_eliminada ,
					fecha_modificacion=curdate(),
     				pcname=_pcname,
					status=_status
		WHERE	id  = _id 

