CREATE DEFINER=`root`@`localhost` PROCEDURE `spInsertHospitalizacion`(
out _id int ,
out _SecuenciaHO int ,
out _No_Historial int ,
in _Fecha_Hora_Ingreso datetime,
in _cobertura_Maxima int ,
in _fecha_nacimiento datetime,
in _Normal bit,
in _Cirugia_Dia bit,
in _Fecha_de_Registro DATE,
in _Presupuesto nvarchar(50),
in _Pre_Factura int,
in _paciente int,
out _NumeroAtencion int ,
in _habitacion int,
in _Procedencias int,
in _Paquete_Atencion int,
in _Nombre_Responsable nvarchar(50),
in _Direccion_Responsable nvarchar(50),
in _Documento_Responsable nvarchar(50),
in _Telefono_Responsable nvarchar(50),
in _Plan_Atencion int,
in _CoaSeguro nvarchar(50),
in _Deducible nvarchar (50),
in _Observaciones nvarchar(50),
in _Siniestro nvarchar(50),
in _Referencia_1 nvarchar(50),
in _Referencia_2 nvarchar(50),
in _Medico_Fecha datetime ,
in _Servicios int,
in _medico int ,
in _principal nvarchar(50) ,
in _asociado_1 nvarchar(50),
in _asociado_2 nvarchar(50),
in _Medico_Observacion nvarchar(200),
in _garantia numeric,
in _Garantia_plan nvarchar (50),
in _Garantia_Coaseguros numeric(10,2),
in _Garantia_Deducible numeric(10,2),
in _Garantia_Moneda nvarchar(50),
in _Garantia_siniestro nvarchar(50),
in _Garantia_referencia_1 nvarchar(50),
in _Garantia_referencia_2 nvarchar(50),
in _Dieta int ,
in _Dieta_Observaciones nvarchar (50),
in _Dieta_Contenido nvarchar(50),
in _Varios_Fecha datetime,
in _Varios_Musica bit,
in _Varios_Musica_desde datetime,
in _Varios_Musica_hasta datetime,
in _Varios_Televisor bit ,
in _Varios_Televisor_desde datetime,
in _Varios_Televisor_hasta datetime,
in _Varios_Periodico bit,
in _Varios_Acompañante bit,
in _Varios_Acepta_Visitas_si bit,
in _Varios_Acepta_Visitas_no bit,
in _Varios_Acepta_Visitas_restringido bit,
in _Varios_Acepta_llamadas bit,
in _Varios_Dieta_aconpañante nvarchar(50),
in _fecha_egreso datetime ,
in _medico_egreso int,
in _principal_egreso nvarchar(50),
in _asociadoEgreso1 nvarchar(50),
in _asociadoEgreso2 nvarchar(50),
in _tipo_egreso int,
in _complicacion bit,
in _tipo_complicacion int,
in _des_campo1 NVARCHAR(50),
in _des_campo2 NVARCHAR(50),
in _des_campo3 int,
in _fecha_registro datetime ,
in _usuario_registro int ,
in _fecha_modificacion datetime ,
in _usuario_modificacion int ,
in _pcname NVARCHAR(50),
in _status int,
in _codigo_cama int,
in _traspaso int,
in _fecha_hora_traspaso datetime,
in _genero nvarchar(50),
in _observacion_traspaso nvarchar(100),
out _transaccion int ,
in _titular int,
in _tipo_egreso_alta int,
in _derivacion bit,
in _hospital int,
in _status_documento bit,
in _osbservacion_documento nvarchar(4000),
in _observacion_eliminada nvarchar(4000)
)
begin 
DECLARE NumeroAtencionAnual INT;
DECLARE SecuenciaMensual INT;
DECLARE SecuenciaEdad INT;
DECLARE SecuenciaMedicina INT; 
DECLARE SecuenciaCirugia INT; 
DECLARE edad INT;
/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 


START TRANSACTION; 


SET NumeroAtencionAnual = 0;
SET SecuenciaMensual = 0 ;
SET SecuenciaEdad = 0 ;
SET SecuenciaMedicina = 0 ;
SET SecuenciaCirugia = 0;
set SecuenciaMedicina = 0;
set SecuenciaEdad  = 0;
set SecuenciaCirugia  = 0;

-- /// LLEVAMOS LA SECUENCIA
update tbSecuencia
  set secuencia=secuencia+1
where codigo=2;

set _SecuenciaHO := (select secuencia 
  from tbsecuencia
  where codigo=2); 
  
  -- -/////// NUMERO TRANSACCION
  update tbSecuencia
  set secuencia=secuencia+1
	where codigo=4;
-- /// retornamors la transaccion a historia clinica refrencia
  
  set _No_Historial := (select secuencia 
  from tbsecuencia
  where codigo=4 ); 

-- /// retornamors la transaccion a historia clinica  
    set _transaccion := (select secuencia 
  from tbsecuencia
  where codigo=4);
  
-- --//// NUMERO DE ATENCION
		SET _NumeroAtencion := 0;
		-- =( SELECT COUNT (*)FROM tbHospitalizacion	where paciente = @paciente) + 1
  
    -- ******* fin de actualizaciones y set ******** 


    
   
    IF (YEAR(_Fecha_Hora_Ingreso)=2013) then
    
         
           		SET NumeroAtencionAnual  := ( SELECT COUNT(*) FROM tbHospitalizacion
		         where YEAR(_Fecha_Hora_Ingreso) = YEAR(_Fecha_Hora_Ingreso)) + 1 +2500;
    
  ELSE
    
    		SET NumeroAtencionAnual  := ( SELECT COUNT(*) FROM tbHospitalizacion
		         where YEAR(_Fecha_Hora_Ingreso) = YEAR(_Fecha_Hora_Ingreso)) + 1;    
    END if;
    
     IF (YEAR(_Fecha_Hora_Ingreso)=2013) then
    
         
           		SET SecuenciaMensual  =( SELECT COUNT(*) FROM tbHospitalizacion
		         where MONTH(_Fecha_Hora_Ingreso) = MONTH(_Fecha_Hora_Ingreso)) + 1 +2500;
		       
		       
  ELSE
    
    		SET SecuenciaMensual  =( SELECT COUNT(*) FROM tbHospitalizacion
		         where MONTH(_Fecha_Hora_Ingreso) = MONTH(_Fecha_Hora_Ingreso)) + 1;   
		       
     END if;
     
	IF _Cirugia_Dia = 'False' then
		BEGIN
		  Set edad = ( select DATEDIFF(_fecha_nacimiento , curdate() ) from tbPaciente where id = _paciente );
		   IF (edad <= 2) then 
			   BEGIN
				 SET SecuenciaEdad  =( SELECT max(SecuenciaEdad) FROM tbHospitalizacion) + 1;
				 SET SecuenciaMedicina      =( SELECT max(SecuenciaMedicina ) FROM tbHospitalizacion);
				 SET SecuenciaCirugia  =( SELECT max(SecuenciaCirugia ) FROM tbHospitalizacion);
				 
                  IF (isnull(SecuenciaCirugia) = 0) then
						BEGIN
							 SET SecuenciaCirugia  = '0';
						END;
				  end if;       
				  IF (isnull(SecuenciaEdad)  = 0) then
					BEGIN
						 SET SecuenciaEdad = '1';
					END;
				  end if;  
				  IF (isnull(SecuenciaMedicina)  = 0) then
					BEGIN
						SET SecuenciaMedicina = '0';
					END;
				   end if;
                 END;  
			END if;
          END;
      ELSE
		BEGIN 
			if _Cirugia_Dia = 'True' then 
				SET SecuenciaEdad  =( SELECT max(SecuenciaEdad) FROM tbHospitalizacion); 
				SET SecuenciaMedicina      =( SELECT max(SecuenciaMedicina ) FROM tbHospitalizacion);
				SET SecuenciaCirugia  =( SELECT max(SecuenciaCirugia ) FROM tbHospitalizacion)+1;
            END IF;
            IF (isnull(SecuenciaCirugia)  = 0) then
				BEGIN
					SET SecuenciaCirugia  = '1';
				END;
			end if;
            IF ( isnull(SecuenciaEdad)  = 0) then
				BEGIN
					SET SecuenciaEdad = '0';
				END;
			end if;   
			IF (isnull(SecuenciaMedicina)  = 0 )then
				BEGIN
					SET SecuenciaMedicina = '0';
				END;
			end if;
        
        END;
	END IF;
   
   if  DAY(_Fecha_Hora_Ingreso ) = 31 then
	BEGIN 
		set SecuenciaMedicina  = 0;
		set SecuenciaEdad  = 0;
		set SecuenciaCirugia  = 0;
	END;
   end if;     
	if DAY(_Fecha_Hora_Ingreso ) =30 then
		begin
			set SecuenciaMedicina = 0;
			set SecuenciaEdad  = 0;
			set SecuenciaCirugia  = 0;
		end;
     end if;   
	IF MONTH (_Fecha_Hora_Ingreso) = 2 then

		BEGIN 
				IF DAY(_Fecha_Hora_Ingreso)= 28 then
					BEGIN
						set SecuenciaMedicina = 0;
						set SecuenciaEdad  = 0;
						set SecuenciaCirugia  = 0;
                    END;
		          
		          ELSE 
					BEGIN 
						IF DAY(_Fecha_Hora_Ingreso)= 29 then
							set SecuenciaMedicina = 0;
							set SecuenciaEdad  = 0;
							set SecuenciaCirugia  = 0;
						end if;   
					 END;
                   end if;
           END;        
	 END if; 
     
if not exists (select id from tbHospitalizacion where Paciente = _paciente and status  IN (1,3) ) then 
   begin
		
	iNSERT INTO tbHospitalizacion
		(
SecuenciaHO,
No_Historial,
Fecha_Hora_Ingreso,
cobertura_Maxima,
fecha_nacimiento,
Normal,
Cirugia_Dia,
Fecha_de_Registro,
Presupuesto,
Pre_Factura,
Paciente,
NumeroAtencion,
habitacion,
Procedencias,
Paquete_Atencion,
Nombre_Responsable,
Direccion_Responsable,
Documento_Responsable,
Telefono_Responsable,
Plan_Atencion,
CoaSeguro,
Deducible,
Observaciones,
Siniestro,
Referencia_1,
Referencia_2,
Medico_Fecha,
Servicios,
medico,
principal,
asociado_1,
asociado_2,
Medico_Observacion,
garantia,
Garantia_plan,
Garantia_Coaseguros,
Garantia_Deducible,
Garantia_moneda,
Garantia_siniestro,
Garantia_referencia_1,
Garantia_referencia_2,
Dieta,
Dieta_Observaciones,
Dieta_Contenido,
Varios_Fecha,
Varios_Musica,
Varios_Musica_desde,
varios_Musica_hasta,
Varios_Televisor,
Varios_Televisor_desde,
varios_Televisor_hasta,
Varios_Periodico,
Varios_Acompaniante,
Varios_Acepta_Visitas_si,
Varios_Acepta_Visitas_no,
Varios_Acepta_Visitas_restringido,
Varios_Acepta_llamadas,
Varios_Dieta_aconpaniante,
fecha_egreso,
medico_egreso,
principal_egreso,
asociadoEgreso1,
asociadoEgreso2,
tipo_egreso,
complicacion,
tipo_complicacion,
des_campo1,
des_campo2,
des_campo3,
fecha_registro,
usuario_registro,
fecha_modificacion,
usuario_modificacion,
pcname,
status,
codigo_cama,
traspaso,
fecha_hora_traspaso,
genero,
observacion_traspaso,
transaccion,
titular,
tipo_egreso_alta,
derivacion,
hospital,
status_documento,
osbservacion_documento,
NumeroAtencionAnual,
NumeroAtencionMensual,
SecuenciaEdad,
SecuenciaMedicina,
SecuenciaCirugia,
observacion_eliminada)
		VALUES (_SecuenciaHO  ,
				_No_Historial ,
				curdate(),
				_cobertura_Maxima ,
				_fecha_nacimiento,
				_Normal ,
				_Cirugia_Dia ,
				curdate(),
				_Presupuesto ,
				_Pre_Factura ,
				_paciente ,
				_NumeroAtencion,
				_habitacion ,
				_Procedencias ,
				_Paquete_Atencion ,
				_Nombre_Responsable ,
				_Direccion_Responsable ,
				_Documento_Responsable ,
				_Telefono_Responsable ,
				_Plan_Atencion ,
				_CoaSeguro ,
				_Deducible ,
				_Observaciones ,
				_Siniestro ,
				_Referencia_1 ,
				_Referencia_2 ,
				_Medico_Fecha ,
				_Servicios ,
				_medico ,
				_principal ,
				_asociado_1 ,
				_asociado_2 ,
				_Medico_Observacion ,
				_garantia ,
				_Garantia_plan ,
				_Garantia_Coaseguros ,
				_Garantia_Deducible ,
				_Garantia_Moneda ,
				_Garantia_siniestro ,
				_Garantia_referencia_1 ,
				_Garantia_referencia_2 ,
				_Dieta ,
				_Dieta_Observaciones ,
				_Dieta_Contenido ,
				_Varios_Fecha ,
				_Varios_Musica ,
				_Varios_Musica_desde ,
				_Varios_Musica_hasta ,
				_Varios_Televisor ,
				_Varios_Televisor_desde ,
				_Varios_Televisor_hasta ,
				_Varios_Periodico ,
				_Varios_Acompañante ,
				_Varios_Acepta_Visitas_si ,
				_Varios_Acepta_Visitas_no ,
				_Varios_Acepta_Visitas_restringido ,
				_Varios_Acepta_llamadas ,
				_Varios_Dieta_aconpañante ,
				_fecha_egreso,  
				_medico_egreso, 
				_principal_egreso, 
				_asociadoEgreso1 ,
				_asociadoEgreso2 ,
				_tipo_egreso ,
				_complicacion ,
				_tipo_complicacion ,
				_des_campo1 ,
				_des_campo2	 ,
				_des_campo3,
				curdate() ,
				_usuario_registro ,
				curdate(),
				_usuario_modificacion ,
				_pcname,
				_status,
				_codigo_cama,
				_traspaso ,
				_fecha_hora_traspaso ,
				_genero,
				_observacion_traspaso,
				_transaccion ,
				_titular,
				_tipo_egreso_alta,
				_derivacion ,
				_hospital ,
				_status_documento ,
				_osbservacion_documento ,
				NumeroAtencionAnual,
				SecuenciaMensual ,
				SecuenciaEdad ,
				SecuenciaMedicina ,
				SecuenciaCirugia,
				_observacion_eliminada  );
				
				
/*
Actualización de Tabla Clientes en Administracion
basandonos en la cedula del paciente y del doctor.
*/           



/*
	UPDATE Administracion.Clientes
	SET VenCodigo =case when(select top 1 t1.VenCodigo
							from Administracion.Vendedores t1,  tbMedico t2 where t1.VenCedula=t2.cedula 
							and T2.id=_medico) IS null then '0' else (select top 1 t1.VenCodigo
							from Administracion.Vendedores t1,  tbMedico t2 where t1.VenCedula=t2.cedula 
							and T2.id=_medico) end 
	WHERE CliCodigo=(SELECT cedula FROM tbPaciente WHERE ID=_paciente)

*/
/*
Fin de Actualizacion
*/
end;				
				
				set _id := (select id from tbhospitalizacion order by id desc limit 1);
				-- select _id,_SecuenciaHo,_No_Historial,_NumeroAtencion,_transaccion;

end if;   
    

     
 


COMMIT;
 


/*Mandamos 0 si todo salio bien*/ 
SELECT 0 as error; 
		  
end