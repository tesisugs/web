delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarEmergenciasanios`(
	IN `anio1` varchar(5) ,
	IN `anio2` varchar(5)
)
begin
select 
(SELECT count(id) as numero_ingreso FROM tb1registroadmision
where date_format(fecha,'%Y') = anio1 ) as year1 ,
(
SELECT count(id) as numero_ingreso FROM tb1registroadmision 
where date_format(fecha,'%Y') = anio2) as year2;
end
// delimiter //