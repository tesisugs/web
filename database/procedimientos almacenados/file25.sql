USE [MajomaControlHospitalario]
GO
/****** Object:  StoredProcedure [dbo].[spInsertIngresoPacienteConsultaExterna2]    Script Date: 4/6/2018 18:46:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spInsertIngresoPacienteConsultaExterna2]
@id int output,
@codigo int output,
@paciente nvarchar(50),
@cedula_paciente nvarchar(50),
@fecha_nacimiento smalldatetime,
@telefono_paciente nvarchar(50),
@observacion nvarchar(50),
@direccion_paciente nvarchar (50),
@dependencias nvarchar (50),
@medico int,
@nombre_representante nvarchar(50),
@cedula_representante nvarchar(50),
@direccion_representante nvarchar(50),
@telefono_representante nvarchar(50),
@observacion_eliminada nvarchar(50),
@fecha_registro_paciente smalldatetime,
@hora_registro_paciente nvarchar(50),
@des_campo1 nvarchar(50),
@des_campo2 nvarchar(50),
@des_campo3 int ,
@usuario_ingreso int ,
@fecha_ingreso smalldatetime,
@usuario_modificacion int,
@fecha_modificacion smalldatetime,
@pcname nvarchar(50),
@status int,
@titular_representante int,
@principal nvarchar(50) ,
@asociado_1 nvarchar(50),
@asociado_2 nvarchar(50),
@NumeroAtencion int output,
@transaccion int output,
@factura nvarchar(50),
@tipo_seguro int,
@tipo_consulta int,
@id_consulta int,

@fuente_informacion nvarchar(50) = null,
@persona_entrega nvarchar(50) = null,
@cedula_persona_entrega nvarchar(13) = null,
@tipo_acompañante varchar(5) = null
AS

declare @w_error int, @w_rowcount int, @w_exito smallint
select @w_exito=0


begin tran 
	--/// LLEVAMOS LA SECUENCIA
	update tbSecuencia
		set secuencia=secuencia+1
	where codigo=5
    select @w_error=@@error, @w_rowcount=@@rowcount  
        if (@w_error = 0)    
          begin    
            if @w_rowcount=0
              begin    
                select @w_exito=1
                goto SalirFin    
              end    
          end    
        else  
          begin    
            select @w_exito=1
            goto SalirFin    
          end   
	select @codigo=secuencia 
		from tbsecuencia
	where codigo=5
	--/// llevamos la transaccion del ta bala secuencia
	update tbSecuencia
		set secuencia=secuencia+1
	where codigo=4
	select @w_error=@@error, @w_rowcount=@@rowcount  
        if (@w_error = 0)    
          begin    
            if @w_rowcount=0
              begin    
                select @w_exito=1
                goto SalirFin    
              end    
          end    
        else  
          begin    
            select @w_exito=1
            goto SalirFin    
          end   
	--/// llevamos el numero de atencion del paciente 
	
	SET @NumeroAtencion  =( SELECT COUNT (*)FROM tbIngresoPacienteConsultaExterna 
		where paciente = @paciente) + 1

	--/// llevamos la transaccion 
	select @transaccion =secuencia 
	 from tbsecuencia
	where codigo=4
	declare  @@fecha AS date = GETDATE ()
--------------------------------------
------////insertamos en la tabla de consulta externa
	INSERT INTO dbo.tbIngresoPacienteConsultaExterna
			VALUES (@codigo,
					@paciente,
					@cedula_paciente ,
					@fecha_nacimiento ,
					@telefono_paciente ,
					@observacion ,
					@direccion_paciente,
					@dependencias ,
					@medico ,
					@nombre_representante ,
					@cedula_representante ,
					@direccion_representante ,
					@telefono_representante,
					@observacion_eliminada ,
					@fecha_registro_paciente ,
					@hora_registro_paciente ,
					@des_campo1 ,
					@des_campo2 ,
					@des_campo3  ,
					@usuario_ingreso  ,
					GETDATE()  ,
					@usuario_modificacion ,
					GETDATE () ,
					@pcname ,
					@status ,
					@titular_representante,
					@principal ,
					@asociado_1 ,
					@asociado_2 ,
					@NumeroAtencion ,
					@transaccion , @factura ,@tipo_seguro,
					@fuente_informacion,
					@persona_entrega,
					@cedula_persona_entrega,
					@tipo_acompañante  )
					SELECT @id = @@IDENTITY 
			select @w_error=@@error, @w_rowcount=@@rowcount  
			if (@w_error = 0)    
				begin    
				  if @w_rowcount=0
					begin    
					  select @w_exito=1
					  goto SalirFin    
					end    
				  end    
			else  
			  begin    
               select @w_exito=1
			   goto SalirFin    
			  end   
			  
 ----------------------------------------------------
 -----------///insertamos en la tabla de tipo de consulta externa
       insert into dbo.tbConsultaExternaTipoConsulta
             values(@id,
                    @paciente,
                    @nombre_representante,
                    @tipo_consulta,
                    @id_consulta,
                    @des_campo1,
                    @des_campo2,
                    @des_campo3,
                    @usuario_ingreso,
                    GETDATE(),
                    @usuario_ingreso,
                    GETDATE(),
                    @pcname,
                    @status)
        select @w_error=@@error, @w_rowcount=@@rowcount  
			if (@w_error = 0)    
				begin    
				  if @w_rowcount=0
					begin    
					  select @w_exito=1
					  goto SalirFin    
					end    
				  end    
			else  
			  begin    
               select @w_exito=1
			   goto SalirFin    
			  end  
-------------------------------------------------------------
------------//actualizamos la tabla de hospitalizacion de las consultas externas
			update tbHospitalizacionConsultaExterna
			set status = 0,
			    des_campo1 = @id
			where id = (select top 1 id  from tbHospitalizacionConsultaExterna where id_hospitalizacion = @id_consulta
			              and paciente = @paciente and status = 1 )             
 
  SalirFin:
			if (@w_exito= 0)    
				begin    
					commit tran
					return @id
					return @NumeroAtencion
					return @transaccion
				 end    
			else  
				begin    
					rollback tran
					return 1    
				end   

