create procedure SpRpFirmas

-- SpRptConsentimientoInformado '12763'
-- SpRpFirmas '61903','1','0','1'
(in _atencion int,
in _servicio int,
in _visita int,
in _documento int
)
select * from  MajomaImagenes.tbFirmasPorAtencion 
where tipo_servicio = _servicio
and id_atencion = _atencion
and id_visita = _visita
and id_tipo_documento = _documento
and status=1