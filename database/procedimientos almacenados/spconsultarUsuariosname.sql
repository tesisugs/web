// delimiter //
create procedure spconsultarUsuariosname(in name_ varchar(30) , in role int)
begin 
if role = 0 then 
	  select u.id , u.nombres , u.apellidos , u.name , r.name as role  from users u
  				join role_user ru on ru.user_id = u.id 
            join roles r on r.id = ru.role_id
            where u.estado = 1 and u.name like concat('%',name_,'%' ) ;
	
else 
	  select u.id , u.nombres , u.apellidos , u.name , r.name as role  from users u
  				join role_user ru on ru.user_id = u.id 
            join roles r on r.id = ru.role_id
            where u.estado = 1 and u.name like concat('%',name_,'%' )  and r.id = role;
end if;

end
// delimiter //
          