CREATE DEFINER=`root`@`localhost` PROCEDURE `SpInsertHospitalizacionTraspaso`(
out _id int ,
in _id_hospitalizacion int,
in _paciente_traspaso int,
in _fecha_ingreso datetime,
in _fecha_salida datetime ,
in _hora_ingreso nvarchar(50),
in _hora_salida nvarchar(50),
in _dias_transcurridos nvarchar(50),
in _horas_transcurridas nvarchar(50),
in _valor nvarchar(50),
in _des_campo1            nvarchar(50),
in _des_campo2            nvarchar(50),
in _des_campo3            decimal(10,2),
in _usuario_ingreso       int,
in _usuario_modificacion       int,
in _pcname                nvarchar(50),
in _status                int,
in _observacion_traslado nvarchar (100),
in _codigo_cama_anterior int,
in _codigo_cama_traslado  int,
out _numero_traslado int  ,
in _secuencia int 

)
begin
/*Handler para error SQL*/ 
-- DECLARE EXIT HANDLER FOR SQLEXCEPTION 
-- BEGIN 
-- SELECT 1 as error; 
-- ROLLBACK; 
-- END; 

/*Handler para error SQL*/ 
-- DECLARE EXIT HANDLER FOR SQLWARNING 
-- BEGIN 
-- SELECT 1 as error; 
-- ROLLBACK; 
-- END; 

-- START TRANSACTION; 

	 set  _numero_traslado  :=( SELECT COUNT(*) FROM tbTraspasos 
	 where paciente_traspaso  = _paciente_traspaso and id_hospitalizacion = _id_hospitalizacion  ) + 1;

update tbSecuencia
  set secuencia=secuencia+1
where codigo=2;


 -- -//// NUMERO DE ATENCION
		
		
	iNSERT INTO tbTraspasos(
							id_hospitalizacion,
                            paciente_traspaso,
                            fecha_ingreso,
                            fecha_salida,
                            hora_ingreso,
                            hora_salida,
							dias_trascurridos,
                            horas_transcurridas,
                            valor,
                            des_campo1,
                            des_campo2,
                            des_campo3,
                            usuario_ingreso,
                            usuario_modificacion,
                            fecha_modificacion,
                            pcname,
                            status ,
							observacion_traslado,
                            codigo_cama_anterior,
                            codigo_cama_traslado,
                            numero_traslado,
                            secuencia) 
		VALUES (_id_hospitalizacion ,
				_paciente_traspaso ,
				_fecha_ingreso ,
				_fecha_salida ,
				_hora_ingreso ,
				_hora_salida ,
				_dias_transcurridos,
				_horas_transcurridas ,
				_valor ,
				_des_campo1,
				_des_campo2 ,
				_des_campo3 ,
				_usuario_ingreso,
				_usuario_modificacion,
				curdate(),
				_pcname,
				_status ,
				_observacion_traslado,
				_codigo_cama_anterior ,
				_codigo_cama_traslado,
				_numero_traslado,
				_secuencia );
                
                UPDATE	tbHospitalizacion
				SET	fecha_modificacion =curdate()
				,usuario_modificacion =_usuario_ingreso 
				,pcname=_pcname,codigo_cama=_codigo_cama_traslado
				WHERE	id = _id_hospitalizacion;
                
                UPDATE	tbCamasPorSala
				 SET	status_camas =  3
				,ocupante ='.'
				WHERE	codigo = _codigo_cama_anterior;
                
                
                -- select _secuencia=secuencia  from tbsecuencia where codigo=2 union all
				
                -- SELECT id from tbTraspasos order by id desc  limit 1;
	/*Fin de transaccion*/ 
-- COMMIT; 


/*Mandamos 0 si todo salio bien*/ 
-- SELECT 0 as error; 
end