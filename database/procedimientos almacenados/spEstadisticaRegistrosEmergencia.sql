delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spEstadisticaRegistrosEmergencia`()
begin
select
 (select count(id) as enero from tb1registroadmision where fecha_ingreso between '2018/01/01' and '2018/01/31') as enero, 
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/02/01' and '2018/02/28') as febrero,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/03/01' and '2018/03/31') as marzo,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/04/01' and '2018/04/30') as abril,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/05/01' and '2018/05/31') as mayo,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/06/01' and '2018/06/30') as junio,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/07/01' and '2018/07/31') as julio,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/08/01' and '2018/08/31') as agosto,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/09/01' and '2018/09/30') as septiembre,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/10/01' and '2018/10/31') as octubre,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/11/01' and '2018/11/30') as noviembre,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between '2018/12/01' and '2018/12/31') as dicimebre  
;
end
// delimiter //