CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarStatusHospitalizacionTodos2`()
begin
SELECT '%' AS id,
       'Todos' AS Descripcion
      UNION all
SELECT CONVERT(id,CHAR(5)) as Codigo,
       descripcion   AS Descripcion
  FROM tbStatusHospitalizacion   
WHERE status=1;
END