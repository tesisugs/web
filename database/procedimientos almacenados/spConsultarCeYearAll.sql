delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarCeYearAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)
)
begin
select *from tbConsultaExternaTurnosDiarios  where date_format(fecha_registro_paciente,'%Y') = desde or date_format(fecha_registro_paciente,'%Y') = hasta order by fecha_registro_paciente desc;
end
// delimiter //