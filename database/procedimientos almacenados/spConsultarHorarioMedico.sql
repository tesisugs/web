create procedure spConsultarHorarioMedico
(
-- spConsultarHorarioMedico
in _medico nvarchar(50)
)
select h.id,
		h.medico,
		concat_ws('',m.apellidos , m.nombres ) 'Nombre Medico',
		fecha_desde1,
		fecha_hasta1,
		status_fecha1,
		fecha_desde2,
		fecha_hasta2,
		status_fecha2,
		
		fecha_desde3,
		fecha_hasta3,
		status_fecha3,
		
		fecha_desde4,
		fecha_hasta4,
		status_fecha4,
		
		fecha_desde5,
		fecha_hasta5,
		status_fecha5,
		
		fecha_desde6,
		fecha_hasta6,
		status_fecha6,
		
		fecha_desde7,
		fecha_hasta7,
		status_fecha7
		
		  from tbHorariosMedicos h, tbMedico m
		  where h.medico = m.id 
		  and h.medico = _medico

		   order by m.apellidos
