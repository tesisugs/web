delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spEstadisticaRegistrosEmergenciaYears`(in fecha1 varchar(4), in fecha2 varchar(4) )
begin
select
 (select count(id) as enero from tb1registroadmision where fecha_ingreso between  concat(fecha1,'/01/01')  and concat(fecha1,'/01/31') ) as enero, 
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/02/01')  and concat(fecha1,'/02/28')) as febrero,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/03/01')  and concat(fecha1,'/03/31')) as marzo,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/04/01')  and concat(fecha1,'/04/30')) as abril,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/05/01')  and concat(fecha1,'/05/31')) as mayo,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/06/01')  and concat(fecha1,'/06/30')) as junio,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/07/01')  and concat(fecha1,'/07/31')) as julio,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/08/01')  and concat(fecha1,'/08/31')) as agosto,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/09/01')  and concat(fecha1,'/09/30')) as septiembre,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/10/01')  and concat(fecha1,'/10/31')) as octubre,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/11/01')  and concat(fecha1,'/11/30')) as noviembre,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha1,'/12/01')  and concat(fecha1,'/06/31')) as dicimebre,
(select count(id) as enero1 from tb1registroadmision where fecha_ingreso between  concat(fecha2,'/01/01')  and concat(fecha2,'/01/31') ) as enero1, 
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/02/01')  and concat(fecha2,'/02/28')) as febrero1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/03/01')  and concat(fecha2,'/03/31')) as marzo1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/04/01')  and concat(fecha2,'/04/30')) as abril1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/05/01')  and concat(fecha2,'/05/31')) as mayo1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/06/01')  and concat(fecha2,'/06/30')) as junio1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/07/01')  and concat(fecha2,'/07/31')) as julio1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/08/01')  and concat(fecha2,'/08/31')) as agosto1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/09/01')  and concat(fecha2,'/09/30')) as septiembre1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/10/01')  and concat(fecha2,'/10/31')) as octubre1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/11/01')  and concat(fecha2,'/11/30')) as noviembre1,
(select count(id) as febrero from tb1registroadmision where fecha_ingreso between concat(fecha2,'/12/01')  and concat(fecha2,'/06/31')) as dicimebre1;
end
// delimiter //