delimiter //
create procedure spNotificacionMedicoLLamado(in _id int, in _respuesta varchar(10))
begin 
declare usuario int;
declare role int;
declare notificacion int;

set usuario:= (select usuario_ingreso from tbEnvioCorreoMedicos  where id =_id); -- el usuario que envio el correo
set role:= (select role_id from role_user where user_id = usuario); -- el rol de este usuario

insert into  tbnotificaciones(rol_id,titulo,descripcion,icono,estado,fecha_ingreso,usuario_ingreso,fecha_modificacion,usuario_modificacion) 
values(role,"Medicos al llamado",concat("el medico ha respondio al llamado",_respuesta),1,1,now(),1,now(),1); -- se crea la notificacion

set notificacion := (select id from tbnotificaciones order by id desc limit 1); -- se obtiene el id de la notificacion

insert into  tbuser_notification(user_id,notification_id,estado,fecha_ingreso,usuario_ingreso) 
values(usuario,notificacion,1,now(),1); -- se ingresa la notificaciones individuales



end 
// delimiter //

