delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarEmergenciasSemanasAll`(
	IN `fecha1` varchar(15),
	IN `fecha2` varchar(15)
)
begin
  SELECT * from tb1registroadmision where date_format(fecha_ingreso,'%U') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%U') and
			date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%Y')  
  union all
   SELECT * from tbhospitalizacion where date_format(fecha_ingreso,'%U') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%U') and
			date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%Y');  
		
end
// delimiter //