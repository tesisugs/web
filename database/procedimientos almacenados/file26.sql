
create PROCEDURE spInsertConsultaExternaTurnosDiarios2 (
out _turnos_diarios int ,
in _consulta_externa int ,
in _paciente nvarchar(50),
in _cedula_paciente nvarchar(50),
out _turnos int ,
in _medico int,
in _observacion nvarchar(50),
in _fecha_registro_paciente date,
in _hora_registro_paciente nvarchar(50),
in _des_campo1 nvarchar(50),
in _des_campo2 nvarchar(50),
in _des_campo3 int ,
in _usuario_ingreso int ,
in _usuario_modificacion int,
in _pcname nvarchar(50),
in _status int,
in _titular_representante int
)
-- /// LLEVAMOS LA SECUENCIA
-- numero de atencion del paciente 
begin
	declare @@fechaini as date = @fecha_registro_paciente
	declare @@fechafin as date = @fecha_registro_paciente
	SET @turnos   =( SELECT COUNT (*)FROM tbIngresoPacienteConsultaExterna 
		where  fecha_registro_paciente between @@fechaini and @@fechafin 
		and status = 1 		and   medico    = @medico ) 

--/// llevamos la transaccion 
  -- 0925973968 peña dr guerra 
-- declare  @@fecha AS date = GETDATE ()
--------------------------------------
INSERT INTO tbConsultaExternaTurnosDiarios
		VALUES (@consulta_externa,
				@paciente,
				@cedula_paciente ,
				@turnos  ,
				@medico ,
				@observacion  ,
				@fecha_registro_paciente ,
				@hora_registro_paciente ,
				@des_campo1 ,
				@des_campo2 ,
				@des_campo3  ,
				@usuario_ingreso  ,
				GETDATE()  ,
				@usuario_modificacion ,
				GETDATE () ,
				@pcname ,
				@status ,
				@titular_representante	 )
				return @turnos
				
end