Delimiter //
create procedure spConsultarPacienteHospitalizacionEgresado
(
in _paciente  int
-- spConsultarPacienteHospitalizacionEgresado 136645
)
begin
declare _w_fecha1 datetime; 
declare _w_fecha2 datetime;
declare _w_asistencia  varchar(30);

  set _w_fecha1 = curdate() - 15;
  set _w_fecha2 = curdate();
  
if not exists (select id from tbHospitalizacionConsultaExterna where paciente = _paciente limit 1) then
   
	
       select tb1.id,
           '1' 'Consulta No',
           -- @w_asistencia  Asistencia,
           case when ( select tb4.medico from tbIngresoPacienteConsultaExterna tb4  where tb4.paciente = @paciente
                        and  tb4.fecha_registro_paciente between  tb1.fecha_egreso + 1 and 
                          _w_fecha2 and tb4.status = 1 and tb4.tipo_ingreso = 1 ) is null then ' ' 
                           else 'YA ASISTIO A LA CONSULTA' end Asistencia,
                           concat_ws(' ',tb2.apellido_paterno ,  tb2.apellido_materno , tb2.primer_nombre , tb2.segundo_nombre) Paciente,
           tb1.medico Id_medico,
           (select concat_ws(' ',tb3.apellidos , tb3.nombres ) from tbMedico tb3 where tb3.id = tb1.medico and tb3.status = 1) Medico,
           tb1.fecha_egreso FechaEgresoHosp,
           tb1.fecha_egreso + 15 FechaLimiteConsulta
           
     from tbHospitalizacion tb1, tbPaciente tb2
     where tb1.Paciente = _paciente 
         -- AND tb1.id = (SELECT MAX(ID) FROM tbHospitalizacion WHERE Paciente = @paciente AND status = 2  )
         AND tb1.id = (SELECT MAX(ID) FROM tbHospitalizacion WHERE Paciente = _paciente AND status = 2  and Procedencias = 1  and 
                fecha_egreso between (_w_fecha2  - 40) and   _w_fecha2)
         and   tb1.paciente = tb2.id
          and tb1.Procedencias = 1
          and tb1.status = 2
       --   and tb1.Cirugia_Dia = 0
    union all
   
     select tb1.id,
            '2' 'Consulta No',
            case when ( select  tb4.medico from tbIngresoPacienteConsultaExterna tb4  where tb4.paciente = _paciente
                        and  tb4.fecha_registro_paciente between  (tb1.fecha_egreso + 15) and 
                            _w_fecha2 and tb4.status = 1) is null then ' ' 
                           else 'YA ASISTIO A LA CONSULTA' end Asistencia,
          concat_ws(tb2.apellido_paterno ,  tb2.apellido_materno , tb2.primer_nombre , tb2.segundo_nombre )  Paciente,
            tb1.medico Id_medico,
            (select concat_ws(' ',tb5.apellidos , tb5.nombres ) from tbMedico tb5 where tb5.id = tb1.medico and tb5.status = 1) Medico,
                          tb1.fecha_egreso FechaEgresoHosp,
              tb1.fecha_egreso + 30 FechaLimiteConsulta
            
            
     from tbHospitalizacion tb1,tbPaciente tb2, Administracion.Requisiciones_Pacientes tb3
     where tb1.Paciente = @paciente
     and tb1.Paciente = tb2.id
     and tb1.Procedencias = 1
     and tb3.CliCodigo = ( case when (select clicodigo from Administracion.Clientes 
      where Administracion.Clientes.CliNombre like concat(tb2.apellido_paterno , ' ', tb2.apellido_materno ,' ', tb2.primer_nombre , ' ' , tb2.segundo_nombre , '%'  )  
      and Administracion.Clientes.CliTercerizadora not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR')) IS null then tb2.cedula else ( select   clicodigo from Administracion.Clientes 
      where Administracion.Clientes.CliNombre like concat(tb2.apellido_paterno , ' ' , tb2.apellido_materno , ' ', tb2.primer_nombre , ' ' , tb2.segundo_nombre , '%' )  
      and Administracion.Clientes.CliTercerizadora not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR'))  end );
        
	
   
  
 else  
  select tb1.id_hospitalizacion id,
             tb1.numero_consulta 'Consulta No',
             case when tb1.status = 0 then 'YA ASISTIO' else ' ' end Asistencia,
            concat_ws(' ',tb3.apellido_paterno , tb3.apellido_materno , tb3.primer_nombre , tb3.segundo_nombre ) Paciente,
			tb2.medico Id_medico,
             (select concat_ws(' ',tb4.apellidos,tb4.nombres ) from tbMedico tb4 where tb4.id = tb2.medico and tb4.status = 1) Medico,
             tb2.fecha_egreso FechaEgresoHosp,
             tb1.fecha_limite FechaLimiteConsulta
      from tbHospitalizacionConsultaExterna tb1, tbHospitalizacion tb2, tbPaciente tb3
      where tb1.paciente = _paciente 
            and tb1.id_hospitalizacion = (SELECT MAX(ID) FROM tbHospitalizacion WHERE Paciente = _paciente AND status = 2  
           -- and Procedencias = 1 
             and  Procedencias in (1, 2,4,5,30,31,53)
            and  fecha_egreso between (_w_fecha2  - 40) and   _w_fecha2)
  --    and tb1.status = 1
      and tb1.id_hospitalizacion = tb2.id
      and tb1.paciente = tb3.id;
   --   and tb2.Normal = 1
   
end if;

end 


// delimiter //