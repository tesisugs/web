CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarAgendaMedicoTurnosAgenda`(
in _medico nvarchar(100),
in _fecha date
-- SpConsultarAgendaMedicoTurnosAgenda '10',
-- SpConsultarAgendaMedicoTurnos '9'
)
SELECT c .turnos_diarios 'TURNOS DIARIOS',
	C.consulta_externa  'ID', 
	concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre , p.segundo_nombre ) AS 'PACIENTE',
	(select concat(apellidos,' ',nombres ) as Nombres from tbMedico where id = c.medico ) 'MEDICO',
	c.fecha_registro_paciente 'FECHA',
	c.hora_registro_paciente 'HORA' ,
	c.des_campo1 'HORA FIN',
     concat('C',' ',convert((select numero_camas  from tbCamasPorSala where codigo = (select consultorio_default from tbMedico where id = c.medico)),char))'CONSULTORIO' ,
	c.turnos 'TURNO',
    c.status  'ESTADO'
FROM tbConsultaExternaTurnosDiarios c, tbPaciente p
     where c.paciente = p.id 
		and c.fecha_registro_paciente = _fecha
		and c.status <> 3
		and medico = _medico
		order by convert(c.turnos,SIGNED) asc