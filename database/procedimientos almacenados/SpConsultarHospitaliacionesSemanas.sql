delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarHospitaliacionesSemanas`(
	IN `fecha1` varchar(15),
	IN `fecha2` varchar(15)
)
begin
  SELECT * from tbhospitalizacion where date_format(Fecha_de_Registro,'%U') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%U') and
			date_format(Fecha_de_Registro,'%Y') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%Y')  
  union all
   SELECT * from tbhospitalizacion where date_format(Fecha_de_Registro,'%U') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%U') and
			date_format(Fecha_de_Registro,'%Y') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%Y');  
		
end
// delimiter //
