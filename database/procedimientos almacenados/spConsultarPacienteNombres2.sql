delimiter //
create procedure spConsultarPacienteNombres2
(
in _apellido_paterno nvarchar(50),
in _desde date,
in _hasta date,
in _modo int
)
begin
declare w_fecha1 datetime; 
declare w_fecha2 datetime;
 set w_fecha1 := curdate() - 1;
  set w_fecha2 := curdate();

-- - pacientes privados que no pasan por la emergencia
if _modo = 1 then 
  
     SELECT id CodPaciente,
            '0'  RegistroAdmision,
			cedula Cedula, 
			fecha_ingreso FechaIngreso,
            concat(apellido_paterno ,' ', apellido_materno ,' ', primer_nombre ,' ', segundo_nombre )as Paciente
     from tbPaciente   
     where concat(apellido_paterno ,' ', apellido_materno  ) like concat('%',_apellido_paterno,'%')
     and status = 1; 
     -- and tipo_seguro = 49
end if;
 if  _modo = 2 then
     SELECT 
            c.paciente CodPaciente,
            c .id  RegistroAdmision,
			concat(apellido_paterno ,' ', apellido_materno ,' ', primer_nombre ,' ', segundo_nombre )as Paciente,
            (select concat(apellido_paterno ,' ', apellido_materno ,' ', primer_nombre ,' ', segundo_nombre ) from tbPaciente where id = c.afiliado )responsable  ,
            c.fecha  FechaAsistencia ,
            c.hora  HoraAsistencia,
           (select concat(apellidos ,' ', nombres ) from tbMedico  where id= c.doctor) Medico
         FROM tb1RegistroAdmision  c, tbPaciente p
     where c.paciente = p.id 
     
        AND concat(p.apellido_paterno , ' ' , p.apellido_materno   ) LIKE concat('%',_apellido_paterno,'%')
        and c.fecha  between w_fecha1 and w_fecha2 
     and c.status = 1;
 end if;    
     
  
  end
 -- and fecha_ingreso between @desde and @hasta
// delimiter //