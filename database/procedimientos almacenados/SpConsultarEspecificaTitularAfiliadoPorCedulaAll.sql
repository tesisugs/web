CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarEspecificaTitularAfiliadoPorCedulaAll`(in _cedula  varchar(13) )
begin
select	id,
		cedula,
		tipo_identificacion,
		primer_nombre,
		segundo_nombre,
		apellido_paterno,
		apellido_materno,
		genero,
		fecha_nacimiento,
		lugar_nacimiento,
		ocupacion,
		estado_civil,
		tipo_sangre,
		nacionalidad,
		pais,
		provincia,
		ciudad,
		direccion,
		telefono,
		celular,
		otro,
		observacion,
		lugar_trabajo,
		tipo_parentesco,
		tipo_beneficiario,
		cedula_titular,
		tipo_seguro,
		des_campo1,
		des_campo2,
		des_campo3,
		usuario_ingreso,
		fecha_ingreso,
		usuario_modificacion,
		fecha_modificacion,
		pcname,
		status,
		status_discapacidad,
		carnet_conadis,
		status_otro_seguro,
		tipo_seguro_iess,
		descripcion_otro_seguro,
		etnico,
		parroquia
  from tbPaciente 
where cedula=_cedula and status=1;
  
  select *
  from tbTipoIngreso tb2
  where tb2.codigo = (select tipo_seguro from tbPaciente where cedula = _cedula);
end