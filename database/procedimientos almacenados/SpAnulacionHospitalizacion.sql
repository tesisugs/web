delimiter //
CREATE  PROCEDURE `SpAnulacionHospitalizacion`(
in _id	int	,
in _id_hospitalizacion	int	,
in _paciente	int	,
in _observacion	nvarchar(20)	,
in _usuario_ingreso	int	,
in _fecha_modificacion datetime,

in _codigo NUMERIC,
in _status_camas nvarchar(50),
in _ocupante nvarchar(50),

in _usuario_modificacion varchar(50),
in _observacion_eliminada varchar(50)

)
begin

/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLWARNING 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

START TRANSACTION; 
 INSERT INTO tbHospitalizacionAnulacion (id_hospitalizacion,paciente,observacion,fecha_ingreso,
										des_campo1,des_campo2,des_campo3,usuario_ingreso,
                                        usuario_modificacion,fecha_modificacion)
				VALUES (_id_hospitalizacion				,
						_paciente		,
						_observacion	,
						curdate()	,
						'.'	,
						'.',	
						'0'	,
						_usuario_ingreso	,
						_usuario_ingreso		,
						_fecha_modificacion	);

UPDATE	tbCamasPorSala
		SET		status_camas =_status_camas  
				,ocupante =_ocupante 												
		WHERE	codigo = _codigo;

UPDATE	tbHospitalizacion  
          set		usuario_modificacion =_usuario_modificacion,
          observacion_eliminada = _observacion_eliminada ,
					fecha_modificacion=curdate(),
     				pcname=_pcname,
					status=_status
		WHERE	id  = _id;


                        
select id from tbHospitalizacionAnulacion  order by id desc limit 1;

COMMIT; 


/*Mandamos 0 si todo salio bien*/ 
SELECT 0 as error; 

end
// delimiter //