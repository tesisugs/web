delimiter //
create  procedure SpInsertPacienteInformacionAdicional(
in _paciente  int,
in _nombres_padre   varchar(200),
in _nombres_madre   varchar(200),
in _conyugue       varchar(200),
in _des_campo1     nvarchar(50),
in _des_campo2            nvarchar(50),
in _des_campo3            decimal(10,2),
in _usuario_ingreso       int,
in _pcname                nvarchar(50),
in _cedula_seg_progenitor   nvarchar(13),
in _fecha_cedulacion datetime ,
in _fecha_fallecimiento datetime ,
in _instruccion varchar(30) ,
in _cobertura_compartida char(1) ,
in _prepagada int,
in _issfa char(1),
in _isspol char(1),
in _iess char(1) 
-- SELECT * FROM TBPACIENTE
)
begin 

if not exists( select id  from tbPacienteInformacionAdicional where paciente = _paciente) then
		
        insert into tbPacienteInformacionAdicional
				values( _paciente ,
				_nombres_padre,
				_nombres_madre,
				_conyugue,
				_des_campo1,
				_des_campo2,
				_des_campo3,
				_usuario_ingreso,
				GETDATE(),
				_usuario_ingreso,
				GETDATE(),
				_pcname,
				1,
				_cedula_seg_progenitor,
                _fecha_cedulacion,
                _fecha_fallecimiento,
                _instruccion,
                _cobertura_compartida,
                _prepagada,
                _issfa,
                _isspol,
                _iess
                
         ); 
else
  update tbPacienteInformacionAdicional
    set nombres_padre = _nombres_padre,
		nombres_madre = _nombres_madre,
		conyugue = _conyugue,
		usuario_modificacion = _usuario_ingreso,
		cedula_seg_progenitor = _cedula_seg_progenitor,
		fecha_modificacion = GETDATE()
	where paciente = _paciente;	
end if; 

end
// delimiter //