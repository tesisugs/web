delimiter //
create procedure spConsultarMedicoPorCodigo
-- spConsultarMedicoPorCodigo 59
( in _id int )
begin
SELECT id ,
concat_ws(' ',apellidos , nombres  )
 as NombreCompletos  ,registro_sanitario,registro_profesional , numero_atencion , des_campo1
   FROM tbMedico 
where id= _id;
SELECT *,(select e.descripcion from tbEspecializacion e where e.codigo=m.especializacion)EspMedico
   FROM tbMedico m
where id=_id;
end
// delimiter //
