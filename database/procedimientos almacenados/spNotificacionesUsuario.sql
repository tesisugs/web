CREATE DEFINER=`root`@`localhost` PROCEDURE `spNotificacionesUsuario`( in usuario int)
begin
select n.descripcion , n.titulo, n.fecha_ingreso, un.user_id , un.notification_id 
from tbuser_notification un
join tbnotificaciones n
on un.notification_id = n.id
where un.estado = 1 and un.user_id = usuario;
end