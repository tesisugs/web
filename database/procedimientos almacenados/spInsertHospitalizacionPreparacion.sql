
delimiter //
create PROCEDURE spInsertHospitalizacionPreparacion
(
out _id int ,
in _hospitalizacion int ,
in _paciente int ,
in _titular int,
in _temperatura decimal(18,2),
in _pulso decimal(18,2),
in _presion_arterial decimal(18,2),
in _respiracion decimal(18,2),
in _peso decimal(18,2),
in _estatura decimal(18,2),
in _talla decimal(18,2),
in _discapacidad nvarchar(50),
in _carnet_conais nvarchar(50),
in _des_campo1 nvarchar(50),
in _des_campo2 nvarchar(50),
in _des_campo3 int,
in _usuario_ingreso int ,
in _fecha_ingreso datetime ,
in _usuario_modificacion int ,
in _fecha_modificacion datetime ,
in _pcname nvarchar(50),
in _status int
)
-- --------------------

begin
INSERT INTO tbHospitalizacionPreparacionSV
(
hospitalizacion,
paciente,
titular,
parental,
via_oral,
orina,
drenaje,
otros,
aseo,
banio,
dieta_a,
numero_comidas,
numero_micciones,
numero_dispocisiones,
activida_fisica,
cambio_sonda,
recanalizacion,
des_campo1,
des_campo2,
des_campo3,
usuario_ingreso,
fecha_ingreso,
usuario_modificacion,
fecha_modificacion,
pcname,
status,
observacion)
		VALUES (_hospitalizacion,
				_paciente,
				_titular,
				_temperatura,
				_pulso,
				_presion_arterial,
				_respiracion,
				_peso,
				_estatura,
				_talla,
				_discapacidad,
				_carnet_conais,
				_des_campo1,
				_des_campo2,
				_des_campo3,
				_usuario_ingreso,
				getdate(),
				_usuario_modificacion,
				curdate(),
				_pcname,
				_status);

				SELECT id from	tbHospitalizacionPreparacionSV order by id desc limit 1;			
end
// delimiter //

