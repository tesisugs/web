delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultar1RegistroAdmision`(
in _codigo  int)
begin 
select id,
       numero_atencion,
       afiliado,
       paciente,
       fecha,
       hora,
       responsable,
       observacion,
       doctor,
       des_campo1,
       des_campo2,
       des_campo3,
       usuario_ingreso,
       fecha_ingreso,
       usuario_modificacion,
       fecha_modificacion,
       pcname,
       status,
       tipo_ingreso,
	   fuente_informacion,
       persona_entrega,
       derivacion,
       hospital,
	   cedula_persona_entrega,
	   tipo_acompañante
        
from tb1RegistroAdmision 
where id = _codigo;


SELECT TIMESTAMPDIFF(YEAR,p.fecha_nacimiento,c.fecha)  as edad,
TIMESTAMPDIFF(month,p.fecha_nacimiento,curdate())  as meses,
TIMESTAMPDIFF(day,p.fecha_nacimiento,c.fecha)  as dia,
 ((datediff( p.fecha_nacimiento, c.fecha) )/365) - (floor(datediff(p.fecha_nacimiento, c.fecha)/365))  as dia,
		concat_ws(' ',c.fecha_ingreso,'') fecha_emision,
      p.cedula,
(select tb1.descripcion from tbTipoIngreso tb1 where tb1.codigo = c.tipo_ingreso) seguro,
(select concat_ws(' ',m.apellidos,m.nombres  ) from tbMedico m where m.id = c.doctor ) medico
,(select concat_ws(' ',d.cie_1,d.des_cie_1   ) from tb11DiagnosticoIngreso d where d.codigo_ingreso = c.id) as cie101
,(select concat_ws(' ',d.cie_2,d.des_cie_2   ) from tb11DiagnosticoIngreso d where d.codigo_ingreso = c.id and d.cie_2 <> '00000' ) as  cie102
,(select concat_ws(' ',d.cie_3,d.des_cie_3   ) from tb11DiagnosticoIngreso d where d.codigo_ingreso = c.id and d.cie_3 <> '00000' ) as cie103,
(select da.cie_1 from tb12DiagnosticoAlta da where da.codigo_ingreso = c.id) as  'CIE10 Alta'

from tb1RegistroAdmision c, tbPaciente p
where c.id = _codigo
and c.paciente = p.id;

-- -para consulta externa

SELECT TIMESTAMPDIFF(YEAR,p.fecha_nacimiento,c.fecha_registro_paciente)  as edad,
TIMESTAMPDIFF(month,p.fecha_nacimiento,c.fecha_registro_paciente)  as meses,
TIMESTAMPDIFF(day,p.fecha_nacimiento,c.fecha_registro_paciente)  as dia,
concat_ws(' ',c.fecha_ingreso,'') fecha_emision

from tbIngresoPacienteConsultaExterna c, tbPaciente p
where c.id =  _codigo
and c.paciente = p.id;

select concat(d.cie_1  ,'  ', ( case when d.cie_2  = '00000' then ' ' else d.cie_2 end ), ' ' )   'DX'
from tb1RegistroAdmision c, tb11DiagnosticoIngreso d
where c.id = _codigo
and d.codigo_ingreso = c.id;
-- M4j0m4*2018#
end
// delimiter //