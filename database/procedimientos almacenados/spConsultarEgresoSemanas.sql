delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarEgresoSemanas`(
	IN `fecha1` varchar(15),
	IN `fecha2` varchar(15)
)
begin
select 
( SELECT count(distinct(id)) from tbegresos where
 date_format(fecha_ingreso,'%U') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%U') and
 date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%Y')   ) as numero_ingreso_semana1,
 ( SELECT count(distinct(id))  from tbegresos  where
 date_format(fecha_ingreso,'%U') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%U') and
 date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%Y')  ) as numero_ingreso_semana2 ;
end
// delimiter //