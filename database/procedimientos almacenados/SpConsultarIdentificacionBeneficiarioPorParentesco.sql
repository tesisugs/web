create procedure SpConsultarIdentificacionBeneficiarioPorParentesco (
in _parentesco  varchar(2)
)
select codigo,descripcion 
  from tbIdentificacionBeneficiario
where status=1
  and parentesco=_parentesco 
  ORDER BY id 