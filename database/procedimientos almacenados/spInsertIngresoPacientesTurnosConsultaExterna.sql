delimiter //
create procedure spInsertIngresoPacientesTurnosConsultaExterna (
out _turnos_diarios int ,
in _consulta_externa int ,
out _turnos int ,

-- variables de ->spInsertIngresoPacienteConsultaExterna2
out _id int ,
out _codigo int ,
in _paciente varchar(50),
in _cedula_paciente varchar(50),
in _fecha_nacimiento datetime,
in _telefono_paciente varchar(50),
in _observacion varchar(50),
in _direccion_paciente varchar (50),
in _dependencias varchar (50),
in _medico int,
in _nombre_representante varchar(50),
in _cedula_representante varchar(50),
in _direccion_representante varchar(50),
in _telefono_representante varchar(50),
in _observacion_eliminada varchar(50),
in _fecha_registro_paciente datetime,
in _hora_registro_paciente varchar(50),
in _des_campo1 varchar(50),
in _des_campo2 varchar(50),
in _des_campo3 int ,
in _usuario_ingreso int ,
in _fecha_ingreso datetime,
in _usuario_modificacion int,
in _fecha_modificacion datetime,
in _pcname varchar(50),
in _status int,
in _titular_representante int,
in _principal varchar(50) ,
in _asociado_1 varchar(50),
in _asociado_2 varchar(50),
out _NumeroAtencion int ,
out _transaccion int ,
in _factura varchar(50),
in _tipo_seguro int,
in _tipo_consulta int,
in _id_consulta int,
-- datos que aun se le pasa null
in _fuente_informacion nvarchar(50) ,
in _persona_entrega nvarchar(50) ,
in _cedula_persona_entrega varchar(13) ,
in _tipo_acompañante varchar(5) 
)
begin 

declare  _fecha  date; 
/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLWARNING 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 
START TRANSACTION;

	-- /// LLEVAMOS LA SECUENCIA
	 update tbSecuencia
		set secuencia=secuencia+1
		where codigo=5;
        
	select _codigo=secuencia 
		from tbsecuencia where codigo= 5;
    	-- /// llevamos la transaccion del ta bala secuencia
	    
	 update tbSecuencia
		set secuencia= _secuencia+1
		where codigo=4;
    -- /// llevamos el numero de atencion del paciente 
    
    SET _NumeroAtencion  := ( SELECT COUNT(*) FROM tbIngresoPacienteConsultaExterna 
		 where paciente = _paciente) + 1;
    -- /// llevamos la transaccion del ta bala secuencia
    
    
	update tbSecuencia
		 set secuencia= (_secuencia+1)
		  where codigo=4;
          
            -- /// llevamos la transaccion 
	 set _transaccion := (select secuencia  from tbsecuencia where codigo=4);
     
		set _fecha := curdate();
        
        INSERT INTO tbIngresoPacienteConsultaExterna
			VALUES (_codigo,
					_paciente,
					_cedula_paciente ,
					_fecha_nacimiento ,
					_telefono_paciente ,
					_observacion ,
					_direccion_paciente,
					_dependencias ,
					_medico ,
					_nombre_representante ,
					_cedula_representante ,
					_direccion_representante ,
					_telefono_representante,
					_observacion_eliminada ,
					_fecha_registro_paciente ,
					_hora_registro_paciente ,
					_des_campo1 ,
					_des_campo2 ,
					_des_campo3  ,
					_usuario_ingreso  ,
					curdate()  ,
					_usuario_modificacion ,
					curdate() ,
					_pcname ,
					_status ,
					_titular_representante,
					_principal ,
					_asociado_1 ,
					_asociado_2 ,
					_NumeroAtencion ,
					_transaccion , _factura ,_tipo_seguro,
					_fuente_informacion,
					_persona_entrega,
					_cedula_persona_entrega,
					_tipo_acompañante  );
                    
                    -- --------------------------------------------------
 -- ---------///insertamos en la tabla de tipo de consulta externa
       insert into tbConsultaExternaTipoConsulta
             values(_id,
                    _paciente,
                    _nombre_representante,
                    _tipo_consulta,
                    _id_consulta,
                    _des_campo1,
                    _des_campo2,
                    _des_campo3,
                    _usuario_ingreso,
                    curdate(),
                    _usuario_ingreso,
                    curdate(),
                    _pcname,
                    _status);
                    
-- ------------------ insert turnos diarios -------------------------------------
		INSERT INTO tbConsultaExternaTurnosDiarios
				(turnos_diarios,
		consulta_externa,
		paciente,
		cedula_paciente,
		turnos,
		medico,
		observacion,
		fecha_registro_paciente,
		hora_registro_paciente,
		des_campo1,
		des_campo2,
		des_campo3,
		usuario_ingreso,
		fecha_ingreso,
		usuario_modificacion,
		fecha_modificacion,
		pcname,
		status,
		titular_representante)
				VALUES (_consulta_externa,
						_paciente,
						_cedula_paciente ,
						_turnos  ,
						_medico ,
						_observacion  ,
						_fecha_registro_paciente ,
						_hora_registro_paciente ,
						_des_campo1 ,
						_des_campo2 ,
						_des_campo3  ,
						_usuario_ingreso  ,
						curdate()  ,
						_usuario_modificacion ,
						curdate() ,
						_pcname ,
						_status ,
						_titular_representante);
						select _turnos;                    
							
-- ----------//actualizamos la tabla de hospitalizacion de las consultas externas
			update tbHospitalizacionConsultaExterna
			set status = 0,
			    des_campo1 = _id
			where id = (select id  from tbHospitalizacionConsultaExterna where id_hospitalizacion = _id_consulta
			              and paciente = _paciente and status = 1 limit 1 );             
 
	/*Fin de transaccion*/ 
COMMIT; 


/*Mandamos 0 si todo salio bien*/ 
SELECT 0 as error;    


end
// delimiter //