 delimiter // 
create procedure spConsultarHorarioMedicoDetalles ()
-- spConsultarHorarioMedicoDetalles
begin 
select 
		concat_ws(' ',m.apellidos , m.nombres )'NOMBRE_MEDICO',
		e.descripcion 'ESPECIALIZACION',
		-- LUNES
        case when status_fecha1 = 1 then concat(convert(fecha_desde1,time), ' -- ' ,convert(fecha_hasta1,time))   
		else '' end as 'LUNES' ,
        -- martes
        case when status_fecha2 = 1 then concat(convert(fecha_desde2,time), ' -- ' ,convert(fecha_hasta2,time)   )   
		else '' end as 'MARTES',
        
        -- MIERCOLES
		case when status_fecha3 = 1 then concat(convert(fecha_desde3,time), ' -- ' ,convert(fecha_hasta3,time)   )   
		else '' end as 'Miercoles',
		
         -- JUEVES
		case when status_fecha4 = 1 then concat(convert(fecha_desde4,time), ' -- ' ,convert(fecha_hasta4,time)   )   
		else '' end as 'Jueves',
        
        -- VIERNES 
        	case when status_fecha5 = 1 then concat(convert(fecha_desde5,time), ' -- ' ,convert(fecha_hasta5,time)   )   
		else '' end as 'Viernes',
		
         -- Sabado 
        	case when status_fecha6 = 1 then concat(convert(fecha_desde6,time), ' -- ' ,convert(fecha_hasta6,time)   )   
		else '' end as 'Sabado',
        
         -- Domingo
        	case when status_fecha7 = 1 then concat(convert(fecha_desde7,time), ' -- ' ,convert(fecha_hasta7,time)   )   
		else '' end as 'Domingo'
		  from tbHorariosMedicos h, tbMedico m, tbEspecializacion e
		  where h.medico = m.id 
		  AND e.codigo = m.especializacion
		 
		   order by m.apellidos;


end
// delimiter //