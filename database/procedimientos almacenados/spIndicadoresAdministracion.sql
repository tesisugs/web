delimiter //
create procedure spIndicadoresAdministracion ()
begin
select
( select count(id) as pacientes from tbpaciente p where p.fecha_ingreso = curdate() ) as pacientes ,
( select count(id) as emergencias  from tb1registroadmision  where fecha_ingreso = curdate()) as emergencias ,
( select count(id) as hospitalizacion  from tbhospitalizacion  where fecha_registro = curdate() ) as hospitalizacion ,
( select count(id) as egresos  from tbegresos  where fecha_ingreso1 = curdate() ) as egresos,
( select count(id) as CE from tbingresopacienteconsultaexterna  where fecha_registro_paciente = curdate() )as CExterna ;
end 
// delimiter 
