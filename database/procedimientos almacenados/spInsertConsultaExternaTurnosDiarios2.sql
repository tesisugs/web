CREATE DEFINER=`root`@`localhost` PROCEDURE `spInsertConsultaExternaTurnosDiarios2`(
out _turnos_diarios int ,
in _consulta_externa int ,
in _paciente nvarchar(50),
in _cedula_paciente nvarchar(50),
out _turnos int ,
in _medico int,
in _observacion nvarchar(50),
in _fecha_registro_paciente date,
in _hora_registro_paciente nvarchar(50),
in _des_campo1 nvarchar(50),
in _des_campo2 nvarchar(50),
in _des_campo3 int ,
in _usuario_ingreso int ,
in _usuario_modificacion int,
in _pcname nvarchar(50),
in _status int,
in _titular_representante int
)
begin
	 declare fechaini  date;
    declare fechafin  date;
	  
	 set fechaini  :=  _fecha_registro_paciente;
     set fechafin := _fecha_registro_paciente;
     
	 SET _turnos   := ( SELECT COUNT(*) FROM tbConsultaExternaTurnosDiarios  -- tbIngresoPacienteConsultaExterna 
		 where  fecha_registro_paciente between fechaini and fechafin 
		 and status = 1 		and   medico    = _medico );
         
        -- if(_turnos < 1) then 
        -- set _turnos := _turnos + 1;
        -- end if;

-- /// llevamos la transaccion 
  -- 0925973968 peña dr guerra 
-- declare  @@fecha AS date = GETDATE ()
-- ------------------------------------
INSERT INTO tbConsultaExternaTurnosDiarios
		(
consulta_externa,
paciente,
cedula_paciente,
turnos,
medico,
observacion,
fecha_registro_paciente,
hora_registro_paciente,
des_campo1,
des_campo2,
des_campo3,
usuario_ingreso,
fecha_ingreso,
usuario_modificacion,
fecha_modificacion,
pcname,
status,
titular_representante)
		VALUES (_consulta_externa,
				_paciente,
				_cedula_paciente ,
				_turnos  ,
				_medico ,
				_observacion  ,
				_fecha_registro_paciente ,
				_hora_registro_paciente ,
				_des_campo1 ,
				_des_campo2 ,
				_des_campo3  ,
				_usuario_ingreso  ,
				curdate()  ,
				_usuario_modificacion ,
				curdate() ,
				_pcname ,
				_status ,
				_titular_representante);
				
                select _turnos;
	
  
  select _turnos;
end