delimiter //
create PROCEDURE SpConsultarAgendaMedicoFechas
-- SpConsultarIngresosPacientesConsultaExternaMEdicos
-- SpConsultarIngresosPacientesConsultaExternaMEdicos '51'
(
in _medico varchar(100),
in _fecha date
)
begin 
-- SpConsultarAgendaMedicoFechas '192','27/04/2017'
-- SpConsultarAgendaMedicoTurnos '9'
SELECT COUNT(*)
         FROM tbIngresoPacienteConsultaExterna 
    where fecha_registro_paciente = _fecha
  and status = 1
     and medico = _medico;
     
     SELECT consulta_externa ,fecha_registro_paciente, hora_registro_paciente,des_campo1
         FROM tbConsultaExternaTurnosDiarios  
    where fecha_registro_paciente = _fecha
     and status <> 3
     and medico = _medico order by consulta_externa desc
     limit 1;
end 
// delimiter //     
     
