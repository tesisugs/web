create procedure spConsultarMedicosLLamados2 (
in _fechaini datetime,
in _fechafin datetime
)
-- spConsultarMedicosLLamados '2017/16/11', '2017/17/11'
select e.id,
		p.apellido_paterno +' '+ p.apellido_materno +' '+ p.primer_nombre +' '+ p.segundo_nombre as 'PACIENTE',
		m.apellidos +' '+ m.nombres as 'MEDICO',
		es.descripcion as 'ESPECIALIDAD',
		CONVERT(e.fecha_modificacion,date) as 'FECHA',
		  CONVERT ( CONVERT  (e.fecha_modificacion, time),char(5) ) as 'HORA' ,   
		i.descripcion as 'FINANCIADOR',
		e.respuesta AS 'RESPUESTA',
		e.observacion AS 'DESCRIPCION',
		e.correo AS 'CORREO',
		s.name as 'USUARIO',
		case when e.respuesta = '1' then 
		'SI' ELSE 'NO' END as 'RESPUESTA_2'
		 from  tbEnvioCorreoMedicos e, tbPaciente p, 
		 tbMedico m, tbTipoIngreso i, users s , tbEspecializacion es
		 where e.paciente = p.id
		 and e.medico = m.id
		 and e.tipo_seguro = i.codigo
		 AND e.usuario_ingreso = s.codigo
		 AND m.especializacion = es.codigo
		 and convert(e.fecha_ingreso,date) between convert(_fechaini,date) and convert(_fechafin,date)
