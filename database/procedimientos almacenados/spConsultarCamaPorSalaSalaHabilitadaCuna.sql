create procedure spConsultarCamaPorSalaSalaHabilitadaCunas
(
in _sala int,
in _descripcion_camas int
)
SELECT COUNT(*) as 'total_camas'
   FROM tbCamasPorSala 
where sala =_sala  
and  status_camas  = 1
and descripcion_camas  = _descripcion_camas;  

-- spConsultarCamaPorSalaSalaHabilitadaCunas '1','1'
