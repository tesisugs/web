 delimiter //
 create procedure spConsultarMedicosLlamadoIngresoHospitalario( in _paciente int , in _medico int , in  _fecha varchar(10) )
 begin
 select respuesta from tbenviocorreomedicos where paciente = _paciente and medico = _medico and 
 fecha_ingreso between str_to_date(_fecha,'%Y-%m-%d') and  DATE_ADD(str_to_date(_fecha,'%Y-%m-%d'), INTERVAL 1 DAY)
  order by id desc limit 1; 
end  
// delimiter //