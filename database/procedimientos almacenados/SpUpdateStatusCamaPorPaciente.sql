
create PROCEDURE SpUpdateStatusCamaPorPaciente (
in _codigo NUMERIC,
in _status_camas nvarchar(50),
in _ocupante nvarchar(50)
)
UPDATE	tbCamasPorSala
		SET		status_camas =_status_camas  
				,ocupante =_ocupante 												
		WHERE	codigo = _codigo
