create PROCEDURE SpConsultarIngresosPacientesTurnos (
-- SpConsultarIngresosPacientesTurnos '','%','2014-05-02','2014-05-02'
in _paciente nvarchar(50),
in _medico nvarchar(2),
in _desde date,
in _hasta date
)
  SELECT c .turnos_diarios  ,
         c.paciente ,
         c.medico ,
         c.turnos ,
        -- c.nombre_representante ,
        concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre ,p.segundo_nombre ) as DesPaciente,
         (select concat_ws(' ',apellidos , nombres ) as Nombres from tbMedico where id = c.medico ) nombre_medico,
           c.fecha_registro_paciente  ,
            c.hora_registro_paciente 
         FROM tbConsultaExternaTurnosDiarios  c, tbPaciente p
     where c.paciente = p.id 
     AND concat_ws(' ',p.apellido_paterno,p.apellido_materno , p.primer_nombre ,p.segundo_nombre) LIKE concat('%',_paciente,'%') 
     AND  CONVERT(c.medico,CHAR(2)) LIKE _medico
     and c.fecha_registro_paciente between _desde and _hasta 
     and c.status = 1 order by c.turnos   
     -- select * from tbConsultaExternaTurnosDiarios 
     -- select * from tbMedico 