delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE ` spConsultarEgresosRangos`(
	IN `fecha1` varchar(15) ,
	IN `fecha2` varchar(15)
)
begin
SELECT count(id) as numero_ingreso FROM tbegresos
where fecha_ingreso >= date_format(fecha1,'%Y-%m-%e') and fecha_ingreso<= date_format(fecha2,'%Y-%m-%e');
end
// delimiter //