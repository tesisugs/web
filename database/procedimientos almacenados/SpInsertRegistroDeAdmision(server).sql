CREATE DEFINER=`root`@`localhost` PROCEDURE `SpInsertRegistroDeAdmision`(
out _id int ,
in _afiliado int,
in _paciente int,
in _fecha datetime,
in _hora varchar(7),
in _responsable varchar(50),
in _observacion varchar(500),
in _doctor  int,
in _usuario_ingreso int,
in _pcname varchar(50),
in _tipo_ingreso int,
out _numero_atencion int,
in _campo3 varchar(100),
in _fuente_informacion varchar(50) ,
in _persona_entrega varchar(50) ,
in _derivacion bit ,
in _hospital int,
in _cedula_persona_entrega varchar(13) ,
in _tipo_acompañante varchar(5) 
)
begin 
-- select * from  dbo.tb1RegistroAdmision
-- declare @Fecha_registro as date = getdate()
set _numero_atencion := (select  count(*) From tb1RegistroAdmision where paciente = _paciente) + 1;
-- set _numero_atencion := (numero_atencion +1 );
	     -- set @k2 = @k + 1	;
-- declare  @@fecha as date = getdate()
-- declare  fecha as date =  convert(date,now());
-- declare  hora as varchar(5) = CONVERT (nvarchar(5), CONVERT  (time, now()));
insert into tb1RegistroAdmision (
  numero_atencion,
  afiliado ,
  paciente ,
  fecha ,
  hora ,
  responsable ,
  observacion ,
  doctor ,
  des_campo1 ,
  des_campo2 ,
  des_campo3 ,
  usuario_ingreso,
  fecha_ingreso,
  usuario_modificacion,
  fecha_modificacion,
  pcname,
  status,
  tipo_ingreso,
  fuente_informacion,
  persona_entrega,
  derivacion,
  hospital,
  cedula_persona_entrega,
  tipo_acompañante
 )
  values ( _numero_atencion, -- Secuencia atencion 
           _afiliado, -- Id del responsable afiliado
           _paciente, -- Id de Paciente 
           convert(curdate(),date),
           CONVERT (CONVERT (now(), time),char(8)) ,
           _responsable ,
           _observacion, 
           _doctor ,
           _campo3,
           1,
           '.',
           _usuario_ingreso ,
            now(),
           _usuario_ingreso,
           now(),
           _pcname ,
           1    ,_tipo_ingreso,
           _fuente_informacion,
           _persona_entrega,
           _derivacion,
           _hospital,
           _cedula_persona_entrega,
           _tipo_acompañante
           );
set _id := (select  id From tb1RegistroAdmision order by id desc limit 1);      
select _id;
select _numero_atencion;
end