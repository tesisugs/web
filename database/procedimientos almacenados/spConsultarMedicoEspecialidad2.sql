create  procedure spConsultarMedicoEspecialidad2 (
in _especialidad int
)
SELECT h.medico 'id' , concat_ws(' ',m.apellidos ,m.nombres  ) as NombreCompletos  ,numero_atencion 
   FROM tbHorariosMedicos h , tbMedico m
   
where h.medico = m.id 
 and especializacion = _especialidad
order by NombreCompletos 
