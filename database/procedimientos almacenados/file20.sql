USE [MajomaControlHospitalario]
GO
/****** Object:  StoredProcedure [dbo].[spConsultarPacienteHospitalizacionEgresado]    Script Date: 4/6/2018 18:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spConsultarPacienteHospitalizacionEgresado]
@paciente as int
-- spConsultarPacienteHospitalizacionEgresado 136645
as
declare @w_fecha1 as smalldatetime, @w_fecha2 as smalldatetime, @w_asistencia as varchar(30)
  set @w_fecha1 = GETDATE() - 15
  set @w_fecha2 = GETDATE()
  
if not exists (select top 1 id from tbHospitalizacionConsultaExterna where paciente = @paciente)
   begin
                    
      
     select tb1.id,
           '1' 'Consulta No',
           --@w_asistencia  Asistencia,
           case when ( select top 1 tb4.medico from tbIngresoPacienteConsultaExterna tb4  where tb4.paciente = @paciente
                        and  tb4.fecha_registro_paciente between  tb1.fecha_egreso + 1 and 
                          @w_fecha2 and tb4.status = 1 and tb4.tipo_ingreso = 1 ) is null then ' ' 
                           else 'YA ASISTIO A LA CONSULTA' end Asistencia,
           tb2.apellido_paterno + ' ' +  tb2.apellido_materno + ' ' + tb2.primer_nombre + ' ' + tb2.segundo_nombre Paciente,
           tb1.medico Id_medico,
           (select tb3.apellidos + ' ' + tb3.nombres from tbMedico tb3 where tb3.id = tb1.medico and tb3.status = 1) Medico,
           tb1.fecha_egreso FechaEgresoHosp,
           tb1.fecha_egreso + 15 FechaLimiteConsulta
           
     from tbHospitalizacion tb1, tbPaciente tb2
     where tb1.Paciente = @paciente 
         -- AND tb1.id = (SELECT MAX(ID) FROM tbHospitalizacion WHERE Paciente = @paciente AND status = 2  )
         AND tb1.id = (SELECT MAX(ID) FROM tbHospitalizacion WHERE Paciente = @paciente AND status = 2  and Procedencias = 1  and 
                fecha_egreso between (@w_fecha2  - 40) and   @w_fecha2)
         and   tb1.paciente = tb2.id
          and tb1.Procedencias = 1
          and tb1.status = 2
       --   and tb1.Cirugia_Dia = 0
    union all
    /*
       select  tb1.id,
             '2' 'Consulta No',
             case when ( select top 1 tb4.medico from tbIngresoPacienteConsultaExterna tb4  where tb4.paciente = @paciente
                        and  tb4.fecha_registro_paciente between  (tb1.fecha_egreso + 15) and 
                            @w_fecha2 and tb4.status = 1) is null then ' ' 
                           else 'YA ASISTIO A LA CONSULTA' end Asistencia,
              tb2.apellido_paterno + ' ' +  tb2.apellido_materno + ' ' + tb2.primer_nombre + ' ' + tb2.segundo_nombre Paciente,
              tb1.medico Id_medico,
              (select tb5.apellidos + ' ' + tb5.nombres from tbMedico tb5 where tb5.id = tb1.medico and tb5.status = 1) Medico,
              tb1.fecha_egreso FechaEgresoHosp,
              tb1.fecha_egreso + 30 FechaLimiteConsulta
              
       from tbHospitalizacion tb1,tbPaciente tb2, ControlHospitalario.dbo.tbCirugiasProgramadas tb3
       where tb1.paciente = @paciente
       and tb1.Paciente = tb3.id_paciente
       and tb3.horaRealInicio between tb1.fecha_registro and tb1.fecha_egreso
       and tb1.Paciente = tb2.id
         and tb1.Procedencias = 1
     --  and tb1.Cirugia_Dia = 0   
     */
     select tb1.id,
            '2' 'Consulta No',
            case when ( select top 1 tb4.medico from tbIngresoPacienteConsultaExterna tb4  where tb4.paciente = @paciente
                        and  tb4.fecha_registro_paciente between  (tb1.fecha_egreso + 15) and 
                            @w_fecha2 and tb4.status = 1) is null then ' ' 
                           else 'YA ASISTIO A LA CONSULTA' end Asistencia,
            tb2.apellido_paterno + ' ' +  tb2.apellido_materno + ' ' + tb2.primer_nombre + ' ' + tb2.segundo_nombre Paciente,
            tb1.medico Id_medico,
            (select tb5.apellidos + ' ' + tb5.nombres from tbMedico tb5 where tb5.id = tb1.medico and tb5.status = 1) Medico,
                          tb1.fecha_egreso FechaEgresoHosp,
              tb1.fecha_egreso + 30 FechaLimiteConsulta
            
            
     from tbHospitalizacion tb1,tbPaciente tb2, Administracion.dbo.Requisiciones_Pacientes tb3
     where tb1.Paciente = @paciente
     and tb1.Paciente = tb2.id
     and tb1.Procedencias = 1
     and tb3.CliCodigo = ( case when (select clicodigo from Administracion.dbo.Clientes 
      where Administracion.dbo.Clientes.CliNombre like  tb2.apellido_paterno + ' ' + tb2.apellido_materno + ' '+ tb2.primer_nombre + ' ' + tb2.segundo_nombre + '%'  
      and Administracion.dbo.Clientes.CliTercerizadora not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR')) IS null then tb2.cedula else ( select top 1  clicodigo from Administracion.dbo.Clientes 
      where Administracion.dbo.Clientes.CliNombre like  tb2.apellido_paterno + ' ' + tb2.apellido_materno + ' '+ tb2.primer_nombre + ' ' + tb2.segundo_nombre + '%'  
      and Administracion.dbo.Clientes.CliTercerizadora not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR'))  end )
        
              
   end
 else  
   begin
      select tb1.id_hospitalizacion id,
             tb1.numero_consulta 'Consulta No',
             case when tb1.status = 0 then 'YA ASISTIO' else ' ' end Asistencia,
             tb3.apellido_paterno + ' ' + tb3.apellido_materno + ' ' + tb3.primer_nombre + ' ' + tb3.segundo_nombre Paciente,
             tb2.medico Id_medico,
             (select tb4.apellidos + ' ' + tb4.nombres from tbMedico tb4 where tb4.id = tb2.medico and tb4.status = 1) Medico,
             tb2.fecha_egreso FechaEgresoHosp,
             tb1.fecha_limite FechaLimiteConsulta
      from tbHospitalizacionConsultaExterna tb1, tbHospitalizacion tb2, tbPaciente tb3
      where tb1.paciente = @paciente 
            and tb1.id_hospitalizacion = (SELECT MAX(ID) FROM tbHospitalizacion WHERE Paciente = @paciente AND status = 2  
           -- and Procedencias = 1 
             and  Procedencias in (1, 2,4,5,30,31,53)
            and  fecha_egreso between (@w_fecha2  - 40) and   @w_fecha2)
  --    and tb1.status = 1
      and tb1.id_hospitalizacion = tb2.id
      and tb1.paciente = tb3.id
   --   and tb2.Normal = 1
      
   
   end
   



