delimiter //
create PROCEDURE SpConsultarIngresosPacientesConsultaExternaNumeroAtencionMedicos
-- SpConsultarIngresosPacientesConsultaExternaNumeroAtencionMedicos '1'
( in _medico varchar(100)
)
begin
declare desde  date;
declare hasta  date;

set desde := curdate();
set hasta := curdate(); 
      SELECT c .id ,
         c.fecha_registro_paciente  ,
       c.hora_registro_paciente 
      FROM tbIngresoPacienteConsultaExterna c, tbMedico  m
           where c.medico  = m.id 
       AND  CONVERT(c.medico,CHAR(100) ) LIKE _medico
     and c.fecha_registro_paciente between desde   and hasta;
     -- if @@NumeroAtencion <= 12
      
   --  select * from tbPaciente 	
end 
   // delimiter //