delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarCePeriodosAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)
)
begin
select *from tbConsultaExternaTurnosDiarios where fecha_registro_paciente between concat(desde,'/01/01') and concat(hasta,'/12/31') order by fecha_registro_paciente desc;
end
// delimiter //