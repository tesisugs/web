drop procedure spDatosUsuario;
delimiter //
create procedure spDatosUsuario(in _id int)
begin 
select u.id as id, u.nombres as nombres, u.apellidos as apellidos, u.name as usuario , ru.role_id as rol , di.ip as ip ,di.departamento_id as departamento, u.created_at as fecha
from users u 
join role_user ru on u.id = ru.user_id 
join tbdireccionesip di on u.id = di.usuario_id 
where u.id =  _id; 
end 
// delimiter //