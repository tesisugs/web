
delimiter //
create PROCEDURE SpConsultarIngresosPacientesConsultaExternaPreparacionForm005_1
-- SpConsultarIngresosPacientesConsultaExternaPreparacionForm005_1 '47340'
(in id nvarchar(100))
begin
  SELECT c .id ,
         c.paciente ,
         c.medico ,
         c.nombre_representante ,
         concat_ws(' ',p.apellido_paterno , p.apellido_materno ,p.primer_nombre , p.segundo_nombre ) AS DesPaciente,
         (select concat(apellidos, ' ',nombres ) as Nombres from tbMedico where id = c.medico ) nombre_medico,
           c.fecha_registro_paciente  ,
            c.hora_registro_paciente ,
            (select temperatura  from tbConsultaExternaPreparacion where consulta_externa  = c.id)temperatura,
            (select pulso   from tbConsultaExternaPreparacion where consulta_externa  = c.id)pulso,
            (select presion_arterial  from tbConsultaExternaPreparacion where consulta_externa  = c.id)presion_arterial,
            (select respiracion   from tbConsultaExternaPreparacion where consulta_externa  = c.id)respiracion,
            (select peso   from tbConsultaExternaPreparacion where consulta_externa  = c.id)peso,
            (select estatura   from tbConsultaExternaPreparacion where consulta_externa  = c.id)estatura,
            (select talla   from tbConsultaExternaPreparacion where consulta_externa  = c.id)talla,
            (select descripcion  from tbDependencias where codigo  = c.dependencias ) dependencias,
            (select observacion  from tbConsultaExternaDiagnosticos where consulta_externa  = c.id limit 1 ) des_1,
            (select  des_campo2  from tbConsultaExternaDiagnosticos where consulta_externa  = c.id limit 1 ) des_2,
            (select tce.descripcion from tbTipoConsultaExterna tce where tce.codigo=(select cet.tipo_consulta from tbConsultaExternaTipoConsulta cet where cet.consulta_externa=( select cep.id from tbIngresoPacienteConsultaExterna cep where cep.id=_id)))tipo_consulta
            
         FROM tbIngresoPacienteConsultaExterna c, tbPaciente p 
     where c.paciente = p.id 
     and c.id = _id
     and c.status = 1 order by c.fecha_ingreso desc;

select * from dbo.tbConsultaExternaEvolucionPrescripcionLaboratorio where consulta_externa=_id;
select * from dbo.tbConsultaExternaEvolucionRayox where consulta_externa=_id;
end
// delimiter //