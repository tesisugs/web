
create PROCEDURE SpUpdateConsultaExternaDiagnostico
(
in _id nvarchar(50),
in _observacion nvarchar(50),
in _principal NVARCHAR(50),
in _asociado_1 NVARCHAR(50),
in _asociado_2 NVARCHAR(50),
in _usuario_modificacion int ,
in _pcname NVARCHAR(50)
)

UPDATE	tbIngresoPacienteConsultaExterna  
		SET		observacion  = _observacion ,
				principal= _principal,
		        asociado_1=_asociado_1,
		        asociado_2=_asociado_2 ,
			    usuario_modificacion=_usuario_modificacion,
				fecha_modificacion=curdate(),
				pcname=_pcname 	
		WHERE	id   = _id 
		
        
