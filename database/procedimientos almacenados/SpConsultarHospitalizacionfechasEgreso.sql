delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarHospitalizacionfechasEgreso`(

in _paciente varchar(50),
in _desde  date,
in _hasta  date
-- @sala nvarchar(50)
)
begin
 -- select * from tbStatusHospitalizacion
SELECT h.id,
		concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre , p.segundo_nombre) AS PACIENTE,
           h.genero as GENERO,
       h.fecha_registro as 'FECHA_INGRESO',
             i.descripcion as   'TIPO_DE_INGRESO',
            (select descripcion from tbStatusHospitalizacion where id =	h.status ) ESTADO
      FROM tbHospitalizacion  h,
           tbpaciente p , tbTipoIngreso i 
       
 WHERE h.paciente=p.id
  and i.codigo = h.Procedencias 
 AND  h.status in(1,3) 
 and h.Fecha_de_Registro between _desde  AND _hasta
    AND concat_ws(' ',p.apellido_paterno ,p.apellido_materno)  LIKE concat('%',_paciente,'%');
end
// delimiter //