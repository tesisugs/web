delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarPacientesYearAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)
)
begin
select *from tbpaciente  where date_format(fecha_registro,'%Y') = desde or date_format(fecha_registro,'%Y') = hasta order by fecha_registro desc;
end
// delimiter //