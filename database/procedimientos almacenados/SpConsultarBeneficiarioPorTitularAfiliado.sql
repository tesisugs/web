create procedure SpConsultarBeneficiarioPorTitularAfiliado (
in _cedula  varchar(10)
)
// 0915909014
seleCT id                 'Id',
       cedula             'Cedula',
       concat_ws(' ',apellido_paterno,apellido_materno,primer_nombre,segundo_nombre) as 'Nombres',
       Genero             'Genero',
       tipo_parentesco    'Parentesco',
       tipo_beneficiario  'Tipo Beneficiario',
       tipo_seguro        'Tipo Seguro'
FROM tbPaciente 
WHERE cedula_titular=_cedula
and status = 1