delimiter //
create procedure SpInsertFirmaPorAtencion
-- TIPO SERVICIO = CONSULTA EXT -EMERG -HOS TBCONSULTAARBOL
-- TIPO DOCUMENTO = tbDocumentos, Lab,Rx si va porque Carlitos recien explica como es el proceso de hospitalizacion
-- ID ATENCION = el id de la consulta, emerg, consulat, hospita
(
out _id  INT ,
in _tipo_servicio INT,
in _id_atencion int,
in _id_visita int,
in _id_tipo_documento int,
in _fecha_atencion datetime,
in _firma  blob,
in _status int,
in _usuario_ingreso int,
in _usuario_modificacion int,
in _pcname VARCHAR(50)
)
-- select * from tbdocumentoporpaciente
begin
insert into MajomaImagenes.tbFirmasPorAtencion
(
`tipo_servicio`,
`id_atencion`,
`id_visita`,
`id_tipo_documento`,
`fecha_atencion`,
`firma`,
`status`,
`usuario_ingreso`,
`fecha_ingreso`,
`usuario_modificacion`,
`fecha_modificacion`,
`pcname`)
values(_tipo_servicio,
       _id_atencion,
       _id_visita,  
       _id_tipo_documento,     
       _fecha_atencion,
       _firma,
       _status,
       _usuario_ingreso,
       curdate(),
       _usuario_modificacion,
       curdate(),
       _pcname);
select id from MajomaImagenes.tbFirmasPorAtencion order by id desc limit 1;

end

// delimiter //