delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarCeRangoAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)
)

begin
select *from tbConsultaExternaTurnosDiarios  where fecha_registro_paciente between desde and hasta order by fecha_registro_paciente desc;
end
// delimiter //