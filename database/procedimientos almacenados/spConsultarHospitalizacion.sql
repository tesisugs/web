delimiter //
create procedure spConsultarHospitalizacion(in _id int)
begin
select dp.descripcion as principal , da1.descripcion as diagnostico1 ,da2.descripcion as diagnostico2 , concat_ws(' ',m.nombres,m.apellidos) as medico,
h.id , h.SecuenciaHO , h.No_Historial , h.Fecha_de_Registro , h.codigo_cama  , h.Paciente
from tbhospitalizacion h
join tbdiagnostico dp on dp.codigo = h.principal
join tbdiagnostico da1 on da1.codigo = h.asociado_1
join tbdiagnostico da2 on da2.codigo= h.asociado_2
join tbmedico m on m.id = h.medico
where h.id = _id;
end 
// delimiter //