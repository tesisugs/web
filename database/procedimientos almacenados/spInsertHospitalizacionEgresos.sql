delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spInsertHospitalizacionEgresos`(
out _id int ,

in _id_hospitalizacion  int,
in _medico_egreso int,
in _principal_egreso NVARCHAR(50),
in _asociadoEgreso1 NVARCHAR(50),
in _asociadoEgreso2 NVARCHAR(50),
in _tipo_egreso int,
in _complicacion bit,
in _tipo_complicacion int,
in _fecha_ingreso datetime,
in _fecha_salida datetime ,
in _hora_ingreso nvarchar(50),
in _hora_salida nvarchar(50),

in _tipo_egreso_alta int,

in _codigo_cama int,
in _status_camas nvarchar(50),

in _Procedencias nvarchar(50),
in _dias_transcurridos nvarchar(50),
in _horas_transcurridas nvarchar(50),
in _valor nvarchar(50),
in _des_campo1            nvarchar(50),
in _des_campo2            nvarchar(50),
in _des_campo3            decimal(10,2),
in _usuario_ingreso       int,
in _pcname                nvarchar(50),
in _status                int,
in _observacion           varchar(250)
)
begin 
declare w_fecha_ingreso datetime; 
declare w_fecha_egreso datetime; 
declare w_paciente int; 
declare w_clicodigo varchar(15);
declare w_nombre varchar(250); 
declare w_status int;
/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLWARNING 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

START TRANSACTION; 

	set w_paciente := (select paciente from  tbHospitalizacion where id = _id_hospitalizacion);
    set w_fecha_ingreso := (select Fecha_de_Registro from  tbHospitalizacion where id = _id_hospitalizacion);
    set w_status := (select status from  tbHospitalizacion where id = _id_hospitalizacion);
	
        
if (w_status <> 2) then 

	 iNSERT INTO tbEgresos 
		VALUES (_id_hospitalizacion,
				_fecha_ingreso ,
				curdate(),
				_hora_ingreso ,
				_hora_salida ,
				_codigo_cama ,
				_Procedencias ,
				_dias_transcurridos ,
				_horas_transcurridas,
				_valor, 
				_des_campo1,
				_des_campo2,
				_des_campo3,
				_usuario_ingreso,
				curdate(),
				_usuario_ingreso,
				curdate(),
				_pcname,
				_status ,
				_observacion);
                
				set @id = @@IDENTITY;
                
                
                UPDATE	tbHospitalizacion
				SET	fecha_egreso=curdate()  
				,medico_egreso = _medico_egreso 
				,principal_egreso=_principal_egreso 
				,asociadoEgreso1=_asociadoEgreso1 
				,asociadoEgreso2=_asociadoEgreso2 
				,tipo_egreso =_tipo_egreso 
				,complicacion=_complicacion 
				,tipo_complicacion=_tipo_complicacion 
		     	,fecha_modificacion =curdate()
				,usuario_modificacion =_usuario_ingreso 
				,pcname=_pcname
				,status= 2 
				,tipo_egreso_alta=_tipo_egreso_alta
				WHERE	id = _id_hospitalizacion;
                
                 UPDATE	tbCamasPorSala
				 SET	status_camas =  3
				,ocupante ='.'
				WHERE	codigo = _codigo_cama;
                
                 if _Procedencias in ('1','4','5','30','31','53') then 
					
                    set w_clicodigo := (select cedula
				   from tbRelacionPacienteCliente
				   where paciente = w_paciente);
                    
	        	 
					set w_nombre := ( select concat_ws(' ',apellido_paterno ,apellido_materno ,primer_nombre,segundo_nombre) 
					from tbPaciente where id = w_paciente ); 
                    
					insert into tbHospitalizacionConsultaExterna(
						  id_hospitalizacion, 
						numero_consulta, 
						  paciente, 
						  tipo_hospitalizacion, 
						fecha_limite,
						des_campo1,
						des_campo3,
						usuario_ingreso,
						fecha_ingreso,
						pcname,
						status)	
							values(_id_hospitalizacion,
									1,
									w_paciente,
									
						case when( (select e.clicodigo from Administracion.Requisiciones_Pacientes e where e.CliCodigo = 
							( case when (select clicodigo from Administracion.Clientes   where Administracion.Clientes.CliNombre like concat(w_nombre,'%')  
										  and Administracion.Clientes.CliTercerizadora 
										  not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR'))
								IS null  then w_clicodigo
								else ( select  clicodigo from Administracion.Clientes  where Administracion.Clientes.CliNombre like concat(_w_nombre ,'%') 
												   and Administracion.Clientes.CliTercerizadora 
												   not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR')limit 1)  end ) 
									and e.Final between _w_fecha_ingreso and curdate() limit 1   ) ) 
												   IS null then 1 else 2 end  ,
						curdate() + 15,   
						_des_campo1,
						_des_campo3,
						_usuario_ingreso,
						curdate(),_pcname,1
										);
             end if;                           
			/*	if exists
				 (select   e.CliCodigo from Administracion.Requisiciones_Pacientes e where e.CliCodigo = 
                    ( case when 
						(select clicodigo from Administracion.Clientes 
							 where Administracion.Clientes.CliNombre like concat(w_nombre ,'%') 
							  and Administracion.Clientes.CliTercerizadora not in 
							  ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 
                              'FAMIHOSPITAL', 'FAMIESCUELA', 'FAMIHOGAR')) 
							  IS null  then w_clicodigo
							 else ( select   clicodigo from Administracion.Clientes  where Administracion.Clientes.CliNombre like concat(w_nombre,'%') 
								   and Administracion.Clientes.CliTercerizadora 
								   not in ('FAMIGUARDERIA','EMPLEADOS REEMPLACISTAS','FAMIHOSPEVENTUAL','ESCUELA EMPLEADOS','EMPLEADOS', 'HOGAR EMPLEADOS', 'FAMIHOSPITAL', 
											'FAMIESCUELA', 'FAMIHOGAR') limit 1) 
							end ) and e.Final between w_fecha_ingreso and curdate() ) then            
		  
							 insert into tbHospitalizacionConsultaExterna(id_hospitalizacion, numero_consulta, paciente, tipo_hospitalizacion, fecha_limite,
													 des_campo1,des_campo3,usuario_ingreso,fecha_ingreso,pcname,status)
							
							 values(_id_hospitalizacion,2,_w_paciente,2 ,curdate() + 30,   _des_campo1,_des_campo3,_usuario_ingreso,GETDATE(),_pcname,1);
				end if;               
                             
							
                 
                 end if; */

end if;        
			
	/*Fin de transaccion*/ 
COMMIT; 


/*Mandamos 0 si todo salio bien*/ 
SELECT 0 as error; 
		  
end
// delimiter //