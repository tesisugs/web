CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarIngresosPacientesAdmisionPorUsuario`(
-- SpConsultarIngresosPacientesAdmisionPorUsuario '25','21/4/2015','21/4/2015'
-- @usuario int
)
SELECT 
-- ( select usuario from MajomaSecurity.dbo.SEG_USUARIO where codigo = c.usuario_ingreso) USUARIO,
         -- c.paciente CodPaciente,
         -- c.afiliado  ,
         (select name from users where id = c.usuario_ingreso ) as Usuario,
         concat_ws(' ', p.apellido_paterno , p.apellido_materno , p.primer_nombre ,p.segundo_nombre) as Paciente,
         (select descripcion from tbTipoIngreso where codigo = c.tipo_ingreso )AS 'TIPO DE SEGURO' ,
         c.fecha as FECHA,
         c.hora as HORA  
         FROM tb1RegistroAdmision  c, tbPaciente p
     where -- c.paciente = p.id and 
     convert(c.fecha,date)  between CONVERT(curdate(),date) and CONVERT(curdate(),date) 
     and c.status = 1
     -- and c.usuario_ingreso =@usuario
     order by c.fecha,3,2