DELIMITER $$
USE `majomacontrolhospitalario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarTitularAfiliadoPorApellidoAll`(
in _apellido  nvarchar(50) )
begin
select * 
  from tbpaciente 
WHERE concat_ws('',apellido_paterno  , apellido_materno , primer_nombre ,  segundo_nombre ) LIKE concat('%',_apellido,'%')
  and status=1;

end$$

DELIMITER ;