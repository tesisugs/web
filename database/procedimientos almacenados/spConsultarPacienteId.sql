CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarPacienteId`(
-- spConsultarPacienteId '106521'
in _id varchar(50)
)
begin
		SELECT * 
		   FROM tbPaciente 
		where id  =_id
		and status = 1;

		select tb2.descripcion pais,
			   tb3.descripcion provincia,
			   tb4.descripcion ciudad,
			   tb5.descripcion etnia,
			   concat_ws(' ',tb1.apellido_paterno ,tb1.apellido_materno, tb1.primer_nombre , tb1.segundo_nombre ) as nombre
		from tbPaciente tb1, tbPais tb2, tbProvincia tb3, tbCiudad tb4 , tbTipoCultura tb5
		where tb1.id = _id
		and tb1.pais = tb2.codigo
		and tb1.provincia = tb3.codigo
		and tb1.ciudad = tb4.codigo
		and tb1.etnico = tb5.raza
		and tb1.status = 1;

		select *
		from tbConsultaExternaTurnosDiarios t
		where t.consulta_externa= _id;
end