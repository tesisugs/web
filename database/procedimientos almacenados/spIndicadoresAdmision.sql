delimiter //
create procedure spIndicadoresAdmision	()
begin
select
(select count(codigo) from tbcamasporsala where status_camas = 1 ) as habilitadas , 
(select count(codigo) from tbcamasporsala where status_camas = 2 ) as ocupada,
(select count(codigo) from tbcamasporsala where status_camas = 3 ) as desinfecta,
(select count(codigo) from tbcamasporsala where status_camas = 4 ) as dañada ;
end 
// delimiter 