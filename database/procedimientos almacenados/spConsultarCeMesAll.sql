delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarCeMesAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)

)
begin
select *from tbConsultaExternaTurnosDiarios where date_format(fecha_registro_paciente,'%Y-%m')   = date_format((str_to_date(desde, '%Y-%m-%d')),'%Y-%m') or 
date_format(fecha_registro_paciente ,'%Y-%m')   = date_format((str_to_date(hasta, '%Y-%m-%d')),'%Y-%m'); 
end
// delimiter //