delimiter //
create PROCEDURE SpConsultarAgendaMedicoTurnos2
(
-- SpConsultarIngresosPacientesConsultaExternaMEdicos
-- SpConsultarIngresosPacientesConsultaExternaMEdicos '51'
in _medico nvarchar(100)
)
-- SpConsultarAgendaMedicoTurnos '192'
-- SpConsultarAgendaMedicoTurnos '9'
begin 
SELECT c .turnos_diarios 'TURNOS DIARIOS',
C.consulta_externa  'ID',
  concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre , p.segundo_nombre )  AS 'PACIENTE',
        (select concat_ws(' ',apellido_paterno , apellido_materno , primer_nombre ,segundo_nombre ) from tbPaciente where id = c.titular_representante ) 'REPRESENTANTE',
		(select concat_ws(' ', apellidos , nombres ) as Nombres from tbMedico where id = c.medico ) 'MEDICO',
        'C' AS 'CONSULTORIO', 
       (select numero_camas  from tbCamasPorSala where codigo = (select consultorio_default from tbMedico where id = c.medico))'NUMERO_CONSULTORIO' ,
         c.turnos 'TURNO',
		c.status  'ESTADO'
         FROM tbConsultaExternaTurnosDiarios c, tbPaciente p
     where c.paciente = p.id 
       and CONVERT(c.fecha_ingreso,date) = CONVERT(curdate(),date)     
		and c.status <> 3
		and medico = _medico
		order by c.turnos;
   
end
// delimiter //