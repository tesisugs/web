create PROCEDURE  SpConsultarAgendaMedicoTurnos2
-- SpConsultarIngresosPacientesConsultaExternaMEdicos
-- SpConsultarIngresosPacientesConsultaExternaMEdicos '51'
( in _medico varchar(100) )
-- SpConsultarAgendaMedicoTurnos '192'
-- SpConsultarAgendaMedicoTurnos '9'
begin 
select c .turnos_diarios 'TURNOS DIARIOS',
C.consulta_externa  'ID', concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre ,p.segundo_nombre ) as paciente,
(select concat_ws(' ',apellido_paterno , apellido_materno , primer_nombre , segundo_nombre ) from tbPaciente where id = c.titular_representante ) 'REPRESENTANTE',
(select concat_ws(' ',apellidos , nombres ) as Nombres from tbMedico where id = c.medico ) 'MEDICO',
'C' AS 'CONSULTORIO', 
(select numero_camas  from tbCamasPorSala where codigo = (select consultorio_default from tbMedico where id = c.medico))'NUMERO_CONSULTORIO' ,
         c.turnos 'TURNO',
    c.status  'ESTADO'
         FROM tbConsultaExternaTurnosDiarios c, tbPaciente p
     where c.paciente = p.id 
     and CONVERT( c.fecha_ingreso,date) = CONVERT(getdate(),date)     
   and c.status <> 3
     and medico = _medico
     order by convert(c.turnos,signed) asc
end 
// delimiter //