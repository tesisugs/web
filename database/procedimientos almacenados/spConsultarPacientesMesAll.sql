delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarPacientesMesAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)

)
begin
select *from tbpaciente where date_format(fecha_ingreso,'%Y-%m')   = date_format((str_to_date(desde, '%Y-%m-%d')),'%Y-%m') or 
date_format(fecha_ingreso ,'%Y-%m')   = date_format((str_to_date(hasta, '%Y-%m-%d')),'%Y-%m'); 
end
// delimiter //