delimiter //
create PROCEDURE spInsertConsultaExternaEvolucionPrescripcion(
out _id int ,
in _consulta_externa int ,
in _paciente nvarchar(50),
in _titular int,
in _codigo_far nvarchar(50),
in _descripcion_far nvarchar(50),
in _linea_far nvarchar(50),
in _observacion nvarchar(50),
in _des_campo1 nvarchar(50),
in _des_campo2 nvarchar(50),
in _des_campo3 int ,
in _usuario_ingreso int ,
in _fecha_ingreso smalldatetime,
in _usuario_modificacion int,
in _fecha_modificacion smalldatetime,
in _pcname nvarchar(50),
in _status int,
in _numero int
)
-- --------------------
INSERT INTO tbConsultaExternaEvolucionPrescripcion
		(
		`consulta_externa`,
		`paciente`,
		`titular`,
		`codigo_far`,
		`descripcion_far`,
		`linea_far`,
		`observacion`,
		`des_campo1`,
		`des_campo2`,
		`des_campo3`,
		`usuario_ingreso`,
		`fecha_ingreso`,
		`usuario_modificacion`,
		`fecha_modificacion`,
		`pcname`,
		`status`,
		`numero`)
		VALUES (_consulta_externa ,
				_paciente	,
				_titular ,
				_codigo_far ,
				_descripcion_far ,
				_linea_far ,
				_observacion ,
				_des_campo1,
				_des_campo2 ,
				_des_campo3  ,
				_usuario_ingreso,
				curdate(),
				_usuario_modificacion ,
				curdate(),
				_pcname ,
				_status,
				_numero);
				select id from tbConsultaExternaEvolucionPrescripcion order by id desc limit 1;
end 
delimiter //                