CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarIngresosPacientesConsultaExternaPreparacionHoy`(
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
SELECT c .id ,
         c.paciente ,
         c.medico ,
         c.nombre_representante ,
         concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre ,p.segundo_nombre) AS DesPaciente,
         (select concat_ws(' ',apellidos , nombres )  as Nombres from tbMedico where id = c.medico ) as  nombre_medico,
           c.fecha_registro_paciente  ,
            c.hora_registro_paciente ,
            (select temperatura  from tbConsultaExternaPreparacion where consulta_externa  = c.id)temperatura,
            (select pulso   from tbConsultaExternaPreparacion where consulta_externa  = c.id)pulso,
            (select presion_arterial  from tbConsultaExternaPreparacion where consulta_externa  = c.id)presion_arterial,
            (select respiracion   from tbConsultaExternaPreparacion where consulta_externa  = c.id)respiracion,
            (select peso   from tbConsultaExternaPreparacion where consulta_externa  = c.id)peso,
            (select estatura   from tbConsultaExternaPreparacion where consulta_externa  = c.id)estatura,
            (select talla   from tbConsultaExternaPreparacion where consulta_externa  = c.id)talla,
            (select descripcion  from tbDependencias where codigo  = c.dependencias ) dependencias
         FROM tbIngresoPacienteConsultaExterna c, tbPaciente p 
     where c.paciente = p.id 
     and c.fecha_registro_paciente between curdate() and curdate() 
     and c.status = 1 order by c.fecha_ingreso   desc