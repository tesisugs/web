CREATE DEFINER=`root`@`localhost` PROCEDURE `spIndicadoresRol`(in rol int)
begin

IF rol = 1 THEN
   select 
 (select count(id) from tbhospitalizacion where fecha_registro = curdate()) as Ingresos;
ELSEIF rol =2 THEN
	select
(select count(codigo) from tbcamasporsala where status_camas = 1) as Habilitada, 
(select count(codigo) from tbcamasporsala where status_camas = 2) as ocupada, 
(select count(codigo) from tbcamasporsala where status_camas = 3) as desinfeccion, 
(select count(codigo) from tbcamasporsala where status_camas = 4) as averiada ;
ELSE
  select 0 as error;
END IF;
end