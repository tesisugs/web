delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarEmergenciasPeriodosAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)
)
begin
select *from tb1registroadmision where fecha_registro between concat(desde,'/01/01') and concat(hasta,'/12/31') order by fecha_registro desc;
end
// delimiter //