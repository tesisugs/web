drop procedure spUsuariosShow
delimiter //
create procedure spUsuariosShow(in _id int)
begin

select u.nombres as nombres, u.apellidos as apellido,u.name as name, u.created_at as fecha_creacion , u.updated_at as fecha_actualizacion , di.ip as ip ,di.departamento_id as departamento , ru.role_id as rol
from users u 
join tbdireccionesip di
on u.id = di.usuario_id
join role_user ru
on   ru.user_id = u.id
 where u.id= _id;
end 
// delimiter //