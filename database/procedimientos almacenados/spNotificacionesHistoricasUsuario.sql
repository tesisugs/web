delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spNotificacionesHistoricasUsuario`(
	IN `usuario` int
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
begin
select n.descripcion , n.titulo, n.fecha_ingreso, un.user_id , un.notification_id 
from tbuser_notification un
join tbnotificaciones n
on un.notification_id = n.id
where un.user_id = usuario;
end 
// delimiter //