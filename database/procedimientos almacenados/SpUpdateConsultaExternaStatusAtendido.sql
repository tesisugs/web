delimiter //
create PROCEDURE SpUpdateConsultaExternaStatusAtendido (
in _id nvarchar(50),
in _des_campo1 NVARCHAR(50),
in _usuario_modificacion int ,
in _pcname NVARCHAR(50)
) 
begin
UPDATE	dbo.tbIngresoPacienteConsultaExterna  
		SET		des_campo1 = _des_campo1 ,
			    usuario_modificacion=_usuario_modificacion,
				fecha_modificacion=curdate(),
				pcname=_pcname 	
		WHERE	id   = _id ;
end        
		-- select * from tbIngresoPacienteConsultaExterna 
// delimiter //