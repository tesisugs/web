delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarEgresosSemanasAll`(
	IN `fecha1` varchar(15),
	IN `fecha2` varchar(15)
)
begin
  SELECT * from tbegresos where date_format(fecha_ingreso1,'%U') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%U') and
			date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%Y')  
  union all
   SELECT * from tbegresos where date_format(fecha_ingreso1,'%U') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%U') and
			date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%Y');  
end
// delimiter //