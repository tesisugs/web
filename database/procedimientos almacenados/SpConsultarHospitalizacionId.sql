CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarHospitalizacionId`(in _id int )
begin 
  SELECT *  FROM tbHospitalizacion where id = _id;
-- SpConsultarHospitalizacionId 14320
-- SpConsultarHospitalizacionId 15430

  SELECT *
  ,(select s.descripcion from tbsala s where s.codigo=h.habitacion) DesSala
  ,(select c.nombre_cama from tbCamasPorSala c where c.codigo=h.codigo_cama) DesCama
  ,(select t.descripcion from tbTipoIngreso t where t.codigo=h.Procedencias) DesSeguro
    FROM tbHospitalizacion h where id = _id;
-- SpConsultarHospitalizacionId '197'_cama
-- select id from tbEpicrisisCuadroClinico where id_hospitalizacion = 15430
--  select *
-- from tbepicrisisdiagnostico
-- where epicrisis = (select id from tbEpicrisisCuadroClinico where id_hospitalizacion = @id)

select principal_egreso  + '    ' +( case when asociadoEgreso1  = '00000' then ' ' else asociadoEgreso1 end )+ '    ' +
        (case when asociadoEgreso2  = '00000' then '' else asociadoEgreso2 end) 'DX',
        convert(fecha_egreso,date) Fecha_Egreso
 from tbHospitalizacion
 where id = _id;
 end