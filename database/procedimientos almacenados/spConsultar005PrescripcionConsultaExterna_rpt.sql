 delimiter // 
create  procedure spConsultar005PrescripcionConsultaExterna_rpt
(
in _paciente nvarchar(50),
in _desde nvarchar(50)
)
begin
SELECT *
from dbo.tbConsultaExternaEvolucionPrescripcion b
where b.consulta_externa = _paciente 
-- Where b.paciente = @paciente 
and CONVERT(b.fecha_ingreso,date) = CONVERT(_desde,datetime);
end

// delimiter //