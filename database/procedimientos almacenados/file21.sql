delimiter //
create procedure spConsultarPacienteCartaIESS (in _paciente  int)
-- spConsultarPacienteCartaIESS 121077
begin 
 declare  w_fecha1 date;
  set w_fecha1 := curdate();
   begin
      select tb1.id id,
             '1' 'Consulta No',
            ' ' Asistencia,
             concat_ws(' ',tb3.apellido_paterno , tb3.apellido_materno ,tb3.primer_nombre ,tb3.segundo_nombre ) as Paciente,
             tb1.medico Id_medico,
             (select concat_ws(' ',tb4.apellidos ,tb4.nombres ) from tbMedico tb4 where tb4.id = tb1.medico and tb4.status = 1) Medico,
             tb1.fecha_inicio FechaInicioConsulta,
             tb1.fecha_fin FechaLimiteConsulta
      from tbconsultaexternacartaiess tb1, tbPaciente tb3
      where tb1.paciente = _paciente 
     and CONVERT(w_fecha1,date) between tb1.fecha_inicio and tb1.fecha_fin
      and tb1.paciente = tb3.id
      and tb1.status = 1
      and tb1.des_campo1 = 1;
     
   end
   // delimiter //