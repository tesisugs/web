// delimiter //
create procedure spStatusSalasCunas()
begin
select sala as sala2 , (select distinct descripcion from tbsala where codigo = sala2 limit 1 ) as nombre_sala
,(select count(codigo) from tbcamasporsala where sala = sala2 and status_camas = 1 and descripcion_camas = 2 ) as disponibles 
, (select count(codigo) from tbcamasporsala where sala = sala2 and status_camas = 2 and descripcion_camas = 2 ) as ocupadas 
,(select count(codigo) from tbcamasporsala where sala = sala2 and status_camas = 3 and descripcion_camas = 2 ) as desinfección ,
(select count(codigo) from tbcamasporsala where sala = sala2 and status_camas = 4 and descripcion_camas = 2 ) as averiada 
 from tbcamasporsala 
group by sala;
end 
// delimiter //


