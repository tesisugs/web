delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarCeanios`(
	IN `anio1` varchar(5) ,
	IN `anio2` varchar(5)
)
begin
select 
(SELECT count(id) as numero_ingreso FROM tbConsultaExternaTurnosDiarios
where date_format(fecha_registro_paciente,'%Y') = anio1 ) as year1 ,
(
SELECT count(id) as numero_ingreso FROM tbConsultaExternaTurnosDiarios 
where date_format(fecha_registro_paciente,'%Y') = anio2) as year2;
end
// delimiter //