
create PROCEDURE SpConsultarIngresosPacientesHospitalizacionPreparacion
-- SpConsultarIngresosPacientesHospitalizacionPreparacion
-- SpConsultarIngresosPacientesHospitalizacionPreparacion 'Ang','%','%','05/07/2016','09/08/2016'
(
in _paciente nvarchar(100),
in _medico nvarchar(100),
in _dependencias nvarchar(50),
in _desde date,
in _hasta date
)
  SELECT c .id ,
         c.Paciente ,
         c.medico ,
         c.nombre_Responsable as nombre_representante,
         p.apellido_paterno + ' ' + p.apellido_materno + ' ' + p.primer_nombre + ' ' + p.segundo_nombre AS DesPaciente,
         (select apellidos +' '+ nombres as Nombres from tbMedico where id = c.medico ) nombre_medico,
           c.fecha_Hora_Ingreso as fecha_registro_paciente,
            c.fecha_Hora_Ingreso as hora_registro_paciente,
            (select temperatura  from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)temperatura,
            (select pulso   from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)pulso,
            (select presion_arterial  from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)presion_arterial,
            (select respiracion   from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)respiracion,
            (select peso   from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)peso,
            (select estatura   from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)estatura,
            (select talla   from tbHospitalizacionPacientePreparacion where hospitalizacion  = c.id)talla,
            (select descripcion  from tbDependencias where codigo  = c.Servicios ) dependencias
         FROM tbHospitalizacion c, tbPaciente p 
     where c.paciente = p.id 
   -- select * from tbhospitalizacion
     AND concat_ws(' ',p.apellido_paterno , p.apellido_materno  ) LIKE concat('%',_paciente,'%') 
     AND  CONVERT(c.medico,CHAR(50) ) LIKE _medico
     AND  CONVERT(c.Servicios,CHAR(50))  LIKE _dependencias
     and c.Fecha_de_Registro between _desde and _hasta 
     and c.STATUS <> 4 
     order by c.Fecha_de_Registro   desc
