delimiter //
create procedure spConsultarDirecionesIpTodos()
begin
select di.id, d.nombre as departamento ,concat_ws(' ',u.nombres,u.apellidos) as nombres, uu.name as usuario_ingreso, uuu.name as usuario_modificacion, 
 di.ip,di.fecha_ingreso,di.fecha_modificacion,
 (select if(di.estado = 1,'activo','inactivo') ) as estado
from tbdireccionesip di inner join tbdepartamentos d on d.id = di.departamento_id
inner join users u on  di.usuario_id = u.id
inner join users uu on  di.usuario_ingreso = uu.id
inner join users uuu on  di.usuario_modificacion = uuu.id;
end
// delimiter //