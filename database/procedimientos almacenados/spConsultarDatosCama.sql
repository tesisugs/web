delimiter //
create procedure spConsultarDatosCama(in _codigo int)
begin
select s.descripcion as sala , tc.descripcion as tipo_cama , cs.numero_camas
from tbcamasporsala cs
join tbsala s on s.codigo = cs.sala 
join tbtipocama tc on tc.codigo = cs.descripcion_camas
where cs.codigo = _codigo;
end
// delimiter //

