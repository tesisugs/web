delimiter //
create PROCEDURE spInsertConsultaExternaDiagnostico (
out _id int,
in _consulta_externa int ,
in _paciente nvarchar(50),
in _titular int,
in _principal nvarchar(50),
in _asociado nvarchar(50),
in _asociado2 nvarchar(50),
in _observacion nvarchar(50),
in _des_campo1 nvarchar(50),
in _des_campo2 nvarchar(50),
in _des_campo3 int ,
in _usuario_ingreso int ,
in _fecha_ingreso datetime,
in _usuario_modificacion int,
in _fecha_modificacion datetime,
in _pcname nvarchar(50),
in _status int
)
-- --------------------
begin
INSERT INTO tbConsultaExternaRayosxDiagnosticos
		(
		`consulta_externa`,
		`paciente`,
		`titular`,
		`principal`,
		`asociado`,
		`asociado2`,
		`principalCiap2`,
		`asociadoCiap`,
		`asociadoCiap2`,
		`observacion`,
		`des_campo1`,
		`des_campo2`,
		`des_campo3`,
		`usuario_ingreso`,
		`fecha_ingreso`,
		`usuario_modificacion`,
		`fecha_modificacion`,
		`pcname`,
		`status`)
		VALUES (_consulta_externa ,
				_paciente	,
				_titular ,
				_principal ,
				_asociado ,
				_asociado2 ,
				_observacion ,
				_des_campo1,
				_des_campo2 ,
				_des_campo3  ,
				_usuario_ingreso,
				curdate(),
				_usuario_modificacion ,
				curdate(),
				_pcname ,
				_status); 
				select id from tbConsultaExternaEvolucionPrescripcionLaboratorio  order by id desc limit 1;
end 
// delimiter //				