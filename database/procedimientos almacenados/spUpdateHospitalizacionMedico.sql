create PROCEDURE spUpdateHospitalizacionMedico
(
in _id int ,
in _Fecha_Hora_Ingreso datetime,
in _usuario_modificacion int ,
in _pcname NVARCHAR(50),
in _status int,
in _medicoTraspaso int,
in _servicios_medico int

) 
UPDATE	tbHospitalizacion
		SET		Medico_Fecha=_Fecha_Hora_Ingreso 
				,fecha_modificacion =curdate ()
				,usuario_modificacion =_usuario_modificacion 
				,pcname=_pcname
				,Servicios = _servicios_medico
				,medico=_medicoTraspaso 
			
													
		WHERE	id = _id
		and status in (1,3)
 

