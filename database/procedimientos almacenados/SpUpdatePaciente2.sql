delimiter //
create procedure SpUpdatePaciente2 (
in _id                    int,
in _cedula                varchar(13),
in _tipo_identificacion   int,
in _primer_nombre         varchar(50),
in _segundo_nombre        varchar(50),
in _apellido_paterno      varchar(50),
in _apellido_materno      varchar(50),
in _genero                varchar(1),
in _fecha_nacimiento      datetime,
in _lugar_nacimiento      varchar(100),
in _ocupacion             varchar(100),
in _estado_civil          int,
in _tipo_sangre           int,
in _nacionalidad          int,
in _pais                  int,
in _provincia             int,
in _ciudad                int,
in _direccion             varchar(500),
in _telefono              varchar(10),
in _celular               varchar(10),
in _otro                  varchar(10),
in _cedula_titular        varchar(13),
in _observacion           varchar(1000),
in _lugar_trabajo         varchar(100),
in _tipo_parentesco       varchar(2),
in _tipo_beneficiario     varchar(5),
in _tipo_seguro           int,
in _des_campo1            varchar(50),
in _des_campo2            varchar(50),
in _des_campo3            decimal(10,2),
in _usuario_modificacion  int,
in _pcname                varchar(50),
in _status                INT,
in _status_discapacidad VARCHAR(1),
in _carnet_conadis VARCHAR(50),
in _status_otro_seguro VARCHAR(1),
in _tipo_seguro_iess VARCHAR(1),
in _descripcion_otro_seguro VARCHAR(50),
in _parroquia int ,

-- datos adicionales
in _nombres_padre   varchar(200),
in _nombres_madre   varchar(200),
in _conyugue       varchar(200),
in _des_campo1IA     varchar(50),
in _des_campo2IA     varchar(50),
in _des_campo3IA     decimal(10,2),
in _cedula_seg_progenitor   varchar(13),
in _fecha_cedulacion datetime ,
in _fecha_fallecimiento datetime ,
in _instruccion varchar(30) ,
in _cobertura_compartida char(1) ,
in _prepagada int,
in _issfa char(1) ,
in _isspol char(1) ,
in _iess char(1) 
)
begin 
/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

/*Handler para error SQL*/ 
DECLARE EXIT HANDLER FOR SQLWARNING 
BEGIN 
SELECT 1 as error; 
ROLLBACK; 
END; 

START TRANSACTION; 
	update tbPaciente 
		set
           cedula=_cedula,
           tipo_identificacion=_tipo_identificacion,
           primer_nombre=_primer_nombre,
           segundo_nombre=_segundo_nombre,
           apellido_paterno=_apellido_paterno,
           apellido_materno=_apellido_materno,
           genero=_genero,
           fecha_nacimiento=_fecha_nacimiento,
           lugar_nacimiento=_lugar_nacimiento,
           ocupacion=_ocupacion,
           estado_civil=_estado_civil,
           tipo_sangre=_tipo_sangre,
           nacionalidad=_nacionalidad,
           pais=_pais,
           provincia=_provincia,
           ciudad=_ciudad,
           direccion=_direccion,
           telefono=_telefono,
           celular=_celular,
           otro=_otro,
           observacion=_observacion,
           lugar_trabajo=_lugar_trabajo,
           tipo_parentesco=_tipo_parentesco,
           tipo_beneficiario=_tipo_beneficiario,
           cedula_titular=_cedula_titular,
           tipo_seguro=_tipo_seguro,
           des_campo1=_des_campo1,
           des_campo2=_des_campo2,
           des_campo3=_des_campo3,
           usuario_modificacion=_usuario_modificacion,
           fecha_modificacion=curdate(),
           pcname=_pcname,
           status=_status,
           status_discapacidad=_status_discapacidad,
			carnet_conadis=_carnet_conadis,
			status_otro_seguro=_status_otro_seguro,
			tipo_seguro_iess=_tipo_seguro_iess,
			descripcion_otro_seguro=_descripcion_otro_seguro,
			parroquia = _parroquia
	where id=_id;
	
          
  if not exists( select id  from tbPacienteInformacionAdicional where paciente = _id) then 
          insert into tbPacienteInformacionAdicional
				values( _id ,
				_nombres_padre,
				_nombres_madre,
				_conyugue,
				_des_campo1IA,
				_des_campo2IA,
				_des_campo3IA,
				_usuario_modificacion,
				GETDATE(),
				_usuario_modificacion,
				curdate(),
				_pcname,
				1,
				_cedula_seg_progenitor,
                _fecha_cedulacion,
                _fecha_fallecimiento,
                _instruccion,
                _cobertura_compartida,
                _prepagada,
                _issfa,
                _isspol,
                _iess
                
         );
else
		update tbPacienteInformacionAdicional
			set nombres_padre = _nombres_padre,
				nombres_madre = _nombres_madre,
				conyugue = _conyugue,
				usuario_modificacion = _usuario_modificacion,
				cedula_seg_progenitor = _cedula_seg_progenitor,
				fecha_modificacion = curdate(),
				fecha_cedulacion = _fecha_cedulacion,
				fecha_fallecimiento = _fecha_fallecimiento,
				instruccion = _instruccion,
				cobertura_compartida = _cobertura_compartida,
				prepagada = _prepagada,
				issfa = _issfa,
				isspol = _isspol,
				iess = _iess
		where paciente = _id;	
end if;
        		
	if not exists(select 1 from tbRelacionPacienteCliente where paciente = _id) then 
		   insert into tbRelacionPacienteCliente
		     values(_id,_cedula, '.');
     end if;        
			
	/*Fin de transaccion*/ 
COMMIT; 


/*Mandamos 0 si todo salio bien*/ 
SELECT 0 as error; 
		  
end

// delimiter //