// delimiter //
create procedure SpUpdateMedicollamado(
in _id                int,
in _respuesta int,
in _descripcion nvarchar(50),
in _usuario int
)
begin
update  tbEnvioCorreoMedicos
   set 
     respuesta = _respuesta,
     observacion = _descripcion,
    usuario_modificacion  = _usuario,
     fecha_modificacion = curdate()
   where id=_id;
end
// delimiter // 