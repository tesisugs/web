delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarIngresosPacientesCENumeroAtencionMedicosDia`( in _medico varchar(100), in _fecha date)
begin
declare desde  date;
declare hasta  date;

set desde := _fecha;
set hasta := _fecha; 
      SELECT c .id ,
         c.fecha_registro_paciente  ,
       c.hora_registro_paciente 
      FROM tbIngresoPacienteConsultaExterna c, tbMedico  m
           where c.medico  = m.id 
       AND  CONVERT(c.medico,CHAR(100) ) LIKE _medico
     and c.fecha_registro_paciente between desde   and hasta;
     -- if @@NumeroAtencion <= 12
      
   --  select * from tbPaciente 	
end

// delimiter //