CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarAtencionPaciente`(
	IN `usuario` int
)
begin
SELECT paciente
         FROM tb1RegistroAdmision  
     where  paciente =  usuario and 
     convert(fecha,date)  between CONVERT(curdate(),date) and CONVERT(curdate(),date) 
     and status = 1;
end