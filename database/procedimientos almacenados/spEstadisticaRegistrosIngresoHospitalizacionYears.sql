delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spEstadisticaRegistrosIngresoHospitalizacionYears`()
begin
select
 (select count(id) as enero from tbhospitalizacion where Fecha_de_Registro between '2018/01/01' and '2018/01/31') as enero, 
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/02/01' and '2018/02/28') as febrero,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/03/01' and '2018/03/31') as marzo,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/04/01' and '2018/04/30') as abril,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/05/01' and '2018/05/31') as mayo,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/06/01' and '2018/06/30') as junio,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/07/01' and '2018/07/31') as julio,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/08/01' and '2018/08/31') as agosto,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/09/01' and '2018/09/30') as septiembre,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/10/01' and '2018/10/31') as octubre,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/11/01' and '2018/11/30') as noviembre,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2018/12/01' and '2018/12/31') as dicimebre,
 (select count(id) as enero from tbhospitalizacion where Fecha_de_Registro between '2017/01/01' and '2017/01/31') as enero1, 
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/02/01' and '2017/02/28') as febrero1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/03/01' and '2017/03/31') as marzo1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/04/01' and '2017/04/30') as abril1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/05/01' and '2017/05/31') as mayo1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/06/01' and '2017/06/30') as junio1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/07/01' and '2017/07/31') as julio1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/08/01' and '2017/08/31') as agosto1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/09/01' and '2017/09/30') as septiembre1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/10/01' and '2017/10/31') as octubre1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/11/01' and '2017/11/30') as noviembre1,
(select count(id) as febrero from tbhospitalizacion where Fecha_de_Registro between '2017/12/01' and '2017/12/31') as dicimebre1    
;
end
// delimiter //