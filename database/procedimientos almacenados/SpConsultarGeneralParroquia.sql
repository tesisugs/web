USE `majomacontrolhospitalario`;
DROP procedure IF EXISTS `SpConsultarGeneralParroquia`;

DELIMITER $$
USE `majomacontrolhospitalario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarGeneralParroquia`( in
_ciudad int )
begin
-- exec SpConsultarGeneralParroquia 15
Select codigo,
descripcion
   from tbParroquia
where ciudad=_ciudad
order by descripcion ;
end$$

DELIMITER ;
