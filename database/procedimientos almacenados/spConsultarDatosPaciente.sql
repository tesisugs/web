delimiter //
create procedure spConsultarDatosPaciente(in id_paciente int)
begin
select s.descripcion as seguro, c.descripcion as raza , ec.descripcion  as estado_civil, par.descripcion as parentesco ,g.descripcion as genero 
, concat_ws(' ',titular.apellido_paterno , titular.apellido_materno , titular.primer_nombre , titular.segundo_nombre) as titular,
titular.telefono as tel_titular , titular.direccion as dir_titular , p.status_discapacidad , p.carnet_conadis , p.cedula , titular.cedula as cedula_titular,
concat_ws(' ',p.apellido_paterno , p.apellido_materno , p.primer_nombre , p.segundo_nombre) as paciente, p.fecha_nacimiento
from tbpaciente p
join tbTipoingreso s on s.codigo = p.tipo_seguro 
join tbTipoCultura c on c.raza= p.etnico 
join tbestadocivil ec on ec.codigo= p.estado_civil
join tbparentesco par on par.codigo= p.tipo_parentesco
join tbgenero g on g.abreviatura= p.genero
join tbpaciente titular on titular.cedula = p.cedula
where p.id = id_paciente;
end 
// delimiter //