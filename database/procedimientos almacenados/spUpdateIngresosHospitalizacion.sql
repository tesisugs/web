	create PROCEDURE spUpdateIngresosHospitalizacion
(
in _id int ,
in _Procedencias nvarchar(50),
in _Nombre_Responsable nvarchar(50),
in _Direccion_Responsable nvarchar(50),
in _Documento_Responsable nvarchar(50),
in _Telefono_Responsable nvarchar(50),
in _Observaciones nvarchar(50),
in _Servicios int,
in _medico int ,
in _principal nvarchar(50) ,
in _asociado_1 nvarchar(50),
in _asociado_2 nvarchar(50),
in _Medico_Observacion nvarchar(50),
in _fecha_modificacion datetime ,
in _usuario_modificacion int ,
in _pcname NVARCHAR(50),
in _derivacion bit ,
in _hospital int,
in _status_documento bit,
in _osbservacion_documento NVARCHAR(400)
)
UPDATE	tbHospitalizacion
		SET		Nombre_Responsable=_Nombre_Responsable 
				,Direccion_Responsable=_Direccion_Responsable 
				,Documento_Responsable=_Documento_Responsable 
				,Telefono_Responsable =_Telefono_Responsable
				,Observaciones=_Observaciones 
				,Servicios=_Servicios
				,medico=_medico 
				,principal=_principal 
				,asociado_1=_asociado_1 
				,asociado_2=_asociado_2 
				,Medico_Observacion=_Medico_Observacion 
				,fecha_modificacion =curdate ()
				,usuario_modificacion =_usuario_modificacion 
				,pcname=_pcname
				,derivacion=_derivacion  
				,hospital=_hospital
				,status_documento=_status_documento 
				,osbservacion_documento=_osbservacion_documento
				
		WHERE	id = _id;
