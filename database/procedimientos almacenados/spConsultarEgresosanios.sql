delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarEgresosanios`(
	IN `anio1` varchar(5) ,
	IN `anio2` varchar(5)
)
begin
select 
(SELECT count(id) as numero_ingreso FROM tbegresos
where date_format(fecha_ingreso1,'%Y') = anio1 ) as year1 ,
(
SELECT count(id) as numero_ingreso FROM tbegresos 
where date_format(fecha_ingreso1,'%Y') = anio2) as year2;
end
// delimiter //