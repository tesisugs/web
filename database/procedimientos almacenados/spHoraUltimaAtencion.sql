delimiter //
create procedure spHoraUltimaAtencionDia(in medico int, in _fecha date)
begin
declare desde  date;
declare hasta  date;

set desde := _fecha;
set hasta := _fecha; 
      SELECT 
       c.hora_registro_paciente 
      FROM tbIngresoPacienteConsultaExterna c, tbMedico  m
           where c.medico  = m.id 
       AND  CONVERT(c.medico,CHAR(100) ) LIKE _medico
     and c.fecha_registro_paciente between desde   and hasta order by c.id desc limit 1;
     -- if @@NumeroAtencion <= 12
      
   --  select * from tbPaciente 	
end
// delimiter 