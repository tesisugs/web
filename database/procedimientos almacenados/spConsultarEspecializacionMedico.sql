create procedure spConsultarEspecializacionMedico (
-- spConsultarEspecializacionMedico 6
in _idMedico int
)
Select e.descripcion as especializacion from tbMedico m
join tbEspecializacion e on m.especializacion = e.codigo 
where m.id = _idMedico