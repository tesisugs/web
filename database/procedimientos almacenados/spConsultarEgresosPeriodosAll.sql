delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `spConsultarEgresosPeriodosAll`(
	IN `desde` varchar(10) ,
	IN `hasta` varchar(10)

)
begin
select *from tbegresos where fecha_registro1 between concat(desde,'/01/01') and concat(hasta,'/12/31') order by fecha_registro1 desc;
end
// delimiter //