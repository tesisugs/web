delimiter //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SpConsultarCeSemanasAll`(
	IN `fecha1` varchar(15),
	IN `fecha2` varchar(15)
)
begin
  SELECT * from tbConsultaExternaTurnosDiarios where date_format(fecha_registro_paciente,'%U') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%U') and
			date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha1, '%Y-%m-%d')),'%Y')  
  union all
   SELECT * from tbConsultaExternaTurnosDiarios where date_format(fecha_registro_paciente,'%U') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%U') and
			date_format(fecha_ingreso,'%Y') = date_format((str_to_date(fecha2, '%Y-%m-%d')),'%Y');  
		
end
// delimiter //