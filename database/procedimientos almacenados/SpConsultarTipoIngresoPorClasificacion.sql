delimiter $$
CREATE PROCEDURE SpConsultarTipoIngresoPorClasificacion ( in
-- exec SpConsultarTipoIngresoPorClasificacion 1
id_ClasificacionSeguro int
)
begin
select  codigo,
        descripcion
from tbTipoIngreso
where status = 1
and idClasificacionSeguro = id_ClasificacionSeguro ;
end $$
delimiter $$