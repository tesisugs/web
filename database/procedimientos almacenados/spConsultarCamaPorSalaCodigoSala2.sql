create procedure spConsultarCamaPorSalaCodigoSala2
(
in _sala nvarchar(50)
)
-- spConsultarCamaPorSalaCodigoSala2 '2'
select tbcs.codigo ,
		tbcs.sala ,
		tbcs.status_camas ,
				tbcs.procedimiento ,
		tbcs.descripcion_camas ,
		tbs.descripcion DESCRIPCION_SALA,
		tbcs.numero_camas NUMERO_CAMA,
		tbt.descripcion DESCRIPCION_CAMA,
		tbe.descripcion DESCRIPCION_ESTADO,

		(select descripcion  from tbProcedimiento  where id = tbcs.procedimiento ) PROCEDIMIENTO_CAMA
		 from tbCamasPorSala tbcs , tbSala tbs,
		 tbTipoCama   tbt, tbEstadoCamas tbe 
		 where tbcs.sala = tbs.codigo 
		 and tbcs.descripcion_camas = tbt.codigo 
		 and tbcs.status_camas = tbe.codigo 
		
		 and tbcs .sala = _sala 
		 order by  tbcs.numero_camas 