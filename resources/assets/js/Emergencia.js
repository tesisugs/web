Vue.component('modal', {
    template: '#modal-template'
});
var app=new Vue({
        el: '#aplicacion',
        data:{
            nuevo : 'activo',
            guardar : 'inactivo',
            editar : 'inactivo',
            imprimir : 'inactivo',
            digitalizar : 'inactivo',
            cerrar : 'inactivo',
            Modal: false,
            showModal: false,
            buscar_cedula: 'inactivo',
            buscar_nombre : 'activo',
            in_cedula : '',

            discapacidad : '',
            otro_seguro : '',
            visibilidad: true,
            apellido: '',
            listaAfiliados : [],
            listaAfiliadosCedula: [],
            listaInfoAdicional : [],
            listaInfoTitular : [],
            cmbidentificacion : [],
            cmbSangre : [],
            cmbNacionalidad : [],
            cmbParentesco : [],
            cmbGenero: [],
            cmbEstadoCivil : [],
            cmbEtnia : [],
            cmbCiudad : [],
            cmbParroquia : [],
            cmbProvincia : [],
            cmbSeguro : [],
            cmbOtroSeguro : [],
            cmbBeneficiario : [],
            cmbDocumentosIess : [],

            id : '',
            cedula: '',
            tipo_identificacion : '',
            primer_nombre : '',
            segundo_nombre : '',
            apellido_paterno : '',
            apellido_materno : '',
            genero : '',
            fecha_nacimiento : '',
            lugar_nacimiento : '',
            ocupacion : '',
            estado_civil : '',
            tipo_sangre : '',
            nacionalidad : '',
            pais : '',
            provincia : '',
            ciudad : '',
            parroquia : '',
            direccion : '',
            telefono : '',
            celular : '',
            otro : '',
            observacion : '',
            lugar_trabajo : '',
            tipo_parentesco : '',
            tipo_beneficiario : '',
            cedula_titular : '',
            tipo_seguro : '',
            des_campo1 : '',
            des_campo2 : '',
            des_campo3 : '',
            usuario_ingreso : '',
            fecha_ingreso : '',
            usuario_modificacion : '',
            fecha_modificacion : '',
            pcname : '',
            status : '',
            status_discapacidad : '',
            carnet_conadis : '',
            status_otro_seguro : '',
            tipo_seguro_iess : '',
            descripcion_otro_seguro : '',
            etnico : '',


            id_a : '',
            paciente : '',
            nombres_padre : '',
            nombres_madre : '',
            conyugue : '',
            des_campo1_a : '',
            des_campo2_a : '',
            des_campo3_a : '',
            usuario_ingreso_a : '',
            fecha_ingreso_a : '',
            usuario_modificacion_a : '',
            pcname_a : '',
            status_a : '',
            cedula_seg_progenitor : '',
            fecha_cedulacion : '',
            fecha_fallecimiento : '',
            instruccion : '',
            cobertura_compartida : '',
            prepagada : '',
            issfa : '',
            isspol : '',
            iess : '',


            // información del titular o afiliado
            apellidos :'',
            nombres : '',
            observacionTitular : '',




            // variables temporales

            TempProvincia :0,
            TempCiudad :0,
            TempParroquia :0,

            CedulaPueba : '0923956916'

        },
        
        created:function () {
            
        },
    
        methods: {

            CargarCombosAll: function () {


                axios.get('tipoidentificacion').then(response => {
                    this.cmbidentificacion  = response.data
            })

                var urlProvincia = 'provincia/1';
                axios.get(urlProvincia).then(response => {
                    this.cmbProvincia = response.data
            })


                var urlCiudad = 'ciudad/1';
                axios.get(urlCiudad).then(response => {
                    this.cmbCiudad = response.data
            })
                var urlParroquia = 'parroquia/1';
                axios.get(urlParroquia).then(response => {
                    this.cmbParroquia = response.data
            })

                var urlBeneficiario = 'parentesco/1';
                axios.get(urlBeneficiario).then(response => {
                    this.cmbBeneficiario = response.data
            })

                axios.get('otroseguro').then(response => {
                    this.cmbOtroSeguro  = response.data
            })
                axios.get('seguro').then(response => {
                    this.cmbSeguro  = response.data
            })
                axios.get('etnia').then(response => {
                    this.cmbEtnia  = response.data
            })
                axios.get('nacionalidad').then(response => {
                    this.cmbNacionalidad  = response.data
            })


                axios.get('estadoCivil').then(response => {
                    this.cmbEstadoCivil  = response.data
            })

                axios.get('sangre').then(response => {
                    this.cmbSangre = response.data
            })

                axios.get('genero').then(response => {
                    this. cmbGenero  = response.data
            })

                axios.get('parentescoPaciente').then(response => {
                    this.cmbParentesco  = response.data
            })

                axios.get('documentosiess').then(response => {
                    this.cmbDocumentosIess  = response.data
            })



            },

            Nuevo: function () {
              
                    this.guardar = 'activo',
                    this.nuevo = 'inactivo',
                    this.cerrar = 'activo',
                    this.visibilidad = false,
                    this.buscar_cedula = 'activo',
                    this.buscar_nombre = 'inactivo'
                    this.tipo_identificacion = "1";
                    this.genero = "1";
                    this.nacionalidad =1;
                    this.etnico =1;
                    this.provincia=1;
                    this.ciudad=1;
                    this.parroquia=1;
                    this.tipo_sangre = 1;
                    this.tipo_seguro_iess = 1;
                    this.tipo_parentesco = 1;
                    this.estado_civil = 1;

            },
            Cerrar: function () {

                this.guardar = 'inactivo',
                    this.nuevo = 'activo',
                    this.cerrar = 'inactivo',
                    this.imprimir = 'inactivo',
                    this.digitalizar = 'inactivo',
                    this.visibilidad = true,
                    this.buscar_cedula = 'inactivo',
                    this.buscar_nombre= 'activo',
                    this.modal = true;
                    this.in_cedula = '',

                    this.discapacidad = '',
                    this.otro_seguro = '',
                    this.visibilidad= true,
                    this.apellido= '',
                    this.listaAfiliados = [],
                    this.listaAfiliadosCedula= [],
                    this.listaInfoAdicional = [],
                    this.listaInfoTitular = [],
                    this.cmbidentificacion = [],
                    this.cmbSangre = [],
                    this.cmbNacionalidad = [],
                    this.cmbParentesco = [],
                    this.cmbGenero= [],
                    this.cmbEstadoCivil = [],
                    this.cmbEtnia = [],
                    this.cmbCiudad = [],
                    this.cmbParroquia = [],
                    this.cmbProvincia = [],
                    this.cmbSeguro = [],
                    this.cmbOtroSeguro = [],
                    this.cmbBeneficiario = [],
                    this.cmbDocumentosIess = [],

                    this.id  ='',
                    this.cedula=  '',
                    this.tipo_identificacion = '',
                    this.primer_nombre = '',
                    this.segundo_nombre= '',
                    this.apellido_paterno = '',
                    this.apellido_materno = '',
                    this.genero = '',
                    this.fecha_nacimiento = '',
                    this.lugar_nacimiento = '',
                    this.ocupacion = '',
                    this.estado_civil = '',
                    this.tipo_sangre = '',
                    this.nacionalidad = '',
                    this.pais = '',
                    this.provincia = '',
                    this.ciudad = '',
                    this.parroquia = '',
                    this.direccion = '',
                    this.telefono = '',
                    this.celular = '',
                    this.otro = '',
                    this.observacion = '',
                    this.lugar_trabajo = '',
                    this.tipo_parentesco = '',
                    this.tipo_beneficiario = '',
                    this.cedula_titular = '',
                    this.tipo_seguro = '',
                    this.des_campo1 = '',
                    this.des_campo2 = '',
                    this.des_campo3 = '',
                    this.usuario_ingreso = '',
                    this.fecha_ingreso = '',
                    this.usuario_modificacion = '',
                    this.fecha_modificacion = '',
                    this.pcname = '',
                    this.status = '',
                    this.status_discapacidad = '',
                    this.carnet_conadis = '',
                    this.status_otro_seguro = '',
                    this.tipo_seguro_iess = '',
                    this.descripcion_otro_seguro = '',
                    this.etnico = '',


                    this.id_a = '',
                    this.paciente = '',
                    this.nombres_padre = '',
                    this.nombres_madre = '',
                    this.conyugue = '',
                    this.des_campo1_a = '',
                    this.des_campo2_a = '',
                    this.des_campo3_a = '',
                    this.usuario_ingreso_a = '',
                    this.fecha_ingreso_a= '',
                    this.usuario_modificacion_a = '',
                    this.pcname_a = '',
                    this.status_a = '',
                    this.cedula_seg_progenitor = '',
                    this.fecha_cedulacion = '',
                    this.fecha_fallecimiento = '',
                    this.instruccion = '',
                    this.cobertura_compartida = '',
                    this.prepagada = '',
                    this.issfa = '',
                    this.isspol = '',
                    this.iess = '',


                    // información del titular o afiliado
                    this.apellidos ='',
                    this.nombres = '',
                    this.observacionTitular = '',




                    // variables temporales

                    this.TempProvincia =0,
                    this.TempCiudad =0,
                    this.TempParroquia =0,

                    this.CedulaPueba = '0923956916'
            },


            pasarDatos: function (cedula) {
                this.in_cedula = cedula;
                this.ConsultarCedula();
                this.modal = false;
            },


            cargarCombos : function (data) {

                var urlProvincia = 'provincia/'+data[0].nacionalidad+'';
                axios.get(urlProvincia).then(response => {
                    this.cmbProvincia = response.data
                })


                var urlCiudad = 'ciudad/'+data[0].provincia+'';
                axios.get(urlCiudad).then(response => {
                    this.cmbCiudad = response.data
                })
                var urlParroquia = 'parroquia/'+data[0].ciudad+'';
                axios.get(urlParroquia).then(response => {
                    this.cmbParroquia = response.data
                })

                var urlBeneficiario = 'parentesco/'+data[0].tipo_parentesco+'';
                axios.get(urlBeneficiario).then(response => {
                    this.cmbBeneficiario = response.data
            })

                var tempProvicia = data[0].provincia;
                this.TempProvincia = parseInt(tempProvicia);

                var tempCiudad = data[0].ciudad;
                this.TempCiudad = parseInt(tempCiudad);

                var tempParroquia = data[0].parroquia;
                this.TempParroquia = parseInt(tempParroquia);

            },

            InformacionAdicional : function (data) {


                var urlInfo = 'registropacientes/informacionadicional/'+data[0].id+'';
                alert(urlInfo);
                axios.get(urlInfo).then(response => {
                    this.listaInfoAdicional = response.data;
                this.nombres_padre = this.listaInfoAdicional[0].nombres_padre;
                this.nombres_madre = this.listaInfoAdicional[0].nombres_madre;
                this.conyugue = this.listaInfoAdicional[0].conyugue;


            })
            },
            getUsers: function() {
                var urlUsers = 'registropacientes/TitularAfiliadoPorApellido/'+this.apellido+'';
                axios.get(urlUsers).then(response => {
                    this.listaAfiliados  = response.data
                })
            },

            InformacionTitular : function (data) {
                var urlTitular = 'registropacientes/TitularAfiliadoPorCedula/'+data[0].cedula_titular+'';
                        axios.get(urlTitular).then(response => {
                            this.listaInfoTitular = response.data;
                this.apellidos =""+this.listaInfoTitular[0].apellido_paterno+" " + this.listaAfiliadosCedula[0].apellido_materno+""  ;
                this.nombres =""+this.listaInfoTitular[0].primer_nombre+" " + this.listaAfiliadosCedula[0].segundo_nombre+""  ;
                this.observacionTitular = this.listaInfoTitular[0].observacion;

            })
                },


            ConsultarCedula: function() {

                this.imprimir = 'activo';

                axios.get('otroseguro').then(response => {
                    this.cmbOtroSeguro  = response.data
                })
                axios.get('seguro').then(response => {
                    this.cmbSeguro  = response.data
                })
                axios.get('etnia').then(response => {
                    this.cmbEtnia  = response.data
                 })
                axios.get('nacionalidad').then(response => {
                    this.cmbNacionalidad  = response.data
                })


                axios.get('estadoCivil').then(response => {
                    this.cmbEstadoCivil  = response.data
                })

                axios.get('sangre').then(response => {
                    this.cmbSangre = response.data
                 })

                axios.get('genero').then(response => {
                    this. cmbGenero  = response.data
                })

                axios.get('parentescoPaciente').then(response => {
                    this.cmbParentesco  = response.data
                })

                axios.get('documentosiess').then(response => {
                    this.cmbDocumentosIess  = response.data
            })


                var urlUsers = 'registropacientes/TitularAfiliadoPorCedula/'+this.in_cedula+'';
                axios.get(urlUsers).then(response => {
                    this.listaAfiliadosCedula  = response.data;
                this.apellido_paterno = this.listaAfiliadosCedula[0].apellido_paterno;
                this.id = this.listaAfiliadosCedula[0].id;
                this.tipo_identificacion = this.listaAfiliadosCedula[0].tipo_identificacion;
                this.primer_nombre = this.listaAfiliadosCedula[0].primer_nombre;
                this.segundo_nombre = this.listaAfiliadosCedula[0].segundo_nombre;
                this.apellido_paterno = this.listaAfiliadosCedula[0].apellido_paterno;
                this.apellido_materno = this.listaAfiliadosCedula[0].apellido_materno;
                this.genero = this.listaAfiliadosCedula[0].genero;
                this.fecha_nacimiento = this.listaAfiliadosCedula[0].fecha_nacimiento;
                this.lugar_nacimiento = this.listaAfiliadosCedula[0].lugar_nacimiento;
                this.ocupacion = this.listaAfiliadosCedula[0].ocupacion;
                this.estado_civil= this.listaAfiliadosCedula[0].estado_civil;
                this.tipo_sangre= this.listaAfiliadosCedula[0].tipo_sangre;
                this.nacionalidad =this.listaAfiliadosCedula[0].nacionalidad;
                this.pais = this.listaAfiliadosCedula[0].pais;
                this.provincia = this.listaAfiliadosCedula[0].provincia;
                this.ciudad = this.listaAfiliadosCedula[0].ciudad;
                this.direccion = this.listaAfiliadosCedula[0].direccion;
                this.telefono = this.listaAfiliadosCedula[0].telefono;
                this.celular = this.listaAfiliadosCedula[0].celular;
                this.otro = this.listaAfiliadosCedula[0].otro;
                this.observacion = this.listaAfiliadosCedula[0].observacion;
                this.lugar_trabajo  = this.listaAfiliadosCedula[0].lugar_trabajo;
                this.tipo_parentesco  = this.listaAfiliadosCedula[0].tipo_parentesco;
                this.tipo_beneficiario  = this.listaAfiliadosCedula[0].tipo_beneficiario;
                this.cedula_titular  = this.listaAfiliadosCedula[0].cedula_titular;
                this.tipo_seguro  = this.listaAfiliadosCedula[0].tipo_seguro;
                this.des_campo1  = this.listaAfiliadosCedula[0].des_campo1;
                this.des_campo2  = this.listaAfiliadosCedula[0].des_campo2;
                this.des_campo3  = this.listaAfiliadosCedula[0].des_campo3;
                this.usuario_ingreso = this.listaAfiliadosCedula[0].usuario_ingreso;
                this.fecha_ingreso  = this.listaAfiliadosCedula[0].fecha_ingreso;
                this.usuario_modificacion  = this.listaAfiliadosCedula[0].usuario_modificacion;
                this.fecha_modificacion  = this.listaAfiliadosCedula[0].fecha_modificacion;
                this.pcname  = this.listaAfiliadosCedula[0].pcname;
                this.status  = this.listaAfiliadosCedula[0].status;
                this.status_discapacidad  = this.listaAfiliadosCedula[0].status_discapacidad;
                this.carnet_conadis  = this.listaAfiliadosCedula[0].carnet_conadis;
                this.status_otro_seguro  = this.listaAfiliadosCedula[0].status_otro_seguro;
                this.tipo_seguro_iess  = this.listaAfiliadosCedula[0].tipo_seguro_iess;
                this.descripcion_otro_seguro  = this.listaAfiliadosCedula[0].descripcion_otro_seguro;
                this.etnico  = this.listaAfiliadosCedula[0].etnico;
                this.parroquia  = this.listaAfiliadosCedula[0].parroquia;

                 this.cargarCombos(response.data);
                 this.InformacionAdicional(response.data);
                 this.InformacionTitular(response.data);
            })

            }





        }

    });


