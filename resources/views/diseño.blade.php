@extends('layouts.principal')
@section('titulo')
    <h1>REGISTRO PACIENTES</h1>
@endsection
@section('title','registro de pacientes')
@section('css')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/paper-bootstrap-wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet">
    <style>
        .body {
            font-size: 1.3rem;
        }
        .tab-content  {
            font-size: 1.3rem;
        }

        .form-control-control {
            display: block;
            width: 100%;
            padding: 7px;
            font-size: 1.59rem;
            line-height: 1.9;
            color: #495057;
            background-color: #FFF;
            background-clip: padding-box;
            border: 2px solid #ced4da;
            border-radius: 4px;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        .form-control-control::-ms-expand {
            background-color: transparent;
            border: 0;
        }

        .form-control-control:focus {
            color: #495057;
            background-color: #FFF;
            border-color: #009688;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .form-control-control::-webkit-input-placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control:-ms-input-placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control::-ms-input-placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control::placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control:disabled, .form-control-control[readonly] {
            background-color: #e9ecef;
            opacity: 1;
        }

        select.form-control-control:not([size]):not([multiple]) {
            /*height: calc(2.0625rem + 4px);*/
            padding: 12px;
        }

        select.form-control-control:focus::-ms-value {
            color: #495057;
            background-color: #FFF;
        }

        .form-control-control-file,
        .form-control-control-range {
            display: block;
            width: 100%;
        }

    </style>
@endsection
@section('body','"font-size:0.900rem"')
@section('contenido')

    {!! Toastr::message() !!}
    @include('flash::message')
    <div class="col-lg-12" id="aplicacion" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml"
         xmlns:v-on:key.enter="http://www.w3.org/1999/xhtml" xmlns:v-on.keyup="http://www.w3.org/1999/xhtml">
        <div class="col-md-12" id="escaner">
            <form action="diseño" method="post" id="form1" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="col-md-10 col-md-offset-1">
                    <h5 class="info-text"> Escanee las información.</h5>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header"> Docuemento</div>
                                <div class="card-body">
                                    <img style="height:300px; width:100%; display: block" alt="imagen escaneada">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <p>Documento</p>
                            <div class="col-md-12">

                                <select class="js-example-basic-multiple js-states form-control-control" id="documentos" style="width: 105%" name="documentos[]" multiple="multiple">

                                </select>
                                <input name="uploadedfile[]" type="file" multiple />
                                <textarea class="form-control-control" name="observacion" placeholder="observación"></textarea>
                                <br>
                                <div>
                                    <button class="btn btn-primary" type="button" onclick="scanToJpg()"><i class="fa fa-refresh" aria-hidden="true"></i>Cargar </button>
                                    <button class="btn btn-primary" type="button" onclick="enviar()"><i class="fa fa-floppy-o" aria-hidden="true"></i>Grabar</button>
                                    <button class="btn btn-primary" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i>Limpiar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </div>


@endsection

@section('script')

    <script type="text/javascript">

        function enviar() {
            document.getElementById("form1").submit()
        }

        function validarForm() {
            alert("hola");
            return false;
        }

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.get("emergencia/documentosiess", function (datos) {
                $.each(JSON.parse(datos), function (key, value) {
                    $("#documentos").append("<option value=" + value.codigo + ">" + value.descripcion+ "</option>");
                });
            });
        });
    </script>
    <script src = "//asprise.azureedge.net/scannerjs/scanner.js"  type = "text / javascript" > </ script>
    <script src="{{asset('js/jquery.bootstrap.wizard.js')}}"></script>
    <script src="{{asset('js/paper-bootstrap-wizard.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    <script>
        $('#flash-overlay-modal').modal().delay(3000).fadeOut(350);
    </script>
@endsection
