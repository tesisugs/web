<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">

    <h4>Estimado(a) Dr(a).: {{$medico}} </h4>
    <p>Hospital Leon Beccera de Guayaquil, le informa que el dia {{$fecha}}
        usted ha sido solicitado para el servicio de hospitalizacion del paciente: {{$paciente}} con tipo de seguro
      {{$seguro}} por favor confirmar a la brevedad del caso</p>
    <br>
    <p>Recuerde que es su responsabilidad el cuidado de la información</p>
    <br>
    <p>Saludos cordiales</p>

    <p>Soporte electronico</p>

     <a href="http://majomacontrolhospitalario.test:8090/aceptar/{{$id}}/{{$token}}">Confirmar</a>
    <br>
    <a href="http://majomacontrolhospitalario.test:8090/declinar/{{$id}}/{{$token}}">Declinar </a>
</div>

</body>
</html>