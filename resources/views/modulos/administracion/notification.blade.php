@extends('layouts.principal')
@section('title','Notificaciones')
@section('titulo','Notificaciones')
@section('contenido')


    <div class="col-md-12" id="notificaciones">
        @include('flash::message')
        <div class="tile">
           <form action="{{route('administracion.storenotificacionesrol')}}" method="post" id="form_notificaciones_rol">
               {{csrf_field()}}
                <div class="tile-body">

                        <div class="form-group row">
                            <label class="control-label col-md-1">Titulo:</label>
                            <input maxlength="30" required class="form-control col-md-4" name="titulo_nr" v-model="titulo_nr" type="text" placeholder="Tiltulo de la notificacion">
                            <label class="control- col-md-1">Prioridad</label>
                            <select class="form-control col-md-4" required v-model="icono_nr" name="icono_nr" >
                                <option value="1" > General</option>
                                <option value="2" > Importante</option>
                                <option value="3" > Urgente </option>
                            </select>
                        </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input v-model="todos" name="todos" class="form-check-input" type="checkbox">Todos los roles
                            </label>
                        </div>
                    </div>

                        <div class="form-group row">
                            <label class="control-label col-md-8">Descripción:</label>
                            <label v-if="todos === false" class="control-label col-md-4">Roles:</label>
                        </div>

                        <div class="form-group row">
                            <textarea maxlength="250" required class="form-control col-md-8" name="descripcion_nr" v-model="descripcion_nr" rows="4" placeholder="Descripción de la notificación"></textarea>
                            <select multiple  v-if="todos === false" v-model="rol" class="form-control col-md-4" name="rol[]" >
                                <option v-for="dato in  cmb_roles" :value="dato.id"> @{{dato.name}} </option>
                            </select>
                        </div>
                </div>
                <div class="tile-footer">
                    <button class="btn btn-primary" type="button" @click="enviar()"><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>&nbsp;&nbsp;&nbsp;
                    <a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancelar</a>
                </div>
           </form>
        </div>
    </div>

   <!-- <div class="col-md-6" id="notificacionrapida">
        <div class="tile">
          <form method="post"   action="{{route('administracion.storenotificacionespush')}}">
              {/{csrf_field()}}
            <h3 class="tile-title">Notificaciones Rapidas</h3>
            <div class="tile-body">
                    <div class="form-group">
                        <label class="control-label">Titulo:</label>
                        <input class="form-control" v-model="titulo_p" type="text" placeholder="Ingrese el titulo">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Enlace:</label>
                        <input class="form-control" type="text" v-model="enlace_p" placeholder="Ingrese el enlace">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Descripcion:</label>
                        <textarea class="form-control" v-model="descripcion_p" rows="4" placeholder="Ingrese la descripcion de la notificación"></textarea>
                    </div>
            </div>
            <div class="tile-footer">
                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancelar</a>
            </div>
          </form>
        </div>
    </div> -->

@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {

            $.get("roles", function (datos) {
                $.each(datos, function (key, value) {
                    $("#roles").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
        var app = new Vue ({
            el:'#notificaciones',
            data : {
                errores : [],
                cmb_roles : [],
                // notificacion por rol
                todos : false,
                titulo_nr : '',
                enlace_nr : '',
                icono_nr : '',
                descripcion_nr : '',
                rol : [],
                // notificacion por push
                titulo_p : '',
                enlace_p : '',
                descripcion_p : '',


            },

            created : function() {

                axios.get('/administracion/roles').then(response => {
                    this.cmb_roles  = response.data
                 })
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "600",
                    "hideDuration": "3000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
            },
            methods : {

                enviar : function() {

                    if(this.titulo_nr === '')
                    {
                        this.errores.push("seleccione el titulo");
                    }

                    if(this.icono_nr === '')
                    {
                        this.errores.push("seleccione el icono");
                    }

                    if(this.descripcion_nr === '')
                    {
                        this.errores.push("seleccione la descripcion");
                    }

                    if(this.todos === false && this.rol.length === 0)
                    {
                        this.errores.push("seleccione si todos los roles o solo algunos");
                    }

                    if( this.errores.length === 0) {
                        document.getElementById("form_notificaciones_rol").submit();
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                }


            }

        });

    </script>


@endsection