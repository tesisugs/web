@extends('layouts.principal')
@section('titulo','Detalles de informes')
@section('title','Detalles de informes')
@section('contenido')

    <div class="col-md-12" id="estadistica" >
        <input type="hidden" value="{{$desde}}" id="desde">
        <input type="hidden" value="{{$hasta}}" id="hasta">
        <input type="hidden" value="{{$tipo}}" id="tipo">
        <div class="tile">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tile" v-if="parametro==='periodo'">
                        <h3 class="tile-title">Ingresos Hospitalarios - <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartP"></canvas>
                        </div>
                        <table>
                            <tr>
                                <th>fecha1</th>
                                <th>fecha2</th>
                            </tr>
                            <tr>
                               <td>550</td>
                               <td>560</td>
                            </tr>
                        </table>
                    </div>
                    <div class="tile" v-if="parametro === 'mes'">
                        <h3 class="tile-title">Ingresos Hospitalarios - Meses <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChart"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'year'">
                        <h3 class="tile-title">Ingresos Hospitalarios - años <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartY"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'semana'">
                        <h3 class="tile-title">Ingresos Hospitalarios -Semanas <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartS"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'rango'">
                        <h3 class="tile-title">Ingresos Hospitalarios - desde <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9" >
                            <canvas class="embed-responsive-item"  id="horizontalBarChartR"></canvas>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div v-if="parametro === 'mes'" class="table-responsive">
        <table class="table table-bordered table-sm">
            <tr>
                <th>fecha1</th>
                <th>fecha2</th>
            </tr>
            <tr>
                <td>550</td>
                <td>560</td>
            </tr>
        </table>
    </div>


@endsection

@section('script')
    <!-- <script type="text/javascript"  src ="{{asset('js/plugins/chart.js')}}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script>
        var app = new Vue ({
            el:'#estadistica',
            data : {

                // variables para la busqueda
                parametro : '',
                mes1 : '',
                mes2 : '',
                p_mes1 : '',
                p_mes2 : '',
                cmbEstadisticayears : [],
                year1 : '',
                year2 : '',
                p_year1 : '',
                p_year2 : '',
                año2 : '',

                cmbEstadisticaweeks : [],
                p_semana1 : '',
                p_semana2 : '',
                semana1 : '',
                semana2 : '',

                cmbEstadisticasrango : [],
                p_rango1 :'',
                p_rango2 :'',
                rango1 :'',
                rango2 :'',


                //
                cmbEstadistica : [],
                // meses del 1 grafico
                enero : 0,
                febrero : 0,
                marzo : 0,
                abril : 0,
                mayo : 0,
                junio : 0,
                julio : 0,
                agosto : 0,
                septiembre : 0,
                octubre : 0,
                noviembre : 0,
                diciembre : 0,
                enero1 : 0,
                febrero1 : 0,
                marzo1 : 0,
                abril1 : 0,
                mayo1: 0,
                junio1 : 0,
                julio1 : 0,
                agosto1 : 0,
                septiembre1 : 0,
                octubre1 : 0,
                noviembre1 : 0,
                diciembre1 : 0,

                cmbEstadisticaMes : [],


            },
            created : function() {
                //alert($("#desde").val());
                //alert($("#hasta").val());
                //alert($("#tipo").val());
                this.parametro = $("#tipo").val();

                if($("#tipo").val() === "periodo"){
                    this.periodo();
                }else if($("#tipo").val()==="mes"){
                        this.meses();
                }else if($("#tipo").val()==="year"){
                        this.year();
                }else if($("#tipo").val()==="semana"){
                        this.semanas();
                }else if($("#tipo").val()==="rango"){
                        this.rango();
                }
                /*
                axios.get('/administracion/estadisticasingresohospitalario/years').then(response => {
                    this.cmbEstadistica= response.data

                this.enero = this.cmbEstadistica[0].enero;
                this.febrero = this.cmbEstadistica[0].febrero;
                this.marzo = this.cmbEstadistica[0].marzo;
                this.abril = this.cmbEstadistica[0].abril;
                this.mayo = this.cmbEstadistica[0].mayo;
                this.junio = this.cmbEstadistica[0].junio;
                this.julio = this.cmbEstadistica[0].julio;
                this.agosto = this.cmbEstadistica[0].agosto;
                this.septiembre = this.cmbEstadistica[0].septiembre;
                this.octubre = this.cmbEstadistica[0].octubre;
                this.noviembre = this.cmbEstadistica[0].noviembre;
                this.diciembre = this.cmbEstadistica[0].dicimebre;

                this.enero1 = this.cmbEstadistica[0].enero1;
                this.febrero1 = this.cmbEstadistica[0].febrero1;
                this.marzo1 = this.cmbEstadistica[0].marzo1;
                this.abril1 = this.cmbEstadistica[0].abril1;
                this.mayo1 = this.cmbEstadistica[0].mayo1;
                this.junio1 = this.cmbEstadistica[0].junio1;
                this.julio1 = this.cmbEstadistica[0].julio1;
                this.agosto1 = this.cmbEstadistica[0].agosto1;
                this.septiembre1 = this.cmbEstadistica[0].septiembre1;
                this.octubre1 = this.cmbEstadistica[0].octubre1;
                this.noviembre1 = this.cmbEstadistica[0].noviembre1;
                this.diciembre1 = this.cmbEstadistica[0].dicimebre1;

                var data = {
                    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                        "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "rgba(151,187,205,1)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#ff100e",
                            pointHighlightFill: "#493aff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: [
                                this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                                this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre
                            ]

                        },
                        {
                            label: "My Second dataset",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: [this.enero1,this.febrero1,this.marzo1,this.abril1,this.mayo1,this.junio1,
                                this.julio1,this.agosto1,this.septiembre1,this.octubre1,this.noviembre1,this.diciembre1]
                        }


                    ]
                };
                var pdata = [
                    {
                        value: 300,
                        color:"#F7464A",
                        highlight: "#FF5A5E",
                        label: "Red"
                    },
                    {
                        value: 50,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Green"
                    },
                    {
                        value: 100,
                        color: "#FDB45C",
                        highlight: "#FFC870",
                        label: "Yellow"
                    }
                ];

                var ctxl = $("#lineChartDemo").get(0).getContext("2d");
                var lineChart = new Chart(ctxl).Line(data);

            })

                */
            },

            methods : {

                validar : function(){
                    if(this.parametro==="periodo"){
                        this.periodo();
                    }else if(this.parametro==="mes"){
                        if(this.p_mes1 === '' || this.p_mes2 === '' ) {
                            toastr.error("Selecciones los meses para realizar la consulta.")
                        } else{
                            this.meses();
                        }
                    }else if(this.parametro==="year"){
                        if(this.p_year1 === '' || this.p_year2 === '' ) {
                            toastr.error("Selecciones los años , para realizar la consulta.")
                        } else{
                            this.year();
                        }
                    }else if(this.parametro==="semana"){
                        if(this.p_semana1 === '' || this.p_semana2 === '' ) {
                            toastr.error("Selecciones lass semanas , para realizar la consulta.")
                        } else{
                            this.semanas();
                        }
                    }else if(this.parametro==="rango"){
                        if(this.p_rango1 === '' || this.p_rango2 === '' ) {
                            toastr.error("Seleccione el rango de fechas  para realizar la consulta.")
                        } else if(this.p_rango2 < this.p_rango1) {
                            toastr.error("La primera fecha  no puede ser mayor a la segunda.")
                        } else{
                            this.rango();
                        }
                    }
                    //this.periodo()
                },

                meses : function () {

                    axios.get('/administracion/estadisticas/ingresos/hospitalario/mes/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticaMes = response.data
                    this.mes1 = this.cmbEstadisticaMes[0].numero_ingreso_mes1;
                    this.mes2 = this.cmbEstadisticaMes[0].numero_ingreso_mes2;

                    var ctx = document.getElementById('horizontalBarChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Meses"],
                            datasets: [{
                                label: $("#desde").val(),
                                data: [this.mes1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: $("#hasta").val(),
                                    data: [this.mes2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });

                },

                year : function(){
                    axios.get('/administracion/estadisticas/ingresos/hospitalario/años/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticayears = response.data
                    this.year1 = this.cmbEstadisticayears[0].year1;
                    this.year2 = this.cmbEstadisticayears[0].year2;

                    var ctx = document.getElementById('horizontalBarChartY').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["años"],
                            datasets: [{
                                label:  "año: "+this.p_year1,
                                data: [this.year1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "año: "+this.p_year2,
                                    data: [this.year2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                semanas : function() {
                    axios.get('/administracion/estadisticas/ingresos/hospitalario/semanas/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticasemanas = response.data
                    this.semana1 =  this.cmbEstadisticasemanas[0].numero_ingreso_semana1;
                    this.semana2 =  this.cmbEstadisticasemanas[0].numero_ingreso_semana2;

                    var ctx = document.getElementById('horizontalBarChartS').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Semanas"],
                            datasets: [{
                                label:  "semana: "+this.p_semana1,
                                data: [this.semana1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "semana: "+this.p_semana2,
                                    data: [this.semana2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                rango : function() {
                    axios.get('/administracion/estadisticas/ingresos/hospitalario/rangos/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticasemanas = response.data
                    this.rango1 = this.cmbEstadisticasemanas[0].numero_ingreso;


                    var ctx = document.getElementById('horizontalBarChartR').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Total de registros"],
                            datasets: [
                                {
                                    label:  "Desde "+this.p_rango1+'hasta '+this.p_rango2,
                                    data: [this.rango1],

                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                },

                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                periodo : function () {
                    axios.get('/administracion/estadisticasingresohospitalario/years').then(response => {
                        this.cmbEstadistica= response.data

                    this.enero = this.cmbEstadistica[0].enero;
                    this.febrero = this.cmbEstadistica[0].febrero;
                    this.marzo = this.cmbEstadistica[0].marzo;
                    this.abril = this.cmbEstadistica[0].abril;
                    this.mayo = this.cmbEstadistica[0].mayo;
                    this.junio = this.cmbEstadistica[0].junio;
                    this.julio = this.cmbEstadistica[0].julio;
                    this.agosto = this.cmbEstadistica[0].agosto;
                    this.septiembre = this.cmbEstadistica[0].septiembre;
                    this.octubre = this.cmbEstadistica[0].octubre;
                    this.noviembre = this.cmbEstadistica[0].noviembre;
                    this.diciembre = this.cmbEstadistica[0].dicimebre;

                    this.enero1 = this.cmbEstadistica[0].enero1;
                    this.febrero1 = this.cmbEstadistica[0].febrero1;
                    this.marzo1 = this.cmbEstadistica[0].marzo1;
                    this.abril1 = this.cmbEstadistica[0].abril1;
                    this.mayo1 = this.cmbEstadistica[0].mayo1;
                    this.junio1 = this.cmbEstadistica[0].junio1;
                    this.julio1 = this.cmbEstadistica[0].julio1;
                    this.agosto1 = this.cmbEstadistica[0].agosto1;
                    this.septiembre1 = this.cmbEstadistica[0].septiembre1;
                    this.octubre1 = this.cmbEstadistica[0].octubre1;
                    this.noviembre1 = this.cmbEstadistica[0].noviembre1;
                    this.diciembre1 = this.cmbEstadistica[0].dicimebre1;

                    var ctx = document.getElementById('horizontalBarChartP').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                                "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                            datasets: [{
                                label:  "año 2018 ",
                                data: [ this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                                    this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "2017",
                                    data: [this.enero1,this.febrero1,this.marzo1,this.abril1,this.mayo1,this.junio1,
                                        this.julio1,this.agosto1,this.septiembre1,this.octubre1,this.noviembre1,this.diciembre1],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                excel : function() {
                    if(this.parametro === 'mes') {
                        this.desde = this.p_mes1;
                        this.hasta = this.p_mes2;
                    }
                    if(this.parametro === 'periodo') {
                        this.desde = "2017";
                        this.hasta = "2018";
                    }
                    if(this.parametro === 'year') {
                        this.desde = this.p_year1;
                        this.hasta = this.p_year2;
                    }
                    if(this.parametro === 'rango') {
                        this.desde = this.p_rango1;
                        this.hasta = this.p_rango2;
                    }
                    if(this.parametro === 'semana') {
                        this.desde = this.p_semana1;
                        this.hasta = this.p_semana2;
                    }
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "estadisticas/excel/"+this.desde+"/"+this.hasta+"/"+this.parametro;
                    a.click();
                },
                pdf : function(){
                    if(this.parametro === 'mes') {
                        this.desde = this.p_mes1;
                        this.hasta = this.p_mes2;
                    }
                    if(this.parametro === 'periodo') {
                        this.desde = "2017";
                        this.hasta = "2018";
                    }
                    if(this.parametro === 'year') {
                        this.desde = this.p_year1;
                        this.hasta = this.p_year2;
                    }
                    if(this.parametro === 'rango') {
                        this.desde = this.p_rango1;
                        this.hasta = this.p_rango2;
                    }
                    if(this.parametro === 'semana') {
                        this.desde = this.p_semana1;
                        this.hasta = this.p_semana2;
                    }
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "estadisticas/pdf/"+this.desde+"/"+this.hasta+"/"+this.parametro;
                    a.click();
                }
            }

        });


    </script>
@endsection