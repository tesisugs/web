@extends('layouts.principal')
@section('titulo','Detalles de informes')
@section('title','Detalles de informes')
@section('contenido')

    <div class="col-md-12" id="estadistica" >
        <input type="hidden" value="{{$desde}}" id="desde">
        <input type="hidden" value="{{$hasta}}" id="hasta">
        <input type="hidden" value="{{$tipo}}" id="tipo">
        <div class="tile">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tile" v-if="parametro==='periodo'">
                        <h3 class="tile-title">Emergencias - Periodo <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartP"></canvas>
                        </div>

                    </div>
                    <div class="tile" v-if="parametro === 'mes'">
                        <h3 class="tile-title">Emergencias - Meses <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChart"></canvas>
                        </div>
                    </div>

                    <div class="tile" v-if="parametro === 'year'">
                        <h3 class="tile-title">Emergencias - años <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartY"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'semana'">
                        <h3 class="tile-title"> Emergencias -Semanas <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartS"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'rango'">
                        <h3 class="tile-title"> Emergencias - desde <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9" >
                            <canvas class="embed-responsive-item"  id="horizontalBarChartR"></canvas>
                        </div>
                    </div>
                </div>
                <p v-if="parametro === 'mes'" >En el mes de @{{name_mes}}  hubo un total de @{{mes1}}   emergencias comparado con el mes de @{{name_mes2}} que hubo un total de @{{mes2}} emergencias </p>
                <p v-if="parametro === 'year'" >En el año de @{{name_year}}  hubo un total de @{{year1}}   emergencias comparado con el año de @{{name_year2}} que hubo un total de @{{year2}} emergencias</p>
                <p v-if="parametro === 'rango'" > Desde @{{name_rango1}} hasta @{{name_rango2}} hubo un total de @{{rango1}} emergencias</p>
                <p v-if="parametro === 'semana'" > La semana numero  @{{name_semana}} hubo un total de @{{semana1}} emergencias comparado con la semana numero @{{name_semana2}} hubo un total de @{{semana2}} emergencias </p>
                <div v-if="periodo">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Meses</th>
                            <th>Año 2018</th>
                            <th>año 2017</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> Enero </td>
                            <th>@{{enero}}</th>
                            <th>@{{enero1 }}</th>
                        </tr>
                        <tr>
                            <td> Febrero </td>
                            <td>@{{febrero}}</td>
                            <td>@{{febrero1 }}</td>
                        </tr>
                        <tr>
                            <td> Marzo </td>
                            <td>@{{marzo}}</td>
                            <td>@{{marzo1 }}</td>
                        </tr>
                        <tr>
                            <td> Abril </td>
                            <td>@{{abril}}</td>
                            <td>@{{ abril1}}</td>
                        </tr>
                        <tr>
                            <td> Mayo </td>
                            <td>@{{mayo}}</td>
                            <td>@{{mayo1 }}</td>
                        </tr>
                        <tr>
                            <td> Junio </td>
                            <td>@{{junio}}</td>
                            <td>@{{junio1 }}</td>
                        </tr>
                        <tr>
                            <td> Julio </td>
                            <td>@{{julio}}</td>
                            <td>@{{julio1 }}</td>
                        </tr>
                        <tr>
                            <td> Agosto </td>
                            <td>@{{agosto}}</td>
                            <td>@{{ agosto1}}</td>
                        </tr>
                        <tr>
                            <td> Septiembre </td>
                            <td>@{{septiembre}}</td>
                            <td>@{{septiembre1 }}</td>
                        </tr>
                        <tr>
                            <td> Octubre</td>
                            <td>@{{octubre}}</td>
                            <td>@{{octubre1}}</td>
                        </tr>
                        <tr>
                            <td> Noviembre  </td>
                            <td>@{{noviembre}}</td>
                            <td>@{{noviembre1}}</td>
                        </tr>
                        <tr>
                            <td> Diciembre </td>
                            <td>@{{diciembre}}</td>
                            <td>@{{diciembre1}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>



@endsection

@section('script')
    <!-- <script type="text/javascript"  src ="{{asset('js/plugins/chart.js')}}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script>
        var app = new Vue ({
            el:'#estadistica',
            data : {

                // variables para la busqueda
                parametro : '',
                mes1 : '',
                mes2 : '',
                p_mes1 : '',
                p_mes2 : '',
                cmbEstadisticayears : [],
                year1 : '',
                year2 : '',
                p_year1 : '',
                p_year2 : '',
                año2 : '',

                cmbEstadisticaweeks : [],
                p_semana1 : '',
                p_semana2 : '',
                semana1 : '',
                semana2 : '',

                cmbEstadisticasrango : [],
                p_rango1 :'',
                p_rango2 :'',
                rango1 :'',
                rango2 :'',


                //
                cmbEstadistica : [],
                // meses del 1 grafico
                enero : 0,
                febrero : 0,
                marzo : 0,
                abril : 0,
                mayo : 0,
                junio : 0,
                julio : 0,
                agosto : 0,
                septiembre : 0,
                octubre : 0,
                noviembre : 0,
                diciembre : 0,
                enero1 : 0,
                febrero1 : 0,
                marzo1 : 0,
                abril1 : 0,
                mayo1: 0,
                junio1 : 0,
                julio1 : 0,
                agosto1 : 0,
                septiembre1 : 0,
                octubre1 : 0,
                noviembre1 : 0,
                diciembre1 : 0,

                cmbEstadisticaMes : [],

                name_mes : '',
                name_mes2 : '',
                name_year : '',
                name_year2 : '',
                name_semana : '',
                name_semana2 : '',
                name_rango1 : '',
                name_rango2 : '',
                name_periodo : '',
                name_periodo :'',




            },
            created : function() {
                //alert($("#desde").val());
                //alert($("#hasta").val());
                //alert($("#tipo").val());
                this.parametro = $("#tipo").val();

                if($("#tipo").val() === "periodo"){
                    this.periodo();
                }else if($("#tipo").val()==="mes"){
                    this.meses();
                }else if($("#tipo").val()==="year"){
                    this.year();
                }else if($("#tipo").val()==="semana"){
                    this.semanas();
                }else if($("#tipo").val()==="rango"){
                    this.rango();
                }
            },

            methods : {

                validar : function(){
                    if(this.parametro==="periodo"){
                        this.periodo();
                    }else if(this.parametro==="mes"){
                        if(this.p_mes1 === '' || this.p_mes2 === '' ) {
                            toastr.error("Selecciones los meses para realizar la consulta.")
                        } else{
                            this.meses();
                        }
                    }else if(this.parametro==="year"){
                        if(this.p_year1 === '' || this.p_year2 === '' ) {
                            toastr.error("Selecciones los años , para realizar la consulta.")
                        } else{
                            this.year();
                        }
                    }else if(this.parametro==="semana"){
                        if(this.p_semana1 === '' || this.p_semana2 === '' ) {
                            toastr.error("Selecciones lass semanas , para realizar la consulta.")
                        } else{
                            this.semanas();
                        }
                    }else if(this.parametro==="rango"){
                        if(this.p_rango1 === '' || this.p_rango2 === '' ) {
                            toastr.error("Seleccione el rango de fechas  para realizar la consulta.")
                        } else if(this.p_rango2 < this.p_rango1) {
                            toastr.error("La primera fecha  no puede ser mayor a la segunda.")
                        } else{
                            this.rango();
                        }
                    }
                    //this.periodo()
                },

                meses : function () {
                    var mes1 = $("#desde").val();
                    var porciones = mes1.split('-');

                    if(porciones[1] == '01'){
                        this.name_mes = "Enero";
                    } else if(porciones[1] == '02') {
                        this.name_mes = "Febrero";
                    }else if(porciones[1] == '03') {
                        this.name_mes = "Marzo";
                    }else if(porciones[1] == '04') {
                        this.name_mes = "Abril";
                    }else if(porciones[1] == '05') {
                        this.name_mes = "Mayo";
                    }else if(porciones[1] == '06') {
                        this.name_mes = "Junio";
                    }else if(porciones[1] == '07') {
                        this.name_mes = "julio";
                    }else if(porciones[1] == '08') {
                        this.name_mes = "Agosto";
                    }else if(porciones[1] == '09') {
                        this.name_mes = "Septiembre";
                    }else if(porciones[1] == '10') {
                        this.name_mes = "Octubre";
                    }else if(porciones[1] == '11') {
                        this.name_mes = "Noviembre";
                    }else if(porciones[1] == '12') {
                        this.name_mes = "Diciembre";
                    }
                    this.name_mes = this.name_mes+' del '+porciones[0];
                    var mes2 = $("#hasta").val();
                    var porciones = mes2.split('-');

                    if(porciones[1] == '01'){
                        this.name_mes2 = "Enero";
                    } else if(porciones[1] == '02') {
                        this.name_mes2 = "Febrero";
                    }else if(porciones[1] == '03') {
                        this.name_mes2 = "Marzo";
                    }else if(porciones[1] == '04') {
                        this.name_mes2 = "Abril";
                    }else if(porciones[1] == '05') {
                        this.name_mes2 = "Mayo";
                    }else if(porciones[1] == '06') {
                        this.name_mes2 = "Junio";
                    }else if(porciones[1] == '07') {
                        this.name_mes2 = "julio";
                    }else if(porciones[1] == '08') {
                        this.name_mes2 = "Agosto";
                    }else if(porciones[1] == '09') {
                        this.name_mes2 = "Septiembre";
                    }else if(porciones[1] == '10') {
                        this.name_mes2 = "Octubre";
                    }else if(porciones[1] == '11') {
                        this.name_mes2 = "Noviembre";
                    }else if(porciones[1] == '12') {
                        this.name_mes2 = "Diciembre";
                    }
                    this.name_mes2 = this.name_mes2+' del '+porciones[0];

                    axios.get('/administracion/estadisticas/emergencias/mes/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticaMes = response.data
                    this.mes1 = this.cmbEstadisticaMes[0].numero_ingreso_mes1;
                    this.mes2 = this.cmbEstadisticaMes[0].numero_ingreso_mes2;

                    var ctx = document.getElementById('horizontalBarChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Meses"],
                            datasets: [{
                                label: $("#desde").val(),
                                data: [this.mes1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: $("#hasta").val(),
                                    data: [this.mes2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                    // window.print();
                },

                year : function(){
                    this.name_year = $("#desde").val();
                    this.name_year2 = $("#hasta").val();
                    axios.get('/administracion/estadisticas/emergencias/años/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticayears = response.data
                    this.year1 = this.cmbEstadisticayears[0].year1;
                    this.year2 = this.cmbEstadisticayears[0].year2;

                    var ctx = document.getElementById('horizontalBarChartY').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["años"],
                            datasets: [{
                                label:  "año: "+this.name_year,
                                data: [this.year1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "año: "+this.name_year2,
                                    data: [this.year2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                    window.print();
                },

                semanas : function() {
                    this.name_semana = this.nombreSemana($("#desde").val());
                    this.name_semana = this.name_semana+'('+$("#desde").val()+')';
                    this.name_semana2 = this.nombreSemana($("#hasta").val());
                    this.name_semana2 = this.name_semana+'('+$("#hasta").val()+')';

                    alert(this.nombreSemana($("#desde").val()));
                    axios.get('/administracion/estadisticas/emergencias/semanas/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticasemanas = response.data
                    this.semana1 =  this.cmbEstadisticasemanas[0].numero_ingreso_semana1;
                    this.semana2 =  this.cmbEstadisticasemanas[0].numero_ingreso_semana2;

                    var ctx = document.getElementById('horizontalBarChartS').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Semanas"],
                            datasets: [{
                                label:  "semana: "+this.name_semana,
                                data: [this.semana1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "semana: "+this.name_semana2,
                                    data: [this.semana2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                    window.print();
                },

                nombreSemana : function(semana) {
                    var now=new Date(semana),i=0,f,sem=(new Date(now.getFullYear(), 0,1).getDay()>0)?1:0;
                    while( (f=new Date(now.getFullYear(), 0, ++i)) < now ){
                        if(!f.getDay()){
                            sem++;
                        }
                    }
                    return sem;
                },

                rango : function() {
                    this.name_rango1 = $("#desde").val();
                    this.name_rango2 = $("#hasta").val();
                    axios.get('/administracion/estadisticas/emergencias/rangos/'+$("#desde").val()+'/'+$("#hasta").val()+'').then(response => {
                        this.cmbEstadisticasemanas = response.data
                    this.rango1 = this.cmbEstadisticasemanas[0].numero_ingreso;


                    var ctx = document.getElementById('horizontalBarChartR').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Total de registros"],
                            datasets: [
                                {
                                    label:  "Desde "+this.name_rango1+' hasta '+this.name_rango2,
                                    data: [this.rango1],

                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                },

                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                    window.print();
                },

                periodo : function () {

                    axios.get('/administracion/estadisticas/emergencias/years/'+2018+'/'+2017).then(response => {
                        this.cmbEstadistica= response.data

                    this.enero = this.cmbEstadistica[0].enero;
                    this.febrero = this.cmbEstadistica[0].febrero;
                    this.marzo = this.cmbEstadistica[0].marzo;
                    this.abril = this.cmbEstadistica[0].abril;
                    this.mayo = this.cmbEstadistica[0].mayo;
                    this.junio = this.cmbEstadistica[0].junio;
                    this.julio = this.cmbEstadistica[0].julio;
                    this.agosto = this.cmbEstadistica[0].agosto;
                    this.septiembre = this.cmbEstadistica[0].septiembre;
                    this.octubre = this.cmbEstadistica[0].octubre;
                    this.noviembre = this.cmbEstadistica[0].noviembre;
                    this.diciembre = this.cmbEstadistica[0].dicimebre;

                    this.enero1 = this.cmbEstadistica[0].enero1;
                    this.febrero1 = this.cmbEstadistica[0].febrero1;
                    this.marzo1 = this.cmbEstadistica[0].marzo1;
                    this.abril1 = this.cmbEstadistica[0].abril1;
                    this.mayo1 = this.cmbEstadistica[0].mayo1;
                    this.junio1 = this.cmbEstadistica[0].junio1;
                    this.julio1 = this.cmbEstadistica[0].julio1;
                    this.agosto1 = this.cmbEstadistica[0].agosto1;
                    this.septiembre1 = this.cmbEstadistica[0].septiembre1;
                    this.octubre1 = this.cmbEstadistica[0].octubre1;
                    this.noviembre1 = this.cmbEstadistica[0].noviembre1;
                    this.diciembre1 = this.cmbEstadistica[0].dicimebre1;

                    var ctx = document.getElementById('horizontalBarChartP').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                                "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                            datasets: [{
                                label:  "año 2018 ",
                                data: [ this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                                    this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "Año 2017",
                                    data: [this.enero1,this.febrero1,this.marzo1,this.abril1,this.mayo1,this.junio1,
                                        this.julio1,this.agosto1,this.septiembre1,this.octubre1,this.noviembre1,this.diciembre1],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                    // window.print();
                },

                excel : function() {
                    if(this.parametro === 'mes') {
                        this.desde = this.p_mes1;
                        this.hasta = this.p_mes2;
                    }
                    if(this.parametro === 'periodo') {
                        this.desde = "2017";
                        this.hasta = "2018";
                    }
                    if(this.parametro === 'year') {
                        this.desde = this.p_year1;
                        this.hasta = this.p_year2;
                    }
                    if(this.parametro === 'rango') {
                        this.desde = this.p_rango1;
                        this.hasta = this.p_rango2;
                    }
                    if(this.parametro === 'semana') {
                        this.desde = this.p_semana1;
                        this.hasta = this.p_semana2;
                    }
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "estadisticas/excel/"+this.desde+"/"+this.hasta+"/"+this.parametro;
                    a.click();
                },
                pdf : function(){
                    if(this.parametro === 'mes') {
                        this.desde = this.p_mes1;
                        this.hasta = this.p_mes2;
                    }
                    if(this.parametro === 'periodo') {
                        this.desde = "2017";
                        this.hasta = "2018";
                    }
                    if(this.parametro === 'year') {
                        this.desde = this.p_year1;
                        this.hasta = this.p_year2;
                    }
                    if(this.parametro === 'rango') {
                        this.desde = this.p_rango1;
                        this.hasta = this.p_rango2;
                    }
                    if(this.parametro === 'semana') {
                        this.desde = this.p_semana1;
                        this.hasta = this.p_semana2;
                    }
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "estadisticas/pdf/"+this.desde+"/"+this.hasta+"/"+this.parametro;
                    a.click();
                }
            }

        });


    </script>
    <script> //window.print()</script>
@endsection
