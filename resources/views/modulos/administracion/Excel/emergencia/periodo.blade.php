<table>
<thead>
<tr>
    <th>id</th>
    <th>numero_atencion</th>
    <th>afiliado</th>
    <th>paciente</th>
    <th>fecha</th>
    <th>hora</th>
    <th>responsable</th>
    <th>observacion</th>
    <th>doctor</th>
    <th>des_campo1</th>
    <th>des_campo2</th>
    <th>des_campo3</th>
    <th>usuario_ingreso</th>
    <th>fecha_ingreso</th>
    <th>usuario_modificacion</th>
    <th>fecha_modificacion</th>
    <th>pcname</th>
    <th>status</th>
    <th>tipo_ingreso</th>
    <th>fuente_informacion</th>
    <th>persona_entrega</th>
    <th>derivacion</th>
    <th>hospital</th>
    <th>cedula_persona_entrega</th>
    <th>tipo_acompañante</th>
</tr>
</thead>
<tbody>
@foreach($resultados as $resultado)
    <tr>
        <td>{{ $resultado->id}} </td>
        <td>{{ $resultado->numero_atencion}} </td>
        <td>{{ $resultado->afiliado}} </td>
        <td>{{ $resultado->paciente}} </td>
        <td>{{ $resultado->fecha}} </td>
        <td>{{ $resultado->hora}} </td>
        <td>{{ $resultado->responsable}} </td>
        <td>{{ $resultado->observacion}} </td>
        <td>{{ $resultado->doctor}} </td>
        <td>{{ $resultado->des_campo1}} </td>
        <td>{{ $resultado->des_campo2}} </td>
        <td>{{ $resultado->des_campo3}} </td>
        <td>{{ $resultado->usuario_ingreso}} </td>
        <td>{{ $resultado->fecha_ingreso}} </td>
        <td>{{ $resultado->usuario_modificacion}} </td>
        <td>{{ $resultado->fecha_modificacion}} </td>
        <td>{{ $resultado->pcname}} </td>
        <td>{{ $resultado->status}} </td>
        <td>{{ $resultado->tipo_ingreso}} </td>
        <td>{{ $resultado->fuente_informacion}} </td>
        <td>{{ $resultado->persona_entrega}} </td>
        <td>{{ $resultado->derivacion}} </td>
        <td>{{ $resultado->hospital}} </td>
        <td>{{ $resultado->cedula_persona_entrega}} </td>
        <td>{{ $resultado->tipo_acompañante }} </td>
    </tr>
@endforeach
</tbody>
</table>