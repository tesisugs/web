<table>
<thead>
<tr>
    <th> id</th>
    <th> cedula</th>
    <th> tipo_identificacion</th>
    <th> primer_nombre</th>
    <th> segundo_nombre</th>
    <th> apellido_paterno</th>
    <th> apellido_materno</th>
    <th> genero</th>
    <th> fecha_nacimiento</th>
    <th> lugar_nacimiento</th>
    <th> ocupacion</th>
    <th> estado_civil</th>
    <th> tipo_sangre</th>
    <th> nacionalidad</th>
    <th> pais</th>
    <th> provincia</th>
    <th> ciudad</th>
    <th> direccion</th>
    <th> telefono</th>
    <th> celular</th>
    <th> otro</th>
    <th> observacion</th>
    <th> lugar_trabajo</th>
    <th> tipo_parentesco</th>
    <th> tipo_beneficiario</th>
    <th> cedula_titular</th>
    <th> tipo_seguro</th>
    <th> des_campo1</th>
    <th> des_campo2</th>
    <th> des_campo3</th>
    <th> usuario_ingreso</th>
    <th> fecha_ingreso</th>
    <th> usuario_modificacion</th>
    <th> fecha_modificacion</th>
    <th> pcname</th>
    <th> status</th>
    <th> status_discapacidad</th>
    <th> carnet_conadis</th>
    <th> status_otro_seguro</th>
    <th> tipo_seguro_iess</th>
    <th> descripcion_otro_seguro</th>
    <th> etnico</th>
    <th> parroquia

</tr>
</thead>
<tbody>
@foreach($resultados as $resultado)
    <tr>
        <td>{{ $resultado->id}} </td>
        <td>{{ $resultado->cedula}} </td>
        <td>{{ $resultado->tipo_identificacion}} </td>
        <td>{{ $resultado->primer_nombre}} </td>
        <td>{{ $resultado->segundo_nombre}} </td>
        <td>{{ $resultado->apellido_paterno}} </td>
        <td>{{ $resultado->apellido_materno}} </td>
        <td>{{ $resultado->genero}} </td>
        <td>{{ $resultado->fecha_nacimiento}} </td>
        <td>{{ $resultado->lugar_nacimiento}} </td>
        <td>{{ $resultado->ocupacion}} </td>
        <td>{{ $resultado->estado_civil}} </td>
        <td>{{ $resultado->tipo_sangre}} </td>
        <td>{{ $resultado->nacionalidad}} </td>
        <td>{{ $resultado->pais}} </td>
        <td>{{ $resultado->provincia}} </td>
        <td>{{ $resultado->ciudad}} </td>
        <td>{{ $resultado->direccion}} </td>
        <td>{{ $resultado->telefono}} </td>
        <td>{{ $resultado->celular}} </td>
        <td>{{ $resultado->otro}} </td>
        <td>{{ $resultado->observacion}} </td>
        <td>{{ $resultado->lugar_trabajo}} </td>
        <td>{{ $resultado->tipo_parentesco}} </td>
        <td>{{ $resultado->tipo_beneficiario}} </td>
        <td>{{ $resultado->cedula_titular}} </td>
        <td>{{ $resultado->tipo_seguro}} </td>
        <td>{{ $resultado->des_campo1}} </td>
        <td>{{ $resultado->des_campo2}} </td>
        <td>{{ $resultado->des_campo3}} </td>
        <td>{{ $resultado->usuario_ingreso}} </td>
        <td>{{ $resultado->fecha_ingreso}} </td>
        <td>{{ $resultado->usuario_modificacion}} </td>
        <td>{{ $resultado->fecha_modificacion}} </td>
        <td>{{ $resultado->pcname}} </td>
        <td>{{ $resultado->status}} </td>
        <td>{{ $resultado->status_discapacidad}} </td>
        <td>{{ $resultado->carnet_conadis}} </td>
        <td>{{ $resultado->status_otro_seguro}} </td>
        <td>{{ $resultado->tipo_seguro_iess}} </td>
        <td>{{ $resultado->descripcion_otro_seguro}} </td>
        <td>{{ $resultado->etnico}} </td>
        <td>{{ $resultado->parroquia: }>
    </tr>
@endforeach
</tbody>
</table>