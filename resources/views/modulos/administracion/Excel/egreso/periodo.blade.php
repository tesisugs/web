 <table>
<thead>
<tr>
    <th>id </th>
    <th>paciente</th>
    <th>fecha_ingreso</th>
    <th>fecha_salida</th>
    <th>hora_ingreso</th>
    <th>hora_salida</th>
    <th>codigo_cama</th>
    <th>procedencia</th>
    <th>dias_trascurridos</th>
    <th>horas_transcurridas</th>
    <th>valor</th>
    <th>des_campo1</th>
    <th>des_campo2</th>
    <th>des_campo3</th>
    <th>usuario_ingreso</th>
    <th>fecha_ingreso1</th>
    <th>usuario_modificacion</th>
    <th>fecha_modificacion</th>
    <th>pcname</th>
    <th>status</th>
    <th>observacion<th>
</tr>
</thead>
<tbody>
@foreach($resultados as $resultado)
<tr>
<td>{{ $resultado->id }} </td>
<td>{{ $resultado->paciente }} </td>
<td>{{ $resultado->fecha_ingreso }} </td>
<td>{{ $resultado->fecha_salida }} </td>
<td>{{ $resultado->hora_ingreso }} </td>
<td>{{ $resultado->hora_salida }} </td>
<td>{{ $resultado->codigo_cama }} </td>
<td>{{ $resultado->procedencia }} </td>
<td>{{ $resultado->dias_trascurridos }} </td>
<td>{{ $resultado->horas_transcurridas }} </td>
<td>{{ $resultado->valor }} </td>
<td>{{ $resultado->des_campo1 }} </td>
<td>{{ $resultado->des_campo2 }} </td>
<td>{{ $resultado->des_campo3 }} </td>
<td>{{ $resultado->usuario_ingreso }} </td>
<td>{{ $resultado->fecha_ingreso1 }} </td>
<td>{{ $resultado->usuario_modificacion }} </td>
<td>{{ $resultado->fecha_modificacion }} </td>
<td>{{ $resultado->pcname }} </td>
<td>{{ $resultado->status }} </td>
<td>{{ $resultado->observacion }}  </td>
</tr>
@endforeach
</tbody>
</table>

