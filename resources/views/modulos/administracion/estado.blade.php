@extends('layouts.principal')
@section('titulo','Indicadores')
@section('title','Indicadores')
@section('contenido')


    <!-- Indicators-->
    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                </div>
            </div>
        </div>

        <div class="row" id="estado">

            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                        <h4>Camas Habilitadas</h4>
                        <p > @{{habilitada}} </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-success">
                        <h4>Camas ocupadas</h4>
                        <p >@{{ocupada}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-info">
                        <h4>Camas en desinfeccion</h4>
                        <p > @{{desinfectada}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-info">
                        <h4>Camas averiadas</h4>
                        <p > @{{averiada}} </p>
                    </div>
                </div>
            </div>

            <hr>

            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                        <h4>Registros por emergencia</h4>
                        <p> @{{ emergencias }} </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                        <h4>Registro de pacientes</h4>
                        <p> @{{pacientes}} </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                        <h4>Ingresos en hospitalizacion</h4>
                        <p> @{{hospitalizacion}}</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                        <h4>Egresos en hospitalizacions</h4>
                        <p>@{{egresos}} </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="bs-component">
                    <div class="alert alert-dismissible alert-danger">
                        <h4>Registro consulta externa</h4>
                        <p>@{{CExterna}}</p>
                    </div>
                </div>
            </div>
        </div>


@endsection

@section('script')
            <script>
                var app = new Vue ({
                    el:'#estado',
                    data : {

                        cmbEstadistica : [],
                        habilitada : 0,
                        ocupada : 0,
                        desinfectada : 0,
                        averiada : 0,
                        pacientes : 0 ,
                        hospitalizacion: 0 ,
                        emergencias: 0 ,
                        egresos: 0 ,
                        CExterna: 0 ,
                    },
                    created : function() {
                        axios.get('/administracion/estadocamas').then(response => {
                            this.cmbEstadistica= response.data
                        this.habilitada = this.cmbEstadistica[0].Habilitada;
                        this.ocupada = this.cmbEstadistica[0].ocupada;
                        this.desinfectada = this.cmbEstadistica[0].desinfeccion;
                        this.averiada = this.cmbEstadistica[0].averiada;
                    })
                        axios.get('/indicadores/administracion').then(response => {
                            this.cmbEstadistica= response.data
                        this.pacientes = this.cmbEstadistica[0].pacientes;
                        this.emergencias = this.cmbEstadistica[0].emergencias;
                        this.hospitalizacion = this.cmbEstadistica[0].hospitalizacion;
                        this.egresos = this.cmbEstadistica[0].egresos;
                        this.CExterna = this.cmbEstadistica[0].CExterna;
                    })
                    },

                    methods : {

                    }

                });


            </script>
@endsection