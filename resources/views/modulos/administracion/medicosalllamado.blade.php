@extends('layouts.principal')
@section('titulo','Médicos al llamado')
@section('title','Médicos al llamado')
@section('contenido')
    <div class="col-md-12" id="medicosLLamado">
        <div class="tile">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#correo">ENVIO DE CORREO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#lista_medico">LISTA MÉDICO</a>
                </li>
            </ul>

            <!-- tabs content -->
            <div class="tab-content">
                <div id="correo" class="container tab-pane active"><br>
                    <div class="row" v-if="enviando === false">
                        <div class="col-lg-6">

                            <div class="form-group row">
                                <label class="control-label col-md-2">Hc</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" placeholder="presione enter para realizar la busqueda" v-on:keyup.13="modalPaciente" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Paciente</label>
                                <div class="col-md-10">
                                    <input class="form-control" v-model="paciente" type="text" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">V. Admision</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="seguro" v-model="tipo_seguro" :value="tipo_seguro" disabled>

                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="control-label col-md-2">Medico</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="medico" v-model="medico" v-on:change="especialidadMedico" >

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2" >Especializacion</label>
                                <div class="col-md-10">
                                    <input class="form-control" v-model="especializacion" type="text" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Correo</label>
                                <div class="col-md-10">
                                    <input type="text" name="correo" v-model="correo"  class="form-control" readonly>
                                </div>
                            </div>
                        </div>

                            <button class=" form-control btn btn-primary" type="button" v-on:click="envioCorreo">Enviar Correo</button>

                    </div>

                    <div class="row" v-if="enviando===true">
                        <div class="col-md-12">
                            <div class="tile">
                                <div class="overlay">
                                    <div class="m-loader mr-4">
                                        <svg class="m-circular" viewBox="25 25 50 50">
                                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"/>
                                        </svg>
                                    </div>
                                    <h3 class="l-text">Enviando</h3>
                                </div>
                                <div class="tile-title-w-btn">
                                    <h3 class="title">Medicos al llamado</h3>
                                </div>
                                <div class="tile-body">
                                    <b>Su correo esta siendo enviado </b><br>
                                    Espere unos momentos por favor.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="lista_medico" class="container tab-pane "><br>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="control-label">Desde</label>
                            <input class="form-control" type="date" v-model="desde">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">Hasta</label>
                            <input class="form-control" type="date" v-model="hasta">
                        </div>
                        <div class="form-group col-md-2 align-self-end">
                            <button class="btn btn-primary" type="button" v-on:click="consultarEnvioCorreos"><i class="fa fa-fw fa-lg fa-check-circle"></i>Consultar</button>
                        </div>
                        <div class="form-group col-md-2 align-self-end" v-if="registro === true">
                            <button class="btn btn-primary btn-lg" v-on:click="descargarExcel"><i class="fa fa-file-excel-o fa-5x" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <hr>
                    <div class="tile">
                        <h4 class="tile-title">Resultados de de busqueda</h4>
                        <table class="table table-sm" v-if="mensaje_registro === false">
                            <thead>
                            <tr>
                                <th>*</th>
                                <th>*</th>
                                <th>PACIENTE</th>
                                <th>MEDICO</th>
                                <th>ESPECIALIDAD</th>
                                <th>FECHA</th>
                                <th>HORA</th>
                                <th>SEGURO</th>
                                <th>RESPUESTA</th>
                                <th>DESCRIPCION</th>
                                <th>USUARIO</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="data in correosEnviados">
                                <td><i class="fa fa-pencil-square-o" aria-hidden="true" @click ="actualizarModal(data.id)"></i></td>
                                <td><i class="fa fa-eye" aria-hidden="true" ></i></td>
                                <td> @{{data.PACIENTE}} </td>
                                <td>@{{data.MEDICO}} </td>
                                <td>@{{data.ESPECIALIDAD}} </td>
                                <td>@{{data.FECHA}} </td>
                                <td>@{{data.HORA}} </td>
                                <td>@{{data.FINANCIADOR}} </td>
                                <td>@{{data.RESPUESTA_2}} </td>
                                <td>@{{data.DESCRIPCION}} </td>
                                <td>@{{data.USUARIO}} </td>
                            </tr>

                            </tbody>
                        </table>
                        <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            No existen registros en las fecha seleccionada
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="modal face" id="buscarPaciente" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        CONSULTAR PACIENTE
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="form-control col-md-4"  placeholder="apellido" v-model="paciente" v-on:keyup.13="validarConsultaInfoPacientes" >
                                <label><input type="checkbox" class="form-control col-md-2" v-model="privado"> Pacientes Privados </label>
                            </div>
                            <p class="text-info">información</p>
                            <hr>
                            <!-- PACIENTES GENRALES -->
                            <table class="table-bordered" v-if="privado === false">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO PACIENTE</td>
                                    <td>REGISTRO ADMISION</td>
                                    <td>PACIENTE</td>
                                    <td>RESPONSABLE</td>
                                    <td>FECHA ASISTENCIA</td>
                                    <td>HORA ASISTENCIA</td>
                                    <td>MEDICO</td>

                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true"></i>  </td>
                                    <td> @{{dato.CodPaciente}}</td>
                                    <td>@{{dato.RegistroAdmision}}</td>
                                    <td>@{{dato.Paciente}}</td>
                                    <td>@{{dato.responsable  }}</td>
                                    <td>@{{dato.FechaAsistencia }}</td>
                                    <td>@{{dato.HoraAsistencia}}</td>
                                    <td>@{{dato.Medico}}</td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- PACIENTES PRIVADOS -->
                            <table class="table-bordered" v-if="privado === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO PACIENTE</td>
                                    <td>REGISTRO ADMISION</td>
                                    <td>CEDULA</td>
                                    <td>FECHA INGRESO</td>
                                    <td>PACIENTE</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" v-on:click="pasarDatosPaciente(dato.Paciente,dato.CodPaciente)"></i>  </td>
                                    <td>@{{dato.CodPaciente}}</td>
                                    <td>@{{dato.RegistroAdmision}}</td>
                                    <td>@{{dato.Cedula}}</td>
                                    <td>@{{dato.FechaIngreso}}</td>
                                    <td>@{{dato.Paciente}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="actualizar2" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Actualizar Registro
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="form-control col-md-4"  placeholder="descripcion">
                            </div>
                            <label><input type="checkbox" class="form-control col-md-2"> SI/NO </label>
                        </div>
                        <hr>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary">Actualizar</button>
                    </div>
                </div>

            </div>

        <!-- Modal -->
        <div class="modal fade" id="actualizar" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Actualizar Registro</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="form-control col-md-12" v-model="descripcion"  placeholder="descripcion">

                            </div>
                            <label><input type="checkbox" id="cbox1" v-model="estado2"> SI/NO</label><br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="actualizar()">Actualizar</button>
                    </div>
                </div>

            </div>
        </div>

    <input type="hidden" id="rpt">
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
        /*
            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medicolist").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#a_medico_list").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#a_especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });


            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#seguroList").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#a_seguro_list").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            }); */

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <script type="text/javascript">
        var app = new Vue ({
            el:'#medicosLLamado',
            data : {

                // --- variable para almacenar los errores

                errores : [],
                // -----
                datosConsulta :[],
                // -----
                // variables de busqueda de paciente-individual
                privado: false,
                paciente :'',
                listaPacientes : [],
                tabla :1,
                paciente_principal : '',
                listaAfiliadosCedula : [],
                especializacion : '',
                medico : 0,
                especializaciones : [],
                paciente_id : 0,
                correo : '',
                tipo_seguro : 0,
                correosEnviados : [],
                desde : '',
                hasta : '',

                registro : false,
                mensaje_registro : false,
                enviando : false,

                descripcion : '',
                estado2 : false,
                id : 0,
            },

            created : function() {


                $.get("medico", function (datos) {
                    $.each(datos, function (key, value) {
                        $("#medico").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                    });
                });

                $.get("especializacion", function (datos) {
                    $.each(datos, function (key, value) {
                        $("#especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                    });
                });


                $.get("seguro", function (datos) {
                    $.each(datos, function (key, value) {
                        $("#seguro").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                    });
                });
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

            },

            methods : {

                validarConsultaInfoPacientes : function () {
                        var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/; // nombres apellidos
                    if(patt3.test(this.paciente) == false)
                    {
                        toastr.error("el nombre del paciente no pueden contener numeros ,caracteres especiales o estar en blanco");
                    }else {
                           this.buscarPaciente();
                        }
                },

                limpiar : function() {
                        this.paciente ='';
                        this.paciente_principal = '';
                        this.especializacion = '';
                        this.medico = 0;
                        this.correo = '';
                        this.tipo_seguro = 0;
                },

                // abrir modal
                modalPaciente : function(){
                    $("#buscarPaciente").modal();
                },

                buscarPaciente : function(){
                    if(this.privado === false)
                    {
                        this.estado = 2 ;
                        // mostrar tabla 1

                    }else if(this.privado === true) {
                        this.estado = 1;

                    }
                    // logica hecha con axios
                    toastr.success("consultando información");
                    var url = 'consultarPacientesNombre/'+this.paciente+'/'+this.estado+'';
                    axios.get(url).then(response => {
                        this.listaPacientes = response.data;
                    })
                },

                especialidadMedico : function () {

                    var url = 'especialidadmedico/'+this.medico+'';
                        axios.get(url).then(response => {
                        this.especializaciones = response.data;
                        this.especializacion = this.especializaciones[0].especializacion;
                        this.correo = this.especializaciones[0].correo;
                    })
                },

                pasarDatosPaciente : function (paciente,codigo) {
                    this.paciente = paciente;
                    var urlUsers = 'consultarpaciente/'+codigo+'';
                    axios.get(urlUsers).then(response => {
                        this.listaAfiliadosCedula  = response.data;
                    this.paciente_id = this.listaAfiliadosCedula[0].id;
                    this.tipo_seguro  = this.listaAfiliadosCedula[0].tipo_seguro;

                })
                    $("#buscarPaciente").modal('hide');
                },

                envio : function(valor){
                    this.enviando = valor;
                },

                actualizar : function () {

                    if(this.descripcion === '' || this.paciente === undefined)
                    {
                        this.errores.push("El campo descripcion no puede estar vacio")
                    }
                    if( this.errores.length === 0) {

                        var parametros = {
                            "_token": "{{ csrf_token() }}",
                            "descripcion" : this.descripcion,
                            "estado" : this.estado2,
                        };
                        $.ajax({
                            data : parametros,
                            url : "updatemedicoalllamado/"+this.id+"",
                            type : "post",
                            async : false,
                            success : function(response){
                                toastr.success('Actualizacion realizada con exito.', 'Exito', {timeOut: 5000});
                                $("#actualizar").modal("hide");
                               this.descripcion = "";
                               this.estado2 = false;
                            },
                            error : function (response,jqXHR) {
                                toastr.error('Error al momento de enviar el correo.', 'Error', {timeOut: 5000});
                            }
                        });
                        this.descripcion = "";
                        this.estado2 = false;

                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                    this.consultarEnvioCorreos();

                },
                actualizarModal : function (id) {
                    this.id = id;
                    $("#actualizar").modal();
                },

                envioCorreo : function () {

                    if(this.paciente === '' || this.paciente === undefined)
                    {
                        this.errores.push("Llene la información del paciente")
                    }

                    if(this.paciente === '' || this.paciente === undefined)
                    {
                        this.errores.push("Llene la información del medico")
                    }

                    if( this.errores.length === 0) {
                        toastr.success("Enviando correo...");
                        this.enviar();
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                    this.consultarEnvioCorreos();
                    //this.envio(false);
                },

                enviar : function () {
                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        //--
                        "paciente_id" : this.paciente_id,
                        "medico" : this.medico,
                        "correo" : this.correo,
                        "seguro" : this.tipo_seguro,
                        "paciente" : this.paciente,
                        "medico_name" : $("#medico option:selected").text() ,
                        "seguro_name" : $("#seguro option:selected").text(),
                    };
                    $.ajax({
                        data : parametros,
                        url : "enviocorreo",
                        type : "post",
                        async : false,
                        success : function(response){
                            if(response[0] === "Error"){
                                toastr.error('Error: '+response[1]+'', 'Error', {timeOut: 5000});
                            }else{
                                toastr.success('Correo enviado.', 'Exito', {timeOut: 5000});
                                $("#rpt").val('id');
                                //this.paciente ='';
                                //this.paciente_principal = '';
                                //this.especializacion = '';
                                //this.medico = 0;
                                //this.correo = '';
                                //this.tipo_seguro = 0;

                            }

                        },
                        error : function (response,jqXHR) {
                            if(response.status === 422)
                            {
                                var errors = $.parseJSON(response.responseText);
                                $.each(errors, function (key, value) {
                                    if($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {
                                            toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                            console.log(key+ " " +value);
                                        });
                                    }else{
                                        toastr.error('Error '+response+' al momento de enviar el correo.', 'Error', {timeOut: 5000});
                                    }
                                });
                            } else {
                                toastr.error('Error: '+response.status+' al momento de enviar el correo.', 'Error', {timeOut: 5000});
                            }
                        }
                    });

                    if($("#rpt").val('id') !== ''){
                        this.paciente ='';
                        this.paciente_principal = '';
                        this.especializacion = '';
                        this.medico = 0;
                        this.correo = '';
                        this.tipo_seguro = 0;
                    }
                },

                consultarEnvioCorreos :function () {

                    if(this.desde === '' && this.hasta === '' )
                    {
                        var dt = new Date();
                        // Display the month, day, and year. getMonth() returns a 0-based number.
                        var month = dt.getMonth()+1;
                        var day = dt.getDate();
                        if( month  < 10) {
                            month = ''+0+month ;
                        }
                        if(day  < 10) {
                            day = ''+0+day ;
                        }
                        var year = dt.getFullYear();
                        this.desde = year+"-"+month+"-"+day+"";
                        this.hasta = year+"-"+month+"-"+day+"";
                        var url = 'correosenviados/'+this.desde+'/'+this.hasta+'';
                        axios.get(url).then(response => {
                            this.correosEnviados = response.data;

                            var num = this.correosEnviados.length;
                            if(num === 0) {
                                this.registro = false;
                                this.mensaje_registro = true;
                            } else {
                                this.registro = true;
                                this.mensaje_registro = false;
                            }
                        })
                    }
                    else if ( (this.desde !== '' && this.hasta === '') || (this.desde === '' && this.hasta !== '')  )
                    {
                        toastr.error('Seleccione la fecha de consulta.', 'Success Alert', {timeOut: 5000});
                    }

                    else if (this.desde !== '' && this.hasta !== '') {
                        var url = 'correosenviados/'+this.desde+'/'+this.hasta+'';
                             axios.get(url).then(response => {
                                this.correosEnviados = response.data;
                        var num = this.correosEnviados.length;
                                if(num === 0) {
                                    this.registro = false;
                                    this.mensaje_registro = true;
                                } else {
                                    this.registro = true;
                                    this.mensaje_registro = false;
                                }
                            })
                    }


                },


                // validación de la consulta para buscar los pacientes
                validarConsulta : function() {

                    if(this.desde === '' || this.desde === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;
                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        if(d < 10) {
                            d = ''+0+d;
                        }
                        this.desde = ''+y+'-'+m+'-'+d+'';

                    }

                    if(this.hasta === '' || this.hasta === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;
                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        if(d < 10) {
                            d = ''+0+d;
                        }
                        this.hasta = ''+y+'-'+m+'-'+d+'';
                    }
                },

                descargarExcel : function() {
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "excel/"+this.desde+"/"+this.hasta+"";
                    a.click();

                }






            }

        });

    </script>



@endsection
