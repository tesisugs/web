@extends('layouts.principal')
@section('titulo')
    <p><a href="{{route('estadisticas.admision')}}">Detalles</a> de atenciones por emergencia</p>
@endsection
@section('title','Detalles de Atenciones Por Emergencia')
@section('contenido')

    <div class="col-md-12" id="estadistica"  v-cloak>
        <div class="tile">
            <div class="row">
                <div class="col-lg-6">
                    <div class="tile" v-if="parametro==='periodo'">
                        <h3 class="tile-title">Atenciones por emergencia - Periodo <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartP"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'mes'">
                        <h3 class="tile-title">Ingresos Hospitalarios - Meses <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChart"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'year'">
                        <h3 class="tile-title">Ingresos Hospitalarios - años <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartY"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'semana'">
                        <h3 class="tile-title">Ingresos Hospitalarios -Semanas <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9">
                            <canvas class="embed-responsive-item" id="horizontalBarChartS"></canvas>
                        </div>
                    </div>
                    <div class="tile" v-if="parametro === 'rango'">
                        <h3 class="tile-title">Ingresos Hospitalarios - desde <span> </span></h3>
                        <div class="embed-responsive embed-responsive-16by9" >
                            <canvas class="embed-responsive-item"  id="horizontalBarChartR"></canvas>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <h3 class="tile-title"> Parametros para visualizar informes</h3>
                    <div class="animated-radio-button">
                        <label><input type="radio" name="parametro" v-model="parametro" value="periodo"><span class="label-text">periodo</span></label>
                        <label><input type="radio" name="parametro" v-model="parametro" value="mes"><span class="label-text">meses</span></label>
                        <label><input type="radio" name="parametro" v-model="parametro" value="year"><span class="label-text">años</span></label>
                        <label><input type="radio" name="parametro" v-model="parametro" value="semana"><span class="label-text">semanas</span></label>
                        <label><input type="radio" name="parametro" v-model="parametro" value="rango"><span class="label-text">Rango de fechas</span></label>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group" v-if="parametro === 'mes'">
                        <h4 class="label-primary">Ingrese los meses que desea comparar</h4>
                        <input class="form-control" type="month" name="mes1" v-model="p_mes1">
                        <input class="form-control" type="month" name="mes2" v-model="p_mes2">
                    </div>
                    <div class="form-group" v-if="parametro === 'year'">
                        <h4>Ingrese los años que desea comparar</h4>
                        <input class="form-control" type="number" name="quantity" v-model="p_year1" min="2000" max="2018">
                        <input class="form-control" type="number" name="quantity" v-model="p_year2" min="2000" max="2018">
                    </div>
                    <div class="form-group" v-if="parametro === 'rango'">
                        <h4>Ingrese el rango de fechas del cual quiere obtener información</h4>
                        <input class="form-control" type="date" v-model="p_rango1">
                        <input class="form-control" type="date" v-model="p_rango2">
                    </div>
                    <div class="form-group" v-if="parametro === 'semana'">
                        <h4>Ingrese las semanas de las cuales quiere obtener información</h4>
                        <input class="form-control" type="date" v-model="p_semana1">
                        <input class="form-control" type="date" v-model="p_semana2">
                        <!--  <input class="form-control" type="date" v-model="p_semana1">
                          <input class="form-control" type="date" v-model="p_semana2"> -->
                    </div>
                    <button class="btn btn-outline-primary" @click="validar">Consultar</button>
                    <button class="btn btn-outline-primary" @click="pdf">Descargar <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
                    <button class="btn btn-outline-primary" @click="excel">Descargar <i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- <script type="text/javascript"  src ="{{asset('js/plugins/chart.js')}}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script>
        var app = new Vue ({
            el:'#estadistica',
            data : {

                // variables para la busqueda
                parametro : '',
                mes1 : '',
                mes2 : '',
                p_mes1 : '',
                p_mes2 : '',
                cmbEstadisticayears : [],
                year1 : '',
                year2 : '',
                p_year1 : '',
                p_year2 : '',
                año2 : '',

                cmbEstadisticaweeks : [],
                p_semana1 : '',
                p_semana2 : '',
                semana1 : '',
                semana2 : '',

                cmbEstadisticasrango : [],
                p_rango1 :'',
                p_rango2 :'',
                rango1 :'',
                rango2 :'',


                //
                cmbEstadistica : [],
                // meses del 1 grafico
                enero : 0,
                febrero : 0,
                marzo : 0,
                abril : 0,
                mayo : 0,
                junio : 0,
                julio : 0,
                agosto : 0,
                septiembre : 0,
                octubre : 0,
                noviembre : 0,
                diciembre : 0,
                enero1 : 0,
                febrero1 : 0,
                marzo1 : 0,
                abril1 : 0,
                mayo1: 0,
                junio1 : 0,
                julio1 : 0,
                agosto1 : 0,
                septiembre1 : 0,
                octubre1 : 0,
                noviembre1 : 0,
                diciembre1 : 0,

                cmbEstadisticaMes : [],


            },
            created : function() {

            },

            methods : {

                validar : function(){
                    if(this.parametro==="periodo"){
                        this.periodo();
                    }else if(this.parametro==="mes"){
                        if(this.p_mes1 === '' || this.p_mes2 === '' ) {
                            toastr.error("Selecciones los meses para realizar la consulta.")
                        } else{
                            this.meses();
                        }
                    }else if(this.parametro==="year"){
                        if(this.p_year1 === '' || this.p_year2 === '' ) {
                            toastr.error("Selecciones los años , para realizar la consulta.")
                        } else{
                            this.year();
                        }
                    }else if(this.parametro==="semana"){
                        if(this.p_semana1 === '' || this.p_semana2 === '' ) {
                            toastr.error("Selecciones lass semanas , para realizar la consulta.")
                        } else{
                            this.semanas();
                        }
                    }else if(this.parametro==="rango"){
                        if(this.p_rango1 === '' || this.p_rango2 === '' ) {
                            toastr.error("Seleccione el rango de fechas  para realizar la consulta.")
                        } else if(this.p_rango2 < this.p_rango1) {
                            toastr.error("La primera fecha  no puede ser mayor a la segunda.")
                        } else{
                            this.rango();
                        }
                    }
                    //this.periodo()
                },
                validarPDF : function() {
                    var year = this.cmbEstadisticayears.length;
                    var semanas = this.cmbEstadisticaweeks.length ;
                    var rango = this.cmbEstadisticasrango.length ;
                    var periodo = this.cmbEstadistica.length ;
                    var mes = this.cmbEstadisticaMes.length ;

                    if(this.parametro==="periodo"){
                        if(periodo===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    } else if(this.parametro==="mes"){
                        if(mes===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else if(this.parametro==="year"){
                        if(year===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else if(this.parametro==="semana"){
                        if(semanas===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else if(this.parametro==="rango") {
                        if(rango===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else {
                        toastr.error("No existen datos para cargar la pagina,seleccione el parametro de busqueda y relice la consulta.");
                    }

                },
                validarExcel : function() {
                    var year = this.cmbEstadisticayears.length;
                    var semanas = this.cmbEstadisticaweeks.length ;
                    var rango = this.cmbEstadisticasrango.length ;
                    var periodo = this.cmbEstadistica.length ;
                    var mes = this.cmbEstadisticaMes.length ;

                    if(this.parametro==="periodo"){
                        if(periodo===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    } else if(this.parametro==="mes"){
                        if(mes===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    }else if(this.parametro==="year"){
                        if(year===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                        }
                        this.excel();
                    }else if(this.parametro==="semana"){
                        if(semanas===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    }else if(this.parametro==="rango") {
                        if(rango===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    } else {
                        toastr.error("No existen datos crear el archivo,seleccione el parametro de busqueda y relice la consulta .")
                    }

                },

                meses : function () {

                    axios.get('/administracion/estadisticas/emergencias/mes/'+this.p_mes1+'/'+this.p_mes2+'').then(response => {
                        this.cmbEstadisticaMes = response.data
                    this.mes1 = this.cmbEstadisticaMes[0].numero_ingreso_mes1;
                    this.mes2 = this.cmbEstadisticaMes[0].numero_ingreso_mes2;

                    var ctx = document.getElementById('horizontalBarChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Meses"],
                            datasets: [{
                                label: this.p_mes1,
                                data: [this.mes1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: this.p_mes2,
                                    data: [this.mes2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });

                },

                year : function(){
                    axios.get('/administracion/estadisticas/emergencias/años/'+this.p_year1+'/'+this.p_year2+'').then(response => {
                        this.cmbEstadisticayears = response.data
                    this.year1 = this.cmbEstadisticayears[0].year1;
                    this.year2 = this.cmbEstadisticayears[0].year2;

                    var ctx = document.getElementById('horizontalBarChartY').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["años"],
                            datasets: [{
                                label:  "año: "+this.p_year1,
                                data: [this.year1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "año: "+this.p_year2,
                                    data: [this.year2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                semanas : function() {
                    axios.get('/administracion/estadisticas/emergencias/semanas/'+this.p_semana1+'/'+this.p_semana2+'').then(response => {
                        this.cmbEstadisticasemanas = response.data
                    this.semana1 =  this.cmbEstadisticasemanas[0].numero_ingreso_semana1;
                    this.semana2 =  this.cmbEstadisticasemanas[0].numero_ingreso_semana2;

                    var ctx = document.getElementById('horizontalBarChartS').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Semanas"],
                            datasets: [{
                                label:  "semana: "+this.p_semana1,
                                data: [this.semana1],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "semana: "+this.p_semana2,
                                    data: [this.semana2],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                rango : function() {
                    axios.get('/administracion/estadisticas/emergencias/rangos/'+this.p_rango1+'/'+this.p_rango2+'').then(response => {
                        this.cmbEstadisticasemanas = response.data
                    this.rango1 = this.cmbEstadisticasemanas[0].numero_ingreso;


                    var ctx = document.getElementById('horizontalBarChartR').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Total de registros"],
                            datasets: [
                                {
                                    label:  "Desde "+this.p_rango1+'hasta '+this.p_rango2,
                                    data: [this.rango1],

                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)'
                                    ],
                                    borderWidth: 1
                                },

                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },

                periodo : function () {
                    axios.get('/administracion/estadisticas/emergencias/years/'+2018+'/'+2017).then(response => {
                        this.cmbEstadistica= response.data

                    this.enero = this.cmbEstadistica[0].enero;
                    this.febrero = this.cmbEstadistica[0].febrero;
                    this.marzo = this.cmbEstadistica[0].marzo;
                    this.abril = this.cmbEstadistica[0].abril;
                    this.mayo = this.cmbEstadistica[0].mayo;
                    this.junio = this.cmbEstadistica[0].junio;
                    this.julio = this.cmbEstadistica[0].julio;
                    this.agosto = this.cmbEstadistica[0].agosto;
                    this.septiembre = this.cmbEstadistica[0].septiembre;
                    this.octubre = this.cmbEstadistica[0].octubre;
                    this.noviembre = this.cmbEstadistica[0].noviembre;
                    this.diciembre = this.cmbEstadistica[0].dicimebre;

                    this.enero1 = this.cmbEstadistica[0].enero1;
                    this.febrero1 = this.cmbEstadistica[0].febrero1;
                    this.marzo1 = this.cmbEstadistica[0].marzo1;
                    this.abril1 = this.cmbEstadistica[0].abril1;
                    this.mayo1 = this.cmbEstadistica[0].mayo1;
                    this.junio1 = this.cmbEstadistica[0].junio1;
                    this.julio1 = this.cmbEstadistica[0].julio1;
                    this.agosto1 = this.cmbEstadistica[0].agosto1;
                    this.septiembre1 = this.cmbEstadistica[0].septiembre1;
                    this.octubre1 = this.cmbEstadistica[0].octubre1;
                    this.noviembre1 = this.cmbEstadistica[0].noviembre1;
                    this.diciembre1 = this.cmbEstadistica[0].dicimebre1;

                    var ctx = document.getElementById('horizontalBarChartP').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                                "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                            datasets: [{
                                label:  "año 2018 ",
                                data: [ this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                                    this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre],

                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)',
                                    'rgba(255,99,132,1)'
                                ],
                                borderWidth: 1
                            },
                                {
                                    label: "2017",
                                    data: [this.enero1,this.febrero1,this.marzo1,this.abril1,this.mayo1,this.junio1,
                                        this.julio1,this.agosto1,this.septiembre1,this.octubre1,this.noviembre1,this.diciembre1],

                                    backgroundColor: [
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                        'rgba(54, 162, 35, 0.2)',
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                        'rgba(255,99,132,1)',
                                    ],
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                });
                },
                validarPDF : function() {
                    var year = this.cmbEstadisticayears.length;
                    var semanas = this.cmbEstadisticaweeks.length ;
                    var rango = this.cmbEstadisticasrango.length ;
                    var periodo = this.cmbEstadistica.length ;
                    var mes = this.cmbEstadisticaMes.length ;

                    if(this.parametro==="periodo"){
                        if(periodo===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    } else if(this.parametro==="mes"){
                        if(mes===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else if(this.parametro==="year"){
                        if(year===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else if(this.parametro==="semana"){
                        if(semanas===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else if(this.parametro==="rango") {
                        if(rango===0){
                            toastr.error("No existen datos para cargar la pagina, relice la consulta.")
                        }else{
                            this.pdf();
                        }
                    }else {
                        toastr.error("No existen datos para cargar la pagina,seleccione el parametro de busqueda y relice la consulta.");
                    }

                },
                validarExcel : function() {
                    var year = this.cmbEstadisticayears.length;
                    var semanas = this.cmbEstadisticaweeks.length ;
                    var rango = this.cmbEstadisticasrango.length ;
                    var periodo = this.cmbEstadistica.length ;
                    var mes = this.cmbEstadisticaMes.length ;

                    if(this.parametro==="periodo"){
                        if(periodo===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    } else if(this.parametro==="mes"){
                        if(mes===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    }else if(this.parametro==="year"){
                        if(year===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                        }
                        this.excel();
                    }else if(this.parametro==="semana"){
                        if(semanas===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    }else if(this.parametro==="rango") {
                        if(rango===0){
                            toastr.error("No existen datos crear el archivo, relice la consulta.")
                        }else{
                            this.excel();
                        }
                    } else {
                        toastr.error("No existen datos crear el archivo,seleccione el parametro de busqueda y relice la consulta .")
                    }

                },

                excel : function() {
                    if(this.parametro === 'mes') {
                        this.desde = this.p_mes1;
                        this.hasta = this.p_mes2;
                    }
                    if(this.parametro === 'periodo') {
                        this.desde = "2017";
                        this.hasta = "2018";
                    }
                    if(this.parametro === 'year') {
                        this.desde = this.p_year1;
                        this.hasta = this.p_year2;
                    }
                    if(this.parametro === 'rango') {
                        this.desde = this.p_rango1;
                        this.hasta = this.p_rango2;
                    }
                    if(this.parametro === 'semana') {
                        this.desde = this.p_semana1;
                        this.hasta = this.p_semana2;
                    }
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "estadisticas/excel/emergencias/"+this.desde+"/"+this.hasta+"/"+this.parametro;
                    a.click();
                },
                pdf : function(){
                    if(this.parametro === 'mes') {
                        this.desde = this.p_mes1;
                        this.hasta = this.p_mes2;
                    }
                    if(this.parametro === 'periodo') {
                        this.desde = "2017";
                        this.hasta = "2018";
                    }
                    if(this.parametro === 'year') {
                        this.desde = this.p_year1;
                        this.hasta = this.p_year2;
                    }
                    if(this.parametro === 'rango') {
                        this.desde = this.p_rango1;
                        this.hasta = this.p_rango2;
                    }
                    if(this.parametro === 'semana') {
                        this.desde = this.p_semana1;
                        this.hasta = this.p_semana2;
                    }
                    var a = document.createElement("a");
                    a.target = "_blank";
                    a.href = "estadisticas/pdf/emergencias"+this.desde+"/"+this.hasta+"/"+this.parametro;
                    a.click();
                }
            }

        });


    </script>
@endsection