@extends('layouts.principal')
@section('titulo','notificaciones')
@section('title','notificaciones')
@section('contenido')
@include('flash::message')
<div class="col-md-12">
    <button class="btn btn-outline-danger"> <a href="{{route('historico.notificaciones')}}"> Ver historico de notificaciones</a>  </button>
</div>
    @foreach ($resultado as $notificacion)
     <br>
     <br>
    <div class="col-md-6">
    <div class="tile">
        <div class="tile-title-w-btn">
            <h3 class="title"> {{$notificacion->titulo}}</h3>
            <p>
                <a class="btn btn-primary icon-btn"
                   href="{{route('notificacion.leida',['id_u' => $notificacion->user_id,'id_n' => $notificacion->notification_id] ) }}">
                    <i class="fa fa-check" aria-hidden="true"></i>Leida
                </a>
            </p>
        </div>
        <div class="tile-body">
            {{$notificacion->descripcion}}
        </div>
        <p>{{$notificacion->fecha_ingreso}}</p>
    </div>
    </div>
    @endforeach



@endsection

