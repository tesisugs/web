@extends('layouts.principal')
@section('titulo')
    <p><a href="{{route('notificaciones.all')}}">Notificaciones</a>/Historial</p>
@endsection
@section('title','Historial notificaciones')
@section('contenido')
    <div class="col-md-8">
        <div class="tile">
            <div class="col-md-12">
                <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Descripcion</th>
                        <th>Fecha</th>

                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($resultado as $dato)
                        <tr>
                            <td>{{$dato->titulo}}</td>
                            <td>{{$dato->descripcion}}</td>
                            <td>{{$dato->fecha_ingreso}}</td>


                        </tr>
                    @empty
                        <p> Error al momento de cargar la información </p>
                    @endforelse


                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable(
            {
                "scrollX": true,
                "language": {
                    search: "Buscar:",
                    "lengthMenu": "Mostrar _MENU_ registros por pagina",
                    "zeroRecords": "Ningun registro encontrado ",
                    "info": "Mostrando pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "Sin registros disponibles",
                    "infoFiltered": "(filtrando de un total de  _MAX_ registros)",
                    paginate: {
                        first:      "Primero",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "Ultimo"
                    },
                }
            }
        );
    </script>


@endsection