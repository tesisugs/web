@extends('layouts.principal')
@section('titulo','Datos')
@section('title','Datos')
@section('contenido')

    <div class="col-md-6" id="estadistica" >
        <div class="tile">
            <h3 class="tile-title">Ingresos Hospitalarios <span> <a href="{{route('administracion.graficos.detalles',['tipo' => 'ingreso'])}}"><i class="fa fa-external-link" aria-hidden="true"></i></a> </span></h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6" id="estadistica2">
        <div class="tile">
            <h3 class="tile-title">Egresos Hospitalarios <span> <a href="{{route('administracion.graficos.detalles',['tipo' => 'egreso'])}}"><i class="fa fa-external-link" aria-hidden="true"></i></a> </span></h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="lineChartDemo2"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6" id="estadistica3">
        <div class="tile">
            <h3 class="tile-title">Registro pacientes <span> <a href="{{route('administracion.graficos.detalles',['tipo' => 'paciente'])}}"><i class="fa fa-external-link" aria-hidden="true"></i></a> </span></h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="registroP"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6" id="estadistica4">
        <div class="tile">
            <h3 class="tile-title">Atenciones por emergecia <span> <a href="{{route('administracion.graficos.detalles',['tipo' => 'emergencia'])}}"><i class="fa fa-external-link" aria-hidden="true"></i></a> </span></h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="AtencionesE"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6" id="estadistica5">
        <div class="tile">
            <h3 class="tile-title">Atenciones de consulta externa <span> <a href="{{route('administracion.graficos.detalles',['tipo' => 'ce'])}}"><i class="fa fa-external-link" aria-hidden="true"></i></a> </span></h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="AtencionesCE"></canvas>
            </div>
        </div>
    </div>






    <!--
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Bar Chart - Egresos hospitalarios <span><i class="fa fa-external-link" aria-hidden="true"></i></span></h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Radar Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="radarChartDemo"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Polar Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="polarChartDemo"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Pie Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title">Doughnut Chart</h3>
            <div class="embed-responsive embed-responsive-16by9">
                <canvas class="embed-responsive-item" id="doughnutChartDemo"></canvas>
            </div>
        </div>
    </div>
    <form method="get" action="{/{route('prueba')}}">
        {/{csrf_field()}}
        <button> pasar a pdf</button>
    </form>

-->
@endsection
@section('script')
<script type="text/javascript"  src ="{{asset('js/plugins/chart.js')}}"></script>
<script>
    var app = new Vue ({
        el:'#estadistica2',
        data : {

            cmbEstadistica : [],
            enero : 0,
            febrero : 0,
            marzo : 0,
            abril : 0,
            mayo : 0,
            junio : 0,
            julio : 0,
            agosto : 0,
            septiembre : 0,
            octubre : 0,
            noviembre : 0,
            diciembre : 0,


        },
        created : function() {
            axios.get('/administracion/estadisticasegresohospitalario').then(response => {
                this.cmbEstadistica= response.data

            this.enero = this.cmbEstadistica[0].enero;
            this.febrero = this.cmbEstadistica[0].febrero;
            this.marzo = this.cmbEstadistica[0].marzo;
            this.abril = this.cmbEstadistica[0].abril;
            this.mayo = this.cmbEstadistica[0].mayo;
            this.junio = this.cmbEstadistica[0].junio;
            this.julio = this.cmbEstadistica[0].julio;
            this.agosto = this.cmbEstadistica[0].agosto;
            this.septiembre = this.cmbEstadistica[0].septiembre;
            this.octubre = this.cmbEstadistica[0].octubre;
            this.noviembre = this.cmbEstadistica[0].noviembre;
            this.diciembre = this.cmbEstadistica[0].dicimebre;

            var data = {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(151,187,205,1)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#ff100e",
                        pointHighlightFill: "#493aff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [
                            this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                            this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre
                        ]

                    },
                    {

                    }


                ]
            };
            var pdata = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                }
            ];

            var ctxl = $("#lineChartDemo2").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data);

            })


        },

        methods : {

            }

    });


</script>

<script>
    var app = new Vue ({
        el:'#estadistica',
        data : {

            cmbEstadistica : [],
            enero : 0,
            febrero : 0,
            marzo : 0,
            abril : 0,
            mayo : 0,
            junio : 0,
            julio : 0,
            agosto : 0,
            septiembre : 0,
            octubre : 0,
            noviembre : 0,
            diciembre : 0,


        },
        created : function() {
            axios.get('/administracion/estadisticasingresohospitalario').then(response => {
                this.cmbEstadistica= response.data

            this.enero = this.cmbEstadistica[0].enero;
            this.febrero = this.cmbEstadistica[0].febrero;
            this.marzo = this.cmbEstadistica[0].marzo;
            this.abril = this.cmbEstadistica[0].abril;
            this.mayo = this.cmbEstadistica[0].mayo;
            this.junio = this.cmbEstadistica[0].junio;
            this.julio = this.cmbEstadistica[0].julio;
            this.agosto = this.cmbEstadistica[0].agosto;
            this.septiembre = this.cmbEstadistica[0].septiembre;
            this.octubre = this.cmbEstadistica[0].octubre;
            this.noviembre = this.cmbEstadistica[0].noviembre;
            this.diciembre = this.cmbEstadistica[0].dicimebre;

            var data = {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(151,187,205,1)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#ff100e",
                        pointHighlightFill: "#493aff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [
                            this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                            this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre
                        ]

                    },
                    {

                    }


                ]
            };
            var pdata = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                }
            ];

            var ctxl = $("#lineChartDemo").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data);

        })


        },

        methods : {

        }

    });


</script>

<script>
    var app = new Vue ({
        el:'#estadistica3',
        data : {

            cmbEstadistica : [],
            enero : 0,
            febrero : 0,
            marzo : 0,
            abril : 0,
            mayo : 0,
            junio : 0,
            julio : 0,
            agosto : 0,
            septiembre : 0,
            octubre : 0,
            noviembre : 0,
            diciembre : 0,


        },
        created : function() {
            axios.get('/administracion/estadisticas/registrospacientes').then(response => {
                this.cmbEstadistica= response.data

            this.enero = this.cmbEstadistica[0].enero;
            this.febrero = this.cmbEstadistica[0].febrero;
            this.marzo = this.cmbEstadistica[0].marzo;
            this.abril = this.cmbEstadistica[0].abril;
            this.mayo = this.cmbEstadistica[0].mayo;
            this.junio = this.cmbEstadistica[0].junio;
            this.julio = this.cmbEstadistica[0].julio;
            this.agosto = this.cmbEstadistica[0].agosto;
            this.septiembre = this.cmbEstadistica[0].septiembre;
            this.octubre = this.cmbEstadistica[0].octubre;
            this.noviembre = this.cmbEstadistica[0].noviembre;
            this.diciembre = this.cmbEstadistica[0].dicimebre;

            var data = {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(151,187,205,1)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#ff100e",
                        pointHighlightFill: "#493aff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [
                            this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                            this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre
                        ]

                    },
                    {

                    }


                ]
            };
            var pdata = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                }
            ];

            var ctxl = $("#registroP").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data);

        })


        },

        methods : {

        }

    });


</script>


<script>
    var app = new Vue ({
        el:'#estadistica4',
        data : {

            cmbEstadistica : [],
            enero : 0,
            febrero : 0,
            marzo : 0,
            abril : 0,
            mayo : 0,
            junio : 0,
            julio : 0,
            agosto : 0,
            septiembre : 0,
            octubre : 0,
            noviembre : 0,
            diciembre : 0,


        },
        created : function() {
            axios.get('/administracion/estadisticas/atenciones/emergencia').then(response => {
                this.cmbEstadistica= response.data

            this.enero = this.cmbEstadistica[0].enero;
            this.febrero = this.cmbEstadistica[0].febrero;
            this.marzo = this.cmbEstadistica[0].marzo;
            this.abril = this.cmbEstadistica[0].abril;
            this.mayo = this.cmbEstadistica[0].mayo;
            this.junio = this.cmbEstadistica[0].junio;
            this.julio = this.cmbEstadistica[0].julio;
            this.agosto = this.cmbEstadistica[0].agosto;
            this.septiembre = this.cmbEstadistica[0].septiembre;
            this.octubre = this.cmbEstadistica[0].octubre;
            this.noviembre = this.cmbEstadistica[0].noviembre;
            this.diciembre = this.cmbEstadistica[0].dicimebre;

            var data = {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(151,187,205,1)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#ff100e",
                        pointHighlightFill: "#493aff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [
                            this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                            this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre
                        ]

                    },
                    {

                    }


                ]
            };
            var pdata = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                }
            ];

            var ctxl = $("#AtencionesE").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data);

        })


        },

        methods : {

        }

    });


</script>

<script>
    var app = new Vue ({
        el:'#estadistica5',
        data : {

            cmbEstadistica : [],
            enero : 0,
            febrero : 0,
            marzo : 0,
            abril : 0,
            mayo : 0,
            junio : 0,
            julio : 0,
            agosto : 0,
            septiembre : 0,
            octubre : 0,
            noviembre : 0,
            diciembre : 0,


        },
        created : function() {
            axios.get('/administracion/estadisticas/atenciones/ce').then(response => {
                this.cmbEstadistica= response.data

            this.enero = this.cmbEstadistica[0].enero;
            this.febrero = this.cmbEstadistica[0].febrero;
            this.marzo = this.cmbEstadistica[0].marzo;
            this.abril = this.cmbEstadistica[0].abril;
            this.mayo = this.cmbEstadistica[0].mayo;
            this.junio = this.cmbEstadistica[0].junio;
            this.julio = this.cmbEstadistica[0].julio;
            this.agosto = this.cmbEstadistica[0].agosto;
            this.septiembre = this.cmbEstadistica[0].septiembre;
            this.octubre = this.cmbEstadistica[0].octubre;
            this.noviembre = this.cmbEstadistica[0].noviembre;
            this.diciembre = this.cmbEstadistica[0].dicimebre;

            var data = {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio",
                    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(151,187,205,1)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#ff100e",
                        pointHighlightFill: "#493aff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [
                            this.enero,this.febrero,this.marzo,this.abril,this.mayo,this.junio,
                            this.julio,this.agosto,this.septiembre,this.octubre,this.noviembre,this.diciembre
                        ]

                    },
                    {

                    }


                ]
            };
            var pdata = [
                {
                    value: 300,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Red"
                },
                {
                    value: 50,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Green"
                },
                {
                    value: 100,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Yellow"
                }
            ];

            var ctxl = $("#AtencionesCE").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data);

        })


        },

        methods : {

        }

    });


</script>





@endsection


















