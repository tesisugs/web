@extends('layouts.principal')
@section('titulo','Agenda Medico')
@section('title','Agenda Medico')
@section('contenido')
    <div class="col-md-12" id="agendaMedico" v-cloak>
        <div class="tile">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-1">
                        <i class="fa fa-refresh fa-3x" aria-hidden="true" @click="consultar"></i>
                        <p>Recargar</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">

                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="tile">
                            <h3 class="tile-title">Pacientes por turno</h3>


                                <div class="table-responsive table-bordered" v-if="mensaje_registro === false" >
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>*</th>
                                            <th>Paciente</th>
                                            <th>TITULAR</th>
                                            <th>MEDICO</th>
                                            <th>CONSULTORIO</th>
                                            <th>TURNOS</th>
                                            <th>ESTADO</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="dato in cmb_medico">
                                            <td> <i class="fa fa-check-square-o" aria-hidden="true"></i>  </td>
                                            <td> @{{dato.PACIENTE}} </td>
                                            <td>@{{dato.REPRESENTANTE}} </td>
                                            <td>@{{dato.MEDICO}} </td>
                                            <td>@{{dato.CONSULTORIO}} </td>
                                            <td>@{{dato.TURNO}} </td>
                                            <td>@{{dato.ESTADO}} </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    No existen datos en la agenda aún.
                                </div>


                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('script')

    <script type="text/javascript">
        var app = new Vue ({
            el:'#agendaMedico',
            data : {
                // --- variable para almacenar los errores
                // -----
                 cmb_medico : [],
                mensaje_registro : false,
            },

            created : function() {

                axios.get('/consulta/agenda/medico').then(response => {
                    this.cmb_medico = response.data

                    var num = this.cmb_medico.length;
                    if(num === 0) {
                    this.mensaje_registro = true;
                    } else {
                    this.mensaje_registro = false;
                    }
                })
            },

            methods : {
                consultar: function () {
                    axios.get('/consulta/agenda/medico').then(response => {
                        this.cmb_medico = response.data
                        var num = this.cmb_medico.length;
                        if(num === 0) {
                            this.mensaje_registro = true;
                        } else {
                            this.mensaje_registro = false;
                        }
                    })
                }
            }

        });

    </script>
@endsection


















