@extends('layouts.principal')
@section('titulo','Preparación De Pacientes')
@section('title','Preparación De Pacientes')
@section('contenido')
    <div class="col-md-12" id="agendaPacientes" v-cloak>
        <div class="tile">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-1">
                        <i class="fa fa-file-o fa-3x"   aria-hidden="true" data-toggle="tooltip" title="Nuevo"  ></i>
                    </div>
                    <div class="col-md-1">
                        <i class="fa fa-search fa-3x"   aria-hidden="true"  data-toggle="tooltip" title="Buscar" @click="consultar" ></i>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">

                    <input class="form-control" placeholder="apellidos" v-model="apellidos" type="text">
                    <br>
                    <div class="form-group row">
                        <label class="label-text col-md-1">Medico:</label>
                        <div class="col-md-4">
                            <select class="form-control" id="medico" v-model="medico" :value="medico">
                            <option></option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Dependencias</label>
                            <div class="col-md-8">
                                <select class="form-control" name="dependencias" v-model="dependencias">
                                    <option v-for="dato in cmbDependencias" :value="dato.codigo"> @{{dato.descripcion}}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="label-text col-md-1">Desde:</label>
                        <input class="form-control col-md-4"  type="date" v-model="desde">
                        <label class="label-text col-md-1" >Hasta:</label>
                        <input class="form-control col-md-4"  type="date" v-model="hasta">
                    </div>

                    <div class="col-md-12">
                        <div class="tile">
                            <h4 class="tile-title">Detalle de busqueda</h4>
                            <div class="table-responsive table-bordered ">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>editar</th>
                                        <th>N°</th>
                                        <th>NOMBRE RESPONSABLE</th>
                                        <th>PACIENTE</th>
                                        <th>NOMBRE MEDICO</th>
                                        <th>FECHA REGISTRO</th>
                                        <th>HORA REGISTRO</th>
                                        <th>TEMPERATURA</th>
                                        <th>PULSO</th>
                                        <th>PRESION A</th>
                                        <th>RESPIRACIÓN</th>
                                        <th>PESO</th>
                                        <th>ESTATURA</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="dato in cmbDatos">
                                        <td><i class="fa fa-pencil-square-o fa-3x" aria-hidden="true" @click="editar(dato.id)"></i></td>
                                        <td>@{{dato.id}}</td>
                                        <td>@{{dato.nombre_representante}}</td>
                                        <td>@{{dato.DesPaciente}}</td>
                                        <td>@{{dato.nombre_medico}}</td>
                                        <td>@{{dato.fecha_registro_paciente}}</td>
                                        <td>@{{dato.hora_registro_paciente}}</td>
                                        <td>@{{dato.temperatura}}</td>
                                        <td>@{{dato.pulso}}</td>
                                        <td>@{{dato.presion_arterial}}</td>
                                        <td>@{{dato.respiracion}}</td>
                                        <td>@{{dato.peso}}</td>
                                        <td>@{{dato.estatura}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--
                            <div class="alert alert-danger" role="alert" >
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                No existen registros en las fecha seleccionada
                            </div> -->
                        </div>


                    </div>

                </div>
            </div>

        </div>

        <div class="modal face" id="signos_vitales" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Signos vitales y detalles del paciente
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <label for="basic-url">DATOS PACIENTE</label>
                       <!--
                        <div class="form-group">
                            <div class="input-group">
                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Paciente</button>
                                </span>
                                <input type="text" class="form-control" id="fecha nacimiento" aria-describedby="basic-addon3">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="input-group">
                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Principal</button>
                                </span>
                                <input type="text" class="form-control" id="fecha nacimiento" aria-describedby="basic-addon3">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group col-md-12">
                                <div class="input-group">
                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Fecha nacimiento</button>
                             </span>
                                    <input type="text" class="form-control" id="fecha nacimiento" aria-describedby="basic-addon3">
                                    <input type="text" class="form-control" id="fecha nacimiento" aria-describedby="basic-addon3">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Edad</button>
                                </span>
                                </div>
                            </div>
                        </div>


                        <p>discapacidad</p>
                        <hr>
                        <div class="form-group col-md-12">
                            <div class="input-group">
                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Discapacidad</button>
                                </span>
                                <input type="text" class="form-control" id="fecha nacimiento" aria-describedby="basic-addon3">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Grado de discapacidad</button>
                                </span>
                                <input type="text" class="form-control" id="fecha nacimiento" aria-describedby="basic-addon3">

                            </div>
                        </div>
                        <label>Signos vitales</label>
                        <hr> -->
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Temperatura</button>
                                </span>
                                    <input type="text" class="form-control" v-model="temperatura_c" placeholder="Grados">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">°C</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->

                            <div class="col-lg-4">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Pulso</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="Grados" v-model="pulso_c">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">xMin</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->

                            <div class="col-lg-4">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Peso</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="" v-model="peso_c">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Kg</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->

                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Presion arterial</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="Grados" v-model="presion_arterial_c">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">MlnGr</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                            <div class="col-lg-6">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Superficie corporal</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="" readonly>
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">M2</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->

                        </div>

                        <div class="form-group row">
                            <div class="col-lg-4">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Estatura</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="CM" v-model="estatura_c">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Cm</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->

                         <!--   <div class="col-lg-4">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Talla</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="CM" v-model="talla_c">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">CM</button>
                                </span>
                                </div><!-- /input-group
                            </div> -->

                            <div class="col-lg-4">
                                <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Respiración</button>
                                </span>
                                    <input type="text" class="form-control" placeholder="" v-model="respiracion_c">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">30xMin</button>
                                </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="actualizar"> Actualizar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medico").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
        var app = new Vue ({
            el:'#agendaPacientes',
            data : {

                cmbDatos : [],
                dependencias : '',

                // --- variable para almacenar los errores

                errores : [],
                especialidad : 0,

                // -----
                atencion:'',
                cedula : '' ,
                nombre : '',
                paciente2 : '',
                responsable: '',
                afinidad : '',
                direccion : '',
                telefono:'',
                seguro : '',
                firmante:'',
                acompanante : '',
                doctor: '',
                observacion : '',
                parentesco : '',
                ejemplo : '',
                cedula_acompanate : '',
                nombre_acompanante : '',

                cmbmedico : [],
                cmbparentesco: [],
                cmbSeguro : [],
                cmbpaciente : [],
                pacientesAdmision : [],
                listaAfiliadosCedula : [],

                //
                id:'',
                fecha_nacimiento : '',
                celular : '',
                otro : '',
                observacion2: '',
                primer_nombre: '',
                segundo_nombre : '',
                apelldio_paterno : '',
                apellido_materno : '',
                edad : '',

                // variables usadas actualmente
                apellidos: '',
                desde :'',
                hasta: '',
                estado : 0,
                estado2 :'',
                cmbMedico : [],
                cmbMedico2 : [],
                cmbEspecializacion :[],
                cmbDependencias : [],
                cmbTipoConsulta : [],

                medico : '',
                especializacion : 0,
                fecha:'',

                cmbpacientte  : [],
                titular_c : '',
                paciente_c : '',
                temperatura_c : '',
                pulso_c: '',
                presion_arterial_c: '',
                respiracion_c: '',
                peso_c: '',
                estatura_c : '',
                talla_c : '',
                id_ : 0,

            },

            created : function() {

                axios.get('/dependencias').then(response => {
                    this.cmbDependencias  = response.data
                })

                axios.get('/especializacion' ).then(response => {
                    this.cmbEspecializacion  = response.data
                })


                axios.get('/tipoconsulta' ).then(response => {
                    this.cmbTipoConsulta  = response.data
                })

                axios.get('/consultarpreparacionhoy').then(response => {
                    this.cmbDatos  = response.data
                })

            },

            methods : {

                validarConsulta : function() {

                    if(this.desde === '' || this.desde === undefined ) {
                        this.errores.push("seleccione la fecha de inicio");
                    }
                    if(this.hasta === '' || this.hasta === undefined ) {
                        this.errores.push("seleccione la fecha de finalización");
                    }
                },

                editar : function(consulta) {
                  $("#signos_vitales").modal();

                  this.consultarPaciente(consulta)
                },

                consultarPaciente : function(consulta){

                    axios.get('/consultar/pacientes/'+consulta+'').then(response => {
                        this.cmbpacientte  = response.data;
                        this.id_ = this.cmbpacientte[0].id;
                        this.titular_c = this.cmbpacientte[0].titular;
                        this.paciente_c = this.cmbpacientte[0].paciente;
                        this.temperatura_c = this.cmbpacientte[0].temperatura;
                        this.pulso_c= this.cmbpacientte[0].pulso;
                        this.presion_arterial_c= this.cmbpacientte[0].presion_arterial;
                        this.respiracion_c= this.cmbpacientte[0].respiracion;
                        this.peso_c= this.cmbpacientte[0].peso;
                        this.estatura_c = this.cmbpacientte[0].estatura;
                        this.talla_c = this.cmbpacientte[0].talla;
                        //this.superficie_corporal =  0,7184*;
                    })
                },

                consultar : function() {
                    this.validarConsulta();
                    //this.validarFecha();
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

                    if( this.errores.length === 0) {

                        axios.get('/consultarpreparacion/'+this.desde+'/'+this.hasta+'/'+this.apellidos+'/'+this.medico+'/'+this.dependencias+'').then(response => {
                            this.cmbDatos  = response.data
                        })

                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },

                    actualizar : function () {
                        var parametros = {
                            "_token": " {{csrf_token() }}",
                           "temperatura" : this.temperatura_c,
                            "pulso" : this.pulso_c,
                            "presion_arterial" : this.presion_arterial_c,
                            "respiracion" : this.respiracion_c,
                            "peso" : this.peso_c,
                            "estatura" : this.estatura_c,
                            "talla" : this.estatura_c,
                        };
                        $.ajax({
                            data : parametros,
                            url : "/preparacion/"+this.id_+"/update",
                            type : "post",
                            success : function(response){
                                //response contiene la respuesta al llamado de tu archivo
                                //aqui actualizas los valores de inmediato llamando a sus respectivas id.
                                console.log("exito");
                                $("#editarUsuarios").modal('hide');
                                toastr.success('Actualización realizada con exito.', 'Exito', {timeOut: 2000});
                            },
                            error : function (response,jqXHR) {
                                toastr.success('Error al momento de actualizar los datos.', 'Success Alert', {timeOut: 2000});
                            }
                        })
                        this.consultar();
                        $("#signos_vitales").modal('hide');
                    },

                recargar : function() {
                    axios.get('/consultarpreparacionhoy').then(response => {
                        this.cmbDatos  = response.data
                    })
                },
            }

        });

    </script>


@endsection

















