<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <style type="text/css" media="print">
        #print_page {
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-style: normal;
            line-height: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            color: #000;
        }

        @page {
            height: auto;
            width: 310px;
            margin: 0;
            padding: 0px;
        }
    </style>
</head>
<body>
<div id="print_page">
    <h3>BENEMERITA SOCIEDAD PROTECTORA DE LA INFANCIA </h3>
    <h3>HOSPITAL LEON BECERRA" & "'' GUAYAQUIL''</h3>
    <p>--------------------------------------</p>
    <h4> CONSULTA EXTERNA </h4>
    <p>TURNO</p>
    <p>{{$turno}}</p>
    <p>CONSULTORIO</p>
    <p>{{$consultorio}}</p>
    <p>PACIENTE</p>
    <p>{{$paciente}}</p>
    <p>MEDICO</p>
    <p>{{$medico}}</p>
    <p>FECHA Y HORA</p>
    <p>{{$fecha}}</p>
    <p>ESPECIALIDAD</p>
    <p>{{$especialidad}}</p>
    <p>VIA DE ADMISION</p>
    <p>{{$admision}}</p>
    <p>{{$tipo_consulta}}</p>
    <p>--------------------------------------</p>

</div>
</body>
</html>