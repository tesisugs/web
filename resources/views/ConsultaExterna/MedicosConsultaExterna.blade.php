@extends('layouts.principal')
@section('titulo','Medicos/ConsultaExterna')
@section('title','Medicos/ConsultaExterna')
@section('css')
    <style>
        .ocupada {
            background-color: #3097D1;
        }

        .desinfeccion {
            background-color: #ff6c5c;
        }

        .revision {
            border-style:outset;
        }
    </style>
@endsection
@section('contenido')
    <div class="col-md-12" id="consulta_medicos" >

        <div class="tile">
            <!-- Menu de la pagina -->
            <div class="form-group row">
                <div class="col-md-1">
                    <i class="fa fa-file-o fa-3x"   aria-hidden="true" data-toggle="tooltip" title="Nuevo"  ></i>
                </div>

                <div class="col-md-1">
                    <i class="fa fa fa-floppy-o fa-3x"   aria-hidden="true" data-toggle="tooltip" title="Guardar" ></i>

                </div>
                <div class="col-md-1">
                    <i class="fa fa-pencil-square-o fa-3x" aria-hidden="true" data-toggle="tooltip" title="H.C Manual"  ></i>
                </div>
                <div class="col-md-1">
                    <i class="fa fa-stethoscope fa-3x" aria-hidden="true" data-toggle="tooltip" title="Signos Vitales"></i>
                </div>

                <div class="col-md-8">
                    <p class="float-right">Numero Atención: 91</p>
                </div>


            </div>
            <hr>
            <div class="row">
                <div class="col-md-8">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#inicio">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#motivo">Motivo/Antecedentes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#revision">Revisión de Organos/Examen Físico</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#diagnostico">Diagnostico</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#prescripcion">Prescripción</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#laboratorio">Laboratorio Clinico</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#imagenologia">Imagenología</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#firma">Firma</a>
                        </li>
                    </ul>
                    <form method="post" id="form" action="">
                    {{ csrf_field() }}
                    <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- inicio -->
                            <div id="inicio" class="container tab-pane active"><br>

                                    <label for="basic-url">DATOS PACIENTE</label>
                                     <div class="form-group">
                                         <div class="input-group">
                                          <span class="input-group-btn">
                                                 <button class="btn btn-default" type="button">Paciente</button>
                                             </span>
                                             <input readonly type="text" class="form-control" v-model="paciente"  aria-describedby="basic-addon3">
                                         </div>
                                     </div>
                                     <div class="form-group col-md-12">
                                         <div class="input-group">
                                          <span class="input-group-btn">
                                                 <button class="btn btn-default" type="button">Titular</button>
                                             </span>
                                             <input readonly type="text" v-model="titular" class="form-control" aria-describedby="basic-addon3">
                                         </div>
                                     </div>

                                     <p>discapacidad</p>
                                     <hr>
                                     <div class="form-group col-md-12">
                                         <div class="input-group">
                                          <span class="input-group-btn">
                                                 <button class="btn btn-default" type="button">Discapacidad</button>
                                             </span>
                                             <input type="text" readonly class="form-control" v-model="discapacidad" aria-describedby="basic-addon3">
                                             <span class="input-group-btn">
                                                 <button class="btn btn-default" type="button">Grado de discapacidad</button>
                                             </span>
                                             <input type="text" readonly class="form-control" v-model="grado_discapacidad" aria-describedby="basic-addon3">

                                         </div>
                                     </div>
                                     <label class="label label-primary">SIGNOS VITALES</label>
                                     <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" readonly type="button">Temperatura</button>
                                </span>
                                                <input type="text" readonly class="form-control" v-model="temperatura_c" placeholder="Grados">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">°C</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                        <div class="col-lg-4">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Pulso</button>
                                </span>
                                                <input type="text" readonly class="form-control" placeholder="Grados" v-model="pulso_c">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">xMin</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                        <div class="col-lg-4">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Peso</button>
                                </span>
                                                <input type="text" readonly class="form-control" placeholder="" v-model="peso_c">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Kg</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Presion arterial</button>
                                </span>
                                                <input type="text" readonly class="form-control" placeholder="Grados" v-model="presion_arterial_c">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">MlnGr</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Superficie corporal</button>
                                </span>
                                                <input type="text" v-model="superficie_corporal"  class="form-control" placeholder="" readonly>
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">M2</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Estatura</button>
                                </span>
                                                <input type="text" readonly class="form-control" placeholder="CM" v-model="estatura_c">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Cm</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                        <div class="col-lg-4">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Talla</button>
                                </span>
                                                <input type="text" readonly class="form-control" placeholder="CM" v-model="talla_c">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">CM</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                        <div class="col-lg-4">
                                            <div class="input-group">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Respiración</button>
                                </span>
                                                <input type="text" readonly class="form-control" placeholder="" v-model="respiracion_c">
                                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">30xMin</button>
                                </span>
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->

                                    </div>



                            </div>
                            <!-- /inicio -->
                            <!-- motivo -->
                            <div id="motivo" class="container tab-pane fade"><br>

                                <div class="form-group row">
                                    <label class="control-label">Motivo de Consulta</label>
                                    <div class="col-md-12">
                                        <textarea v-model="motivo_consulta"  name="motivo_consulta" class="form-control" autocomplete="off"  placeholder=""  cols="3" maxlength="40" ></textarea>
                                    </div>
                                </div>
                                <label class="control-label">Antecedentes Personales</label>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <textarea v-model="antecedentes_personales" maxlength="120" name="antecedentes_personales" autocomplete="off" class="form-control"  placeholder=""  cols="3" ></textarea>
                                    </div>
                                </div>
                                <p>Antecedentes Familiares</p>
                                <div class="form-group row">
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox"  v-model="cardiopatia" ><span class="label-text">1. Cardiopatía</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="diabetis" ><span class="label-text">2. Diabetes</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="vascular" ><span class="label-text">3. Enf.C.Vascular</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="hipertension" value="4"><span class="label-text">4. Hipertensión</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="cancer" ><span class="label-text">5. Cáncer</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="tuberculos" ><span class="label-text">6. Tuberculos</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="mental" ><span class="label-text">7. Enf. Mental</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="infecciosa" name="antecedentes_familiares" value="8"><span class="label-text">8.ENF.Infecciosa</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="malformaciion" name="antecedentes_familiares" value="9"><span class="label-text">9. Malformación</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="otro" name="antecedentes_familiares" value="10"><span class="label-text">10. Otro</span>
                                        </label>
                                    </div>
                                </div>
                                <input v-if="otro===true" class="form-control" type="text" v-model="descripcion_otro" placeholder="descripción otros" autocomplete="off" maxlength="20">

                                <label class="control-label">Enfermedad Actual o Problemas</label>
                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <textarea v-model="enfermedad_problema"   name="observacion" class="form-control"  placeholder=""  cols="6" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /motivo -->
                            <!-- revision -->
                            <div id="revision" class="container tab-pane fade"><br>
                                <div class="form-group row">
                                    <div class="col-sm-4 revision">

                                        <div class="row">
                                            <label class="col-md-6"> 1.ÓRGANOS DE LOS SENTIDOS</label>
                                            <div class="col-md-2">cp<input name="sentidos" v-model="sentidos" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="sentidos" v-model="sentidos" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 2.RESPIRATORIO</label>
                                            <div class="col-md-2">cp<input name="respiratorio" v-model="respiratorio" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="respiratorio" v-model="respiratorio" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 3.CARDIO VASCULAR</label>
                                            <div class="col-md-2">cp<input name="cardio" v-model="cardio" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="cardio" v-model="cardio" value="sp" type="radio"></div>
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 revision" >
                                        <div class="row">
                                            <label class="col-md-6"> 4.DIGESTIVO</label>
                                            <div class="col-md-2">cp<input name="digestivo" v-model="digestivo" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="digestivo" v-model="digestivo" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 5.GENITAL</label>
                                            <div class="col-md-2">cp<input name="genital" v-model="genital" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="genital" v-model="genital" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 6.URINARIO</label>
                                            <div class="col-md-2">cp<input name="urinario" v-model="urinario" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="urinario" v-model="urinario" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 7.MÚSCULO ESQUELETICO</label>
                                            <div class="col-md-2">cp<input name="musculo" v-model="musculo" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="musculo" v-model="musculo" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 8.ENDOCRINO</label>
                                            <div class="col-md-2">cp<input name="endocrino" v-model="endocrino" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="endocrino" v-model="endocrino" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 9.HEMO LINFÁTICO</label>
                                            <div class="col-md-2">cp<input name="hemo" v-model="hemo" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="hemo" v-model="hemo" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 10.NERVIOSO</label>
                                            <div class="col-md-2">cp<input name="nervioso" v-model="nervioso" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="nervioso" v-model="nervioso" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                </div>
                                <textarea v-model="descripcion_revision_organos"  name="" class="form-control"  placeholder=""  cols="3" ></textarea>
                                <p>Examen Fisico Regional</p>
                                <div class="form-group row">
                                    <div class="col-sm-3 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 1.CABEZA</label>
                                            <div class="col-md-2">cp<input name="cabeza" v-model="cabeza" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="cabeza" v-model="cabeza" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 2.CUELLO</label>
                                            <div class="col-md-2">cp<input name="cuello" v-model="cuello" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="cuello" v-model="cuello" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-3 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 3.TORAX</label>
                                            <div class="col-md-2">cp<input name="torax" v-model="torax" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="torax" v-model="torax" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-3 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 4.ABDOMEN</label>
                                            <div class="col-md-2">cp<input name="abdomen" v-model="abdomen" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="abdomen" v-model="abdomen" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-3 revision">
                                        <div class="row">
                                            <label class="col-md-6"> 5.PELVIS</label>
                                            <div class="col-md-2">cp<input name="pelvis" v-model="pelvis" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="pelvis" v-model="pelvis" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-3 revision">
                                        <div class="row">
                                            <label class="col-md-6"> <SMALL>6.EXTREMIDADES</SMALL> </label>
                                            <div class="col-md-2">cp<input name="extremidades" v-model="extremidades" value="cp" type="radio"></div>
                                            <div class="col-md-2">sp<input name="extremidades" v-model="extremidades" value="sp" type="radio"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row " v-if="cabeza === 'sp'|| cabeza === 'cp'">
                                    <label class="col-md-3">1.Cabeza</label>
                                    <input type="text" v-model="desc_cabeza" class=" form-control col-md-8" placeholder="cabeza" autocomplete="off">
                                </div>
                                <div class="form-group row" v-if="cuello === 'sp'|| cuello === 'cp'">
                                    <label class="col-md-3">2.Cuello</label>
                                    <input type="text" v-model="desc_cuello" class=" form-control col-md-8" placeholder="cuello" autocomplete="off">
                                </div>
                                <div class="form-group row" v-if="torax === 'sp'|| torax === 'cp'">
                                    <label class="col-md-3">3.Torax</label>
                                    <input type="text" v-model="desc_torax" class=" form-control col-md-8" placeholder="torax" autocomplete="off">
                                </div>
                                <div class="form-group row" v-if="abdomen === 'sp'|| abdomen === 'cp'">
                                    <label class="col-md-3">4.Abdomen</label>
                                    <input type="text" v-model="desc_abdomen" class=" form-control col-md-8" placeholder="abdomen" autocomplete="off">
                                </div>
                                <div class="form-group row" v-if="pelvis === 'sp'|| pelvis === 'cp'">
                                    <label class="col-md-3">5.Pelvis</label>
                                    <input type="text" v-model="desc_pelvis" class=" form-control col-md-8" placeholder="pelvis" autocomplete="off">
                                </div>
                                <div class="form-group row" v-if="extremidades === 'sp'|| extremidades === 'cp'">
                                    <label class="col-md-3">6.Extremidades</label>
                                    <input type="text" v-model="desc_extremidades" class=" form-control col-md-8" placeholder="extremidades" autocomplete="off">
                                </div>
                            </div>
                            <!-- /revision -->
                            <!-- diagnostico -->
                            <div id="diagnostico" class="container tab-pane fade"><br>
                                <div class="form-group row">
                                    <label class="control-label col-md-1">Principal</label>
                                    <div class="col-md-3">
                                        <input type="text" v-model="principal_codigo" readonly  name="principal" class="form-control"  placeholder=""  >
                                    </div>
                                    <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(1)" ></i>
                                    <div class="col-md-7">
                                        <input type="text" v-model="principal_descripcion" readonly name="" class="form-control"  placeholder=""  >
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-1">Asociado 1</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly  v-model="asociado1_codigo" name="asociado1" class="form-control"  placeholder=""  >
                                    </div>
                                    <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(2)"></i>
                                    <div class="col-md-7">
                                        <input type="text" readonly v-model="asociado1_descripcion" name="" class="form-control"  placeholder=""  >
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-1">Asociado 2</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly v-model="asociado2_codigo" name="asociado2" class="form-control"   placeholder=""  >
                                    </div>
                                    <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(3)"></i>
                                    <div class="col-md-7">
                                        <input type="text" readonly v-model="asociado2_descripcion" name="" class="form-control"  placeholder=""  >
                                    </div>

                                </div>
                                <h3>Notas de evolución</h3>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <textarea v-model="nota_evolucion"  name="nota_evolucion" class="form-control"  placeholder=""  cols="3" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label">Signos Sintomas Actual:</label>
                                    <div class="col-md-12">
                                        <textarea v-model="signos_actuales"  name="signos_actuales" class="form-control"  placeholder=""  cols="3" ></textarea>
                                    </div>
                                </div>
                                <label class="control-label">Plan:</label>
                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <textarea v-model="plan"   name="plan" class="form-control"  placeholder=""  cols="3" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /diagnostico  -->
                            <!-- prescripcion  -->
                            <div id="prescripcion" class="container tab-pane fade"><br>

                                <div class="form-group row">
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="no_existe" name="no_existe" v-on:change="noExiste"><span class="label-text">NO EXISTENTE</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-5">
                                        <label>
                                            <input type="checkbox" v-model="no_medicina" name="no_medicina" v-on:change="noRequiere"><span class="label-text">NO REQUIERE MEDICINA</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Famacoterapia:</label>
                                    <i v-if="busqueda_prescripcion === true" class="fa fa-search-minus fa-2x col-md-1"  aria-hidden="true"  data-toggle="modal" data-target="#buscarMedicamento"></i>
                                    <i v-else readonly class="fa fa-search-minus fa-2x col-md-1"  aria-hidden="true" data-toggle="tooltip" title="buscar" ></i>
                                    <div class="col-md-7">
                                        <input class="form-control" v-model="famacoterapia" v-bind:readonly="visiblidad_famacoterapia">
                                    </div>
                                    <label class="col-md-1">un</label>
                                    <input v-bind:readonly="visiblidad_cantidad" type="text" class="col-md-1" v-model="cantidad">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Prescripción:</label>
                                    <div class="col-md-8">
                                        <textarea v-bind:readonly="visibilidad_prescripcion"  class="form-control" v-model="prescripcion" name="prescripcion" cols="3" rows="3" > </textarea>
                                    </div>
                                    <div class="col-md-2" >
                                        <button v-if="visibilidad_btn_agg_prescripcion === false" class="btn btn-primary" type="button" @click="agregarMedicina()"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                                         <button v-else class="btn btn-default" type="button" ><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                                        
                                        <button class="btn btn-danger" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="table-responsive table-bordered ">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>descripcion</th>
                                            <th>linea</th>
                                            <th>observación</th>
                                            <th>unidad</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="dato in tb_prescripcion">
                                            <td>@{{dato[0]}}</td>
                                            <td>@{{dato[1]}}</td>
                                            <td>@{{dato[2]}}</td>
                                            <td>@{{dato[3]}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /prescripcion  -->
                            <!-- laboratorio-->
                            <div id="laboratorio" class="container tab-pane fade"><br>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Tipo de Examen:</label>
                                    <i class="fa fa-search fa-2x col-md-1" aria-hidden="true"  data-toggle="modal" data-target="#tipoExamen"></i>
                                    <div class="col-md-7">
                                        <input class="form-control" v-model="tipo_examen1" name="tipo_examen1">
                                    </div>
                                    <button class="btn btn-primary" type="button" @click="agregarOrdenLaboratorio"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                                    <button class="btn btn-danger" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                                </div>
                                <div class="table-responsive table-bordered ">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>descripcion</th>
                                                <th>linea</th>
                                                <th>observación</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="dato in tb_laboratorio">
                                                <td>@{{dato[0]}}</td>
                                                <td>@{{dato[1]}}</td>
                                                <td>@{{dato[2]}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /laboratorio-->
                            <div id="imagenologia" class="container tab-pane fade"><br>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Tipo de Examen:</label>
                                    <i class="fa fa-search fa-2x col-md-1" aria-hidden="true" data-toggle="modal" data-target="#buscarImagenologia"></i>
                                    <div class="col-md-7">
                                        <input class="form-control" v-model="tipo_examen2" name="tipo_examen2">
                                    </div>
                                    <button class="btn btn-primary" type="button" @click="agregarImagen"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                                    <button class="btn btn-danger" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </div>
                                <input class="form-control" type="text" v-model="observacion_imagenologia"><br><br>
                                <div class="table-responsive table-bordered ">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>descripcion</th>
                                            <th>linea</th>
                                            <th>observación</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         <tr v-for="dato in tb_imagen">
                                                <td>@{{dato[0]}}</td>
                                                <td>@{{dato[1]}}</td>
                                                <td>@{{dato[2]}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="firma" class="container tab-pane fade"><br>
                                <button class="btn btn-large" type="button"> Plantilla</button>
                                <button class="btn btn-large" type="button"> Repetir</button>
                                <button class="btn btn-large" type="button"> Duplicar</button>
                            </div>
                            <!--


                            <div id="prueba" class="container tab-pane fade"><br>
                                <div class="form-group row">
                                    <div class="animated-checkbox col-md-3">
                                        <label>
                                            <input type="checkbox" v-model="" name="documentos_completos"><span class="label-text">NO EXISTENTE</span>
                                        </label>
                                    </div>
                                    <div class="animated-checkbox col-md-5">
                                        <label>
                                            <input type="checkbox" v-model="" name="documentos_completos"><span class="label-text">NO REQUIERE MEDICINA</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Famacoterapia:</label>
                                    <i class="fa fa-print fa-1x col-md-1" aria-hidden="true" data-toggle="tooltip" title="buscar"></i>
                                    <div class="col-md-5">
                                        <input class="form-control">
                                    </div>
                                    <label class="col-md-1">un</label>
                                    <input class="col-md-1">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Prescripción:</label>
                                    <div class="col-md-5">
                                        <textarea  class="form-control" cols="3" > </textarea>
                                    </div>
                                    <div class="col-md-2" >
                                        <button>boton 1</button>
                                        <button>boton 2</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-bordered ">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>descripcion</th>
                                            <th>linea</th>
                                            <th>observación</th>
                                            <th>unidad</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>descripcion</td>
                                            <td>linea</td>
                                            <td>observación</td>
                                            <td>unidad</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div> -->
                        </div>
                        <!-- Tab panes -->
                    </form>
                </div>
                <div class="col-md-4">
                    <fieldset>
                        <legend>Historial Clínico</legend>


                        <p><small>1000</small> Bryan Estiven Silva Mercado</p>
                        <hr>
                        <b>HISTORIA DIGITAL PACIENTES </b>
                    </fieldset>

                </div>

                <!-- Nav tabs -->

            </div>

        </div>
        <!-- modales -->

        <div class="modal face" id="buscarDiagnostico" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar diagnostico medico
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="diaganostico" v-model="descripcion_diagnostico" v-on:keyup.13="consultarDiagnostico" >
                            </div>
                            <p>Pacientes</p>
                            <hr>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO</td>
                                    <td>DESCRIPCION</td>
                                    <td>TIPO</td>
                                    <td>OBSERVACIÓN</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in lista_diagnostico">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" v-on:click="pasarDatos(dato.codigo,dato.descripcion)"></i>  </td>
                                    <td > @{{dato.codigo}} </td>
                                    <td>@{{dato.descripcion}} </td>
                                    <td>@{{dato.tipo}} </td>
                                    <td>@{{dato.observacion}} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscarMedicamento" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar Medicamento
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="diaganostico" v-model="descripcion_medicamento" v-on:keyup.13="consultarMedicamento" >
                            </div>

                            <hr>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO</td>
                                    <td>DESCRIPCION</td>
                                    <td>TIPO</td>
                                    <td>OBSERVACIÓN</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr >
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="tipoExamen" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        TIPOS DE EXAMENES
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <input class="form-control" type="text">
                        <label>id</label><input type="text"><select><option></option></select><button type="button">Generar</button>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#orina">Orina</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Heces">Heces</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Otros">Otros</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Bioquímicos">Bioquímicos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Homonales">Homonales</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Marcadores">Marcadores tumorales</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Niveles">Niveles Sericos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Microbiología">Microbiología</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Inmonológicos">Inmonológicos Serológiccos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Hormonales">Hormonales</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#Hematológicos">Hematológicos Hemostáticos</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                    <div class="tab-content">
                                        <!-- inicio -->
                                        <div id="orina" class="container tab-pane active"><br>
                                            <label for="basic-url">orina</label>
                                        </div>
                                        <!-- /inicio -->
                                        <div id="Heces" class="container tab-pane "><br>
                                            <label for="basic-url">Heces</label>
                                        </div>
                                        <div id="Otros" class="container tab-pane "><br>
                                            <label for="basic-url">Otros</label>
                                        </div>
                                        <div id="Bioquímicos" class="container tab-pane "><br>
                                            <label for="basic-url">Bioquímicos</label>
                                        </div>
                                        <div id="Homonales" class="container tab-pane "><br>
                                            <label for="basic-url">Homonales</label>
                                        </div>
                                        <div id="Marcadores" class="container tab-pane "><br>
                                            <label for="basic-url">Marcadores</label>
                                        </div>
                                        <div id="Niveles" class="container tab-pane "><br>
                                            <label for="basic-url">Niveles</label>
                                        </div>
                                        <div id="Microbiología" class="container tab-pane "><br>
                                            <label for="basic-url">Microbiología</label>
                                        </div>
                                        <div id="Inmonológicos" class="container tab-pane "><br>
                                            <label for="basic-url">Inmonológicos</label>
                                        </div>
                                        <div id="Hormonales" class="container tab-pane "><br>
                                            <label for="basic-url">Hormonales</label>
                                        </div>
                                        <div id="Hematológicos" class="container tab-pane "><br>
                                            <label for="basic-url">Hematológicos</label>
                                        </div>
                                    </div>
                                    <!-- Tab panes -->
                            </div>


                            <!-- Nav tabs -->

                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscarImagenologia" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        TIPOS DE EXAMENES
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">

                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>

       <!--  <div class="modal face" id="buscarArea" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar camas por sala
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <label for="nombre_sala"> Sala:  </label>
                            <input class="form-control" v-model="nombre_sala" id="nombre_sala" readonly>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>Numero cama</td>
                                    <td>Descripcion cama</td>
                                    <td>Descripcion estado</td>
                                    <td>Nombre paciente</td>
                                    <td>Diagnostico paciente</td>
                                    <td>Descripcion</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaCamasArea">
                                    <td> <i v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'" class="fa fa-check-square-o" aria-hidden="true" @click="consultarCama(dato.codigo,dato.descripcion_camas)"></i>  </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"  > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"> @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"> @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">no aplica</td>

                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"  > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"> @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"> @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">no aplica</td>

                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >no aplica</td>
                                </tr>
                                </tbody>
                            </table>

                            <textarea placeholder="observación">
                             </textarea>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>  -->

    </div>

@endsection
@section('script')

    <script>
        function enviar() {
            document.getElementById("rotulo").submit();
            //document.getElementById("form1").submit()
        }
    </script>

    <script type="text/javascript">
        var app = new Vue ({
            el:'#consulta_medicos',
            data : {
            //botones principales
            nuevo : true,
            guardar :true,
            manual :true,
            signos_vitales : true, 
            //botones formulario

            // texto de la ventana principal
            numero_atenciones : 0,

            //inicio
            	paciente:'',
            	titular:'',
            	discapacidad:'',	
            	grado_discapacidad:'',
            	temperatura_c : '',
            	pulso_c : '',
            	peso_c :'',
            	presion_arterial_c : '',
            	superficie_corporal : '',
            	estatura_c : '',
            	talla_c : '',
            	respiracion_c :'',
            	//motivo antecedentes
            	motivo_consulta:'',
            	antecedentes_personales:'',
                cardiopatia : '',
                diabetis : '',
                vascular : '',
                hipertension : '',
                cancer : '',
                tuberculos : '',
                mental : '',
                infecciosa : '',
                malformaciion : '',
                otro : '',
                descripcion_otro : '',
            	antecedentes_familiares :[],
            	enfermedad_problema : '',
            	//revision organos
            	nervioso : '',
            	hemo :'',
            	endocrino : '',
            	musculo : '',
            	cardio : '',
            	urinario :'',
            	digestivo : '',
            	sentidos :'',
            	respiratorio : '',
            	genital :'',
            	cabeza : '',
                desc_cabeza : '',
            	cuello : '',
                desc_cuello : '',
            	torax : '',
                desc_torax : '',
            	abdomen : '',
                desc_abdomen : '',
            	pelvis : '',
                desc_pelvis : '',
            	extremidades : '',
                desc_extremidades : '',
                descripcion_revision_organos : '',
            	//diagnostico
                descripcion_diagnostico :'',
                lista_diagnostico : '',
            	principal_codigo:'',
            	principal_descripcion : '',
            	asociado1_descripcion :'',
            	asociado1_codigo :'',
            	asociado2_codigo :'',
            	asociado2_descripcion :'',
            	nota_evolucion : '',
            	signos_actuales : '',
            	plan : '', 
            	//prescripcion 
            	no_existe : '',
            	no_medicina : '', 
            	prescripcion : '',
                famacoterapia : '',
                cantidad : '',
                visiblidad_famacoterapia : true,
                visibilidad_prescripcion : false,
                visiblidad_cantidad : false,
                visibilidad_btn_agg_prescripcion : false,
                visibilidad_btn_limpiar_prescripcion : false,
                tb_prescripcion : [],
                busqueda_prescripcion : true,
                descripcion_medicamento : '',
            	//laboratorio
            	tipo_examen1 :'',
                tb_laboratorio : [],
                //imagenologia
                tipo_examen2 :'',
                observacion_imagenologia : "",
                tb_imagen : [],
                // firma


      
         },

            created : function() {

            },


            methods : {

                agregarContenido : function(){

                },
                consultarMedicamento : function(){

                },

                pasarDatosMedicamento : function(){

                },
                // -------------------- acciones del formulario -----------//

                    noExiste : function(){
                        //v-bind:disabled="visibilidad_ID"
                        this.visiblidad_famacoterapia = false;
                        this.visibilidad_prescripcion = false;
                        this.visiblidad_cantidad = false;
                        this.visibilidad_btn_agg_prescripcion = false;
                        this.no_medicina = false;

                    },

                    noRequiere : function(){
                        this.visiblidad_famacoterapia = true;
                        this.visibilidad_prescripcion = true;
                        this.visiblidad_cantidad = true;
                        this.visibilidad_btn_agg_prescripcion = true;
                        this.tb_prescripcion = [];
                        this.tb_prescripcion.push(["no requiere","no requiere","no requiere","no requiere"]);
                        this.no_existe = false;

                    },

                    agregarMedicina : function(){

                        if(this.prescripcion !== "" ){
                            this.prescripcion = this.prescripcion.trim()
                        }
                        if(this.famacoterapia !== "" ){
                            this.famacoterapia= this.famacoterapia.trim()
                        }
                        if(this.cantidad  !== "" ){
                            this.cantidad  = this.cantidad.trim()
                        }


                        if(this.prescripcion === "" || this.famacoterapia=== "" || this.cantidad === "" )
                        {
                            toastr.error("Ingrese la información para agregar el medicamento");
                        } else {
                            this.tb_prescripcion.push([this.famacoterapia,"insumos",this.prescripcion,this.cantidad]);
                            this.famacoterapia = "";
                            this.prescripcion = "";
                            this.cantidad = "";
                        }
                    },

                agregarOrdenLaboratorio : function(){

                    if(this.tipo_examen1 !== "" ){
                        this.tipo_examen1 = this.tipo_examen1.trim()
                    }

                    if(this.tipo_examen1=== "")
                    {
                            toastr.error("Ingrese la información para agregar el medicamento");
                    } else {
                        this.tb_laboratorio.push([this.tipo_examen1,"insumos","sin observación"]);
                        this.tipo_examen1 = "";
                    }
                },

                agregarImagen : function(){
                    if(this.tipo_examen2 !== "" ){
                        this.tipo_examen2 = this.tipo_examen2.trim()
                    }

                    if(this.observacion_imagenologia !== "" ){
                        this.observacion_imagenologia = this.observacion_imagenologia.trim()
                    }

                    if(this.tipo_examen2=== "" || this.observacion_imagenologia ==="" )
                    {
                        toastr.error("Ingrese la información para agregar el tipo de imagen");
                    } else {
                        this.tb_imagen.push([this.tipo_examen2,"insumos",this.observacion_imagenologia]);
                        this.tipo_examen2 = "";
                    }
                },

                // -------------------- /acciones del formulario/ -----------//

                buscarDiagnostico : function(numero) {
                    $("#buscarDiagnostico").modal();
                    this.cargar = numero;
                },

                consultarDiagnostico : function(){
                    var url = '/tipodiagnostico/'+this.descripcion_diagnostico+'';
                    //alert(url);
                    axios.get(url).then(response => {
                        this.lista_diagnostico  = response.data;
                    })
                },

                pasarDatos : function (codigo,descripcion) {
                    if(this.cargar === 1){
                        this.principal_codigo = codigo;
                        this.principal_descripcion = descripcion;
                        this.lista_diagnostico = [];
                    }else if(this.cargar === 2)
                    {
                        this.asociado1_codigo = codigo;
                        this.asociado1_descripcion = descripcion;
                        this.lista_diagnostico = [];
                    } else if(this.cargar === 3) {
                        this.asociado2_codigo = codigo;
                        this.asociado2_descripcion = descripcion;
                        this.lista_diagnostico = [];
                    }

                    $("#buscarDiagnostico").modal('hide');
                },
                //-------------------validar-------------------------//
                validarIngreso : function(){
                     if(this.motivo_consulta === ''){
                         this.errores.push("ingrese el motivo de la consulta")
                     }
                    if(this.antecedentes_personales === ''){
                        this.errores.push("ingrese la información de los antecedentes personales")
                    }

                    if(this.enfermedad_problema === ''){
                        this.errores.push("ingrese la información de los antecedentes personales")
                    }

                    if(this.cabeza === 'sp' || this.cabeza === 'cp' ){
                        if(this.desc_cabeza ===''){
                            this.errores.push("ingrese la descripción de la cabeza")
                        }
                    }

                    if(this.cuello === 'sp' || this.cuello === 'cp' ){
                        if(this.desc_cuello ===''){
                            this.errores.push("ingrese la descripción del cuello")
                        }
                    }

                    if(this.torax === 'sp' || this.torax === 'cp' ){
                        if(this.desc_torax ===''){
                            this.errores.push("ingrese la descripción del torax")
                        }
                    }

                    if(this.abdomen === 'sp' || this.abdomen === 'cp' ){
                        if(this.desc_abdomen ===''){
                            this.errores.push("ingrese la descripción del abdomen")
                        }
                    }

                    if(this.pelvis === 'sp' || this.pelvis === 'cp' ){
                        if(this.desc_pelvis ===''){
                            this.errores.push("ingrese la descripción de la pelvis")
                        }
                    }

                    if(this.extremidades === 'sp' || this.extremidades === 'cp' ){
                        if(this.desc_extremidades ===''){
                            this.errores.push("ingrese la descripción de las extremidades")
                        }
                    }
                    if(this.nota_evolucion === ''){
                        this.errores.push("ingrese las notas de evolución")
                    }

                    if(this.signos_actuales === ''){
                        this.errores.push("ingrese los signos actuales")
                    }
                    if(this.plan === ''){
                        this.errores.push("ingrese el plan")
                    }
                    if(this.principal_codigo){
                        this.errores.push("Agrege el diagnostico principal")
                    }
                    if(this.asociado2_codigo){
                        this.errores.push("Agrege el diagnostico asociado 2")
                    }
                    if(this.asociado1_codigo){
                        this.errores.push("Agrege el diagnostico asociado 1")
                    }

                    if(this.tb_prescripcion === []){
                        this.errores.push("Agrege la prescripción")
                    }
                },


                setCampos : function () {
                    this.motivo_consulta = this.motivo_consulta.trim();
                    this.antecedentes_personales = this.antecedentes_personales.trim();
                    this.enfermedad_problema = this.enfermedad_problema.trim();

                    if(this.desc_cabeza  === ''){
                        this.desc_cabeza = this.desc_cabeza.trim();
                    }
                    if(this.desc_cuello  === ''){
                        this.desc_cuello = this.desc_cuello.trim();
                    }
                    if(this.desc_torax  === ''){
                        this.desc_torax = this.desc_torax.trim();
                    }
                    if(this.desc_abdomen  === ''){
                        this.desc_abdomen = this.desc_abdomen.trim();
                    }
                    if(this.desc_pelvis  === ''){
                        this.desc_pelvis = this.desc_pelvis.trim();
                    }
                    if(this.desc_extremidades  === ''){
                        this.desc_extremidades = this.desc_extremidades.trim();
                    }
                    this.nota_evolucion= this.nota_evolucion.trim();
                    this.signos_actuales= this.signos_actuales.trim();
                    this.plan= this.plan.trim();

                },
                //-------------------/validar/-------------------------//

                //------------------guardar----------------------------//
                insertMedicosCE:function () {

                    var parametros = {
                        "_token": "{{ csrf_token() }}",

                    };
                    $.ajax({
                        data : parametros,
                        url : "/medicos/store/ce",
                        type : "post",
                        async : true,
                        success : function(d){
                            var a = JSON.parse(d);
                            $("#id").val(a.id);

                            toastr.success('Ingreso exitoso.', 'Exito', {timeOut: 5000});
                        },
                        error : function (response,jqXHR) {
                            toastr.error('Error al momento de ingresar el  registro. Error:'+response+'', 'Error', {timeOut: 5000});
                        }

                    });



                },
                //-----------------/guardar/------------------------------//
            }

        });

    </script>



@endsection
