@extends('layouts.principal')
@section('titulo','Agenda Pacientes')
@section('title','Agenda Pacientes')
@section('contenido')
    <div class="col-md-12" id="agendaPacientes" v-cloak>
        <div class="tile">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-1">
                        <i class="fa fa-file-o fa-3x"   aria-hidden="true" @click = "nuevo" ></i>
                        <p >Nuevo</p>
                    </div>

                    <div class="col-md-2 center-block">
                        <i class="fa fa-calendar fa-3x" aria-hidden="true"  data-toggle="modal" data-target="#horario"></i>
                        <p>Horarios Medicos</p>
                    </div>

                </div>
            </div>
            <hr>
            <div class="row">
                    <div class="col-md-3">
                        <div class="form-horizontal">
                            <label class="label-text" for="especialidad">Especialidad</label>
                            <select class="form-control" id="especialidad" v-model="especializacion" v-on:change="consultarEspecialidadMedico">
                                <option v-for="dato in cmbEspecializacion" :value=" dato.codigo" > @{{ dato.descripcion }} </option>
                            </select>
                        </div>
                        <div class="form-horizontal">
                            <label class="label-text" for="medicos">Medico</label>
                            <select class="form-control" id="medicos" :value="medico"  v-model="medico" v-on:change="consultarHorariodMedico">
                                <option id="medico" v-for="dato in cmbMedico" :value="dato.id" > @{{dato.NombreCompletos}} </option>
                            </select>
                        </div>
                        <label class="control-label">Fecha</label>
                        <input v-if="medico != ''" class="form-control"  type="date" v-model="fecha" v-on:change="consultar">
                        <br>
                        <div class="col-md-12">
                            <i class="fa fa-paper-plane fa-3x pull-right" aria-hidden="true" v-on:click="consultar2()" ></i>
                        </div>
                        <br>
                        <br>

                    </div>

                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="tile">
                                <h3 class="tile-title">Detalle de consulta</h3>
                                <div class="table-responsive table-bordered ">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                            <th>Paciente</th>
                                            <th>Medico</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Hora Fin</th>
                                            <th>Consultorio</th>
                                            <th>Turno</th>
                                            <th>Estado</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="dato in turnos_diarios ">
                                                <td>@{{dato.PACIENTE}}</td>
                                                <td>@{{dato.MEDICO}}</td>
                                                <td>@{{dato.FECHA}}</td>
                                                <td>@{{dato.HORA}}</td>
                                                <td>@{{dato.HORA_FIN}}</td>
                                                <td>@{{dato.CONSULTORIO}}</td>
                                                <td>@{{dato.TURNO}}</td>
                                                <td>@{{dato.ESTADO}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
            </div>

        </div>
        <div class="modal face" id="agendar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="{{route('agendapacientes.store')}}" method="post" id="form1">
                     <!--   //{/{csrf_token()}} -->
                        {{csrf_field()}}
                        <div class="modal-header">
                            Nuevo paciente de consulta externa
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" >
                            <div class="tile-body">
                                <div class="form-horizontal">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Numero de atencion</label>
                                        <div class="col-md-8">
                                            <input class="form-control" id="n" type="text" readonly placeholder="N° atencion">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Titular</label>
                                        <div class="col-md-3">
                                            <input class="form-control" name="titular1" type="text" v-model="cedula" placeholder="" v-on:keyup.13="ConsultarCedula">
                                        </div>
                                        <i class="col-md-1 fa fa-search-plus fa-3x" aria-hidden="true"  data-toggle="modal" data-target="#buscar_paciente_modal"></i>
                                        <div class="col-md-5">
                                            <input v-model="nombre" value="" readonly name="nombre_representante" class="form-control" type="text" placeholder=""  >
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Paciente</label>
                                        <div class="col-md-7">
                                            <select class="form-control" v-model="info_paciente" v-on:change="consultarInfoPacinetes()" id="paciente_nombre">
                                                <option v-for="dato in cmbpaciente" :value="dato.Cedula"> @{{dato.Nombres}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control " readonly v-model="edad">
                                        </div>
                                    </div>
                                    <div class="animated-checkbox">
                                        <label>
                                            <input type="checkbox" v-model="revision"><span class="label-text">SOLO REVISION DE EXAMENES</span>
                                        </label>
                                    </div>
                                    <p>Detalle del turno</p>
                                    <hr>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Médico:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" readonly v-model="nombre_medico2" name="medico">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Dependencias</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="dependencias" v-model="dependencias">
                                                <option v-for="dato in cmbDependencias" :value="dato.codigo"> @{{dato.descripcion}}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Consulta</label>
                                        <div class="col-md-8">
                                            <select class="form-control" v-model="tipo_consulta" v-on:change="tipoConsulta()" id="tipo_consulta">
                                                <option v-for="dato in cmbTipoConsulta" :value="dato.codigo"> @{{dato.descripcion}}</option>
                                            </select>
                                        </div>
                                    </div>
                                   <h4>Pacientes Egresados de hospitalizacion</h4>
                                    <div class="ti-tablet">
                                        <div class="table-responsive table-bordered ">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Consulta</th>
                                                    <th>Asistencia</th>
                                                    <th>Paciente</th>
                                                    <th>Medico</th>
                                                    <th>Paciente</th>
                                                    <th>Fecha egreso</th>
                                                    <th>Fecha limite</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="dato in cmbCartaIess ">
                                                    <td>@{{dato.ConsultaNo}}</td>
                                                    <td>@{{dato.Asistencia}}</td>
                                                    <td>@{{dato.Paciente}}</td>
                                                    <td>@{{dato.Medico}}</td>
                                                    <td>@{{dato.Fecha_egreso}}</td>
                                                    <td>@{{dato.FechaLimiteConsulta}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="titular" v-model="id">
                            <input type="hidden" name="paciente_id" v-model="cedula_paciente">
                            <input type="hidden" name="seguro" v-model="seguro">
                            <input type="hidden" name="telefono_representante" v-model="telefono">
                            <input type="hidden" name="direccion_representante" v-model="direccion">
                            <input type="hidden" name="telefono_paciente" v-model="telefono_paciente">
                            <input type="hidden" name="direccion_paciente" v-model="direccion_paciente">
                            <input type="hidden" name="fecha_nacimiento_paciente" v-model="fecha_nacimiento_paciente">
                            <input type="hidden" name="seguro_paciente" v-model="seguro_paciente">
                            <input type="hidden" name="tipo_consulta" v-model="tipo_consulta">
                            <input type="hidden" name="dependencias" v-model="dependencias">
                            <input type="hidden" name="medico" v-model="medico">
                            <input type="hidden" name="nombre_representante" v-model="nombre">
                            <input type="hidden" name="hora_inicio" v-model="hora_inicio">
                            <input type="hidden" name="hora_fin" v-model="hora_fin">

                        </div>
                        <div class="modal-footer">
                                <button v-if="guardar === true" class="btn btn-primary" type="button" @click="validarInsertTurnos"> Guardar <i class="fa fa-floppy-o fa-3x" aria-hidden="true"></i></button>
                                <button v-if="ticket === true" class="btn btn-dark" type="button" @click="imprimirTicket"> Imprimir <i class="fa fa-floppy-o fa-3x" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>

                </div>

            </div>
        <div class="modal face" id="horario" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Horarios Medicos
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body"></div>
                        <iframe height="600" width="800" src="http://majomacontrolhospitalario.test:8090/horariosmedicosdetallefr">

                        </iframe>
                </div>

            </div>
        </div>
        <div class="modal face" id="buscar_paciente_modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta genreal de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <h4> BUSQUEDA POR NOMBRE </h4>
                        <hr>
                        <div class="form-group">
                            <input type="text" class="form-control"  v-model="apellido" v-on:keyup.13="getUsers">
                        </div>
                        <hr>
                        <div style="overflow: scroll">
                            <table class="table table-bordered" style="width: 100%; height:10px;" >
                                <thead>
                                <tr>
                                    <th>Elegir</th>
                                    <th>cedula</th>
                                    <th>primer nombre</th>
                                    <th>segundo nombre</th>
                                    <th>apellido paterno</th>
                                    <th>apellido materno</th>
                                    <th> Tipo de seguro</th>
                                </tr>
                                </thead>
                                <tbody style="height:20px;">
                                <tr v-for="dato in listaAfiliados">
                                    <td> <button class="btn btn-link" v-on:click="PasarDatos(dato.cedula,'no')"> <i class="fa fa-mouse-pointer fa-4x " aria-hidden="true"></i></button> </td>
                                    <td>@{{dato.cedula}}</td>
                                    <td> @{{dato.primer_nombre}} </td>
                                    <td> @{{dato.segundo_nombre}}</td>,
                                    <td> @{{dato.apellido_paterno}}</td>
                                    <td> @{{dato.apellido_materno}}</td>
                                    <td> @{{dato.tipo_seguro}}</td>
                                </tr>
                                </tbody>
                            </table>


                        </div>

                        <div class="modal-footer">

                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="modal face" id="form_ticket" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta genreal de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <form method="get" id="ticket" action="{{route('ticket')}}" target="_blank" >
                            <input type="hidden"  name="paciente" v-model="nombre_paciente">
                            <input type="hidden"  name="seguro" v-model="seguro_paciente">
                            <input type="hidden"  name="tipo_consulta" v-model="tipo_consulta" >
                            <input type="hidden"  name="medico" v-model="medico">
                            <input type="hidden"  name="especialidad" v-model="especializacion">
                            <input type="hidden"  name="fecha" v-model="fecha">
                            <input type="hidden" name="hora" v-model="hora_inicio">
                            <input type="hidden"  name="id" v-model="turno">
                            <input type="submit" id="enviar_ticket">
                        </form>

                    </div>

                        <div class="modal-footer">

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">

            function enviar() {
                document.getElementById("form1").submit();
            }

         </script>

    <script src="{{asset('js/plugins/printThis.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medicoo").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#especializacionn").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
        });
    </script>

    <script type="text/javascript">
        var app = new Vue ({
            el:'#agendaPacientes',
            data : {
                // imprimir ticket
                nombre_paciente : '',
                nombre_medico2 :'',
                nombre_especialidad:'',
                nombre_consulta:'',
                fecha_ticket :'',
                turno :'',
                ticket : false,
                guardar : true,

                // --- variable para almacenar los errores

                errores : [],
                consulta : '',
                turnos_diarios : [],
                ver_fecha : false,

                // -----
                atencion:'',
                cedula : '' ,
                nombre : '',
                paciente2 : '',
                responsable: '',
                afinidad : '',
                direccion : '',
                telefono:'',
                seguro : '',
                firmante:'',
                acompanante : '',
                doctor: '',
                observacion : '',
                parentesco : '',
                ejemplo : '',
                cedula_acompanate : '',
                nombre_acompanante : '',

                cmbmedico : [],
                cmbparentesco: [],
                cmbSeguro : [],
                cmbpaciente : [],
                pacientesAdmision : [],
                listaAfiliadosCedula : [],
                listaAfiliadosCedula2 : [],

                //
                id:'',
                fecha_nacimiento : '',
                celular : '',
                otro : '',
                observacion2: '',
                primer_nombre: '',
                segundo_nombre : '',
                apelldio_paterno : '',
                apellido_materno : '',
                edad : '',

                // variables usadas actualmente
                paciente: '',
                desde :'',
                hasta: '',
                estado : 0,
                estado2 :'',
                cmbMedico : [],
                cmbMedico2 : [],
                cmbEspecializacion :[],
                cmbDependencias : [],
                cmbTipoConsulta : [],
                cmbHospitalizacionEgresado : [],
                fechaLimiteConsulta : [],

                medico : 0,
                especializacion : 0,
                fecha:'',

                // ddias de la semana
                lunes :false,
                martes :false,
                miercoles :false,
                jueves :false,
                viernes :false,
                sabado :false,
                domingo :false,
                res :'',

                paciente_id : 0,
                cedula_paciente : 0,
                nombre_representante : '',
                info_paciente : '',
                fecha_nacimiento_paciente : '',
                direccion_paciente : '',
                telefono_paciente : '',
                seguro_paciente : '',
                dependencias : '',

                cmbDatos2 : [] ,
                cmbDatos3 : [],
                cmbDatos4 : [],
                cmbCartaIess : [],

                horario : [],
                numero : 0,
                numero_atencionesDiarias : [],
                atencionesDiaria : 0,
                numero_atenciones : [],
                atencionesActuales : 0,
                revision : false,
                tiempo_consulta : 0,
                hora_registro_paciente : '',

                hora_fin : '',
                hora_inicio : '',

                tipo_consulta : '',
                otro_dia : false,
                hora_registro_paciente2 : [],
                hora_registro2 : '',
                fecha_registro : '',

                atencionesporDia : [],
                atenciones_por_dia :'',
                ultima_atencion : [],
                hora_ultima_atencion : '',
                fecha_actual : '',
                listaAfiliados : [],
                apellido : '',



            },

            created : function() {

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "600",
                    "hideDuration": "3000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                axios.get('/dependencias').then(response => {
                    this.cmbDependencias  = response.data
                })

                axios.get('/especializacion' ).then(response => {
                    this.cmbEspecializacion  = response.data
                })

                axios.get('/tipoconsulta' ).then(response => {
                    this.cmbTipoConsulta  = response.data
                })

            },


            methods : {
                /* _ FUNCIONES PARA CONSULTAR INFORMACION DE LAS CITAS ________________________*/

                // consulta la especialidad del medico
                consultarEspecialidadMedico: function () {
                    var url = 'especialidadmedicos/'+this.especializacion+'';
                    axios.get(url).then(response => {
                        this.cmbMedico  = response.data;
                    })
                },
                    // consulta que el medico este disponible
                consultarHorariodMedico: function () {

                    var url = 'horariosmedico/'+this.medico+'';
                    axios.get(url).then(response => {
                        this.horario  = response.data;
                    this.nombre_medico = this.horario[0].medico;

                    this.fecha_desde1 = this.horario[0].fecha_desde1;
                    this.fecha_hasta1 = this.horario[0].fecha_hasta1;
                    this.status_fecha1 = this.horario[0].status_fecha1;
                    if(this.status_fecha1 === 1 )
                    {
                        this.lunes = true;

                        var separacion = this.fecha_desde1.split(' ');
                        this.fecha_desde1 = separacion[1];


                        var separacion = this.fecha_hasta1.split(' ');
                        this.fecha_hasta1 = separacion[1];
                    } else {

                    }

                    this.fecha_desde2 = this.horario[0].fecha_desde2;
                    this.fecha_hasta2 = this.horario[0].fecha_hasta2;
                    this.status_fecha2 = this.horario[0].status_fecha2;

                    if(this.status_fecha2 === 1 )
                    {
                        this.martes = true;

                        var separacion = this.fecha_desde2.split(' ');
                        this.fecha_desde2 = separacion[1];


                        var separacion = this.fecha_hasta2.split(' ');
                        this.fecha_hasta2 = separacion[1];
                    }


                    this.fecha_desde3 = this.horario[0].fecha_desde3;
                    this.fecha_hasta3 = this.horario[0].fecha_hasta3;
                    this.status_fecha3 = this.horario[0].status_fecha3;

                    if(this.status_fecha3 === 1 )
                    {
                        this.miercoles = true;

                        var separacion = this.fecha_desde3.split(' ');
                        this.fecha_desde3 = separacion[1];


                        var separacion = this.fecha_hasta3.split(' ');
                        this.fecha_hasta3 = separacion[1];
                    }


                    this.fecha_desde4 = this.horario[0].fecha_desde4;
                    this.fecha_hasta4 = this.horario[0].fecha_hasta4;
                    this.status_fecha4 = this.horario[0].status_fecha4;

                    if(this.status_fecha4 === 1 )
                    {
                        this.jueves = true;

                        var separacion = this.fecha_desde4.split(' ');
                        this.fecha_desde4 = separacion[1];


                        var separacion = this.fecha_hasta4.split(' ');
                        this.fecha_hasta4 = separacion[1];
                    }


                    this.fecha_desde5 = this.horario[0].fecha_desde5;
                    this.fecha_hasta5 = this.horario[0].fecha_hasta5;
                    this.status_fecha5 = this.horario[0].status_fecha5;
                    if(this.status_fecha5 === 1 )
                    {
                        this.viernes = true;

                        var separacion = this.fecha_desde5.split(' ');
                        this.fecha_desde5 = separacion[1];


                        var separacion = this.fecha_hasta5.split(' ');
                        this.fecha_hasta5 = separacion[1];
                    }

                    this.fecha_desde6 = this.horario[0].fecha_desde6;
                    this.fecha_hasta6 = this.horario[0].fecha_hasta6;
                    this.status_fecha6 = this.horario[0].status_fecha6;
                    if(this.status_fecha6 === 1 )
                    {
                        this.sabado = true;

                        var separacion = this.fecha_desde6.split(' ');
                        this.fecha_desde6 = separacion[1];


                        var separacion = this.fecha_hasta6.split(' ');
                        this.fecha_hasta6 = separacion[1];
                    }

                    this.fecha_desde7 = this.horario[0].fecha_desde7;
                    this.fecha_hasta7 = this.horario[0].fecha_hasta7;
                    this.status_fecha7 = this.horario[0].status_fecha7;
                    if(this.status_fecha7 === 1 )
                    {
                        this.domingo = true;

                        var separacion = this.fecha_desde7.split(' ');
                        this.fecha_desde7 = separacion[1];


                        var separacion = this.fecha_hasta7.split(' ');
                        this.fecha_hasta7 = separacion[1];
                    }
                    var mensaje = 'EL medico Atiende en estos dias: ';
                    this.res = this.res.concat(mensaje);
                    if(this.lunes === true )
                    {
                        var lunes = 'lunes -';
                        this.res = this.res.concat(lunes);
                    }
                    if(this.martes === true)
                    {
                        var martes = 'martes-';
                        this.res = this.res.concat(martes);
                    }
                    if(this.miercoles === true)
                    {
                        var miercoles = 'miercoles-';
                        this.res = this.res.concat(miercoles);
                    }
                    if(this.jueves === true)
                    {
                        var jueves = 'jueves-';
                        this.res = this.res.concat(jueves);

                    }
                    if(this.viernes === true)
                    {
                        var viernes = 'viernes-';
                        this.res = this.res.concat(viernes);

                    }
                    if(this.sabado === true)
                    {
                        var sabado = 'Sabado-';
                        this.res = this.res.concat(sabado);
                    }
                    if(this.domingo === true)
                    {
                        var domingo = 'Domingo-';
                        this.res = this.res.concat(domingo);
                    }

                    toastr.info(this.res);

                })

                    this.res = [];
                },

                // consultar si atiende ese dia

                validarFecha : function () {
                    fecha = new Date(this.fecha);
                    var n = fecha.getDay();
                    var new_fecha = n + 1;

                    switch (new_fecha) {
                        case 0 :
                            break;
                        case 1 :
                            if (this.status_fecha1 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 2 :
                            if (this.status_fecha2 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 3 :
                            if (this.status_fecha3 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 4 :
                            if (this.status_fecha4 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 5 :
                            if (this.status_fecha5 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 6 :
                            if (this.status_fecha6 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 7 :
                            if (this.status_fecha7 === 1) {

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        default :
                            this.errores.push("seleccione una fecha valida");
                    }
                },

                /*------------------------------------------------------------------ */


          /*funciones para  validar el ingreso de datos para un nuevo insert*/

                    // consulta el ingreso de los datos
                consultar2: function () {
                    this.validarConsulta();
                    if( this.errores.length === 0) {
                        // document.getElementById('registroTitular').submit()
                        var combo = document.getElementById("medicos");
                        this.nombre_medico2 = combo.options[combo.selectedIndex].text;
                        $("#agendar").modal()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }

                    }
                    this.errores = [];
                    this.res = [];
                },
                    // valida los datos pasados para poder abrir el formulario.
                validarConsulta : function() {
                    if(this.especializacion === 0 || this.especializacion=== undefined)
                    {
                        this.errores.push("seleccione la especializacion");
                    } else {
                        if(this.medico === 0 || this.medico === undefined)
                        {
                            this.errores.push("seleccione el medico");
                        }else {
                            if(this.fecha === '' || this.fecha === undefined)
                            {
                                this.errores.push("seleccione la fecha");
                            }
                            else {
                                // obtenemos la fecha seleccionada por el formulario
                                var fecha_seleccionada = new Date(this.fecha);
                                var b1 = fecha_seleccionada.getMonth();
                                var a1 = fecha_seleccionada.getFullYear();
                                var c1 = fecha_seleccionada.getDate() +1;

                                // obtenemos la fecha actual
                                var fecha_actual = new Date();
                                var a2 = fecha_actual.getFullYear();
                                var b2 = fecha_actual.getMonth();
                                var c2 = fecha_actual.getDate();

                                // las convertimos de nuevo al formato de fecha
                                fecha_seleccionada = new Date(a1,b1,c1,'00','00','00');
                                fecha_actual = new Date(a2,b2,c2,'00','00','00');

                                //alert(fecha_seleccionada );
                                //alert(fecha_actual);

                                // validamos que no sea una fecha memor
                                if( fecha_seleccionada < fecha_actual) {

                                    this.errores.push("seleccione una fecha valida");
                                    this.errores.push("no se puede seleccionar una fecha anterior al dia actual");
                                }
                                  //si el el mismo dia nos aseguramos que no haya sobrepasado el numero de atenciones

                                if(fecha_seleccionada.getTime() === fecha_actual.getTime()) {
                                    // numero de atenciones
                                    if(this.atencionesActuales === this.atencionesDiaria) {
                                        this.errores.push("El medico ya cumplio el numero de atenciones por día.");
                                    } else {
                                        //hora de salida del medico
                                        this.validarHorarioAtencion();
                                        //hora ultima_atencion
                                        this.consultarAtencionDia()
                                    }
                                     //
                                }
                                else {
                                    if(this.atencionesActuales === this.atencionesDiaria) {
                                        this.errores.push("El medico ya cumplio el numero de atenciones por día.");
                                    } else {
                                        this.otro_dia = true;
                                    }
                                    this.consultarAtencionDia();
                                }

                            }
                        }
                    }


                },
                    // se valida la hora de atención del medico en el dia actual
                validarHorarioAtencion : function () {

                    /*--------------------------------------------- */
                    /*/ tomar la fecha del formulario y la fecha actual del sistema para la validación de la fecha */
                    var fecha_actual = new Date();


                    var y = fecha_actual.getFullYear();
                    var me = fecha_actual.getMonth();
                    var d = fecha_actual.getDate();
                    var h = fecha_actual.getHours();
                    var m = fecha_actual.getMinutes();
                    var s = fecha_actual.getSeconds();
                    var n = fecha_actual.getDay();
                    var new_fecha = n //+ 1;
                    this.fecha_registro = new_fecha;

                    switch (new_fecha) {
                        case 0 :
                            break;
                        case 1 :
                            if (this.status_fecha1 === 1) {

                                var momento_actual = new Date(y, me, d, h, m, s);
                                // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                var slip = this.fecha_hasta1.split(':');
                                var hora = slip[0];
                                var minuto = slip[1];
                                var segundo = slip[2];

                                var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                // preguntamos si el medico ya salio
                                if (momento_actual > hora_fin) {
                                    this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                }

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 2 :
                            if (this.status_fecha2 === 1) {
                                var momento_actual = new Date(y, me, d, h, m, s);
                                // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                var slip = this.fecha_hasta2.split(':');
                                var hora = slip[0];
                                var minuto = slip[1];
                                var segundo = slip[2];

                                var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                // preguntamos si el medico ya salio
                                if (momento_actual > hora_fin) {
                                    this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                }

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 3 :
                            if (this.status_fecha3 === 1) {

                                var momento_actual = new Date(y, me, d, h, m, s);
                                // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                var slip = this.fecha_hasta3.split(':');
                                var hora = slip[0];
                                var minuto = slip[1];
                                var segundo = slip[2];

                                var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                // preguntamos si el medico ya salio
                                if (momento_actual > hora_fin) {
                                    this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                }

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 4 :
                            if (this.status_fecha4 === 1) {

                                if (this.status_fecha1 === 1) {

                                    var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var slip = this.fecha_hasta4.split(':');
                                    var hora = slip[0];
                                    var minuto = slip[1];
                                    var segundo = slip[2];

                                    var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                    // preguntamos si el medico ya salio
                                    if (momento_actual > hora_fin) {
                                        this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                    }

                                } else {
                                    this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                }


                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 5 :
                            if (this.status_fecha5 === 1) {

                                var momento_actual = new Date(y, me, d, h, m, s);
                                // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                var slip = this.fecha_hasta5.split(':');
                                var hora = slip[0];
                                var minuto = slip[1];
                                var segundo = slip[2];

                                var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                // preguntamos si el medico ya salio
                                if (momento_actual > hora_fin) {
                                    this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");

                                }

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 6 :
                            if (this.status_fecha6 === 1) {

                                var momento_actual = new Date(y, me, d, h, m, s);
                                // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                var slip = this.fecha_hasta6.split(':');
                                var hora = slip[0];
                                var minuto = slip[1];
                                var segundo = slip[2];

                                var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                // preguntamos si el medico ya salio
                                if (momento_actual > hora_fin) {
                                    this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                }

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        case 7 :
                            if (this.status_fecha7 === 1) {

                                var momento_actual = new Date(y, me, d, h, m, s);
                                // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                var slip = this.fecha_hasta7.split(':');
                                var hora = slip[0];
                                var minuto = slip[1];
                                var segundo = slip[2];

                                var hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');

                                // preguntamos si el medico ya salio
                                if (momento_actual > hora_fin) {
                                    this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                }

                            } else {
                                this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                            }
                            break;
                        default :
                            this.errores.push("seleccione una fecha valida");

                    }
                },
                consultarPacienteHospitalizacionEgresado : function () {

                    axios.get('/consultarPacienteHospitalizacionEgresado/'+this.paciente_id+'' ).then(response => {
                        this.cmbCartaIess  = response.data;
                        this.fechaLimiteConsulta = this.cmbHospitalizacionEgresado.FechaLimiteConsulta;
                    if(this.cmbCartaIess.length === 0) {
                        //this.errores.push("el tipo de consulta no puede ser IESS")
                        alert("el paciente no se encuentra registrado como egresado");
                    }else {
                        var fecha = new date(fechaLimiteConsulta);
                        var fecha2 = new date();

                        if(fecha < fecha2){
                            alert("el tiempo de consulta ya paso")
                        }
                    }

                    })
                },
                consultarPacientesCartaIess : function() {

                    axios.get('/consultarPacientesCartaIess/'+this.paciente_id+'' ).then(response => {
                        this.cmbCartaIess  = response.data;
                    if(this.cmbCartaIess.length === 0) {
                            this.errores.push("el tipo de consulta no puede ser IESS")
                    }
                })
                },

          /*------------------------------------------------------------------ */


          /* _ FUNCIONES DE CARGA DENTRO DEL FORMULARIO ________________________*/

          // creo que no la estoy usando
                consultarPacientes : function() {
                    var url = 'consultarpacientesadmision';
                    axios.get(url).then(response => {
                        this.pacientesAdmision  = response.data;
                    })
                },

                consultarPacientes2 : function() {
                    var url = '/consultar/paciente/'+this.medico+'/'+this.fecha+'/'+this.paciente_id+'';
                    axios.get(url).then(response => {
                        this.pacientesAdmision  = response.data;
                        console.log(this.pacientesAdmision);

                        if(this.pacientesAdmision === 1 ){
                            this.errores.push("El paciente ya cuenta con un turno en esta fecha");
                        }
                    })
                },


                ConsultarBeneficiario : function () {
                    var url = 'emergencia/consultarbeneficiario/'+this.cedula+'';
                    axios.get(url).then(response => {
                        this.cmbpaciente  = response.data;

                    })
                },
                ConsultarCedula : function() {
                    var urlUsers = 'emergencia/registropacientes/TitularAfiliadoPorCedula/'+this.cedula+'';
                    axios.get(urlUsers).then(response => {
                        this.listaAfiliadosCedula  = response.data;
                    this.id = this.listaAfiliadosCedula[0].id;

                    this.primer_nombre = this.listaAfiliadosCedula[0].primer_nombre;
                    this.segundo_nombre = this.listaAfiliadosCedula[0].segundo_nombre;
                    this.apellido_paterno = this.listaAfiliadosCedula[0].apellido_paterno;
                    this.apellido_materno = this.listaAfiliadosCedula[0].apellido_materno;

                    this.nombre = this.primer_nombre + ' '+this.segundo_nombre+' '+this.apellido_paterno+' '+this.apellido_materno+'';
                    this.responsable = this.primer_nombre + ' '+this.segundo_nombre+' '+this.apellido_paterno+' '+this.apellido_materno+'';
                    this.fecha_nacimiento = new Date(this.listaAfiliadosCedula[0].fecha_nacimiento);
                    this.direccion = this.listaAfiliadosCedula[0].direccion;
                    this.telefono = this.listaAfiliadosCedula[0].telefono;
                    this.seguro  = this.listaAfiliadosCedula[0].tipo_seguro;


                    this.celular = this.listaAfiliadosCedula[0].celular;
                    this.otro = this.listaAfiliadosCedula[0].otro;
                    this.observacion2 = this.listaAfiliadosCedula[0].observacion;
                    this.ConsultarBeneficiario(response.data);
                    this.calcular_edad();
                })
                },
                consultarInfoPacinetes : function () {

                    var urlUsers = 'emergencia/registropacientes/TitularAfiliadoPorCedula/'+this.info_paciente+'';
                    axios.get(urlUsers).then(response => {
                        this.listaAfiliadosCedula2  = response.data;

                    this.paciente_id = this.listaAfiliadosCedula2[0].id;
                    this.cedula_paciente = this.listaAfiliadosCedula2[0].cedula;
                    this.fecha_nacimiento_paciente = this.listaAfiliadosCedula2[0].fecha_nacimiento;
                    this.direccion_paciente = this.listaAfiliadosCedula2[0].direccion;
                    this.telefono_paciente = this.listaAfiliadosCedula2[0].telefono;
                    this.seguro_paciente = this.listaAfiliadosCedula2[0].tipo_seguro;
                    this.nombre_paciente = this.listaAfiliadosCedula2[0].primer_nombre+' '+this.listaAfiliadosCedula2[0].segundo_nombre+' '+this.listaAfiliadosCedula2[0].apellido_paterno+' '+this.listaAfiliadosCedula2[0].apellido_materno+'';
                        this.consultarPacientes2()
                    })
                },
                calcular_edad : function () {
                    //this.edad = this.fecha_nacimiento - getdate()
                    //var today = new Date();
                    var hoy = new Date();
                    var cumpleanos = new Date(this.fecha_nacimiento);
                    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                    var m = hoy.getMonth() - cumpleanos.getMonth();

                    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                        edad--;
                    }

                    this.edad = edad;
                },
                tipoConsulta : function() {
                    //alert(this.tipo_consulta);
                    if(this.tipo_consulta === 2) {
                        this.consultarPacienteHospitalizacionEgresado();
                    }
                    else if(this.tipo_consulta === 4)  {
                        this.consultarPacientesCartaIess();
                    }

                },

                getUsers: function() {
                    var urlUsers = 'emergencia/registropacientes/TitularAfiliadoPorApellido/'+this.apellido+'';
                    axios.get(urlUsers).then(response => {
                        this.listaAfiliados  = response.data
                    })
                },
                PasarDatos : function (cedula,titular) {

                    if(titular === 'si') {

                    } else  {
                        this.cedula = cedula;
                        this.ConsultarCedula();
                        $('#buscar_paciente_modal').modal('toggle');

                    }
                },

          /*------------------------------------------------------------------ */

          /*_______Funciones para validar el ingreso de información___________ */

                validarInsertTurnos : function() {

                    if(this.cedula === '' || this.cedula=== undefined)
                    {
                        this.errores.push("Ingrese la cedula del titular");
                    }

                    if(this.nombre === '' || this.nombre=== undefined)
                    {
                        this.errores.push("Haga la busqueda de la información del titular");
                    }

                    if(this.info_paciente === 0 || this.info_paciente=== undefined)
                    {
                        this.errores.push("Seleccione al paciente");
                    }

                    if(this.dependencias === 0 || this.dependencias=== undefined)
                    {
                        this.errores.push("Seleccione la dependencia");
                    }

                    if(this.tipo_consulta === '' || this.tipo_consulta === 0 || this.tipo_consulta === undefined )
                    {
                        this.errores.push("Seleccione el tipo de consulta");
                    }
                    else {
                        if(this.numero.length === 0 && this.tipo_consulta === 4)
                        {
                            this.errores.push("Paciente no cuenta con un registro de Carta de Derivación");
                        }

                        if(this.numero.length === 0 && this.tipo_consulta === 2)
                        {
                            this.errores.push("Paciente no cuenta con consulta Post-Hospitalario(Clinico)");
                        }

                        if(this.seguro_paciente === 1 && this.tipo_consulta === 1)
                        {
                            this.errores.push("Paciente con Tipo de Ingreso IESS, no pude considerarse tipo de consulta Normal");
                        }

                        if(this.seguro_paciente === 3 && this.tipo_consulta === 2)
                        {
                            this.errores.push("Paciente con Tipo de Ingreso PARTICULAR, no pude considerarse tipo de consulta Post-Hospitalario(IESS)");
                        }

                        if(this.seguro_paciente === 3 && this.tipo_consulta === 4)
                        {
                            this.errores.push("Paciente con Tipo de Ingreso PARTICULAR, no pude considerarse tipo de consulta CARTA DE DERIVACION");
                        }

                        if(this.seguro_paciente === 3 && this.tipo_consulta === 5) {
                            this.errores.push("Paciente con Tipo de Ingreso PARTICULAR, no pude considerarse tipo de consulta CALL CENTER(IESS)");
                        }
                    }

                    if( this.revision === true)
                    {
                        this.tiempo_consulta = 10;
                    } else {
                        this.tiempo_consulta = 20;
                    }

                    //this.setHoraConsulta()
                    this.validarFechaIngreso();
                    this.consultarPacientes2();

                    if( this.errores.length === 0) {
                        //this.imprimirTicket();
                        // document.getElementById('registroTitular').submit()
                        //alert(this.hora_inicio);
                        //alert(this.hora_fin);
                        //document.getElementById("form1").submit();
                        this.insertarTurnosDiarios();
                        //alert("fin");
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                        //alert(this.res);
                        //toastr.info(this.res);
                    }
                    this.errores = [];

                },
                insertarTurnosDiarios : function () {
                var parametros = {
                    "_token": " {{csrf_token() }}",
                    "titular" : this.id,
                    "paciente_id" : this.paciente_id,
                    "cedula_paciente" : this.cedula_paciente,
                    "seguro" : this.seguro,
                    "telefono_representante" : this.telefono,
                    "direccion_representante" : this.direccion,
                    "telefono_paciente" : this.telefono_paciente,
                    "direccion_paciente" : this.direccion_paciente ,
                    "fecha_nacimiento_paciente" : this.fecha_nacimiento_paciente,
                    "seguro_paciente" : this.seguro_paciente,
                    "tipo_consulta" : this.tipo_consulta,
                    "dependencias" : this.dependencias ,
                    "medico" : this.medico,
                    "nombre_representante" : this.nombre,
                    "fecha" : this.fecha,
                    "hora_inicio" : this.hora_inicio,
                    "hora_fin" : this.hora_fin,
                };
                $.ajax({
                    data : parametros,
                    url :   "/agendapacientes/store",
                    type : "post",
                    async:  false,
                    success : function(response){
                       var a = JSON.parse(response);
                        $("#n").val(a.turno);
                        toastr.success('Registro ingresado con exito.', 'EXITO', {timeOut: 5000});
                        // imprimir
                    },
                    error : function (response,jqXHR) {
                        toastr.error('Error al momento de ingresar el registro.', 'ERROR', {timeOut: 5000});

                    },
                });

               this.consultar();
               if($("#n").val() !== '' || $("#n").val() !== undefined || $("#n").val() !== 0 ){
                   this.capturarTurno();
                   this.ticket = true;
                   this.guardar = false;
               }



                },

                // llamada para imprimir
                    imprimirTicket : function(){
                        //compact('/paciente/','/medico/','/especialidad/','/admision/','/tipo_consulta/','consultorio','turno','fecha'))
                        //this.capturarTurno();
                        document.getElementById("ticket").submit();
                    },
                capturarTurno : function(){
                    this.turno = $("#n").val();
                },
                // obtengo el ultimo registro del día
                consultarAtencionDia: function () {

                    axios.get('/consultar/atencion/'+this.medico+'/'+this.fecha+'').then(response => {
                        this.ultima_atencion  = response.data;
                        this.hora_ultima_atencion = this.ultima_atencion[0].hora_salida;
                    })
                },

                // saber cuantas atenciones lleva el medico hastaa el momento
                atencionesMedico: function () {

                    axios.get('/atencionesmedico/'+this.medico+'').then(response => {
                        this.numero_atenciones  = response.data;
                    this.atencionesActuales = this.numero_atenciones.length;
                    })
                },
                // saber cuantas atenciones lleva el medico hastaa el momento por día
                atencionesMedicoDia: function () {

                    axios.get('/atencionesmedico/'+this.medico+'/'+this.fecha+'').then(response => {
                        this.numero_atenciones  = response.data;
                    this.atencionesActuales = this.numero_atenciones.length;

                    })
                },

                // saber cuanto es el maximo de atenciones por medico
                numeroAtencionesMedico: function () {

                    //alert('/consultarmedico/'+this.medico+'');
                    axios.get('/consultarmedico/'+this.medico+'').then(response => {
                        this.numero_atencionesDiarias  = response.data;
                        this.atencionesDiaria = this.numero_atencionesDiarias[0].numero_atencion;
                        //alert("adentro")
                        //alert(this.atencionesDiaria);
                    })
                },

                consultarPacienteId : function() {

                    axios.get('/consultar/paciente/'+this.paciente_id+'' ).then(response => {
                        this.cmbDatos4  = response.data
                  })
                },

                /*
                setHoraConsulta : function() {

                    if(this.tiempo_consulta === 10)
                    {
                        var fecha = new Date();
                        fecha.setSeconds(600);
                    } else {
                        var fecha = new Date();
                        fecha.setSeconds(1200);
                    }

                }, */

                /*------------------------------------------------------------------ */


          /*_____________________FUNCIONES DE LA VENTANA PRINCIPAL____________ */

                nuevo : function() {

                    this.revision=false;
                    this.guardar = true;
                    this.medico = '';
                    this.fecha = '';
                    this.ver_fecha = false;
                    this.cmbMedico = [];
                    this.especializacion = '';
                    this.turnos_diarios = [];
                    this.dependencias = '';
                    this.edad = '';
                    this.cedula = '';
                    this.nombre = '';
                    this.info_paciente = [];
                    this.cedula_paciente = '';
                    this.cmbpaciente = [];
                    this.errores = [];
                    this.tipo_consulta = '';
                    $("#n").val('');

                },




                consultar : function () {

                    // cosulta el numero de atenciones en el día seleccionado
                    axios.get('consultar/turno/agenda/'+this.medico+'/'+this.fecha+'').then(response => {
                        this.turnos_diarios  = response.data
                        })

                    // valida si el medico atiende ese día
                    this.validarFecha();

                    // saca el maximo numero de atenciones por medico(el seleccionado)
                    this.numeroAtencionesMedico();

                    //numero de atenciones en ese dia
                    this.atencionesMedicoDia();
                },
          /*___________________________________________________________________ */



                // valida las fechas de consulta
                validarFechaIngreso : function () {


                    if(this.otro_dia === true)
                    {
                        // obtenemos la fecha seleccionada por el formulario
                        var fecha_seleccionada = new Date(this.fecha);
                        var b1 = fecha_seleccionada.getMonth();
                        var a1 = fecha_seleccionada.getFullYear();
                        var c1 = fecha_seleccionada.getDate() +1;

                        // el insert se va a realizar en otra fecha

                        // preguntamos cuantas atenciones hay en esa fecha
                        if(this.atencionesActuales === 0) {
                            // comienza la logica del primer insert a la base
                            // traemos la hora de comienzo del medico segun el día

                            var day = fecha_seleccionada.getDay();
                            this.fecha_registro = day + 1;
                            switch (this.fecha_registro) {
                                case 0 :
                                    break;
                                case 1 :
                                    if (this.status_fecha1 === 1) {

                                        //var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var split = this.fecha_desde1.split(':');
                                        var hora = split[0];
                                        var minuto = split[1];
                                        var segundo = split[2];

                                        this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                        this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                        var prueba = new Date(this.hora_fin);
                                        this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                        //alert(this.hora_fin);

                                        //this.hora_fin = new Date(this.hora_fin);
                                        var h = this.hora_fin.getHours();
                                        var m = this.hora_fin.getMinutes();
                                        var s = this.hora_fin.getSeconds();

                                        this.hora_fin = h+":"+m+":"+s+"";


                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 2 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_desde1.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                    this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                    var prueba = new Date(this.hora_fin);
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);

                                    //this.hora_fin = new Date(this.hora_fin);
                                    var h = this.hora_fin.getHours();
                                    var m = this.hora_fin.getMinutes();
                                    var s = this.hora_fin.getSeconds();

                                    this.hora_fin = h+":"+m+":"+s+"";
                                    break;
                                case 3 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_desde1.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                    this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                    var prueba = new Date(this.hora_fin);
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);

                                    //this.hora_fin = new Date(this.hora_fin);
                                    var h = this.hora_fin.getHours();
                                    var m = this.hora_fin.getMinutes();
                                    var s = this.hora_fin.getSeconds();

                                    this.hora_fin = h+":"+m+":"+s+"";
                                    break;
                                case 4 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_desde1.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                    this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                    var prueba = new Date(this.hora_fin);
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);

                                    //this.hora_fin = new Date(this.hora_fin);
                                    var h = this.hora_fin.getHours();
                                    var m = this.hora_fin.getMinutes();
                                    var s = this.hora_fin.getSeconds();

                                    this.hora_fin = h+":"+m+":"+s+"";
                                    break;
                                case 5 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_desde1.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                    this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                    var prueba = new Date(this.hora_fin);
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);

                                    //this.hora_fin = new Date(this.hora_fin);
                                    var h = this.hora_fin.getHours();
                                    var m = this.hora_fin.getMinutes();
                                    var s = this.hora_fin.getSeconds();

                                    this.hora_fin = h+":"+m+":"+s+"";
                                    break;
                                case 6 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_desde1.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                    this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                    var prueba = new Date(this.hora_fin);
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);

                                    //this.hora_fin = new Date(this.hora_fin);
                                    var h = this.hora_fin.getHours();
                                    var m = this.hora_fin.getMinutes();
                                    var s = this.hora_fin.getSeconds();

                                    this.hora_fin = h+":"+m+":"+s+"";
                                    break;
                                case 7 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_desde1.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    this.hora_inicio = hora+":"+minuto+":"+segundo+"";

                                    this.hora_fin = new Date(a1, b1,c1, hora, minuto, '00');

                                    var prueba = new Date(this.hora_fin);
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);

                                    //this.hora_fin = new Date(this.hora_fin);
                                    var h = this.hora_fin.getHours();
                                    var m = this.hora_fin.getMinutes();
                                    var s = this.hora_fin.getSeconds();

                                    this.hora_fin = h+":"+m+":"+s+"";
                                    break;
                                default :
                                    this.errores.push("seleccione una fecha valida");

                            }
                        }
                        else {

                            var day = fecha_seleccionada.getDay();
                            this.fecha_registro = day + 1;
                            switch (this.fecha_registro) {
                                case 0 :
                                    break;
                                case 1 :
                                    if (this.status_fecha1 === 1) {

                                        //var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var split = this.fecha_hasta1.split(':');
                                        var hora = split[0];
                                        var minuto = split[1];
                                        var segundo = split[2];
                                        // traemos la hora de la ultima atención
                                        var split2 = this.hora_ultima_atencion.split(':');
                                        var hora2 = split2[0];
                                        var minuto2 = split2[1];
                                        var segundo2 = split2[2];

                                        //las pasamos a formato fecha para poder realizar la comparación

                                        var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                        this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                        // le agregamos los minutos segun el tipo de consulta
                                        this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                        //alert(this.hora_fin);
                                        // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                        if(this.hora_fin > hora_salida)
                                        {
                                            this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var x =this.hora_fin.getHours() ;
                                            var y = this.hora_fin.getMinutes() ;
                                            var z = '00';
                                            if(y === 0){
                                                y = y+'0';
                                            }
                                            this.hora_fin = x + ':' + y + ':' + z + '';
                                            this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                            //alert(this.hora_fin);
                                            //alert(this.hora_inicio);
                                        }
                                    }
                                    break;
                                case 2 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_hasta2.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];
                                    // traemos la hora de la ultima atención
                                    var split2 = this.hora_ultima_atencion.split(':');
                                    var hora2 = split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //las pasamos a formato fecha para poder realizar la comparación

                                    var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                    this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                    // le agregamos los minutos segun el tipo de consulta
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);
                                    // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                    if(this.hora_fin > hora_salida)
                                    {
                                        this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                    } else {
                                        var x = this.hora_fin.getHours();
                                        var y = this.hora_fin.getMinutes();
                                        var z = '00';
                                        if (y === 0) {
                                            y = y + '0';
                                        }
                                        this.hora_fin = x + ':' + y + ':' + z + '';
                                        this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                        //alert(this.hora_fin);
                                        //alert(this.hora_inicio);
                                    }
                                    break;
                                case 3 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_hasta3.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];
                                    // traemos la hora de la ultima atención
                                    var split2 = this.hora_ultima_atencion.split(':');
                                    var hora2 = split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //las pasamos a formato fecha para poder realizar la comparación

                                    var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                    this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                    // le agregamos los minutos segun el tipo de consulta
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);
                                    // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                    if(this.hora_fin > hora_salida)
                                    {
                                        this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                    } else {
                                        var x = this.hora_fin.getHours();
                                        var y = this.hora_fin.getMinutes();
                                        var z = '00';
                                        if (y === 0) {
                                            y = y + '0';
                                        }
                                        this.hora_fin = x + ':' + y + ':' + z + '';
                                        this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                        //alert(this.hora_fin);
                                        //alert(this.hora_inicio);
                                    }
                                    break;
                                case 4 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_hasta4.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];
                                    // traemos la hora de la ultima atención
                                    var split2 = this.hora_ultima_atencion.split(':');
                                    var hora2 = split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //las pasamos a formato fecha para poder realizar la comparación

                                    var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                    this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                    // le agregamos los minutos segun el tipo de consulta
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);
                                    // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                    if(this.hora_fin > hora_salida)
                                    {
                                        this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                    } else {
                                        var x = this.hora_fin.getHours();
                                        var y = this.hora_fin.getMinutes();
                                        var z = '00';
                                        if (y === 0) {
                                            y = y + '0';
                                        }
                                        this.hora_fin = x + ':' + y + ':' + z + '';
                                        this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                        //alert(this.hora_fin);
                                        //alert(this.hora_inicio);
                                    }
                                    break;
                                case 5 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_hasta5.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];
                                    // traemos la hora de la ultima atención
                                    var split2 = this.hora_ultima_atencion.split(':');
                                    var hora2 = split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //las pasamos a formato fecha para poder realizar la comparación

                                    var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                    this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                    // le agregamos los minutos segun el tipo de consulta
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);
                                    // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                    if(this.hora_fin > hora_salida)
                                    {
                                        this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                    } else {
                                        var x = this.hora_fin.getHours();
                                        var y = this.hora_fin.getMinutes();
                                        var z = '00';
                                        if (y === 0) {
                                            y = y + '0';
                                        }
                                        this.hora_fin = x + ':' + y + ':' + z + '';
                                        this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                        //alert(this.hora_fin);
                                        //alert(this.hora_inicio);
                                    }
                                    break;
                                case 6 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_hasta6.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];
                                    // traemos la hora de la ultima atención
                                    var split2 = this.hora_ultima_atencion.split(':');
                                    var hora2 = split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //las pasamos a formato fecha para poder realizar la comparación

                                    var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                    this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                    // le agregamos los minutos segun el tipo de consulta
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);
                                    // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                    if(this.hora_fin > hora_salida)
                                    {
                                        this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                    } else {
                                        var x = this.hora_fin.getHours();
                                        var y = this.hora_fin.getMinutes();
                                        var z = '00';
                                        if (y === 0) {
                                            y = y + '0';
                                        }
                                        this.hora_fin = x + ':' + y + ':' + z + '';
                                        this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                        //alert(this.hora_fin);
                                        //alert(this.hora_inicio);
                                    }
                                    break;
                                case 7 :
                                    //var momento_actual = new Date(y, me, d, h, m, s);
                                    // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                    var split = this.fecha_hasta7.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];
                                    // traemos la hora de la ultima atención
                                    var split2 = this.hora_ultima_atencion.split(':');
                                    var hora2 = split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //las pasamos a formato fecha para poder realizar la comparación

                                    var hora_salida = new Date(a1, b1,c1, hora, minuto, '00');

                                    this.hora_fin = new Date(a1, b1,c1, hora2, minuto2, '00');

                                    // le agregamos los minutos segun el tipo de consulta
                                    this.hora_fin.setMinutes( this.hora_fin.getMinutes() + this.tiempo_consulta);
                                    //alert(this.hora_fin);
                                    // comparamos si la consulta es mayor al tiempo de trabajo del medico
                                    if(this.hora_fin > hora_salida)
                                    {
                                        this.errores.push("el tiempo de consulta sobrepasa el horario de atención del medico")
                                    } else {
                                        var x = this.hora_fin.getHours();
                                        var y = this.hora_fin.getMinutes();
                                        var z = '00';
                                        if (y === 0) {
                                            y = y + '0';
                                        }
                                        this.hora_fin = x + ':' + y + ':' + z + '';
                                        this.hora_inicio = hora2+':'+minuto2+':'+segundo2+'';
                                        //alert(this.hora_fin);
                                        //alert(this.hora_inicio);
                                    }
                                    break;
                                default :
                                    this.errores.push("seleccione una fecha valida");

                            }
                        }

                    }
                    else {
                        // el ingreso es el mismo día


                        var fecha_seleccionada = new Date(this.fecha);
                        var me = fecha_seleccionada.getMonth();
                        var y = fecha_seleccionada.getFullYear();
                        var d = fecha_seleccionada.getDate()+1;
                        var day = fecha_seleccionada.getDay();

                        var fecha_actual = new Date();
                        var h = fecha_actual.getHours();
                        var m = fecha_actual.getMinutes();
                        var s = fecha_actual.getSeconds();


                        if(this.atencionesActuales === 0) {
                            // nos guiamos con la fecha de inicio
                            var day = fecha_seleccionada.getDay();
                            this.fecha_registro = day + 1;
                            switch (this.fecha_registro) {
                                case 0 :
                                    break;
                                case 1 :
                                    if (this.status_fecha1 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta1.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde1.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                        this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }


                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 2 :
                                    if (this.status_fecha2 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta2.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde2.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                         this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }


                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 3 :
                                    if (this.status_fecha3 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta3.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde3.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                        this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }

                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 4 :
                                    if (this.status_fecha4 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta4.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde4.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                        this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }


                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 5 :
                                    if (this.status_fecha5 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta5.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde5.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                        this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }

                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 6 :
                                    if (this.status_fecha6 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta6.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde6.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                        this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }

                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                case 7 :
                                    if (this.status_fecha7 === 1) {

                                        var momento_actual = new Date(y, me, d, h, m, s);
                                        // traemos el dato de la hora de finalizacion de las atenciones segun el medido
                                        var slip = this.fecha_hasta7.split(':');
                                        var hora = slip[0];
                                        var minuto = slip[1];
                                        var segundo = slip[2];

                                        var slip2 = this.fecha_desde7.split(':');
                                        var hora2 = slip2[0];
                                        var minuto2 = slip2[1];
                                        var segundo2 = slip2[2];

                                        //alert(momento_actual);

                                        this.hora_fin = new Date(y, me, d, hora, minuto, segundo, '00');
                                        this.hora_inicio = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        var hora_inicio_temp = new Date(y, me, d, hora2, minuto2, segundo2, '00');

                                        // preguntamos si el medico ya salio
                                        if (momento_actual > this.hora_fin) {
                                            this.errores.push("ya paso la hora de atención del medico, seleccione otro día.");
                                        } else if(momento_actual < this.hora_inicio)
                                        {
                                            // tomamos la hora de inicio y haccemos el insert

                                            this.hora_inicio.setMinutes( this.hora_inicio.getMinutes() + this.tiempo_consulta);
                                            //alert(this.hora_inicio);

                                            //this.hora_fin = new Date(this.hora_fin);
                                            var h = this.hora_inicio.getHours();
                                            var m = this.hora_inicio.getMinutes();
                                            var s = this.hora_inicio.getSeconds();

                                            var h2 =hora_inicio_temp.getHours();
                                            var m2 = hora_inicio_temp.getMinutes();
                                            var s2 = hora_inicio_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";


                                        } else{
                                            //hago el insert en el momento actual
                                            var momento_actual_temp = new Date(y, me, d, h, m, s);
                                            momento_actual.setMinutes( momento_actual.getMinutes() + this.tiempo_consulta);

                                            if(momento_actual > this.hora_fin) {
                                                this.errores.push("el tiempo de la consulta excede el horario de atención del medico");
                                            } else{
                                                var h = momento_actual.getHours();
                                                var m = momento_actual.getMinutes();
                                                var s = momento_actual.getSeconds();

                                                var h2 =momento_actual_temp.getHours();
                                                var m2 = momento_actual_temp.getMinutes();
                                                var s2 = momento_actual_temp.getSeconds();

                                                if(m === 0){
                                                    m = m+'0';
                                                }
                                                if(m2 === 0){
                                                    m2 = m2+'0';
                                                }
                                                if(s === 0){
                                                    s = s+'0';
                                                }
                                                if(s2 === 0){
                                                    s2 = s2+'0';
                                                }
                                                this.hora_inicio = h2+":"+m2+":"+s2+"";
                                                this.hora_fin = h+":"+m+":"+s+"";

                                            }
                                        }

                                    } else {
                                        this.errores.push("el medico no atiende este dia , seleccione otra fecha");
                                    }
                                    break;
                                default :
                                    this.errores.push("seleccione una fecha valida");

                            }
                        }
                        else {
                            // nos guiamos con la fecha del ultimo ingreso
                            switch (this.fecha_registro) {
                                case 0 :
                                    break;
                                case 1 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta1.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //var hora3=  this.fecha_seleccionada.getHours();
                                    //var m = this.fecha_seleccionada.getMinutes();
                                    //var s = this.fecha_seleccionada.getSeconds();

                                    //this.hora_inicio = hora+":"+minuto+":"+segundo+"";
                                    // variables locales que guardan la info

                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = this.fecha_hora_registro.getHours();
                                            var m = this.fecha_hora_registro.getMinutes();
                                            var s = this.fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                case 2 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta2.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //var h=  this.fecha_seleccionada.getHours();
                                    //var m = this.fecha_seleccionada.getMinutes();
                                    //var s = this.fecha_seleccionada.getSeconds();

                                    //this.hora_inicio = hora+":"+minuto+":"+segundo+"";
                                    // variables locales que guardan la info

                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = fecha_hora_registro.getHours();
                                            var m = fecha_hora_registro.getMinutes();
                                            var s = fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                case 3 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta3.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //var h=  this.fecha_seleccionada.getHours();
                                    //var m = this.fecha_seleccionada.getMinutes();
                                    //var s = this.fecha_seleccionada.getSeconds();

                                    //this.hora_inicio = hora+":"+minuto+":"+segundo+"";
                                    // variables locales que guardan la info

                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = fecha_hora_registro.getHours();
                                            var m = fecha_hora_registro.getMinutes();
                                            var s = fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                case 4 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta4.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];


                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = fecha_hora_registro.getHours();
                                            var m = fecha_hora_registro.getMinutes();
                                            var s = fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                case 5 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta5.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //var h=  this.fecha_seleccionada.getHours();
                                    //var m = this.fecha_seleccionada.getMinutes();
                                    //var s = this.fecha_seleccionada.getSeconds();

                                    //this.hora_inicio = hora+":"+minuto+":"+segundo+"";
                                    // variables locales que guardan la info

                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = fecha_hora_registro.getHours();
                                            var m = fecha_hora_registro.getMinutes();
                                            var s = fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                case 6 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta6.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //var h=  this.fecha_seleccionada.getHours();
                                    //var m = this.fecha_seleccionada.getMinutes();
                                    //var s = this.fecha_seleccionada.getSeconds();

                                    //this.hora_inicio = hora+":"+minuto+":"+segundo+"";
                                    // variables locales que guardan la info

                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = fecha_hora_registro.getHours();
                                            var m = fecha_hora_registro.getMinutes();
                                            var s = fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                case 7 :
                                    // obtenemos  la hora del ultimo ingreso
                                    var split = this.hora_ultima_atencion.split(':');
                                    var hora = split[0];
                                    var minuto = split[1];
                                    var segundo = split[2];

                                    var split2 = this.fecha_hasta7.split(':');
                                    var hora2= split2[0];
                                    var minuto2 = split2[1];
                                    var segundo2 = split2[2];

                                    //var h=  this.fecha_seleccionada.getHours();
                                    //var m = this.fecha_seleccionada.getMinutes();
                                    //var s = this.fecha_seleccionada.getSeconds();

                                    //this.hora_inicio = hora+":"+minuto+":"+segundo+"";
                                    // variables locales que guardan la info

                                    var fecha_hora_ultimo_registro = new  Date(y, me,d, hora, minuto, segundo);

                                    var fecha_hora_fin = new Date(y, me,d, hora2, minuto2, segundo2);

                                    var fecha_hora_registro = new Date(y, me,d, h, m, s);

                                    if(fecha_hora_registro > fecha_hora_fin){
                                        this.errores.push("ya termino el horario de atención del medico")
                                    } else if(fecha_hora_registro > fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora actual

                                        var fecha_hora_registro_temp = new Date(y, me,d, h, m, s);
                                        fecha_hora_registro.setMinutes(fecha_hora_registro.getMinutes() + this.tiempo_consulta);
                                        if(fecha_hora_registro > fecha_hora_fin ){
                                            this.errores.push("Ya termino el horario de atención del medico")
                                        } else{
                                            var h = fecha_hora_registro.getHours();
                                            var m = fecha_hora_registro.getMinutes();
                                            var s = fecha_hora_registro.getSeconds();

                                            var h2 =fecha_hora_registro_temp.getHours();
                                            var m2 = fecha_hora_registro_temp.getMinutes();
                                            var s2 = fecha_hora_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }

                                    }else if (fecha_hora_registro < fecha_hora_ultimo_registro){
                                        // el ingreso se hara en base a la hora del ultimo registro

                                        var fecha_hora_ultimo_registro_temp = new  Date(y, me,d, hora, minuto, segundo);
                                        fecha_hora_ultimo_registro.setMinutes(fecha_hora_ultimo_registro.getMinutes() + this.tiempo_consulta);

                                        if(fecha_hora_ultimo_registro > fecha_hora_fin ){
                                            this.errores.push("El tiempo de la consulta sobrepasa el horario de atención del medico")
                                        } else {
                                            var h = fecha_hora_ultimo_registro.getHours();
                                            var m = fecha_hora_ultimo_registro.getMinutes();
                                            var s = fecha_hora_ultimo_registro.getSeconds();

                                            var h2 =fecha_hora_ultimo_registro_temp.getHours();
                                            var m2 = fecha_hora_ultimo_registro_temp.getMinutes();
                                            var s2 = fecha_hora_ultimo_registro_temp.getSeconds();

                                            if(m === 0){
                                                m = m+'0';
                                            }
                                            if(m2 === 0){
                                                m2 = m2+'0';
                                            }
                                            if(s === 0){
                                                s = s+'0';
                                            }
                                            if(s2 === 0){
                                                s2 = s2+'0';
                                            }
                                            this.hora_inicio = h2+":"+m2+":"+s2+"";
                                            this.hora_fin = h+":"+m+":"+s+"";
                                        }
                                    }
                                    break;
                                default :
                                    this.errores.push("seleccione una fecha valida");

                            }
                        }

                    }

                    /*
                    var mensaje = 'EL medico Atiende en estos dias: ';
                    this.res = this.res.concat(mensaje);
                    if(this.lunes === true )
                    {
                        var lunes = 'lunes -';
                         this.res = this.res.concat(lunes);
                    }
                    if(this.martes === true)
                    {
                        var martes = 'martes-';
                         this.res = this.res.concat(martes);
                    }
                    if(this.miercoles === true)
                    {
                        var miercoles = 'miercoles-';
                         this.res = this.res.concat(miercoles);
                    }
                    if(this.jueves === true)
                    {
                        var jueves = 'jueves-';
                         this.res = this.res.concat(jueves);

                    }
                    if(this.viernes === true)
                    {
                        var viernes = 'viernes-';
                         this.res = this.res.concat(viernes);

                    }
                    if(this.sabado === true)
                    {
                        var sabado = 'Sabado-';
                         this.res = this.res.concat(sabado);
                    }
                    if(this.domingo === true)
                    {
                        var domingo = 'Domingo-';
                         this.res = this.res.concat(domingo);
                    }
                        */

                },
                //
            }

        });

    </script>



@endsection



















