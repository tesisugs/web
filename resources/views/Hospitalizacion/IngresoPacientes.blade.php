@extends('layouts.principal')
@section('titulo','Ingresos')
@section('title','Ingresos')
@section('css')
    <style>
        .ocupada {
            background-color: #3097D1;
        }

        .desinfeccion {
            background-color: #ff6c5c;
        }
    </style>
@endsection
@section('contenido')
    <div class="col-md-12" id="ingreso">

        <div class="tile">
            <!-- Menu de la pagina -->
            <div class="form-group row">
                <div class="col-md-1">
                    <i v-if="nuevo === true" @click="limpiar" class="fa fa-file-o fa-3x" style="color: #0091ff;"   aria-hidden="true" data-toggle="tooltip" title="Agregar un nuevo registro"   ></i>
                    <i v-else class="fa fa-file-o fa-3x"   aria-hidden="true" data-toggle="tooltip" title="Agregar un nuevo registro" v-bind:class = "{'inactivo':'inactivo'}"    ></i>
                </div>

                <div class="col-md-1">
                    <i v-if="guardar === true" class="fa fa fa-floppy-o fa-3x" style="color: #0091ff;" aria-hidden="true" data-toggle="tooltip" title="Guardar" @click="validarRegistro()" ></i>
                    <i v-else  class="fa fa-floppy-o fa-3x fa-3x"   aria-hidden="true" data-toggle="tooltip" title="Guardar" v-bind:class = "{'inactivo':'inactivo'}"  ></i>
                </div>

                <div class="col-md-1">
                      <i class="fa fa-print fa-3x" v-if="imprimir === true" style="color: #0091ff;"  @click="imprimirRotulo " aria-hidden="true" data-toggle="tooltip" title="Imprimir"  ></i>
                    <i v-else class="fa fa-print fa-3x" aria-hidden="true"  data-toggle="tooltip" title="imprimir" v-bind:class = "{'inactivo':'inactivo'}"></i>
                </div>

            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#general">General</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#medico">Médico</a>
                </li>
            </ul>
            <form method="post" id="form" action="{{route('hospitalizacion.ingresopacientes')}}">
                {{ csrf_field() }}

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="general" class="container tab-pane active"><br>
                    <form>
                        <div class="tile-body">

                            <div class="form-group animated-radio-button">
                                <label><input type="radio" v-model="tipo" value="normal" name="optradio"><span class="label-text">Hospitalización</span></label>
                                <label><input type="radio" v-model="tipo" value="cirugia" name="optradio"><span class="label-text">Cirugia Ambulatoria</span></label>

                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Paciente</label>
                                        <div class="col-md-8">
                                            <input v-model="paciente_principal" readonly class="form-control " type="text" name="paciente" placeholder="">
                                        </div>
                                        <i class="fa fa-search fa-2x col-md-1" aria-hidden="true" v-on:click="consultarPaciente"></i>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Area</label>
                                        <div class="col-md-4">
                                            <select class="form-control" v-model="area" id="sala" name="sala" :value="area" >

                                            </select>
                                        </div>
                                        <i class="fa fa-search fa-2x col-md-1" aria-hidden="true" v-on:click="consultarAreas"></i>
                                        <label class="control-label col-md-2">Habilitadas</label>
                                        <input v-model="habilitadas" readonly  name="habilitadas" class="col-md-2 form-control" type="text" placeholder=""  >
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Unidad</label>
                                        <div class="col-md-8">
                                            <select class="form-control" disabled v-model="unidad" :value="unidad" id="unidad" name="unidad" >

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">N° de unidad asignada</label>
                                        <div class="col-md-8">
                                            <input v-model="unidad_asignada" readonly name="N_unidad_asignada"  class="form-control" type="text" placeholder=""  >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Fecha de nacimiento</label>
                                        <div class="col-md-5">
                                            <input v-model="fecha_nacimiento" readonly name="fecha_nacimiento" class="form-control" type="date" placeholder=""  >
                                        </div>
                                        <input type="text" v-model="edad"  readonly class="col-md-2 form-control" name="edad" placeholder="">
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Genero</label>
                                        <div class="col-md-3">
                                            <input v-model="genero"  name="genero" readonly class="form-control" type="text" placeholder=""  >
                                        </div>
                                        <label class="control-label col-md-2">V. Admision </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="procedencia" id="seguroList" readonly v-model="tipo_admision" :value="tipo_admision"></select>
                                        </div>
                                    </div>

                                   <div class="form-group row">
                                       <div class="animated-checkbox col-md-3">
                                           <label>
                                               <input type="checkbox" v-model="derivacion" name="derivacion" ><span class="label-text">Derivación</span>
                                           </label>
                                       </div>
                                        <select class="col-md-8 form-control" v-model="hospital" name="hospital" id="hospital" v-show="derivacion === true">
                                            <option></option>
                                        </select>
                                   </div>

                                    <div class="animated-checkbox">
                                        <label>
                                            <input type="checkbox" v-model="documentos_completos" name="documentos_completos"><span class="label-text">Documentos completos</span>
                                        </label>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label">Observacion documentos completos</label>
                                        <div class="col-md-10">
                                            <textarea v-model="observacion_documentos"  name="observacion_documentos" class="form-control"  placeholder=""  cols="3" ></textarea>
                                        </div>
                                    </div>
                                    <label class="control-label">Observaciones</label>
                                    <div class="form-group row">

                                        <div class="col-md-10">
                                            <textarea v-model="observacion" readonly  name="observacion" class="form-control"  placeholder=""  cols="3" ></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Fecha de registro hospitalizacion</label>
                                        <div class="col-md-9">
                                            <input type="text" readonly value="yyyy/mm/dd h:m:s" id="fecha_registro"  name="fecha_registro" class="form-control" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">N. atencion:</label>
                                        <div class="col-md-3">
                                            <input id="atencion" readonly name="n_atencion" class="form-control" type="text">
                                        </div>
                                        <label class="control-label col-md-2">N. trans:</label>
                                        <div class="col-md-3">
                                            <input id="transaccion" readonly   name="n_trans" class="form-control" type="text" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Secuencia HO</label>
                                        <div class="col-md-7">
                                            <input  id="secuencia" name="secuencia_ho" readonly class="form-control" type="text" placeholder=""  >

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Fecha y hora de ingreso area</label>
                                        <div class="col-md-9">
                                            <input  id="fecha_hora_registro" readonly   name="f_h_ingresoArea" class="form-control" type="text" placeholder="yyyy/mm/dd" >
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Nombre</label>
                                        <div class="col-md-9">
                                            <input  v-model="nombre_completo" readonly  name="nombre_responsable" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Direccion</label>
                                        <div class="col-md-9">
                                            <input  v-model="direccion"  readonly name="direccion_responsable" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Documento</label>
                                        <div class="col-md-9">
                                            <input v-model="documento" readonly  name="documento_responsable" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Telefono</label>
                                        <div class="col-md-9">
                                            <input v-model="telefono"  readonly  name="telefono_responsable" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div id="medico" class="container tab-pane fade"><br>
                                <div class="form-group row">
                                    <label class="control-label col-md-1">Fecha</label>
                                    <div class="col-md-8">
                                        <input  v-model="fecha" name="fecha_medico" class="form-control" type="date" placeholder="" >
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="control-label col-md-1">Servicios</label>
                                    <div class="col-md-8">
                                        <select v-model="servicios"   name="servicio" class="form-control" id="especializacion" v-on:change="consultarEspecialidadMedico">

                                        </select>
                                    </div>
                                </div>
                                  <div class="form-group row">
                                    <label class="control-label col-md-1">Medico</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="medico" id="medicos" :value="medico"  v-model="medico" v-on:change="consultarPacienteMedico();" >
                                            <option  v-for="dato in cmbMedico" :value="dato.id" > @{{dato.NombreCompletos}} </option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                <label class="control-label col-md-1">Principal</label>
                                <div class="col-md-2">
                                    <input type="text" v-model="principal_codigo" v-on:keyup.13="consultarDiagnosticoCodigo(1)"  name="principal" class="form-control"  autocomplete="off"  v-bind:readonly="principal" maxlength="5" >
                                </div>
                                    <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(1)" ></i>
                                    <i class=" col-md-1 fa fa-recycle fa-3x" aria-hidden="true" v-on:click="limpiarDiagnostico(1)" ></i>
                                <div class="col-md-4">
                                    <input type="text" v-model="principal_descripcion" readonly name="" class="form-control"  placeholder=""  >
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-1">Asociado 1</label>
                                <div class="col-md-2">
                                    <input type="text" v-on:keyup.13="consultarDiagnosticoCodigo(2)" v-model="asociado1_codigo" name="asociado1" class="form-control"  autocomplete="off"  v-bind:readonly="asociado1" maxlength="5" >
                                </div>
                                <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(2)"></i>
                                <i class=" col-md-1 fa fa-recycle fa-3x" aria-hidden="true" v-on:click="limpiarDiagnostico(2)" ></i>
                                <div class="col-md-4">
                                    <input type="text" readonly v-model="asociado1_descripcion" name="" class="form-control"  placeholder=""  >
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-1">Asociado 2</label>
                                <div class="col-md-2">
                                    <input type="text" v-on:keyup.13="consultarDiagnosticoCodigo(3)" v-model="asociado2_codigo" name="asociado2" class="form-control"   autocomplete="off"  v-bind:readonly="asociado2" maxlength="5" >
                                </div>
                                <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(3)"></i>
                                <i class=" col-md-1 fa fa-recycle fa-3x" aria-hidden="true" v-on:click="limpiarDiagnostico(3)" ></i>
                                <div class="col-md-4">
                                    <input type="text" readonly v-model="asociado2_descripcion" name="" class="form-control"  placeholder=""  >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-1">Observación</label>
                                <div class="col-md-8">
                                <textarea class="form-control" v-model="observacion_m" name="observacion_medico" placeholder=""  cols="2"> </textarea>
                                </div>
                            </div>
                    <br>
                    <input type="hidden" v-model="Titularid" name="titular_id">
                    <input type="hidden" v-model="id" name="id">


                </div>

            </div>
            </form>
        </div>
        <!-- modales -->
        <div class="modal face" id="buscarDiagnostico" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar diagnostico medico
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="diaganostico" v-model="descripcion_diagnostico" v-on:keyup.13="consultarDiagnostico" >
                            </div>
                            <p>Pacientes</p>
                            <hr>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO</td>
                                    <td>DESCRIPCION</td>
                                    <td>TIPO</td>
                                    <td>OBSERVACIÓN</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in lista_diagnostico">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" v-on:click="pasarDatos(dato.codigo,dato.descripcion)"></i>  </td>
                                    <td > @{{dato.codigo}} </td>
                                    <td>@{{dato.descripcion}} </td>
                                    <td>@{{dato.tipo}} </td>
                                    <td>@{{dato.observacion}} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscarPaciente" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        CONSULTAR PACIENTE
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="form-control col-md-4"  placeholder="APELLIDOS" v-model="paciente" v-on:keyup.13="validarConsultaInfoPacientes" >
                               <label class="control-label col-md-4"><input type="checkbox" class="form-control col-md-2" v-model="privado"> Pacientes Privados </label>
                            </div>
                            <p class="text-info">información</p>
                            <hr>
                            <!-- PACIENTES GENRALES -->
                            <table class="table-bordered" v-if="privado === false && mensaje_registro === false ">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO PACIENTE</td>
                                    <td>REGISTRO ADMISION</td>
                                    <td>PACIENTE</td>
                                    <td>RESPONSABLE</td>
                                    <td>FECHA ASISTENCIA</td>
                                    <td>HORA ASISTENCIA</td>
                                    <td>MEDICO</td>

                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true"></i>  </td>
                                    <td> @{{dato.CodPaciente}}</td>
                                    <td>@{{dato.RegistroAdmision}}</td>
                                    <td>@{{dato.Paciente}}</td>
                                    <td>@{{dato.responsable  }}</td>
                                    <td>@{{dato.FechaAsistencia }}</td>
                                    <td>@{{dato.HoraAsistencia}}</td>
                                    <td>@{{dato.Medico}}</td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- PACIENTES PRIVADOS -->
                            <table class="table-bordered" v-if="privado === true && mensaje_registro === false ">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO PACIENTE</td>
                                    <td>REGISTRO ADMISION</td>
                                    <td>CEDULA</td>
                                    <td>FECHA INGRESO</td>
                                    <td>PACIENTE</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPaciente">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" v-on:click="pasarDatosPaciente(dato.Paciente,dato.CodPaciente)"></i>  </td>
                                    <td>@{{dato.CodPaciente}}</td>
                                    <td>@{{dato.RegistroAdmision}}</td>
                                    <td>@{{dato.Cedula}}</td>
                                    <td>@{{dato.FechaIngreso}}</td>
                                    <td>@{{dato.Paciente}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                             No existen registros
                        </div>


                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscarArea" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar camas por sala
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <label for="nombre_sala"> Sala:  </label>
                            <input class="form-control" v-model="nombre_sala" id="nombre_sala" readonly>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>Numero cama</td>
                                    <td>Descripcion cama</td>
                                    <td>Descripcion estado</td>
                                    <td>Nombre paciente</td>
                                    <td>Diagnostico paciente</td>
                                    <td>Descripcion</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaCamasArea">
                                    <td> <i v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'" class="fa fa-check-square-o" aria-hidden="true" @click="consultarCama(dato.codigo,dato.descripcion_camas)"></i>  </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"  > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"> @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"> @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">no aplica</td>

                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"  > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"> @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"> @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">no aplica</td>

                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >no aplica</td>
                                </tr>
                                </tbody>
                            </table>
                            <!--
                            <textarea placeholder="observación">
                             </textarea>   -->
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4 form-control"  placeholder="Paciente" v-model="paciente" v-on:keyup.13="consultar">
                            </div>
                            <hr>
                            <table class="table-bordered" v-if="registro === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>PACEINTE</td>
                                    <td>GENERO</td>
                                    <td>FECHA INGRESO</td>
                                    <td>TIPO INGRESO</td>
                                    <td>ESTADO</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" @click="pasarDatosPacienteConsulta(dato.PACIENTE,dato.id)"></i>  </td>
                                    <td> @{{dato.PACIENTE}} </td>
                                    <td>@{{dato.GENERO}} </td>
                                    <td>@{{dato.FECHA_INGRESO}} </td>
                                    <td>@{{dato.TIPO_DE_INGRESO}} </td>
                                    <td>@{{dato.ESTADO}} </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                No existen registros en las fecha seleccionada
                            </div>
                        </div>
                        <hr>
                        <div class="tile-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form method="get" id="rotulo" action="{{route('hospitalizacion.rotulo.ingreso')}}" target="_blank" >
            <input type="hidden"  name="hc" id="id">
            <input type="hidden"  name="paciente" v-model="paciente_principal">
            <input type="hidden"  name="edad" v-model="edad">
            <input type="hidden"  name="habitacion" v-model="nombre_sala">
            <input type="hidden"  name="convenio" v-model="nombre_convenio">
            <input type="hidden"  name="numero_cama" v-model="unidad_asignada">
            <input type="hidden"  name="hora" v-model="hora">
            <input type="hidden"  name="medico" v-model="nombre_medico">
            <input type="hidden"  name="fecha" v-model="fecha_rotulo">
            <input v-if="rotulo === true" type="submit" id="enviar_rotulo">
        </form>
    </div>

@endsection
@section('script')

    <script>
        function enviar() {
            document.getElementById("rotulo").submit();
            //document.getElementById("form1").submit()
        }
    </script>

    <script type="text/javascript">
        var app = new Vue ({
            el:'#ingreso',
            data : {
                 // boton de rotulo
                    rotulo :false,
                // variables para el menu interno
                buscar2 : 'activo',
                nuevo : false,
                buscar : 'inactivo',
                imprimir : false,
                guardar: true,
                cmbMedico : [],
                // variables formulario
                    //general
                    area : '1',
                    paciente_principal :'',
                    habilitadas :'',
                    unidad :'',
                    fecha_nacimiento :'',
                    edad :'',
                    unidad_asignada :'',
                    genero :'',
                    tipo_cama :'',
                    tipo_admision :0,
                    derivacion :false,
                    hospital :'',
                    documentos_completos: '',
                    observacion: '',
                    observacion_documentos :'',
                    telefono :'',
                    documento:'',
                    direccion :'',
                    nombre :'',
                    fecha_hora_ingreso :'',
                    secuencia_ho :0,
                    n_atencion : 0,
                    n_transaccion :0,
                    fecha_registro :'',
                   //medico
                    fecha: '',
                    servicios:'',
                    medico:'',
                    principal_codigo :'',
                    asociado1_codigo :'',
                    asociado2_codigo :'',
                    principal_descripcion :'',
                    asociado1_descripcion :'',
                    asociado2_descripcion :'',
                    observacion_m :'',
                    principal : false,
                    asociado1 : false,
                    asociado2:false,
                // variables para consulta de paciente
                paciente_fecha: '',
                desde :'',
                hasta: '',
                estado : 0,
                estado2 :'',
                listaPacientes : [],
                cmbEstado :[],

                // variables del modal para la consulta de diagnostico
                cargar :0,
                descripcion_diagnostico :'',
                lista_diagnostico : [],
                lista_diagnosticoCodigo : [],

                // variables de busqueda de paciente-individual
                privado: false,
                mensaje_registro : false,
                registro : false,
                paciente :'',
                listaPaciente : [],
                tabla :1,
                tipo_parentesco : '',
                listaAfiliadosCedula : [],
                ListaDatosTitular : [],
                Titularid  : '',
                Titularprimer_nombre  : '',
                Titularsegundo_nombre  : '',
                Titularapellido_paterno  : '',
                Titularapellido_materno  : '',
                Titulardireccion  : '',
                Titulartelefono  : '',
                Titularcelular  : '',

                id : 0,
                apellidos : '',
                nombres : '',
                nombre_completo : '',
                codigo_cama :'' ,

                // variables para consulta de camas por sala
                listaCamasArea : [],
                nombre_sala :'',
                listaCamas : [],
                listaCamashabilitadas : [],
                errores : [],

                tipo : '',
                id_hospitalizacion : '',
                consulta : 0,

                // variables nuevas para imprimir
                estado_civil : '',
                etnia  :'',
                estatus_discapacidad : '',
                carnet_conadis : '',

                //rotulo
                nombre_habitacion : '',
                nombre_medico : '',
                nombre_convenio : '',
                hc_rotulo :'',
                fecha_rotulo : '',
                hora: '',

                respuesta_medico : [],
            },

            created : function() {
                // carga los estados de hospitalización --- borrar
                this.buscar = "activo";
                axios.get('/hospitalizacion/estadoshospitalizacion').then(response => {
                    this.cmbEstado  = response.data
                });
                // inicializa los mensajes
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "5000",
                    "hideDuration": "5000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
            },
            methods : {

                validarConsultaInfoPacientes : function () {
                    var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/; // nombres apellidos
                    if(patt3.test(this.paciente) == false)
                    {
                        toastr.error("el nombre del paciente no pueden contener numeros ,caracteres especiales o estar en blanco");
                    }else {
                        this.buscarPaciente();
                    }
                },

                /*------------------------ consulta------------------------------ */
                consultar : function () {
                    if(this.paciente === ''){
                        this.errores.push("ingrese los nombres del paciente")
                    }
                    if( this.errores.length === 0) {
                        this.consultarPacientes()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },
                consultarPacientes : function()  {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/todos/"+this.paciente+"";
                    axios.get(hospitalizacion ).then(response => {
                        this.listaPacientes  = response.data;
                    var num = this.listaPacientes.length;
                    if(num === 0) {
                        this.registro = false;
                        this.mensaje_registro = true;
                    } else {
                        this.registro = true;
                        this.mensaje_registro = false;
                    }
                })
                },
                consultarCama : function (codigo,cama) {
                    var url = 'consultarcamas/'+codigo+'';
                    axios.get(url).then(response => {
                        this.listaCamas = response.data;
                    this.unidad_asignada = this.listaCamas[0].numero_camas;
                    this.unidad = this.listaCamas[0].descripcion_camas;
                    this.codigo_cama = codigo;
                })

                    var url2 = 'consultarcamashabilitadas/'+this.area+'/'+cama+'';
                    axios.get(url2).then(response => {
                        this.listaCamashabilitadas = response.data;
                    this.habilitadas = this.listaCamashabilitadas[0].total_camas;

                })



                    $("#buscarArea").modal('hide');
                },
                consultarEspecialidadMedico: function () {
                    var url = 'especialidadmedicos/'+this.servicios+'';
                    axios.get(url).then(response => {
                        this.cmbMedico  = response.data;
                })
                },
                buscarPaciente : function(){

                    if(this.paciente === '') {
                        this.errores.push("debe ingresar el nombre del paciente para realizar la busqueda");
                        if( this.errores.length === 0) {
                        } else {
                            var num = this.errores.length;
                            for(i=0; i<num;i++) {
                                toastr.error(this.errores[i]);
                            }
                        }
                        this.errores = [];
                    } else {
                        toastr.success("Consultado información....",{timeOut: 300});
                        if(this.privado === false)
                        {
                            this.estado = 2 ;
                            // mostrar tabla 1
                            this.tabla = 1;
                        }else if(this.privado === true) {
                            this.estado = 1;
                            this.tabla =2;
                        }

                        this.paciente = this.paciente.trim();
                        // logica hecha con axios
                        var url = 'consultarPacientesNombre/'+this.paciente+'/'+this.estado+'';
                        axios.get(url).then(response => {
                            this.listaPaciente = response.data;
                        var num = this.listaPaciente.length;
                        //dependiendo del numero de registros muestra un mensaje en la vista
                        if(num === 0) {
                            this.registro = false;
                            this.mensaje_registro = true;
                        } else {
                            this.registro = true;
                            this.mensaje_registro = false;
                        }
                    })
                    }


                },
                consultarDiagnostico : function(){
                    // busca el diagnostico por nombre
                    var url = 'tipodiagnostico/'+this.descripcion_diagnostico+'';
                    axios.get(url).then(response => {
                        this.lista_diagnostico  = response.data;
                })
                },

                consultarPacienteMedico: function(){
                    // busca el diagnostico por nombre
                    var url = 'consultar/paciente/medico/'+this.id+'/'+this.medico+'/'+this.fecha;
                    axios.get(url).then(response => {
                        this.respuesta_medico = response.data;
                        if(this.respuesta_medico.length === 0){
                            this.errores.push("No existe un correo enviado al medico con la información del paciente.\",\"Medicos al llamado\"")
                        }else {
                            if(this.respuesta_medico[0].respuesta == 0 ) {
                                this.errores.push("El medico no ha aceptado al paciente.\",\"Medicos al llamado\"")
                            }
                        }

                    })
                },
                consultarDiagnosticoCodigo : function(tipo){
                    // busca el diagnostico por codigo
                    if(tipo === 1){
                        var url = 'tipodiagnostico/'+this.principal_codigo+'/codigo';
                        axios.get(url).then(response => {
                            this.lista_diagnosticoCodigo  = response.data;
                            if(this.lista_diagnosticoCodigo.length === 0  ){
                                toastr.error("El número de codigo no se encuentra asociado a ningun diagnostico.")
                            } else {
                                this.principal_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                                this.principal = true;
                            }
                         })
                    }

                    if(tipo === 2){
                        var url = 'tipodiagnostico/'+this.asociado1_codigo+'/codigo';
                        axios.get(url).then(response => {
                            this.lista_diagnosticoCodigo  = response.data;
                            if(this.lista_diagnosticoCodigo.length === 0  ){
                                toastr.error("El número de codigo no se encuentra asociado a ningun diagnostico.")
                            } else {
                                this.asociado1_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                                this.asociado1 = true;
                            }
                        })
                    }

                    if(tipo === 3){
                        var url = 'tipodiagnostico/'+this.asociado2_codigo+'/codigo';
                        axios.get(url).then(response => {
                            this.lista_diagnosticoCodigo  = response.data;
                            if(this.lista_diagnosticoCodigo.length === 0  ){
                                toastr.error("El número de codigo no se encuentra asociado a ningun diagnostico.")
                            }else {
                                this.asociado2_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                                this.asociado2 = true;
                            }
                         })
                    }


                },
                /*------------------------ /consulta------------------------------ */
                /*------------------------ PASAR DATOS------------------------------ */
                pasarDatos : function (codigo,descripcion) {
                    if(this.cargar === 1){
                        this.principal_codigo = codigo;
                        this.principal_descripcion = descripcion;
                        this.lista_diagnostico = [];
                        this.principal = true;
                    }else if(this.cargar === 2)
                    {
                        this.asociado1_codigo = codigo;
                        this.asociado1_descripcion = descripcion;
                        this.lista_diagnostico = [];
                        this.asociado1 = true;
                    } else if(this.cargar === 3) {
                        this.asociado2_codigo = codigo;
                        this.asociado2_descripcion = descripcion;
                        this.lista_diagnostico = [];
                        this.asociado2 = true;
                    }

                    $("#buscarDiagnostico").modal('hide');
                },
                pasarDatosPacienteConsulta : function (paciente,codigo) {

                    alert('entre');
                    this.paciente_principal = paciente;

                    var url =  "consultarhospitalizacionid/"+codigo+'';

                    axios.get(url).then(response => {
                        this.datosConsulta  = response.data;

                    this.fecha_nacimiento = this.datosConsulta[0].fecha_nacimiento;
                    this.tipo_admision = this.datosConsulta[0].Procedencias;
                    this.observacion_documentos = this.datosConsulta[0].osbservacion_documento;
                    this.observacion = this.datosConsulta[0].Observaciones;
                    this.genero = this.datosConsulta[0].genero;
                    this.derivacion = this.datosConsulta[0].derivacion;
                    this.hospital = this.datosConsulta[0].hospital;

                    this.id = this.datosConsulta[0].id;
                    this.secuencia_ho = this.datosConsulta[0].SecuenciaHO;
                    this.n_historial = this.datosConsulta[0].No_historial;
                    this.fecha_hora_ingreso = this.datosConsulta[0].Fecha_Hora_Ingreso;
                    this.fecha_registro = this.datosConsulta[0].Fecha_de_Registro;

                    this.transaccion = this.datosConsulta[0].transaccion;

                    this.paciente_id = this.datosConsulta[0].Paciente;

                    this.area = this.datosConsulta[0].habitacion;

                    this.nombre_responable= this.datosConsulta[0].Nombre_Responsable;
                    this.direccion_responsable = this.datosConsulta[0].Direccion_Responsable;
                    this.telefono = this.datosConsulta[0].Telefono_Responsable;
                    this.documento = this.datosConsulta[0].Documento_Responsable;
                    this.servicios = this.datosConsulta[0].Servicios;
                    this.medico = this.datosConsulta[0].medico;
                    this.principal_codigo = this.datosConsulta[0].principal;
                    this.asociado1_codigo = this.datosConsulta[0].asociado_1;
                    this.asociado2_codigo = this.datosConsulta[0].asociado_2;
                    this.unidad_asignada = this.datosConsulta[0].codigo_cama;
                    //alert(codigo_cama);
                    this.observacion_m = this.datosConsulta[0].Medico_Observacion;
                    this.medico_fecha = this.datosConsulta[0].Medico_Fecha;

                    this.consultarCama(this.unidad_asignada);
                })

                    $("#consultarPaciente").modal('hide');
                    $("#buscar").modal('hide');

                },
                /*------------------------ /PASAR DATOS------------------------------ */


                /*------------------------ VALIDACIONES------------------------------ */
                validarRegistro :function () {



                    if(this.consulta === 1){
                        this.errores.push("EL paciente ya se encuentre hospitalizado");
                    }

                    if(this.tipo === '' ){
                        this.errores.push("Seleccione si es una hospitalizacion o una cirugía");
                    }

                    if(this.paciente_principal === '' || this.paciente_principal=== undefined)
                    {
                        this.errores.push("Consulte la información del paciente (nombre,dirección)");
                    }

                    if(this.unidad_asignada === '' || this.unidad_asignada === undefined)
                    {
                        this.errores.push("Consulte la información de la habitación ");
                    }

                    if(this.derivacion === true )
                    {
                        if(this.hospital === '' || this.hospital === undefined)
                        {
                            this.errores.push("Ingrese de que hospital ha sido derivado");
                        }

                    }

                    if(this.observacion_documentos === '' || this.observacion_documentos === undefined)
                    {
                        this.errores.push("Ingrese la observación sobre los documentos");
                    }

                    if(this.observacion_m === '' || this.observacion_m === undefined)
                    {
                       // this.errores.push("Ingrese la observación del medico");
                    }

                    if(this.servicios ==='' || this.servicios === undefined)
                    {
                        this.errores.push("Ingrese la especialidad");
                    }

                    if(this.medico === '' || this.medico === undefined)
                    {
                        this.errores.push("Ingrese el medico");
                    }

                    if(this.fecha === '' || this.fecha === undefined)
                    {
                        this.errores.push("Ingrese la fecha del medico");
                    }

                    if(this.principal_descripcion === '' || this.principal_descripcion=== undefined)
                    {
                        this.errores.push("Ingrese el diagnostico principal");
                    }
                    if(this.asociado1_descripcion === '' || this.asociado1_descripcion=== undefined)
                    {
                        this.errores.push("Ingrese el diagnostico asociado 1");
                    }
                    if(this.asociado2_descripcion === '' || this.asociado2_descripcion === undefined)
                    {
                        this.errores.push("Ingrese el diagnostico asociado 2");
                    }



                    if( this.errores.length === 0) {
                        this.insertHospitalizacion()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];

                },
                /*------------------------ /VALIDACIONES------------------------------ */

                /*------------------------ INSERT------------------------------ */
                insertHospitalizacion:function () {

                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "fecha_nacimiento" : this.fecha_nacimiento,
                        "tipo" : this.tipo,
                        "id" : this.id ,
                        "sala" : this.area ,
                        "procedencia" : this.tipo_admision ,
                        "nombre_responsable" : this.nombre_completo ,
                        "direccion_responsable" : this.direccion ,
                        "documento_responsable" : this.documento,
                        "telefono_responsable" : this.telefono ,
                        "observacion" : this.observacion,
                        "fecha_medico" : this.fecha,
                        "servicio" : this.servicios,
                        "medico" : this.medico,
                        "principal" : this.principal_codigo ,
                        "asociado1" : this.asociado1_codigo,
                        "asociado2" : this.asociado2_codigo,
                        "observacion_medico" : this.observacion_m ,
                        "codigo_cama" : this.codigo_cama ,
                        "genero"  : this.genero,
                        "titular_id" :this.Titularid,
                        "derivacion" : this.derivacion,
                        "hospital" : this.hospital,
                        "documentos_completos" : this.documentos_completos,
                        "observacion_documentos" : this.observacion_documentos,
                    };
                    $.ajax({
                        data : parametros,
                        url : "ingresohospitalizacion",
                        type : "post",
                        async : false,
                        success : function(d){
                            var a = JSON.parse(d);
                            $("#id").val(a.id);
                            $("#secuencia").val(a.secuencia);
                            //$("#historial").val(a.historial);
                            $("#atencion").val(a.atencion);
                            $("#transaccion").val(a.transaccion);
                            $("#fecha_registro").val(a.fecha_registro);
                            $("#fecha_hora_registro").val(a.fecha_hora_registro);
                            toastr.success('Ingreso de hospitalizacion exitoso.', 'Exito', {timeOut: 5000});
                        },
                        error : function (response,jqXHR) {
                            if(response.status === 422)
                            {
                                var errors = $.parseJSON(response.responseText);
                                $.each(errors, function (key, value) {
                                    if($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {
                                            toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                            console.log(key+ " " +value);
                                        });
                                    }else{
                                        toastr.error('Error al momento de ingresar el  registro: '+''+'', 'Error', {timeOut: 5000});
                                    }
                                });

                            } else {
                                toastr.error('Error al momento de ingresar el  registro: '+''+'', 'Error', {timeOut: 5000});
                            }
                        }

                    });

                    this.secuencia_ho = $("#secuencia").val();
                    if(this.secuencia_ho !== ''){
                        this.guardar = false;
                        this.nuevo = true;
                        this.imprimir = true;
                        // valores
                        var combo = document.getElementById("sala");
                        this.nombre_sala = combo.options[combo.selectedIndex].text;
                        var combo2 = document.getElementById("seguroList");
                        this.nombre_convenio = combo2.options[combo2.selectedIndex].text;
                        var combo3 = document.getElementById("medicos");
                        this.nombre_medico = combo3.options[combo2.selectedIndex].text;
                        this.hc_rotulo = $("#id").val();
                        var fecha =  new Date();
                        this.hora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
                        this.fecha_rotulo = fecha.getDate()+"/"+fecha.getMonth()+"/"+fecha.getFullYear();

                    }

                },
                /*------------------------ /INSERT------------------------------ */
                /*------------------------ ACCIONES------------------------------ */
                limpiar : function () {
                    //borrar información
                    this.guardar = true;
                    this.imprimir = false;
                    this.nuevo =false;

                    this.fecha= '';
                    this.servicios='';
                    this.medico='';
                    this.principal_codigo ='';
                    this.asociado1_codigo ='';
                    this.asociado2_codigo ='';
                    this.principal_descripcion ='';
                    this.asociado1_descripcion ='';
                    this.asociado2_descripcion ='';
                    this.observacion_m ='';

                    this.area = '1';
                    this.paciente_principal ='';
                    this.habilitadas ='';
                    this.unidad ='';
                    this.fecha_nacimiento ='';
                    this.edad ='';
                    this.unidad_asignada ='';
                    this.genero ='';
                    this.tipo_cama ='';
                    this.tipo_admision =0;
                    this.derivacion =false;
                    this.hospital ='';
                    this.documentos_completos= '';
                    this.observacion= '';
                    this.observacion_documentos ='';
                    this.telefono ='';
                    this.documento='';
                    this.direccion ='';
                    this.nombre_completo ='';
                    this.codigo_camma = '';
                    this.paciente = "";
                    //this.fecha_hora_ingreso ='';
                    //this.secuencia_ho =0;
                    //this.this.n_atencion = 0;
                    //this.n_transaccion =0;
                    //this.fecha_registro ='';

                    $("#secuencia").val('');
                    //$("#historial").val(a.historial);
                    $("#atencion").val('');
                    $("#transaccion").val('');
                    $("#fecha_registro").val('');
                    $("#fecha_hora_registro").val('');

                    this.ListaDatosTitular = [];
                    this.cmbMedico = [];
                    this.listaAfiliadosCedula = [];
                    this.listaPaciente = [];
                    this.listaCamashabilitadas= [];
                },
                imprimirRotulo : function(){
                    document.getElementById("rotulo").submit();

                    var a = document.createElement("a");
                    var b = document.createElement("a");
                    var c = document.createElement("a");
                    a.target = "_blank";
                    a.href = "reporte/ingreso/"+this.id;
                    a.click();

                    b.target = "_blank";
                    b.href = "garantia/admision/"+this.id;
                    b.click();

                    c.target = "_blank";
                    c.href = "registro/admision/"+this.id;
                    c.click();
                },
                consultarPaciente : function(){
                    $("#buscarPaciente").modal();
                },
                buscarDiagnostico : function(numero) {
                    $("#buscarDiagnostico").modal();
                    this.cargar = numero;
                },
                limpiarDiagnostico : function(numero){
                  if(numero === 1) {
                      this.principal_codigo = "";
                      this.principal_descripcion = "";
                      this.principal = false;
                  }else if(numero === 2){
                      this.asociado1_codigo = "";
                      this.asociado1_descripcion = "";
                      this.asociado1 = false;
                  } else if(numero === 3){
                      this.asociado2_codigo = "";
                      this.asociado2_descripcion = "";
                      this.asociado2 = false;
                  }
                },
                consultarAreas :function () {
                    var url = 'consultarcamasporsala/'+this.area+'';
                    axios.get(url).then(response => {
                        this.listaCamasArea = response.data;
                })
                    var combo = document.getElementById("sala");
                    this.nombre_sala = combo.options[combo.selectedIndex].text;
                    $("#buscarArea").modal();
                },
                /*------------------------ /ACCIONES------------------------------ */

                //modal para seleccionar las camas






                FormarFechaEdad2 : function (data) {
                    var fecha_nacimiento = new Date(data[0].fecha_nacimiento);
                    var dia = fecha_nacimiento.getDate();
                    var mes = fecha_nacimiento.getMonth();
                    mes = mes + 1;
                    if (mes < 10) {
                        mes = String("0" + mes);
                    }
                    if(dia < 10) {
                        dia = String("0" + dia);
                    }
                    var year = fecha_nacimiento.getFullYear();
                    var Fecha = String(year + "-" + mes + "-" + dia);
                    var hoy = new Date();
                    var cumpleanos = new Date(fecha_nacimiento);
                    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                    var m = hoy.getMonth() - cumpleanos.getMonth();

                    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                        edad--;
                    }

                    this.fecha_nacimiento = Fecha;
                    this.edad = edad;

                },
                calcular_edad : function () {
                    //this.edad = this.fecha_nacimiento - getdate()
                    //var today = new Date();
                    var hoy = new Date();
                    var cumpleanos = new Date(this.fecha_nacimiento);
                    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                    var m = hoy.getMonth() - cumpleanos.getMonth();

                    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                        edad--;
                    }

                    this.edad = edad;
                },

                pasarDatosPaciente : function (paciente,codigo) {
                   this.paciente_principal = paciente;


                        var urlUsers = 'consultarpaciente/'+codigo+'';
                        axios.get(urlUsers).then(response => {
                            this.listaAfiliadosCedula = response.data;
                            this.id = this.listaAfiliadosCedula[0].id;
                    //consultar si el paciente ya se encuentra en la base de datos
                            var url2 = '/hospitalizacion/validarhospitalizacion/' + this.id + '';
                                 axios.get(url2).then(response => {
                                    this.consulta = response.data;
                                    this.errores = [];
                                        if (this.consulta === 1) {
                                            toastr.error("El paciente se encuentra hospitalizado actualmente");
                                            this.errores.push("El paciente se encuentra hospitalizado actualmente");
                                        }
                                 })

                    this.genero = this.listaAfiliadosCedula[0].genero;
                    this.fecha_nacimiento = new Date(this.listaAfiliadosCedula[0].fecha_nacimiento);
                    // convertir la fecha que viene de la base en un tipo legible para el formulario ademas la edad
                    this.FormarFechaEdad2(response.data);
                    this.direccion = this.listaAfiliadosCedula[0].direccion;
                    this.telefono = this.listaAfiliadosCedula[0].telefono;
                    this.observacion = this.listaAfiliadosCedula[0].observacion;
                    this.tipo_seguro = this.listaAfiliadosCedula[0].tipo_seguro;
                    this.tipo_admision = this.listaAfiliadosCedula[0].tipo_seguro; // campos de consulta para cambio
                    this.tipo_parentesco = this.listaAfiliadosCedula[0].tipo_parentesco;
                    this.documento = this.listaAfiliadosCedula[0].cedula_titular;
                    this.estado_civil = this.listaAfiliadosCedula[0].estado_civil;
                    this.etnia = this.listaAfiliadosCedula[0].etnico;
                    this.estatus_discapacidad = this.listaAfiliadosCedula[0].status_discapacidad;
                    this.carnet_conadis = this.listaAfiliadosCedula[0].carnet_conadis;
                    this.tipo_parentesco = this.listaAfiliadosCedula[0].tipo_parentesco;

                    if (this.tipo_parentesco === "AA") {
                        this.nombre_completo = this.paciente_principal;
                        this.direccion = this.listaAfiliadosCedula[0].direccion;
                        this.telefono = this.listaAfiliadosCedula[0].telefono;
                        this.Titularid = this.listaAfiliadosCedula[0].id;
                    } else {

                        this.ConsultarTitularCedula();

                    }
                    $("#buscarPaciente").modal('hide');
                    })
                },

                ConsultarTitularCedula : function() {

                        var urlUsers = 'registropacientes/TitularAfiliadoPorCedula/'+this.documento+'';
                        axios.get(urlUsers).then(response => {
                            this.ListaDatosTitular  = response.data;


                                this.Titularid = this.ListaDatosTitular[0].id;
                                this.Titularprimer_nombre = this.ListaDatosTitular[0].primer_nombre;
                                this.Titularsegundo_nombre = this.ListaDatosTitular[0].segundo_nombre;
                                this.Titularapellido_paterno = this.ListaDatosTitular[0].apellido_paterno;
                                this.Titularapellido_materno = this.ListaDatosTitular[0].apellido_materno;
                                this.Titulardireccion = this.ListaDatosTitular[0].direccion;
                                this.Titulartelefono = this.ListaDatosTitular[0].telefono;
                                this.Titularcelular = this.ListaDatosTitular[0].celular;

                                //this.cedula_titular = this.Titularcedula;
                                this.apellidos = this.Titularapellido_paterno + ' ' + this.Titularapellido_materno;
                                this.nombres = this.Titularprimer_nombre + ' ' + this.Titularsegundo_nombre;
                                this.nombre_completo = this.nombres +""+this.apellidos+"";
                    })
                },

            }

        });

    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $.get("unidad", function (datos) {
                $.each(datos, function (key, value) {
                    $("#unidad").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#seguroList").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medicolist").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("cmbderivacionhospital", function (datos) {
                $.each(datos, function (key, value) {
                    $("#hospital").append("<option value=" + value.id + ">" + value.descripcion + "</option>");
                });

            });

            function enviar () {
                document.getElementById('form').submit();
            }
            $('[data-toggle="tooltip"]').tooltip();
        });
     </script>


@endsection
