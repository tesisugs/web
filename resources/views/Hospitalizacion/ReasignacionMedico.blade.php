@extends('layouts.principal')
@section('titulo','Reasignación Médico/Seguro')
@section('title','Reasignacion Medico')
@section('contenido')
    <div class="col-md-12" id="reasignacion">
        <div class="tile">

            <div class="form-group row">
                <label class="control-label col-md-1" for="paciente">Paciente:</label>
                <input class="form-control col-md-5" type="text" v-bind:readonly="visibilidad"  id="paciente" placeholder="Presione enter dentro del recuadro para realizar la busqueda" value="" v-model="paciente_principal" v-on:keyup.13="buscar" autocomplete="off">
            </div>

            <div class="form-group row">
                <label class="control-label col-md-1" for="sala">Area:</label>
                <div class="col-md-5">
                    <select class="form-control" id="sala" v-model="area" readonly >
                        <option></option>
                    </select>
                </div>
            </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#medico">Reasignación De Médico</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#seguro">Reasignación De Seguro</a>
                </li>
            </ul>

            <!-- tabs content -->
            <div class="tab-content">
                <div id="medico" class="container tab-pane active"><br>

                     <div class="form-group row">
                        <label class="control-label col-md-6" for="a_medico_list" >Médico Actual:</label>
                        <label class="control-label col-md-6" for="a_especializacion"> Servicios </label>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <select class="form-control" id="a_medico_list" disabled :value="m_a" v-model="medico_anterior">
                                <option></option>
                            </select>
                        </div>
                         <div class="col-md-6">
                            <select class="form-control"  disabled v-model="servicio_anterior" id="a_especializacion" :value="servicio_anterior">
                                <option></option>
                            </select>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label class="control-label col-md-6" for="medicolist" >Médico Asignado:</label>
                        <label class="control-label col-md-6" for="especializacion"> Servicios </label>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-6">

                            <select class="form-control" name="medico" id="medicos" :value="medico"  v-model="medico" >
                                <option  v-for="dato in cmbMedico" :value="dato.id" > @{{dato.NombreCompletos}} </option>
                            </select>

                        </div>
                         <div class="col-md-6">

                             <select v-model="servicios"   name="servicio" class="form-control" id="especializacion" v-on:change="consultarEspecialidadMedico">

                             </select>

                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-md-2" for="observacion">Observacion:</label>
                        <textarea class="col-md-10" v-model="observacion_m" id="observacion"> </textarea>
                    </div>
                    <button @click="cambioMedico" v-if="cambio_medico === true"  class="btn btn-primary">Enviar<i class="fa fa-paper-plane fa-3x" aria-hidden="true"  data-toggle="tooltip" title="Cambio de médico"></i></button>
                    <button type="button" class="btn btn-danger" @click="limpiar">Limpiar</button>

                </div>



                <div id="seguro" class="container tab-pane fade"><br>
                    <div class="form-group row">
                        <label class="control-label col-md-4" for="a_seguro_list" >V. Admision Actual:</label>
                        <div class="col-md-6">
                            <select class="form-control" disabled id="a_seguro_list" :value="m_a" v-model="seguro_anterior">
                                <option></option>
                            </select>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label class="control-label col-md-4" for="seguroList">V. Admision Nueva:</label>
                        <div class="col-md-6">
                            <select class="form-control" id="seguroList" v-model="nuevo_seguro">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-2" for="observacion_s" >Observacion:</label>
                        <textarea class="col-md-10" v-model="observacion_s" id="observacion_s"> </textarea>
                    </div>
                    <button @click="cambioSeguro" class="btn btn-primary">Enviar <i class="fa fa-paper-plane fa-3x" aria-hidden="true"  data-toggle="tooltip" title="Cambio de médico"></i></button>
                    <button type="button" class="btn btn-danger" @click="limpiar">Limpiar</button>

                </div>
            </div>
            <input type="hidden" id="respuesta_input">
            <input type="hidden" id="respuesta_input2">
        </div>
       <!-- modales -->
        <div class="modal face" id="buscar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Busqueda de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="Paciente" v-model="paciente" v-on:keyup.13="consultar" >
                                <input class="form-control col-md-3" type="date" v-model="desde"  placeholder="fecha desde" >
                                <input class="form-control col-md-3"   type="date" v-model="hasta" placeholder="fecha hasta " >
                            </div>
                            <hr>
                            <table class="table-bordered" v-if="registro === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>PACIENTE</td>
                                    <td>GENERO</td>
                                    <td>FECHA INGRESO</td>
                                    <td>TIPO INGRESO</td>
                                    <td>ESTADO</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" @click="pasarDatos(dato.id,dato.PACIENTE)"></i>  </td>
                                    <td> @{{dato.PACIENTE}} </td>
                                    <td>@{{dato.GENERO}} </td>
                                    <td>@{{dato.FECHA_INGRESO}} </td>
                                    <td>@{{dato.TIPO_DE_INGRESO}} </td>
                                    <td>@{{dato.ESTADO}} </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                        No existen registros en las fecha seleccionada
                                </div>

                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medicolist").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#a_medico_list").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#a_especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });


            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#seguroList").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#a_seguro_list").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <script type="text/javascript">
        var app = new Vue ({
            el:'#reasignacion',
            data : {

                // --- variable para almacenar los errores

                errores : [],
                visibilidad : false,
                registro : false,
                mensaje_registro : false,


                // -----
                paciente: '',
                desde :'',
                hasta: '',
                estado : 0,
                estado2 :'',
                listaPacientes : [],
                cmbEstado :[],

                // -----
                apellido : '',
                listaAfiliados : [],

                //--
                s_a : '',
                m_a :'',
                paciente_principal : '',
                medico_anterior :'',
                seguro_anterior :'',
                servicio_anterior :0,

                servicio_nuevo:0,
                medico_nuevo : 0,
                nuevo_seguro :0,
                area : 0,
                observacion_m :'',
                observacion_s :'',
                cmbMedico : [],


                // consulta

                datosConsulta :'',

                //---datos para guardar -- //
                id : '',
                Procedencias : '',
                Nombre_Responsable : '',
                Direccion_Responsable : '',
                Documento_Responsable : '',
                Telefono_Responsable : '',
                Observaciones : '',

                medico  : '',
                principal  : '',
                asociado_1 : '',
                asociado_2 : '',
                Medico_Observacion : '',
                //fecha_modificacion  : fecha_modificacion,
                //usuario_modificacion : usuario_modificacion,
                //pcname : pc,
                derivacion : '',
                hospital : '',
                status_documento : '',
                observacion_documento : '',
                status : '',


                servicios : 0,
                cambio_medico : true,


            },

            created : function() {

            },

            methods : {

                //-----------------SEC_CONSULTAR-----------------------//
                consultarEspecialidadMedico: function () {
                    var url = 'especialidadmedicos/'+this.servicios+'';
                    axios.get(url).then(response => {
                        this.cmbMedico  = response.data;
                })
                },
                validarConsulta : function() {

                    if(this.desde === '' || this.desde === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;
                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        if(d < 10) {
                            d = ''+0+d;
                        }
                        this.desde = ''+y+'-'+m+'-'+d+'';

                    }

                    if(this.hasta === '' || this.hasta === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;
                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        if(d < 10) {
                            d = ''+0+d;
                        }
                        this.hasta = ''+y+'-'+m+'-'+d+'';
                    }


                },
                consultar : function () {

                    this.validarConsulta();

                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    if( this.errores.length === 0) {
                        // document.getElementById('registroTitular').submit()
                        this.consultarPacientes()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];


                },
                consultarPacientes : function()  {
                    if(this.paciente === '' && this.desde === '' && this.hasta === '')
                    {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"";
                    } else {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"/"+this.paciente+"";
                    }
                    axios.get(hospitalizacion ).then(response => {
                        this.listaPacientes  = response.data;
                    var num = this.listaPacientes.length;
                    if(num === 0) {
                        this.registro = false;
                        this.mensaje_registro = true;
                    } else {
                        this.registro = true;
                        this.mensaje_registro = false;
                    }
                })
                },
                //----------------/SEC_CONSULTAR-----------------------//
                //-----------------SEC_PASAR DATOS-----------------------//
                pasarDatos : function (id,paciente) {
                    this.paciente_principal = paciente;

                    var url =  "consultarhospitalizacionid/"+id+'';

                    axios.get(url).then(response => {
                        this.datosConsulta  = response.data;

                    this.s_a = this.datosConsulta[0].Procedencias;
                    this.m_a = this.datosConsulta[0].medico;
                    this.seguro_anterior = this.datosConsulta[0].Procedencias;
                    this.medico_anterior = this.datosConsulta[0].medico;
                    this.area = this.datosConsulta[0].habitacion;

                    this.id = this.datosConsulta[0].id;
                    this.Procedencias = this.datosConsulta[0].Procedencias;
                    this.Nombre_Responsable = this.datosConsulta[0].Nombre_Responsable;
                    this.Direccion_Responsable = this.datosConsulta[0].Direccion_Responsable;
                    this.Documento_Responsable = this.datosConsulta[0].Documento_Responsable;
                    this.Telefono_Responsable = this.datosConsulta[0].Telefono_Responsable;
                    this.Observaciones = this.datosConsulta[0].Observaciones;
                    this.Servicios = this.datosConsulta[0].Servicios;
                    this.servicio_anterior = this.datosConsulta[0].Servicios;
                    this.medico  = this.datosConsulta[0].medico;
                    this.principal  = this.datosConsulta[0].principal;
                    this.asociado_1 = this.datosConsulta[0].asociado_1;
                    this.asociado_2 = this.datosConsulta[0].asociado_2;
                    this.Medico_Observacion = this.datosConsulta[0].Medico_Observacion;
                    //this.fecha_modificacion  = this.datosConsulta[0].fecha_modificacion;
                    //this.usuario_modificacion = this.datosConsulta[0].usuario_modificacion;
                    //this.pcname = this.datosConsulta[0].pc;
                    this.derivacion = this.datosConsulta[0].derivacion;
                    this.hospital = this.datosConsulta[0].hospital;
                    this.status_documento = this.datosConsulta[0].status_documento;
                    this.observacion_documento = this.datosConsulta[0].observacion_documento;
                    this.status = this.datosConsulta[0].status;
                    //this.medico_anterior = this.datosConsulta[0].medico;
                    //alert(this.datosConsulta[0].medico);
                })
                    this.visibilidad = true;
                    $("#buscar").modal('hide');

                },
                //----------------/SEC_PASAR DATOS-----------------------//
                //-----------------SEC_VALIDAR-----------------------//
                validarMedicoSeguro : function () {
                    if(this.servicio_nuevo === 0 || this.servicio_nuevo === undefined)
                    {
                        this.errores.push("Seleccione el nuevo servicio");
                    }

                    if(this.medico_nuevo === 0 || this.medico_nuevo === undefined)
                    {
                        this.errores.push("Seleccione el nuevo médico");
                    }

                    if(this.paciente_principal === '' || this.paciente_principal === undefined)
                    {
                        this.errores.push("Realice la busqueda del paciente");
                    }


                    if(this.nuevo_seguro === 0 || this.nuevo_seguro=== undefined)
                    {
                        this.errores.push("Seleccione el nuevo seguro");
                    }

                    if( this.errores.length === 0) {
                        this.cambioMedicoSeguro()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];

                },
                //----------------/SEC_VALIDAR-----------------------//
                //-----------------SEC_BOTONES-----------------------//
                buscar : function () {
                    $("#buscar").modal();
                },
                limpiar : function(){
                    this.medico = "";
                    this.servicios = "";
                    this.nuevo_seguro = "";
                    this.observacion_m = "";
                    this.observacion_s = "";
                    this.paciente_principal = "";
                    this.area = "";
                    this.medico_anterior = "";
                    this.servicio_anterior = "";
                    this.seguro_anterior = "";
                    this.visibilidad = false;
                    this.cambio_medico = true;
                    this.cmbMedico = [];
                    this.datosConsulta = [];
                    this.listaPacientes = [];
                    this.desde = "";
                    this.hasta = "";
                    $("#respuesta_input2").val("");
                    $("#respuesta_input1").val("");
                },
                //----------------/SEC_BOTONES-----------------------//
                //-----------------SEC_INGRESAR-----------------------//
                cambioMedicoSeguro : function () {

                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        //--
                        "id" : this.id,
                        "Procedencias" : this.nuevo_seguro,
                        "Nombre_Responsable" : this.Nombre_Responsable,
                        "Direccion_Responsable" : this.Direccion_Responsable,
                        "Documento_Responsable" : this.Documento_Responsable,
                        "Telefono_Responsable" : this.Telefono_Responsable,
                        "Observaciones" : this.Observaciones,
                        "Servicios" : this.Servicios,
                        "medico"  : this.medico,
                        "principal"  : this.principal,
                        "asociado_1" : this.asociado_1,
                        "asociado_2" : this.asociado_2,
                        "Medico_Observacion" : this.Medico_Observacion,
                        "derivacion" : this.derivacion,
                        "hospital" : this.hospital,
                        "status_documento" : this.status_documento,
                        "observacion_documento" : this.observacion_documento,
                        "status" : this.status,

                    };
                    $.ajax({
                        data : parametros,
                        url : "cambioseguro",
                        type : "post",
                        async : false,
                        success : function(response){
                            toastr.success('Seguro cambiado con exito.', 'Exito', {timeOut: 5000});
                        },
                        error : function (response,jqXHR) {
                            toastr.error('Error al momento de cambiar el seguro.', 'Error', {timeOut: 5000});

                        }
                    });

                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        //--
                        "id" : this.id,
                        "Procedencias" : this.Procedencias,
                        "Nombre_Responsable" : this.Nombre_Responsable,
                        "Direccion_Responsable" : this.Direccion_Responsable,
                        "Documento_Responsable" : this.Documento_Responsable,
                        "Telefono_Responsable" : this.Telefono_Responsable,
                        "Observaciones" : this.Observaciones,
                        "Servicios" : this.Servicios,
                        "medico"  : this.medico_nuevo,
                        "principal"  : this.principal,
                        "asociado_1" : this.asociado_1,
                        "asociado_2" : this.asociado_2,
                        "Medico_Observacion" : this.Medico_Observacion,
                        "derivacion" : this.derivacion,
                        "hospital" : this.hospital,
                        "status_documento" : this.status_documento,
                        "observacion_documento" : this.observacion_documento,
                        "status" : this.status,

                    };
                    $.ajax({
                        data : parametros,
                        url : "cambiomedico",
                        type : "post",
                        async : false,
                        success : function(response){
                            //response contiene la respuesta al llamado de tu archivo
                            //aqui actualizas los valores de inmediato llamando a sus respectivas id.
                            toastr.success('Medico cambiado con exito.', 'Exito', {timeOut: 5000});
                        },
                        error : function (response,jqXHR) {
                            toastr.error('Error al momento de cambiar el médico.', 'Error', {timeOut: 5000});

                        }
                    });



                },
                // corregir
                cambioMedico : function () {

                    if(this.servicios === 0 || this.servicios === undefined)
                    {
                        this.errores.push("Seleccione el servicio");
                    }

                    if(this.medico === 0 || this.medico === undefined)
                    {
                        this.errores.push("Seleccione el médico");
                    }

                    if(this.paciente_principal === '' || this.paciente_principal === undefined)
                    {
                        this.errores.push("Realice primero la consulta del paciente");
                    }

                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    if( this.errores.length === 0) {
                        var parametros = {
                            "_token": "{{ csrf_token() }}",
                            //--
                            "id" : this.id,
                            "Servicios" : this.Servicios,
                            "medico"  : this.medico,
                            "observacion" : this.observacion_m,
                        };
                        $.ajax({
                            data : parametros,
                            url : "cambiomedico",
                            type : "post",
                            async : false,
                            success : function(response){
                                $("#respuesta_input").val("respuesta");
                                toastr.success('Médico cambiado con exito.', 'Exito', {timeOut: 5000});
                            },
                            error : function (response,jqXHR) {
                                if(response.status === 422)
                                {
                                    var errors = $.parseJSON(response.responseText);
                                    $.each(errors, function (key, value) {
                                        if($.isPlainObject(value)) {
                                            $.each(value, function (key, value) {
                                                toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                                console.log(key+ " " +value);
                                            });
                                        }else{
                                            toastr.error('Error al momento de realizar el cambio del médico. '+''+'', 'Error', {timeOut: 5000});
                                        }
                                    });
                                } else {
                                    toastr.error('Error al momento de realizar el cambio de médico::: '+''+'', 'Error', {timeOut: 5000});
                                }

                            }
                        })

                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                    if( $("#respuesta_input").val() !== '') {
                        this.limpiar();
                    }
                },
                cambioSeguro :function () {

                    if(this.nuevo_seguro === 0 || this.nuevo_seguro=== undefined)
                    {
                        this.errores.push("Seleccione el nuevo seguro");
                    }

                    if(this.paciente_principal === '' || this.paciente_principal === undefined)
                    {
                        this.errores.push("Realice primero la consulta del paciente");
                    }

                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    if( this.errores.length === 0) {
                        var parametros = {
                            "_token": "{{ csrf_token() }}",
                            //--
                            "id" : this.id,
                            "Procedencias" : this.nuevo_seguro,
                            "observacion" : this.observacion_s,

                        };
                        $.ajax({
                            data : parametros,
                            url : "cambioseguro",
                            type : "post",
                            async : false,
                            success : function(response){
                                $("#respuesta_input2").val("respuesta");
                                toastr.success('Seguro cambiado con exito.', 'Exito', {timeOut: 5000});
                            },
                            error : function (response,jqXHR) {
                                if(response.status === 422)
                                {
                                    var errors = $.parseJSON(response.responseText);
                                    $.each(errors, function (key, value) {
                                        if($.isPlainObject(value)) {
                                            $.each(value, function (key, value) {
                                                toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                                console.log(key+ " " +value);
                                            });
                                        }else{
                                            toastr.error('Error al momento de realizar el cambio del seguro. '+''+'', 'Error', {timeOut: 5000});
                                        }
                                    });
                                } else {
                                    toastr.error('Error al momento de realizar el cambio del seguro::: '+''+'', 'Error', {timeOut: 5000});
                                }


                            }
                        })
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    if( $("#respuesta_input2").val() !== '') {
                        this.limpiar();
                    }
                    this.errores = [];
                },
                //----------------/SEC_INGRESAR-----------------------//
            }

        });

    </script>



@endsection


















