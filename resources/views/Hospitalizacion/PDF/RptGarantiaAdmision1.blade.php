<HTML>
<HEAD>
<META NAME="Author" CONTENT="Crystal Reports 13.0">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
</HEAD>
<BODY>
<style>    div {position:absolute; z-index:25}
    a {text-decoration:none}
    a img {border-style:none; border-width:0}
    .fc1i9m05da7l28-0 {font-size:13pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fc1i9m05da7l28-1 {font-size:13pt;color:#000000;font-family:Arial;font-weight:bold;text-decoration:underline;}
    .fc1i9m05da7l28-2 {font-size:9pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fc1i9m05da7l28-3 {font-size:11pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fc1i9m05da7l28-4 {font-size:10pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fc1i9m05da7l28-5 {font-size:9pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fc1i9m05da7l28-6 {font-size:7pt;color:#000000;font-family:Arial;font-weight:normal;}
    .ad1i9m05da7l28-0 {border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0;}
    .ad1i9m05da7l28-1 {background-color:#c0c0c0;layer-background-color:#c0c0c0;border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0;}
</style><div style="z-index:15;top:303px;left:8px;border-color:#808080;border-style:solid;border-width:0px;border-top-width:1px;width:293px;"></div>
<div style="z-index:15;top:280px;left:84px;border-color:#808080;border-style:solid;border-width:0px;border-top-width:1px;width:217px;"></div>
<div style="z-index:15;top:326px;left:84px;border-color:#808080;border-style:solid;border-width:0px;border-top-width:1px;width:217px;"></div>
<div style="z-index:15;top:348px;left:395px;border-color:#808080;border-style:solid;border-width:0px;border-top-width:1px;width:331px;"></div>
<div style="z-index:15;top:394px;left:445px;border-color:#808080;border-style:solid;border-width:0px;border-top-width:1px;width:281px;"></div>
<div style="z-index:15;top:371px;left:513px;border-color:#808080;border-style:solid;border-width:0px;border-top-width:1px;width:213px;"></div>
<div style="z-index:3;clip:rect(0px,746px,75px,0px);top:0px;left:0px;width:746px;height:75px;"></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:0px;left:11px;width:115px;height:68px;"><img src="{{asset('storage/LogoHospital.png')}}" border="0" width="115px" height="68px"></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:0px;left:128px;width:618px;height:68px;clip:rect(0px,618px,68px,0px);"><table width="618px" border="0" cellpadding="0" cellspacing="0"><td align="center"><span class="fc1i9m05da7l28-0">BENEMERITA&nbsp;SOCIEDAD&nbsp;PROTECTORA&nbsp;DE&nbsp;LA&nbsp;INFANCIA</span></td></table><table width="618px" border="0" cellpadding="0" cellspacing="0"><td align="center"><span class="fc1i9m05da7l28-0">HOSPITAL&nbsp;"&nbsp;LEON&nbsp;BECERRA&nbsp;"&nbsp;-&nbsp;GUAYAQUIL</span></td></table><table width="618px" border="0" cellpadding="0" cellspacing="0"><td align="center"><span class="fc1i9m05da7l28-0">&nbsp;</span></td></table></div>
<div style="z-index:3;clip:rect(0px,746px,328px,0px);top:75px;left:0px;width:746px;height:328px;"></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:314px;left:395px;width:114px;height:15px;"><table width="214px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Lugar&nbsp;de&nbsp;Trabajo: {{$datos->lugar_trabajo}}</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:246px;left:395px;width:65px;height:15px;"><table width="365px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Nombre&nbsp;: {{$datos->paciente}}</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:269px;left:395px;width:68px;height:15px;"><table width="268px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Cédula&nbsp;N°: {{$datos->cedula}}</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:314px;left:8px;width:68px;height:15px;"><table width="268px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Teléfono: {{$datos->telefono_medico}}</span></td></table></div>
<div class="ad1i9m05da7l28-1" nowrap="true" style="z-index:25;top:223px;left:8px;width:293px;height:15px;"><table width="323px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Atendido&nbsp;por&nbsp;el&nbsp;Doctor: <small>{{$datos->medico_tratante}} </small> </span></td></table></div>
<div class="ad1i9m05da7l28-1" nowrap="true" style="z-index:25;top:75px;left:0px;width:748px;height:19px;clip:rect(0px,746px,19px,0px);"><table width="748px" border="0" cellpadding="0" cellspacing="0"><td align="center"><span class="fc1i9m05da7l28-3">G&nbsp;A&nbsp;R&nbsp;A&nbsp;N&nbsp;T&nbsp;Í&nbsp;A</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:360px;left:395px;width:118px;height:15px;"><table width="218px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Telf.&nbsp;del&nbsp;Trabajo:</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:113px;left:11px;width:737px;height:15px;clip:rect(0px,734px,15px,0px);"><table width="737px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-4">POR&nbsp;LA&nbsp;PRESENTE&nbsp;&nbsp;ASUMO&nbsp;LA&nbsp;GARANTÍA&nbsp;POR&nbsp;LA&nbsp;CUENTA&nbsp;DE&nbsp;GASTOS&nbsp;QUE&nbsp;ORIGINE&nbsp;&nbsp;EL&nbsp;PACIENTE</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:166px;left:8px;width:186px;height:15px;"><table width="286px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-5">HOSPITALIZADO&nbsp;EN&nbsp;LA&nbsp;SALA {{$datos->area}}  </span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:166px;left:306px;width:198px;height:15px;"><table width="398px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-5">A&nbsp;PARTIR&nbsp;DE&nbsp;LA&nbsp;FECHA {{$datos->fecha_ingreso}}&nbsp;HASTA </span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:189px;left:8px;width:179px;height:15px;"><table width="179px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-5">SU&nbsp;EGRESO</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:268px;left:8px;width:72px;height:15px;"><table width="300px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Dirección: {{$datos->direccion_medico}}</span></td></table></div>
<div class="ad1i9m05da7l28-1" nowrap="true" style="z-index:25;top:223px;left:395px;width:293px;height:15px;"><table width="293px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Garantía:</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:291px;left:395px;width:65px;height:15px;"><table width="265px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Teléfono: {{$datos->telefono}}</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:383px;left:395px;width:46px;height:15px;"><table width="46px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-2">Firma:</span></td></table></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:166px;left:299px;width:8px;height:15px;"><table width="8px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m05da7l28-5">#</span></td></table></div>
<div style="z-index:3;clip:rect(0px,746px,15px,0px);top:403px;left:0px;width:746px;height:15px;"></div>
<div class="ad1i9m05da7l28-0" nowrap="true" style="z-index:25;top:403px;left:684px;width:62px;height:15px;clip:rect(0px,62px,15px,0px);"><span class="fc1i9m05da7l28-6"></span></div>
<div style="z-index:3;clip:rect(0px,746px,40px,0px);top:417px;left:0px;width:746px;height:40px;"></div>
<div style="z-index:3;clip:rect(0px,746px,40px,0px);top:1035px;left:0px;width:746px;height:40px;"></div>

<div id="pageNavigator" style="top:1075px;left:0px;font-style:italic;font-weight:100;font-size:smaller"> 
<hr>
</div>
</BODY>
<script>
    window.print();
</script>
</HTML>