<HTML>
<HEAD>
<META NAME="Author" CONTENT="Crystal Reports 13.0">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
</HEAD>
<BODY>

<style>    div {position:absolute; z-index:25}
    a {text-decoration:none}
    a img {border-style:none; border-width:0}
    .fc1i9m02lmyqr4-0 {font-size:9pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fc1i9m02lmyqr4-1 {font-size:17pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fc1i9m02lmyqr4-2 {font-size:15pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fc1i9m02lmyqr4-3 {font-size:13pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fc1i9m02lmyqr4-4 {font-size:6pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fc1i9m02lmyqr4-5 {font-size:10pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fc1i9m02lmyqr4-6 {font-size:11pt;color:#000000;font-family:Arial;font-weight:bold;}
    .ad1i9m02lmyqr4-0 {border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0;}
    .ad1i9m02lmyqr4-1 {background-color:#ffffff;layer-background-color:#ffffff;border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0;}
</style><div style="z-index:10;top:66px;left:32px;width:628px;height:340px;border-color:#000000;border-style:solid;border-width:2px;"><table width="619px" height="331px"><tr><td>&nbsp;</td></tr></table></div>
<div style="z-index:10;top:52px;left:16px;width:654px;height:365px;border-color:#000000;border-style:dashed;border-width:1px;"><table width="648px" height="359px"><tr><td>&nbsp;</td></tr></table></div>
<div style="z-index:10;top:460px;left:16px;width:654px;height:365px;border-color:#000000;border-style:dashed;border-width:1px;"><table width="648px" height="359px"><tr><td>&nbsp;</td></tr></table></div>
<div style="z-index:10;top:476px;left:32px;width:628px;height:340px;border-color:#000000;border-style:solid;border-width:2px;"><table width="619px" height="331px"><tr><td>&nbsp;</td></tr></table></div>
<div style="z-index:10;top:860px;left:16px;width:654px;height:94px;border-color:#000000;border-style:dashed;border-width:1px;"><table width="648px" height="88px"><tr><td>&nbsp;</td></tr></table></div>
<div style="z-index:15;top:150px;left:33px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:631px;"></div>
<div style="z-index:15;top:564px;left:32px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:631px;"></div>
<div style="z-index:15;top:630px;left:504px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:128px;"></div>
<!-- <div style="z-index:15;top:700px;left:504px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:128px;"></div> -->
<div style="z-index:3;clip:rect(0px,768px,36px,0px);top:0px;left:0px;width:768px;height:36px;"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:0px;left:688px;width:66px;height:15px;"><span class="fc1i9m02lmyqr4-0">01/08/2018</span></div>
<div style="z-index:3;clip:rect(0px,768px,926px,0px);top:36px;left:0px;width:768px;height:926px;"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:74px;left:48px;width:98px;height:67px;"><img src="{{asset('storage/LogoHospital.png')}}" border="0" width="98px" height="67px"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:98px;left:152px;width:496px;height:40px;"><table width="496px" border="0" cellpadding="0" cellspacing="0"><td align="center"><span class="fc1i9m02lmyqr4-1">HOSPITAL&nbsp;"LEÓN&nbsp;BECERRA"</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:154px;left:40px;width:136px;height:32px;"><table width="536px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">PACIENTE:{{$datos->paciente}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:194px;left:40px;width:88px;height:32px;"><table width="88px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">EDAD:</span>{{$datos->edad}}</td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:274px;left:40px;width:208px;height:32px;"><table width="408px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">HISTORIA&nbsp;CLINICA: {{$datos->hc}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:314px;left:40px;width:136px;height:32px;"><table width="336px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">CONVENIO: {{$datos->convenio}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:354px;left:40px;width:136px;height:32px;"><table width="536px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">MÉDICO: {{$datos->medico}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:226px;left:40px;width:144px;height:32px;"><table width="344px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">HABITACIÓN: {{$datos->habitacion}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:154px;left:184px;width:448px;height:23px;"><span class="fc1i9m02lmyqr4-3"></span></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:330px;left:520px;width:129px;height:45px;"><img src="{{asset('storage/benemerita.png')}}" border="0" width="129px" height="45px"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:234px;left:568px;width:72px;height:24px;text-align:center;"><table width="72px" border="0" cellpadding="0" cellspacing="0"><tr><td align="center"><span class="fc1i9m02lmyqr4-0">{{$datos->numero_cama}}</span></td></tr></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:226px;left:424px;width:144px;height:32px;"><table width="144px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">N°&nbsp;DE&nbsp;CAMA&nbsp;: </span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:484px;left:48px;width:98px;height:67px;"><img src="{{asset('storage/LogoHospital.png')}}" border="0" width="98px" height="67px"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:508px;left:152px;width:496px;height:40px;"><table width="496px" border="0" cellpadding="0" cellspacing="0"><td align="center"><span class="fc1i9m02lmyqr4-1">HOSPITAL&nbsp;"LEÓN&nbsp;BECERRA"</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:772px;left:40px;width:136px;height:32px;"><table width="536px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">MÉDICO: {{$datos->medico}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:732px;left:40px;width:136px;height:32px;"><table width="536px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">CONVENIO: {{$datos->convenio}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:692px;left:40px;width:208px;height:32px;"><table width="408px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">HISTORIA&nbsp;CLINICA: {{$datos->hc}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:652px;left:40px;width:144px;height:32px;"><table width="344px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">HABITACIÓN: {{$datos->habitacion}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:612px;left:40px;width:88px;height:32px;"><table width="88px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">EDAD:{{$datos->edad}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:572px;left:40px;width:136px;height:32px;"><table width="536px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">PACIENTE: {{$datos->paciente}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:572px;left:184px;width:448px;height:23px;"><span class="fc1i9m02lmyqr4-3"></span></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:660px;left:560px;width:72px;height:24px;text-align:center;"><table width="72px" border="0" cellpadding="0" cellspacing="0"><tr><td align="center"><span class="fc1i9m02lmyqr4-0">{{$datos->numero_cama}}</span></td></tr></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:652px;left:416px;width:144px;height:32px;"><table width="144px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">N°&nbsp;DE&nbsp;CAMA&nbsp;:</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:748px;left:520px;width:129px;height:45px;"><img src="{{asset('storage/benemerita.png')}}" border="0" width="129px" height="45px"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:692px;left:416px;width:80px;height:32px;"><table width="180px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">HORA&nbsp;: {{$datos->hora}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:612px;left:416px;width:80px;height:32px;"><table width="80px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-2">PESO&nbsp;:</span></td></table></div>
<div class="ad1i9m02lmyqr4-1" nowrap="true" style="z-index:25;top:36px;left:16px;width:292px;height:15px;"><table width="292px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-4">RECORTAR&nbsp;EN&nbsp;ESTAS&nbsp;LÍNEAS&nbsp;&nbsp;-&nbsp;&nbsp;PARA&nbsp;LA&nbsp;PUERTA</span></td></table></div>
<div class="ad1i9m02lmyqr4-1" nowrap="true" style="z-index:25;top:444px;left:16px;width:344px;height:15px;"><table width="344px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-4">RECORTAR&nbsp;EN&nbsp;ESTAS&nbsp;LÍNEAS&nbsp;&nbsp;-&nbsp;&nbsp;PARA&nbsp;LA&nbsp;CARPETA&nbsp;DEL&nbsp;PACIENTE</span></td></table><table width="344px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-4">&nbsp;</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:868px;left:123px;width:261px;height:16px;"><span class="fc1i9m02lmyqr4-5"></span></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:868px;left:32px;width:86px;height:16px;"><table width="386px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-6">PACIENTE: {{$datos->paciente}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:899px;left:32px;width:78px;height:16px;"><table width="378px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-6">MÉDICO: {{$datos->medico}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:931px;left:32px;width:90px;height:16px;"><table width="340px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-6">CONVENIO: {{$datos->convenio}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:868px;left:392px;width:38px;height:16px;"><table width="238px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-6">H.C.: {{$datos->hc}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:899px;left:392px;width:79px;height:16px;"><table width="179px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-6">INGRESO: {{$datos->fecha}}</span></td></table></div>
<div class="ad1i9m02lmyqr4-1" nowrap="true" style="z-index:25;top:844px;left:16px;width:344px;height:15px;"><table width="344px" border="0" cellpadding="0" cellspacing="0"><td align="left"><span class="fc1i9m02lmyqr4-4">RECORTAR&nbsp;EN&nbsp;ESTAS&nbsp;LÍNEAS&nbsp;&nbsp;-&nbsp;&nbsp;PARA&nbsp;CONTROL&nbsp;DE&nbsp;ENFERMERÍA</span></td></table></div>
<div style="z-index:3;clip:rect(0px,768px,26px,0px);top:982px;left:0px;width:768px;height:26px;"></div>
<div class="ad1i9m02lmyqr4-0" nowrap="true" style="z-index:25;top:990px;left:0px;width:63px;height:15px;text-align:right;"><table width="63px" border="0" cellpadding="0" cellspacing="0"><tr><td align="right"><span class="fc1i9m02lmyqr4-0">1</span></td></tr></table></div>

<div id="pageNavigator" style="top:1008px;left:0px;font-style:italic;font-weight:100;font-size:smaller"> 
<hr>
</div>
</BODY>
<script>
    window.print();
</script>
</HTML>
