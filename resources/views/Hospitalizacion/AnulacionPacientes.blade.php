@extends('layouts.principal')
@section('titulo','Anulación')
@section('title','Anulación')
@section('contenido')
    <div class="col-md-12" id="anulacion" v-cloak>

        <div class="tile">
            <!-- Menu de la pagina -->
            <div class="form-group row">
               <!-- <div class="col-md-1">
                    <i v-if="nuevo === 'activo' " class="fa fa-file-o fa-3x"   aria-hidden="true"   ></i>
                    <i v-else class=" v-else fa fa-file-o fa-3x"   aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"    ></i>
                    <p >Nuevo</p>
                </div> -->

                <div class="col-md-1">
                    <i v-if="buscar === 'activo'" class="fa fa-search-minus fa-3x"   aria-hidden="true"   id="show-modal" data-toggle="modal" data-target="#buscar" ></i>
                    <i v-else  class="fa fa-search-minus fa-3x"   aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"  ></i>
                    <p >Buscar</p>
                </div>
                <div class="col-md-1">
                    <i v-if="buscar === 'activo'" class="fa fa-search-plus fa-3x"   aria-hidden="true"   id="show-modal" data-toggle="modal" data-target="#buscar2" ></i>
                    <i v-else  class="fa fa-search-plus fa-3x"   aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"  ></i>
                    <p >Cargar</p>
                </div>
                <div class="col-md-1">
                    <i v-if="visualizar" class=" fa fa-eye fa-3x"   aria-hidden="true"  @click="VisualizarInformes" ></i>
                    <i v-else  class="fa fa-eye fa-3x"   aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"  ></i>
                    <p >Visualizar</p>
                </div>

            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#general">General</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#medico">Medico</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="tab" href="#observacion">Observación</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="general" class="container tab-pane active"><br>

                        <div class="tile-body">

                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="optradio" v-model="hospitalizacion_cirugia" value="hospitalizacion">Hospitalizacion </label>
                                <label class="radio-inline"><input type="radio" name="optradio" v-model="hospitalizacion_cirugia" value="cirugia">Cirugia Ambulatoria</label>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Paciente</label>
                                        <div class="col-md-8">
                                            <input v-model="paciente_principal" readonly class="form-control " type="text" name="paciente" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label  col-md-2" for="sala">Area</label>
                                        <div class="col-md-8">
                                            <select class="form-control" v-model="area" disabled id="sala" :value="area" >

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Unidad</label>
                                        <div class="col-md-8">
                                            <select class="form-control" v-model="unidad" disabled :value="unidad" id="unidad" >

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">N° de unidad asignada</label>
                                        <div class="col-md-8">
                                            <input v-model="unidad_asignada" readonly name="N_unidad_asignada" class="form-control" type="text" placeholder=""  >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Fecha de nacimiento</label>
                                        <div class="col-md-5">
                                            <input v-model="fecha_nacimiento" readonly name="fecha_nacimiento" class="form-control" type="text" placeholder="dd/mm/aaaa"  >
                                        </div>
                                        <input type="text" v-model="edad" readonly  class="col-md-2 form-control" placeholder="">
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Genero</label>
                                        <div class="col-md-3">
                                            <input v-model="genero"  name="genero" readonly class="form-control" type="text" placeholder=""  >
                                        </div>
                                        <label class="control-label col-md-2">V. Admision </label>
                                        <div class="col-md-5">
                                            <select class="form-control" disabled id="seguroList" v-model="tipo_admision" :value="tipo_admision"></select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class=" col-md-3" ><input readonly v-model="derivacion" type="checkbox" name="Derivacion" value="">Derivación</label>
                                        <select class="col-md-8 disabled form-control" disabled name="derivacion2">
                                            <option></option>
                                        </select>
                                    </div>

                                    <input type="checkbox" disabled v-model="documentos_completos" readonly name="Documentos completos" value="">Documentos completos<br>

                                    <div class="form-group row">
                                        <label class="control-label">Observacion documentos completos</label>
                                        <div class="col-md-12">
                                            <textarea v-model="observacion_documentos" readonly name="observacionDocumentos" class="form-control"  placeholder=""  cols="3" ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label">Observaciones</label>
                                        <div class="col-md-12">
                                            <textarea v-model="observacion" readonly name="observacion" class="form-control"  placeholder=""  cols="3" ></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Fecha de registro hospitalizacion</label>
                                        <div class="col-md-9">
                                            <input type="text" v-model="fecha_registro" readonly  name="fecha_registro" class="form-control" placeholder="dd/mm/aaaa" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">N. atencion:</label>
                                        <div class="col-md-3">
                                            <input v-model="n_atencion" name="N_atencion" readonly class="form-control" type="text" placeholder=""  >
                                        </div>
                                        <label class="control-label col-md-2">N. trans:</label>
                                        <div class="col-md-3">
                                            <input v-model="transaccion" readonly  name="N_trans" class="form-control" type="text" placeholder=""  >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Secuencia HO</label>
                                        <div class="col-md-8">
                                            <input  v-model="secuencia_ho" readonly name="secuencia_ho" class="form-control" type="text" placeholder=""  >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Fecha y hora de ingreso area</label>
                                        <div class="col-md-9">
                                            <input v-model="fecha_hora_ingreso" readonly  name="f_h_ingresoArea" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Nombre</label>
                                        <div class="col-md-9">
                                            <input  v-model="nombre_responable" readonly name="nombre" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Direccion</label>
                                        <div class="col-md-9">
                                            <input  v-model="direccion_responsable" readonly name="direccion" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Documento</label>
                                        <div class="col-md-9">
                                            <input v-model="documento" readonly  name="documento" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Telefono</label>
                                        <div class="col-md-9">
                                            <input v-model="telefono"  readonly name="telefono" class="form-control" type="text" placeholder="" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        </div>
                <div id="medico" class="container tab-pane fade"><br>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Fecha</label>
                        <div class="col-md-10">
                            <input  v-model="medico_fecha" name="fecha_pm" readonly class="form-control" type="text" placeholder="yyyy/mm/dd">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Servicios</label>
                        <div class="col-md-9">
                            <select v-model="servicios" disabled   name="servicios" class="form-control" id="especializacion" >

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Médico</label>
                        <div class="col-md-9">
                            <select v-model="medico" disabled  name="medico" class="form-control" id="medicolist" >

                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Principal</label>
                        <div class="col-md-3">
                            <input type="text" v-model="principal_codigo" readonly  name="" class="form-control"  placeholder=""  >
                        </div>
                        <div class="col-md-5">
                            <input type="text" v-model="principal_descripcion" readonly name="" class="form-control"  placeholder=""  >
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Asociado 1</label>
                        <div class="col-md-3">
                            <input type="text"  v-model="asociado1_codigo" name="" readonly class="form-control"  placeholder=""  >
                        </div>
                        <div class="col-md-5">
                            <input type="text" v-model="asociado1_descripcion" name="" readonly class="form-control"  placeholder=""  >
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Asociado 2</label>
                        <div class="col-md-3">
                            <input type="text" v-model="asociado2_codigo" name="" readonly class="form-control"  placeholder=""  >
                        </div>
                        <div class="col-md-5">
                            <input type="text" v-model="asociado2_descripcion" readonly name="" class="form-control"  placeholder=""  >
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Observación</label>
                        <div class="col-md-8">
                            <textarea class="form-control" readonly v-model="observacion_m" name="observacion_tm" placeholder=""  cols="2"> </textarea>
                        </div>
                    </div>

                </div>

                <div id="observacion" class="container tab-pane fade"><br>
                    <label class="control-label col-md-2" >Observación:</label><br>
                    <textarea class="form-control" v-model="observacion_eliminada" name="observacion_eliminada" placeholder=""  rows="3" cols="40"></textarea>
                    <br>
                    <button v-if="btn_anular" class="btn btn-danger" @click="validarAnulacion()"> Anular</button>
                </div>
                <input type="hidden" v-model="id" name="id">
                <input type="hidden" v-model="paciente_id" name="paciente_id">



            </div>
        </div>
        <!-- modales -->
        <div class="modal face" id="buscar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta Pacientes Hospitalizados / Traspasados
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="Paciente" v-model="paciente" v-on:keyup.13="consultar" >
                                <input class="form-control col-md-3" type="date" v-model="desde"  placeholder="fecha desde" >
                                <input class="form-control col-md-3"   type="date" v-model="hasta" placeholder="fecha hasta " >
                            </div>
                            <hr>
                            <table class="table-bordered" v-if="registro === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>PACEINTE</td>
                                    <td>GENERO</td>
                                    <td>FECHA INGRESO</td>
                                    <td>TIPO INGRESO</td>
                                    <td>ESTADO</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" @click="pasarDatosPaciente(dato.PACIENTE,dato.id)"></i>  </td>
                                    <td> @{{dato.PACIENTE}} </td>
                                    <td>@{{dato.GENERO}} </td>
                                    <td>@{{dato.FECHA_INGRESO}} </td>
                                    <td>@{{dato.TIPO_DE_INGRESO}} </td>
                                    <td>@{{dato.ESTADO}} </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                No existen registros en las fecha seleccionada
                            </div>
                        </div>
                        <hr>
                        <div class="tile-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal face" id="buscar2" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4 form-control"  placeholder="Paciente" v-model="paciente" v-on:keyup.13="consultar2">
                            </div>
                            <hr>
                            <table class="table-bordered" v-if="registro === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>PACEINTE</td>
                                    <td>GENERO</td>
                                    <td>FECHA INGRESO</td>
                                    <td>TIPO INGRESO</td>
                                    <td>ESTADO</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes2">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" @click="pasarDatosPaciente2(dato.PACIENTE,dato.id)"></i>  </td>
                                    <td> @{{dato.PACIENTE}} </td>
                                    <td>@{{dato.GENERO}} </td>
                                    <td>@{{dato.FECHA_INGRESO}} </td>
                                    <td>@{{dato.TIPO_DE_INGRESO}} </td>
                                    <td>@{{dato.ESTADO}} </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                No existen registros en las fecha seleccionada
                            </div>
                        </div>
                        <hr>
                        <div class="tile-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script>
        function enviar() {
            document.getElementById("form").submit();
            //document.getElementById("form1").submit()
        }
    </script>

    <script type="text/javascript">
        var app = new Vue ({
            el:'#anulacion',
            data : {
                // variables para el menu interno
                nuevo : 'inactivo',
                buscar : 'inactivo',
                imprimir : 'inactivo',
                btn_anular : true,
                visualizar : false,
                // variables formulario
                //general
                hospitalizacion_cirugia : '',
                area : '',
                paciente_principal :'',
                habilitadas :'',
                unidad :0,
                fecha_nacimiento :'',
                edad :'',
                unidad_asignada :0,
                genero :'',
                tipo_cama :'',
                tipo_admision :0,
                derivacion :'',
                documentos_completos: '',
                observacion: '',
                observacion_documentos :'',
                telefono :'',
                documento:'',
                direccion_responsable :'',
                telefono_responsable : '',
                nombre_responable :'',
                fecha_hora_ingreso :'',
                secuencia_ho :0,
                n_atencion : 0,
                transaccion : '',
                n_transaccion :'',
                fecha_registro :'',
                //medico
                fecha: '',
                servicios:'',
                medico:'',
                principal_codigo :'',
                asociado1_codigo :'',
                asociado2_codigo :'',
                principal_descripcion :'',
                asociado1_descripcion :'',
                asociado2_descripcion :'',
                observacion_m :'',
                // variables para consulta de paciente
                paciente_fecha: '',
                desde :'',
                hasta: '',
                estado : 0,
                estado2 :'',
                listaPacientes : [],
                listaPacientes2 : [],
                cmbEstado :[],

                // variables del modal para la consulta de diagnostico
                cargar :0,
                descripcion_diagnostico :'',
                lista_diagnostico : [],

                // variables de busqueda de paciente-individual
                privado: false,
                paciente :'',
                listaPaciente : [],
                tabla :1,
                datosConsulta :[],

                observacion_eliminada : '',
                paciente_id : '',
                id :'',
                errores : [],
                medico_fecha : '',
                mensaje_registro : false,
                registro : false,

            },

            created : function() {
                this.buscar = "activo";

                axios.get('/hospitalizacion/estadoshospitalizacion').then(response => {
                    this.cmbEstado  = response.data
                 })

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "600",
                    "hideDuration": "3000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
            },


            methods : {
                //--------------------------SEC_CONSULTA-----------------//
                consultarDiagnosticoCodigo : function(tipo){
                    // busca el diagnostico por codigo

                        var url = 'tipodiagnostico/'+this.principal_codigo+'/codigo';
                        axios.get(url).then(response => {
                            this.lista_diagnosticoCodigo  = response.data;
                            this.principal_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                        })

                        var url = 'tipodiagnostico/'+this.asociado1_codigo+'/codigo';
                        axios.get(url).then(response => {
                            this.lista_diagnosticoCodigo  = response.data;
                            this.asociado1_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                        })
                        var url = 'tipodiagnostico/'+this.asociado2_codigo+'/codigo';
                        axios.get(url).then(response => {
                            this.lista_diagnosticoCodigo  = response.data;
                            this.asociado2_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                        })
                },
                consultar2 : function () {
                    this.btn_anular = false;
                    this.visualizar = true;
                    if(this.paciente === ''){
                        this.errores.push("ingrese los nombres del paciente")
                    }
                    if( this.errores.length === 0) {
                        this.consultarPacientes2()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },
                consultarPacientes2 : function()  {
                    var hospitalizacion = "/hospitalizacion/pacientes/fecha/todos/"+this.paciente+"";
                    axios.get(hospitalizacion ).then(response => {
                        this.listaPacientes2  = response.data;
                    var num = this.listaPacientes2.length;
                    if(num === 0) {
                        this.registro = false;
                        this.mensaje_registro = true;
                    } else {
                        this.registro = true;
                        this.mensaje_registro = false;
                    }
                })
                },
                consultar : function () {
                    this.btn_anular = true;
                    this.visualizar = false;
                    this.validarConsulta();
                    if( this.errores.length === 0) {
                        this.consultarPacientes()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },
                consultarPacientes : function()  {
                    if(this.paciente === '' && this.desde === '' && this.hasta === '')
                    {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"";
                    } else {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"/"+this.paciente+"";
                    }
                    axios.get(hospitalizacion ).then(response => {
                        this.listaPacientes  = response.data;
                    var num = this.listaPacientes.length;
                    if(num === 0) {
                        this.registro = false;
                        this.mensaje_registro = true;
                    } else {
                        this.registro = true;
                        this.mensaje_registro = false;
                    }
                })
                },
                buscarPaciente : function(){
                    if(this.privado === false)
                    {
                        this.estado = 2 ;
                        // mostrar tabla 1
                        this.tabla = 1;
                    }else if(this.privado === true) {
                        this.estado = 1;
                        this.tabla =2;
                    }
                    // logica hecha con axios
                    var url = 'consultarPacientesNombre/'+this.paciente+'/'+this.estado+'';
                    axios.get(url).then(response => {
                        this.listaPaciente = response.data;
                })
                },
                consultarCama :function(cama) {

                    var url = 'consultarcamas/'+cama+'';
                    axios.get(url).then(response => {
                        this.listaCamas = response.data;
                    this.unidad_asignada = this.listaCamas[0].numero_camas;
                    this.unidad = this.listaCamas[0].descripcion_camas;
                })
                },
                //-------------------------*CONSULTA*-----------------//
                //-------------------------PASAR DATOS-----------------//
                pasarDatosPaciente : function (paciente,codigo) {


                    this.paciente_principal = paciente;

                    var url =  "consultarhospitalizacionid/"+codigo+'';

                    axios.get(url).then(response => {
                        this.datosConsulta  = response.data;

                    if(this.datosConsulta[0].Cirugia_Dia === 0){
                        this.hospitalizacion_cirugia= 'hospitalizacion';
                    } else {
                        this.hospitalizacion_cirugia = 'cirugia';
                    }

                    this.fecha_nacimiento = this.datosConsulta[0].fecha_nacimiento;
                    this.tipo_admision = this.datosConsulta[0].Procedencias;
                    this.observacion_documentos = this.datosConsulta[0].osbservacion_documento;
                    this.observacion = this.datosConsulta[0].Observaciones;
                    this.genero = this.datosConsulta[0].genero;
                    this.derivacion = this.datosConsulta[0].derivacion;
                    this.hospital = this.datosConsulta[0].hospital;

                    this.id = this.datosConsulta[0].id;
                    this.secuencia_ho = this.datosConsulta[0].SecuenciaHO;
                    this.n_historial = this.datosConsulta[0].No_historial;
                    this.fecha_hora_ingreso = this.datosConsulta[0].Fecha_Hora_Ingreso;
                    this.fecha_registro = this.datosConsulta[0].Fecha_de_Registro;

                    this.transaccion = this.datosConsulta[0].transaccion;

                    this.paciente_id = this.datosConsulta[0].Paciente;

                    this.area = this.datosConsulta[0].habitacion;

                    this.nombre_responable= this.datosConsulta[0].Nombre_Responsable;
                    this.direccion_responsable = this.datosConsulta[0].Direccion_Responsable;
                    this.telefono = this.datosConsulta[0].Telefono_Responsable;
                    this.documento = this.datosConsulta[0].Documento_Responsable;
                    this.servicios = this.datosConsulta[0].Servicios;
                    this.medico = this.datosConsulta[0].medico;
                    this.principal_codigo = this.datosConsulta[0].principal;
                    this.asociado1_codigo = this.datosConsulta[0].asociado_1;
                    this.asociado2_codigo = this.datosConsulta[0].asociado_2;
                    this.unidad_asignada = this.datosConsulta[0].codigo_cama;
                    //alert(codigo_cama);
                    this.observacion_m = this.datosConsulta[0].Medico_Observacion;
                    this.medico_fecha = this.datosConsulta[0].Medico_Fecha;

                    this.consultarCama(this.unidad_asignada);
                    this.consultarDiagnosticoCodigo();
                })

                    $("#consultarPaciente").modal('hide');
                    $("#buscar").modal('hide');

                },
                pasarDatosPaciente2 : function (paciente,codigo) {


                    this.paciente_principal = paciente;

                    var url =  "consultarhospitalizacionid/"+codigo+'';

                    axios.get(url).then(response => {
                        this.datosConsulta  = response.data;
                    if(this.datosConsulta[0].Cirugia_Dia === 0){
                        this.hospitalizacion_cirugia= 'hospitalizacion';
                    } else {
                        this.hospitalizacion_cirugia = 'cirugia';
                    }
                    this.fecha_nacimiento = this.datosConsulta[0].fecha_nacimiento;
                    this.tipo_admision = this.datosConsulta[0].Procedencias;
                    this.observacion_documentos = this.datosConsulta[0].osbservacion_documento;
                    this.observacion = this.datosConsulta[0].Observaciones;
                    this.genero = this.datosConsulta[0].genero;
                    this.derivacion = this.datosConsulta[0].derivacion;
                    this.hospital = this.datosConsulta[0].hospital;

                    this.id = this.datosConsulta[0].id;
                    this.secuencia_ho = this.datosConsulta[0].SecuenciaHO;
                    this.n_historial = this.datosConsulta[0].No_historial;
                    this.fecha_hora_ingreso = this.datosConsulta[0].Fecha_Hora_Ingreso;
                    this.fecha_registro = this.datosConsulta[0].Fecha_de_Registro;

                    this.transaccion = this.datosConsulta[0].transaccion;

                    this.paciente_id = this.datosConsulta[0].Paciente;

                    this.area = this.datosConsulta[0].habitacion;

                    this.nombre_responable= this.datosConsulta[0].Nombre_Responsable;
                    this.direccion_responsable = this.datosConsulta[0].Direccion_Responsable;
                    this.telefono = this.datosConsulta[0].Telefono_Responsable;
                    this.documento = this.datosConsulta[0].Documento_Responsable;
                    this.servicios = this.datosConsulta[0].Servicios;
                    this.medico = this.datosConsulta[0].medico;
                    this.principal_codigo = this.datosConsulta[0].principal;
                    this.asociado1_codigo = this.datosConsulta[0].asociado_1;
                    this.asociado2_codigo = this.datosConsulta[0].asociado_2;
                    this.unidad_asignada = this.datosConsulta[0].codigo_cama;
                    //alert(codigo_cama);
                    this.observacion_m = this.datosConsulta[0].Medico_Observacion;
                    this.medico_fecha = this.datosConsulta[0].Medico_Fecha;

                    this.consultarCama(this.unidad_asignada);
                    this.consultarDiagnosticoCodigo();
                })

                    $("#consultarPaciente").modal('hide');
                    $("#buscar2").modal('hide');

                },

                //------------------------*SEC_PASAR DATOS*-----------------//

                //--------------------------SEC_VALIDAR-----------------//
                validarConsulta : function() {
                    /* si no ingresa nada en el campo fecha pone la fecha actual */
                    if(this.desde === '' || this.desde === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;
                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        if(d < 10) {
                            d = ''+0+d;
                        }
                        this.desde = ''+y+'-'+m+'-'+d+'';

                    }

                    if(this.hasta === '' || this.hasta === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;

                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        d += 1;
                        if(d < 10) {
                            d = ''+0+d;

                        }
                        if(m === 2 && d === 28 )
                        {
                            m= 3;
                            d= 1;
                        }

                        if(( m===4 || m===6 || m===9 || m===11 ) && d === 30)
                        {
                            m = m+1;
                            d =1;
                        }

                        if(( m===1 || m===3 || m===5 || m===7 || m===8 || m===10 ) && d === 31)
                        {
                            m = m+1;
                            d =1;
                        }

                        if( m===12 && d === 31)
                        {
                            y= y+1;
                            m = 1;
                            d =1;
                        }

                        this.hasta = ''+y+'-'+m+'-'+d+'';
                    }
                },
                validarAnulacion : function () {

                    if(this.observacion_eliminada === '' || this.observacion_eliminada === undefined)
                    {
                        this.errores.push("Ingrese la observación de la eliminación");
                    }

                    if(this.medico === '' || this.medico === undefined)
                    {
                        this.errores.push("Seleccione la información del paciente a eliminar");
                    }

                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

                    if( this.errores.length === 0) {
                        // document.getElementById('registroTitular').submit()
                        this.anulacionPacientes()
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];

                },
                //-------------------------*VALIDAR*-----------------//

                //--------------------------SEC_iNSERTAR-----------------//
                anulacionPacientes: function () {
                    var anulacion = true;
                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "id" : this.id,
                        "paciente_id" : this.paciente_id,
                        "N_unidad_asignada" : this.unidad_asignada,
                        "status_camas" : 1,
                        "observacion_eliminada" : this.observacion_eliminada,
                    };
                    $.ajax({
                        data : parametros,
                        url : "anulacionhospitalizacion",
                        type : "post",
                        success : function(response){
                            toastr.success('Registro eliminado con exito.', 'Exito', {timeOut: 5000});
                            anulacion = true;
                        },
                        error : function (response,jqXHR) {

                            if(response.status === 422)
                            {
                                var errors = $.parseJSON(response.responseText);
                                $.each(errors, function (key, value) {
                                    if($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {
                                            toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                            console.log(key+ " " +value);
                                        });
                                    }else{
                                        toastr.error('Error al momento de realizar la anulación. '+''+'', 'Error', {timeOut: 5000});
                                    }
                                });
                            } else {
                                toastr.error('Error al momento de realizar la anulación::: '+''+'', 'Error', {timeOut: 5000});
                            }
                        }
                    })

                    if(anulacion === true){
                        this.limpiar();
                    }
                },
                //-------------------------*iNSERTAR*-----------------//
                //--------------------------SEC_ACCIONES-----------------//
                limpiar : function () {
                    //borrar información
                    this.guardar = true;
                    this.imprimir = false;
                    this.nuevo =false;

                    this.fecha= '';
                    this.servicios='';
                    this.medico='';
                    this.principal_codigo ='';
                    this.asociado1_codigo ='';
                    this.asociado2_codigo ='';
                    this.principal_descripcion ='';
                    this.asociado1_descripcion ='';
                    this.asociado2_descripcion ='';
                    this.observacion_m ='';

                    this.area = '1';
                    this.paciente_principal ='';
                    this.habilitadas ='';
                    this.unidad ='';
                    this.fecha_nacimiento ='';
                    this.edad ='';
                    this.unidad_asignada ='';
                    this.genero ='';
                    this.tipo_cama ='';
                    this.tipo_admision =0;
                    this.derivacion =false;
                    this.hospital ='';
                    this.documentos_completos= '';
                    this.observacion= '';
                    this.observacion_documentos ='';
                    this.telefono ='';
                    this.documento='';
                    this.direccion ='';
                    this.nombre_completo ='';
                    this.codigo_camma = '';
                    this.paciente = "";
                    //this.fecha_hora_ingreso ='';
                    //this.secuencia_ho =0;
                    //this.this.n_atencion = 0;
                    //this.n_transaccion =0;
                    //this.fecha_registro ='';

                    $("#secuencia").val('');
                    //$("#historial").val(a.historial);
                    $("#atencion").val('');
                    $("#transaccion").val('');
                    $("#fecha_registro").val('');
                    $("#fecha_hora_registro").val('');

                    this.ListaDatosTitular = [];
                    this.cmbMedico = [];
                    this.listaAfiliadosCedula = [];
                    this.listaPaciente = [];
                    this.listaCamashabilitadas= [];
                },
                VisualizarInformes : function() {


                        document.getElementById("rotulo").submit();

                        var a = document.createElement("a");
                        var b = document.createElement("a");
                        var c = document.createElement("a");
                        a.target = "_blank";
                        a.href = "reporte/ingreso/"+this.id;
                        a.click();

                        b.target = "_blank";
                        b.href = "garantia/admision/"+this.id;
                        b.click();

                        c.target = "_blank";
                        c.href = "registro/admision/"+this.id;
                        c.click();
                }
                //-------------------------*ACCIONES*-----------------//


            }
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $.get("unidad", function (datos) {
                $.each(datos, function (key, value) {
                    $("#unidad").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#seguroList").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medicolist").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });

            $.get("especializacion", function (datos) {
                $.each(datos, function (key, value) {
                    $("#especializacion").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
        });
    </script>


@endsection

