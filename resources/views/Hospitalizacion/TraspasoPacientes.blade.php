@extends('layouts.principal')
@section('titulo','Traspaso Pacientes')
@section('title','Traspaso Pacientes')
@section('css')
    <style>
        .ocupada {
            background-color: #3097D1;
        }

        .desinfeccion {
            background-color: #ff6c5c;
        }
    </style>
@endsection
@section('contenido')
    <div class="col-md-12" id="traspaso" v-cloak>
        <div class="tile">

                <div class="form-group row">
                    <div class="col-sm-1">
                        <i v-if="buscar === 'activo'" class="fa fa-search-minus fa-3x"   aria-hidden="true"   id="show-modal" data-toggle="modal" data-target="#buscar" ></i>
                        <i v-else  class="fa fa-search-minus fa-3x"   aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"  ></i>
                        <p >Buscar</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-1">N°:</label>
                    <div class="col-md-1">
                        <input  v-model="unidad_asignada" readonly name="n_unidad_anterior" class="form-control" type="text" placeholder="" >
                    </div>
                    <label class="control-label col-md-1">T. Cama:</label>
                    <div class="col-md-2">
                        <select class="form-control" disabled="" id="unidad2" v-model="unidad2" :value="unidad2" name="tipo_unidad_anteior">

                        </select>
                    </div>
                    <label class="control-label col-md-1">T. Sala:</label>
                    <div class="col-md-5">
                        <select class="form-control" disabled id="sala2" v-model="area2" :value="area2" name="tipo_sala_anterior">

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-1">Fecha Traslado:</label>
                    <div class="col-md-3">
                        <input    name="fecha_traslado" readonly class="form-control" type="date" placeholder="" >
                    </div>
                    <label class="control-label col-md-1">Ultimo Traslado:</label>
                    <div class="col-md-3">
                        <input v-model="fecha_traslado" readonly  name="ultimo_traslado" class="form-control" type="text" placeholder="aaaa/mm/dd" >
                    </div>
                </div>

                <label class="control-label col-md-2" >Observación Traslado</label><br>
                <textarea class="form-control" v-model="observacion_traslado" name="observacion_traslado" placeholder=""  rows="2" cols="4"></textarea>
                <br>
                <p>Dispobibilidad</p>
                <hr>
                <div class="form-group row">
                    <label class="control-label col-md-1">Habitación:</label>
                    <div class="col-md-4">
                        <select class="form-control" id="sala" v-model="area" :value="area" name="habitacion_nueva">
                        </select>
                    </div>
                    <i class="fa fa-search fa-2x col-md-1" aria-hidden="true" v-on:click="consultarAreas"></i>
                    <input class="form-control col-md-2" readonly type="text" v-model="habilitadas"> <p class="text-info">Habilitadas </p>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-1" >N°: </label>
                    <div class="col-md-3">
                        <input class="form-control" disabled type="text" v-model="n_unidad_asignada" name="nueva_unidad">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-1" >T. Cama </label>
                    <div class="col-md-3">
                        <select class="form-control" disabled id="unidad" v-model="unidad" :value="unidad" name="nuevo_tipo_cama">

                        </select>
                    </div>
                </div>
                <input type="hidden" id="respuesta_input">
                <button v-if="btn_traspaso === true" class="btn btn-primary" @click="insertarTraspaso"> Traslado <i class="fa fa-share" aria-hidden="true" ></i></button>
                <button type="button" class="btn btn-danger"  @click="cancelar">Cancelar<i class="fa fa-eraser" aria-hidden="true" ></i></button>

        </div>
        <!-- modales -->
        <div class="modal face" id="buscarArea" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar camas por sala
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <label for="nombre_sala"> Sala  </label>
                            <input v-model="nombre_sala" class="form-control" aria-readonly id="nombre_sala" readonly>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>Numero cama</td>
                                    <td>Descripcion cama</td>
                                    <td>Descripcion estado</td>
                                    <td>Nombre paciente</td>
                                    <td>Diagnostico paciente</td>
                                    <td>Descripcion</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaCamasArea">
                                    <td> <i v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'" class="fa fa-check-square-o" aria-hidden="true" @click="consultarCama(dato.codigo,dato.descripcion_camas)"></i>  </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"  > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"> @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}"> @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'OCUPADA'"    v-bind:class="{ocupada: dato.DESCRIPCION_ESTADO ==='OCUPADA'}">no aplica</td>

                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"  > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"> @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}"> @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'DESINFECCION'"   v-bind:class="{desinfeccion: dato.DESCRIPCION_ESTADO ==='DESINFECCION'}">no aplica</td>

                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.NUMERO_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.DESCRIPCION_CAMA}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   > @{{dato.DESCRIPCION_ESTADO}} </td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >paciente</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >no aplica</td>
                                    <td v-if="dato.DESCRIPCION_ESTADO === 'HABILITADA'"   >no aplica</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar Pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="Paciente" v-model="paciente" v-on:keyup.13="consultar" >
                                <input class="form-control col-md-3" type="date" v-model="desde"  placeholder="fecha desde" >
                                <input class="form-control col-md-3"   type="date" v-model="hasta" placeholder="fecha hasta " >
                            </div>
                            <hr>
                            <table class="table-bordered" v-if="registro === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>PACEINTE</td>
                                    <td>GENERO</td>
                                    <td>FECHA INGRESO</td>
                                    <td>TIPO INGRESO</td>
                                    <td>ESTADO</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" @click="datosCama(dato.id)"> </i>  </td>
                                    <td> @{{dato.PACIENTE}} </td>
                                    <td>@{{dato.GENERO}} </td>
                                    <td>@{{dato.FECHA_INGRESO}} </td>
                                    <td>@{{dato.TIPO_DE_INGRESO}} </td>
                                    <td>@{{dato.ESTADO}} </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                No existen registros en las fecha seleccionada
                            </div>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="respuesta" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Respuesta
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                <p>EL paciente ha sido trasladado de la sala @{{sala_a}} a la sala @{{sala_b}}  </p>
                            </div>
                        </div>


                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
@section('script')

    <script type="text/javascript">
        var app = new Vue ({
            el:'#traspaso',
            data : {

                sala_a : '',
                sala_b : '',

                btn_traspaso : true,
                // variables para el menu interno
                nuevo : 'inactivo',
                buscar : 'inactivo',
                imprimir : 'inactivo',


                cmbMedico : [],
                // variables formulario
                //general
                area : '1',
                paciente_principal :'',
                habilitadas :'',
                unidad :'',
                fecha_nacimiento :'',
                edad :'',
                unidad_asignada :'',
                genero :'',
                tipo_cama :'',
                tipo_admision :0,
                derivacion :false,
                documentos_completos: '',
                observacion: '',
                observacion_documentos :'',
                telefono :'',
                documento:'',
                direccion :'',
                nombre :'',
                fecha_hora_ingreso :'',
                secuencia_ho :0,
                n_atencion : 0,
                n_transaccion :0,
                fecha_registro :'',
                //medico
                fecha: '',
                servicios:'',
                medico:'',
                principal_codigo :'',
                asociado1_codigo :'',
                asociado2_codigo :'',
                principal_descripcion :'',
                asociado1_descripcion :'',
                asociado2_descripcion :'',
                observacion_m :'',
                // variables para consulta de paciente
                paciente_fecha: '',
                desde :'',
                hasta: '',
                estado : 0,
                estado2 :'',
                listaPacientes : [],
                cmbEstado :[],

                // variables del modal para la consulta de diagnostico
                cargar :0,
                descripcion_diagnostico :'',
                lista_diagnostico : [],

                // variables de busqueda de paciente-individual
                privado: false,
                paciente :'',
                listaPaciente : [],
                tabla :1,
                tipo_parentesco : '',
                listaAfiliadosCedula : [],
                ListaDatosTitular : [],
                Titularid  : '',
                Titularprimer_nombre  : '',
                Titularsegundo_nombre  : '',
                Titularapellido_paterno  : '',
                Titularapellido_materno  : '',
                Titulardireccion  : '',
                Titulartelefono  : '',
                Titularcelular  : '',

                apellidos : '',
                nombres : '',
                nombre_completo : '',

                // variables para consulta de camas por sala
                listaCamasArea : [],
                nombre_sala :'',
                listaCamas : [],
                listaCamashabilitadas : [],
                datosConsulta2 :[],
                datosConsulta : [],

                unidad2 : 0,
                area2 :0,
                t_cama : 0,
                t_sala : 0,
                fecha_traslado : '',
                n_unidad_asignada :0,
                id : 0,
                paciente_id :0,
                observacion_traslado : '',
                codigo_cama : '',
                errores : [],
                codigo_cama_anterior : 0,
                registro :false,
                mensaje_registro : false

            },

            created : function() {
                this.buscar = "activo";
                axios.get('/hospitalizacion/estadoshospitalizacion').then(response => {
                    this.cmbEstado  = response.data
                })
            },
            methods : {
                //--------------------------SEC_CONSULTAR----------------------------//
                datosCama : function(id) {
                    var url =  "consultarhospitalizacionid/"+id+'';

                    axios.get(url).then(response => {
                        this.datosConsulta  = response.data;
                    this.id = this.datosConsulta[0].id;
                    this.paciente_id = this.datosConsulta[0].Paciente;
                    this.area = this.datosConsulta[0].habitacion;

                    this.codigo_cama_anterior = this.datosConsulta[0].codigo_cama;

                    this.fecha_registro = this.datosConsulta[0].Fecha_Hora_Ingreso;
                    this.fecha_traslado = this.datosConsulta[0].fecha_hora_traspaso;

                    var url2 ="consultarcamas/"+this.codigo_cama_anterior+"";
                    axios.get(url2).then(response => {
                        this.datosConsulta2  = response.data;
                    this.unidad2 = this.datosConsulta2[0].descripcion_camas;
                    this.area2 = this.datosConsulta2[0].sala;
                    this.unidad_asignada = this.datosConsulta2[0].numero_camas;
                })
                })

                    $("#consultarPaciente").modal('hide');
                    $("#buscar").modal('hide');
                    this.nuevo = 'activo';
                    this.buscar = 'inactivo';
                },
                consultar : function () {

                    this.validarConsulta();
                    var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/; // nombres apellidos
                    if(patt3.test(this.paciente) == false)
                    {
                        toastr.error("el nombre del paciente no pueden contener numeros ,caracteres especiales");
                    }else {
                        this.consultarPacientes()
                    }
                },
                consultarPacientes : function()  {
                    if(this.paciente === '' && this.desde === '' && this.hasta === '')
                    {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"";
                    } else {
                        var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"/"+this.paciente+"";
                    }
                    toastr.success('Buscando.....', 'Exito', {timeOut: 5000});
                    axios.get(hospitalizacion ).then(response => {

                        this.listaPacientes  = response.data;
                    var num = this.listaPacientes.length;
                    if(num === 0) {
                        this.registro = false;
                        this.mensaje_registro = true;
                    } else {
                        this.registro = true;
                        this.mensaje_registro = false;
                    }
                })
                },
                consultarPaciente : function(){
                    $("#buscarPaciente").modal();
                },
                consultarAreas :function () {
                    var url = 'consultarcamasporsala/'+this.area+'';
                    axios.get(url).then(response => {
                        this.listaCamasArea = response.data;
                })
                    var combo = document.getElementById("sala");
                    this.nombre_sala = combo.options[combo.selectedIndex].text;
                    this.sala_b = this.nombre_sala;
                    $("#buscarArea").modal();
                },
                consultarCama : function (codigo,cama) {
                    this.btn_traspaso = true;
                    var url = 'consultarcamas/'+codigo+'';
                    axios.get(url).then(response => {
                        this.listaCamas = response.data;
                    this.n_unidad_asignada = this.listaCamas[0].numero_camas;
                    this.unidad = this.listaCamas[0].descripcion_camas;
                    this.codigo_cama = codigo;
                })

                    var url2 = 'consultarcamashabilitadas/'+this.area+'/'+cama+'';
                    axios.get(url2).then(response => {
                        this.listaCamashabilitadas = response.data;
                    this.habilitadas = this.listaCamashabilitadas[0].total_camas;

                })
                    $("#buscarArea").modal('hide');
                },
                //-------------------------- /CONSULTAR/----------------------------//
                //--------------------------SEC_PASAR DATOS----------------------------//
                pasarDatosPaciente : function (paciente,codigo) {
                    this.btn_traspaso = true;
                    this.paciente_principal = paciente;


                    var urlUsers = 'consultarpaciente/'+codigo+'';
                    axios.get(urlUsers).then(response => {
                        this.listaAfiliadosCedula  = response.data;

                    this.id = this.listaAfiliadosCedula[0].id;
                    this.secuencia_ho = this.listaAfiliadosCedula[0].SecuenciaHo;
                    this.n_historial = this.listaAfiliadosCedula[0].No_historial;
                    this.fecha_hora_ingreso = this.listaAfiliadosCedula[0].Fecha_Hora_Ingreso;
                    this.fecha_nacimiento = this.listaAfiliadosCedula[0].fecha_nacimiento;
                    this.fecha_registro = this.listaAfiliadosCedula[0].Fecha_de_Registro;
                    this.paciente_id = this.listaAfiliadosCedula[0].Paciente;
                    this.habitacion = this.listaAfiliadosCedula[0].habitacion;
                    this.procedencias = this.listaAfiliadosCedula[0].Procedencias;
                    this.nombre_responsable = this.listaAfiliadosCedula[0].Nombre_Responsable;
                    this.direccion_responsable = this.listaAfiliadosCedula[0].Dirrecion_Responsables;
                    this.telefono_responsable = this.listaAfiliadosCedula[0].Telefono_Responsables;
                    this.observaciones = this.listaAfiliadosCedula[0].Observaciones;
                    this.servicios = this.listaAfiliadosCedula[0].Servicios;
                    this.medicos = this.listaAfiliadosCedula[0].medico;
                    this.principal = this.listaAfiliadosCedula[0].principal;
                    this.asociado_1 = this.listaAfiliadosCedula[0].asociado_1;
                    this.asociado_2 = this.listaAfiliadosCedula[0].asociado_2;


                    this.genero = this.listaAfiliadosCedula[0].genero;
                    this.fecha_nacimiento = new Date(this.listaAfiliadosCedula[0].fecha_nacimiento);
                    // convertir la fecha que viene de la base en un tipo legible para el formulario ademas la edad
                    this.FormarFechaEdad2(response.data);
                    this.direccion = this.listaAfiliadosCedula[0].direccion;
                    this.telefono = this.listaAfiliadosCedula[0].telefono;
                    this.observacion = this.listaAfiliadosCedula[0].observacion;
                    this.tipo_seguro  = this.listaAfiliadosCedula[0].tipo_seguro;
                    this.tipo_admision = this.listaAfiliadosCedula[0].tipo_seguro; // campos de consulta para cambio
                    this.tipo_parentesco  = this.listaAfiliadosCedula[0].tipo_parentesco;
                    this.documento  = this.listaAfiliadosCedula[0].cedula_titular;

                    if(this.tipo_parentesco==="AA") {
                        this.nombre_completo = this.paciente_principal;
                        this.direccion = this.listaAfiliadosCedula[0].direccion;
                        this.telefono = this.listaAfiliadosCedula[0].telefono;
                    }else {

                        this.ConsultarTitularCedula();

                    }
                })



                    $("#consultarPaciente").modal('hide');
                },
                //--------------------------/PASAR DATOS/----------------------------//
                //--------------------------SEC_VALIDAR----------------------------//
                validarInsert :function () {

                    var combo2 = document.getElementById("sala2");
                    this.sala_a= combo2.options[combo2.selectedIndex].text;


                    if(this.area2 === 0 || this.area2 === undefined){
                        this.errores.push("Cargue la información del paciente")
                    }

                    if(this.habilitadas === '' || this.habilitadas === undefined){
                        this.errores.push("Cargue la información de traslado")
                    }

                    if(this.observacion_traslado === '' || this.observacion_traslado=== undefined){
                        this.errores.push("Ingrese la observacción del traslado")
                    }

                },
                validarConsulta : function() {

                    if(this.desde === '' || this.desde === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;
                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        if(d < 10) {
                            d = ''+0+d;
                        }
                        this.desde = ''+y+'-'+m+'-'+d+'';

                    }

                    if(this.hasta === '' || this.hasta === undefined) {
                        var date = new Date();
                        var y = date.getFullYear();
                        var m = date.getMonth() +1;

                        if( m < 10) {
                            m = ''+0+m;
                        }
                        var d = date.getDate();
                        d += 1;
                        if(d < 10) {
                            d = ''+0+d;

                        }
                        if(m === 2 && d === 28 )
                        {
                            m= 3;
                            d= 1;
                        }

                        if(( m===4 || m===6 || m===9 || m===11 ) && d === 30)
                        {
                            m = m+1;
                            d =1;
                        }

                        if(( m===1 || m===3 || m===5 || m===7 || m===8 || m===10 ) && d === 31)
                        {
                            m = m+1;
                            d =1;
                        }

                        if( m===12 && d === 31)
                        {
                            y= y+1;
                            m = 1;
                            d =1;
                        }

                        this.hasta = ''+y+'-'+m+'-'+d+'';
                    }


                },
                //-------------------------- /VALIDAR/----------------------------//
                //--------------------------SEC_INSERT----------------------------//
                insertarTraspaso : function() {

                    this.validarInsert();
                    this.validarConsulta();
                    //this.validarFecha();
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "600",
                        "hideDuration": "3000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

                    if( this.errores.length === 0) {
                        // document.getElementById('registroTitular').submit()
                        this.guardarTraspaso();
                        //this.btn_traspaso= false;
                        this.buscar = 'activo';
                    } else {
                        var num = this.errores.length;
                        for (i = 0; i < num; i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },
                guardarTraspaso : function() {

                    var exito = false;
                    //$("#respuesta_input").val("respuesta");
                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "hospitacion_id" : this.id,
                        "paciente_id" : this.paciente_id,
                        "fecha_registro" : this.fecha_registro,
                        "observacion_traslado" : this.observacion_traslado,
                        "n_unidad_anterior" : this.unidad_asignada,
                        "codigo_cama_traslado" : this.codigo_cama

                    };
                    $.ajax({
                        data : parametros,
                        url : "ingresotraslado",
                        type : "post",
                        success : function(response){
                            toastr.success('Traspaso realizado con exito.', 'Exito', {timeOut: 5000});
                            $("#respuesta_input").val("respuesta");
                            $("#respuesta").modal();
                        },
                        error : function (response,jqXHR) {
                            if(response.status === 422)
                            {
                                var errors = $.parseJSON(response.responseText);
                                $.each(errors, function (key, value) {
                                    if($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {
                                            toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                            console.log(key+ " " +value);
                                        });
                                    }else{
                                        toastr.error('Error al momento de realizar el traspado. '+''+'', 'Error', {timeOut: 5000});
                                    }
                                });
                            } else {
                                toastr.error('Error al momento de realizar el traspado::: '+''+'', 'Error', {timeOut: 5000});
                            }
                        }
                    })
                    this.datosConsulta = [];
                    if( $("#respuesta_input").val() !== '') {
                        console.log($("#respuesta_input").val());
                        this.cancelar();
                        this.btn_traspaso = true;
                    }
                },
                //--------------------------  /INSERT/----------------------------//
                //--------------------------SEC_ACCIONES----------------------------//
                cancelar : function () {
                    this.n_unidad_asignada = "";
                    this.unidad_asignada = 0;
                    this.listaCamasArea = [];
                    this.habilitadas = "";
                    this.nombre_sala ='';
                    this.listaCamas = [];
                    this.listaCamashabilitadas = [];
                    this.datosConsulta2 =[];
                    this.datosConsulta = [];
                    this.unidad2 = 0;
                    this.area2 =0;
                    this.t_cama = 0;
                    this.t_sala = 0;
                    this.fecha_traslado = '';
                    this.n_unidad_asignada =0;
                    this.id = 0;
                    this.paciente_id =0;
                    this.observacion_traslado = '';
                    this.codigo_cama = '';
                    this.errores = [];
                    this.codigo_cama_anterior = 0;
                    this.registro =false;
                    this.mensaje_registro = false;
                },
                //--------------------------/ACCIONES/----------------------------//

            // no las estoy usando //

                buscarPaciente : function(){
                    if(this.privado === false)
                    {
                        this.estado = 2 ;
                        // mostrar tabla 1
                        this.tabla = 1;
                    }else if(this.privado === true) {
                        this.estado = 1;
                        this.tabla =2;
                    }
                    // logica hecha con axios
                    var url = 'consultarPacientesNombre/'+this.paciente+'/'+this.estado+'';
                    axios.get(url).then(response => {
                        this.listaPaciente = response.data;
                })
                },


                FormarFechaEdad2 : function (data) {
                    var fecha_nacimiento = new Date(data[0].fecha_nacimiento);
                    var dia = fecha_nacimiento.getDate();
                    var mes = fecha_nacimiento.getMonth();
                    mes = mes + 1;
                    if (mes < 10) {
                        mes = String("0" + mes);
                    }
                    if(dia < 10) {
                        dia = String("0" + dia);
                    }
                    var year = fecha_nacimiento.getFullYear();
                    var Fecha = String(year + "-" + mes + "-" + dia);
                    var hoy = new Date();
                    var cumpleanos = new Date(fecha_nacimiento);
                    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                    var m = hoy.getMonth() - cumpleanos.getMonth();

                    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                        edad--;
                    }

                    this.fecha_nacimiento = Fecha;
                    this.edad = edad;

                },
                calcular_edad : function () {
                    //this.edad = this.fecha_nacimiento - getdate()
                    //var today = new Date();
                    var hoy = new Date();
                    var cumpleanos = new Date(this.fecha_nacimiento);
                    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                    var m = hoy.getMonth() - cumpleanos.getMonth();

                    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                        edad--;
                    }

                    this.edad = edad;
                },



                ConsultarTitularCedula : function() {

                    var urlUsers = 'registropacientes/TitularAfiliadoPorCedula/'+this.documento+'';
                    axios.get(urlUsers).then(response => {
                        this.ListaDatosTitular  = response.data;


                    this.Titularid = this.ListaDatosTitular[0].id;
                    this.Titularprimer_nombre = this.ListaDatosTitular[0].primer_nombre;
                    this.Titularsegundo_nombre = this.ListaDatosTitular[0].segundo_nombre;
                    this.Titularapellido_paterno = this.ListaDatosTitular[0].apellido_paterno;
                    this.Titularapellido_materno = this.ListaDatosTitular[0].apellido_materno;
                    this.Titulardireccion = this.ListaDatosTitular[0].direccion;
                    this.Titulartelefono = this.ListaDatosTitular[0].telefono;
                    this.Titularcelular = this.ListaDatosTitular[0].celular;

                    //this.cedula_titular = this.Titularcedula;
                    this.apellidos = this.Titularapellido_paterno + ' ' + this.Titularapellido_materno;
                    this.nombres = this.Titularprimer_nombre + ' ' + this.Titularsegundo_nombre;
                    this.nombre_completo = this.nombres +""+this.apellidos+"";
                })




                },



            }

        });

    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $.get("unidad", function (datos) {
                $.each(datos, function (key, value) {
                    $("#unidad").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("unidad", function (datos) {
                $.each(datos, function (key, value) {
                    $("#unidad2").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala2").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });
        });
    </script>


@endsection