@extends('layouts.principal')
@section('titulo','Egresos')
@section('title','Egresos')
@section('contenido')

    <div class="col-md-8 coll-md-offset-4" id="egreso" v-cloak>
                <div class="tile">
                    <div class="col-md-12">
                <div class="form-group row">
                   <!--
                    <div class="col-md-1">
                        <i class="fa fa-file-o fa-3x"   aria-hidden="true"  ></i>
                        <p >Nuevo</p>
                    </div>
                    -->
                    <div class="col-md-1 center-block">
                        <i class="fa fa-search fa-3x" aria-hidden="true" id="show-modal" data-toggle="modal" data-target="#buscar"></i>
                        <p>Buscar</p>
                    </div>
                </div>
            </div>
            <hr>

            <form method="post" action="{{route('hospitalizacion.egresopacientes')}}" id="form">
                {{csrf_field()}}
                <div class="tile-body">
                    <input type="hidden" id="id">
                    <div class="form-group row">
                        <label class="control-label col-md-2">Paciente</label>
                        <div class="col-md-4">
                            <input  name="nombre_paciente" readonly  v-model="paciente_principal" value="" class="form-control" type="text" placeholder=""  >
                        </div>
                        <label class="control-label col-md-2"> Sala</label>
                        <div class="col-md-4">
                            <select class="form-control" disabled id="sala" v-model="sala" :value="sala">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                                <label class="control-label col-md-2">Fecha y hora ingreso</label>
                                    <div class="col-md-4">
                                    <input type="text" readonly v-model="fecha_hora_ingreso" value=""  name="fechaingreso" class="form-control"    >
                                </div>
                                <label class="control-label col-md-2"> Numero cama</label>
                                <div class="col-md-2">
                                    <input  value="" readonly v-model="cama"  name="cama" class="form-control" type="text" placeholder=""  >
                                </div>
                            </div>
                    <div class="form-group row">
                                <label class="control-label col-md-2">Procedencias</label>
                                <div class="col-md-4">
                                    <select class="form-control" disabled id="seguroList" :value="Procedencia" v-model="Procedencia">
                                        <option ></option>
                                    </select>
                                </div>
                                <label class="control-label col-md-2"> Tipo de cama </label>
                                <div class="col-md-3">
                                    <select class="form-control" disabled id="unidad" v-model="unidad">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                    <hr>
                    <div class="form-group row">
                                <label class="control-label col-md-2">Fecha y Hora de Egreso</label>
                                <div class="col-md-7">
                                    <input  type="datetime-local" readonly v-model="fecha_hora_egreso"   name="f_h_egreso" value="" class="form-control"  >
                                </div>
                            </div>
                    <div class="form-group row">
                                <label class="control-label col-md-2" >Medico</label>
                                <div class="col-md-7">
                                        <select class="form-control" id="medicoList" v-model="medico" name="medico">
                                            <option ></option>
                                        </select>
                                </div>
                            </div>
                    <hr>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Principal</label>
                        <div class="col-md-3">
                            <input type="text" v-model="principal_codigo" v-on:keyup.13="consultarDiagnosticoCodigo(1)"  name="principal" class="form-control"  autocomplete="off"  v-bind:readonly="principal_" maxlength="5" >
                        </div>
                        <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" v-on:click="buscarDiagnostico(1)" ></i>
                        <i class=" col-md-1 fa fa-recycle fa-3x" aria-hidden="true" v-on:click="limpiarDiagnostico(1)" ></i>
                        <div class="col-md-5">
                            <input type="text" readonly v-model="principal_descripcion"   name="" class="form-control"  placeholder=""  >
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Asociado 1</label>
                        <div class="col-md-3">
                            <input type="text" v-on:keyup.13="consultarDiagnosticoCodigo(2)" v-model="asociado1_codigo" name="asociado1" class="form-control"  autocomplete="off"  v-bind:readonly="asociado1" maxlength="5" >
                        </div>
                        <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true" id="asociado_1" v-on:click="buscarDiagnostico(2)"></i>
                        <i class=" col-md-1 fa fa-recycle fa-3x" aria-hidden="true" v-on:click="limpiarDiagnostico(2)" ></i>
                        <div class="col-md-5">
                            <input type="text" readonly v-model="asociado1_descripcion"  class="form-control"  placeholder=""  >
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-1">Asociado 2</label>
                        <div class="col-md-3">
                            <input type="text" v-on:keyup.13="consultarDiagnosticoCodigo(3)" id="asociado_2" v-model="asociado2_codigo" name="asociado2" class="form-control"   autocomplete="off"  v-bind:readonly="asociado2" maxlength="5" >
                        </div>
                        <i class=" col-md-1 fa fa-search-plus fa-3x" aria-hidden="true"  v-on:click="buscarDiagnostico(3)"></i>
                        <i class=" col-md-1 fa fa-recycle fa-3x" aria-hidden="true" v-on:click="limpiarDiagnostico(3)" ></i>
                        <div class="col-md-5">
                            <input type="text" readonly v-model="asociado2_descripcion" name="" class="form-control"  placeholder=""  >
                        </div>

                    </div>
                    <div class="form-group row">
                                <label class="control-label col-md-2">Tipo de Egreso</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="egresoList" v-model="tipo_egreso" name="cmbTipoEgreso">
                                    </select>
                                </div>
                            </div>
                    <div class="form-group row">
                                <label class="control-label col-md-2">Tipo de Alta</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="egresoAltaList" name="listaTipoAlta" v-model="tipo_alta" >
                                    </select>
                                </div>
                            </div>
                    <div class="form-group row">
                                <label class="control-label col-md-2">Observación</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" v-model="observacion_egreso" name="observacion" placeholder=""   cols="3" ></textarea>
                                </div>
                    </div>
                </div>

                <div class="tile-footer">
                            <button v-if="btn_guardar === true" class="btn btn-primary" type="button" @click="validarEgreso"><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>&nbsp;&nbsp;&nbsp;
                            <button v-if="btn_imprimir === true" class="btn btn-success" type="button" @click="Imprimir">Imprimir</button>
                            <button v-if="btn_limpiar === true" class="btn btn-danger" type="button" @click="limpiar"><i class="fa fa-fw fa-lg fa-times-circle"></i>Limpiar</button>
                        </div>
            </form>

        </div>
        <div class="modal face" id="buscar" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        EGRESO-BUSQUEDA PACIENTES HOSPITALIZADOS/TRASLADADOS
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="Paciente" v-model="paciente_fecha" v-on:keyup.13="consultar" >
                                <input class="form-control col-md-3" type="date" v-model="desde"  placeholder="fecha desde" >
                                <input class="form-control col-md-3"   type="date" v-model="hasta" placeholder="fecha hasta " >
                            </div>
                            <p>Pacientes</p>
                            <hr>
                            <table class="table-bordered" v-if="registro === true">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>PACIENTE</td>
                                    <td>GENERO</td>
                                    <td>FECHA INGRESO</td>
                                    <td>TIPO INGRESO</td>
                                    <td>ESTADO</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in listaPacientes">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" @click="pasarDatosConsulta(dato.id,dato.PACIENTE)"></i>  </td>
                                    <td> @{{dato.PACIENTE}} </td>
                                    <td>@{{dato.GENERO}} </td>
                                    <td>@{{dato.FECHA_INGRESO}} </td>
                                    <td>@{{dato.TIPO_DE_INGRESO}} </td>
                                    <td>@{{dato.ESTADO}} </td>
                                </tr>
                                </tbody>
                            </table>


                            <div class="alert alert-danger" role="alert" v-if="mensaje_registro === true">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                No existen registros en las fechas seleccionadas
                            </div>
                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <div class="modal face" id="buscarDiagnostico" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consultar diagnostico medico
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="col-md-12 ">
                            <div class="form-group row">
                                <input type="text" class="col-md-4"  placeholder="diaganostico" v-model="descripcion_diagnostico" v-on:keyup.13="consultarDiagnostico" >
                            </div>
                            <p>Pacientes</p>
                            <hr>
                            <table class="table-bordered">
                                <thead>
                                <tr>
                                    <td>*</td>
                                    <td>CODIGO</td>
                                    <td>DESCRIPCION</td>
                                    <td>TIPO</td>
                                    <td>OBSERVACIÓN</td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr v-for="dato in lista_diagnostico">
                                    <td> <i class="fa fa-check-square-o" aria-hidden="true" v-on:click="pasarDatos(dato.codigo,dato.descripcion)"></i>  </td>
                                    <td> @{{dato.codigo}} </td>
                                    <td>@{{dato.descripcion}} </td>
                                    <td>@{{dato.tipo}} </td>
                                    <td>@{{dato.observacion}} </td>
                                </tr>
                                </tbody>
                            </table>


                        </div>
                        <hr>
                        <div class="tile-body">

                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </div>

            </div>

        </div>
        <form method="post" id="ticketEgreso" action="{{route('hospitalizacion.ticket.egreso')}}" target="_blank" >
            {{csrf_field()}}
            <input type="hidden" name="id_hospitalizaacion_t" v-model="id">
            <input type="hidden" name="id_egreso_t" v-model="egreso2">
            <input v-if="rotulo === true" type="submit" id="enviar_rotulo">
        </form>
    </div>




@endsection
@section('script')
    <script>
        function enviar() {
            document.getElementById("form").submit();
            //document.getElementById("form1").submit()
        }
    </script>
    <script type="text/javascript">
        var app = new Vue ({
               el:'#egreso',
               data : {
                   rotulo : false,
                   // variables para el menu interno
                   btn_guardar : true,
                   btn_imprimir : false,
                   btn_limpiar : true,
                   buscar : 'inactivo',
                   imprimir : 'inactivo',
                   // variables formulario
                   //general
                   paciente_fecha: '',
                   desde :'',
                   hasta: '',
                   estado : 0,
                   estado2 :'',
                   listaPacientes : [],
                   cmbEstado :[],

                   // variables del modal para la consulta de diagnostico
                   cargar :0,
                   descripcion_diagnostico :'',
                   lista_diagnostico : [],

                   // variables de busqueda de paciente-individual
                   privado: false,
                   paciente :'',
                   listaPaciente : [],
                   tabla :1,

                   // diagnostico
                   principal_codigo :'',
                   asociado1_codigo :'',
                   asociado2_codigo :'',
                   principal_descripcion :'',
                   asociado1_descripcion :'',
                   asociado2_descripcion :'',

                   // egreso
                   id : '',
                   paciente_principal : '',
                    fecha_hora_ingreso : '',
                    complicacion : '',
                    tipo_complicacion : '',
                    codigo_cama : '',
                    des_campo1 : '',
                    des_campo2 : '',
                    des_campo3 : '',
                   cama : '',
                   sala : '',
                   Procedencia : '',
                   unidad : '',

                   medico : 0,
                   tipo_alta : 0,
                   tipo_egreso : 0,
                   observacion_egreso : '',
                    fecha_hora_egreso : '',
                //consulta
                   errores : [],
                   visibilidad : false,
                   registro : false,
                   mensaje_registro : false,

                   datosConsulta2 : [],

                   principal_ : false,
                   asociado1 : false,
                   asociado2:false,

                    egreso2 : '',

               },

                created : function() {
                    axios.get('/hospitalizacion/estadoshospitalizacion').then(response => {
                        this.cmbEstado
                        = response.data
                    })
                },

                methods : {

                    //--------------------------SEC_CONSULTA-----------------//
                    consultarDiagnosticoCodigo : function(tipo){
                        // busca el diagnostico por codigo
                        if(tipo === 1){
                            var url = 'tipodiagnostico/'+this.principal_codigo+'/codigo';
                            axios.get(url).then(response => {
                                this.lista_diagnosticoCodigo  = response.data;
                            if(this.lista_diagnosticoCodigo.length === 0  ){
                                toastr.error("El número de codigo no se encuentra asociado a ningun diagnostico.")
                            } else {
                                this.principal_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                                this.principal_ = true;
                                document.getElementById(asociado_1).focus();
                            }
                        })
                        }

                        if(tipo === 2){
                            var url = 'tipodiagnostico/'+this.asociado1_codigo+'/codigo';
                            axios.get(url).then(response => {
                                this.lista_diagnosticoCodigo  = response.data;
                            if(this.lista_diagnosticoCodigo.length === 0  ){
                                toastr.error("El número de codigo no se encuentra asociado a ningun diagnostico.")
                            } else {
                                this.asociado1_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                                this.asociado1 = true;
                                document.getElementById(asociado_2).focus();
                            }
                        })
                        }

                        if(tipo === 3){
                            var url = 'tipodiagnostico/'+this.asociado2_codigo+'/codigo';
                            axios.get(url).then(response => {
                                this.lista_diagnosticoCodigo  = response.data;
                            if(this.lista_diagnosticoCodigo.length === 0  ){
                                toastr.error("El número de codigo no se encuentra asociado a ningun diagnostico.")
                            }else {
                                this.asociado2_descripcion = this.lista_diagnosticoCodigo[0].descripcion;
                                this.asociado2 = true;

                            }
                        })
                        }


                    },
                    consultarDiagnostico : function(){
                        var url = 'tipodiagnostico/'+this.descripcion_diagnostico+'';
                        //alert(url);
                        axios.get(url).then(response => {
                            this.lista_diagnostico  = response.data;
                    })
                    },
                    buscarDiagnostico : function(numero) {
                        $("#buscarDiagnostico").modal();
                        this.cargar = numero;
                    },
                    consultar : function () {

                        this.validarConsulta();
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "600",
                            "hideDuration": "3000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        this.validarConsulta();
                        var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/; // nombres apellidos
                        if(patt3.test(this.paciente) == false)
                        {
                            toastr.error("el nombre del paciente no pueden contener numeros ,caracteres especiales");
                        }else {
                            toastr.success("Consultando información");
                            this.consultarPacientes()
                        }


                    },
                    consultarPacientes : function()  {
                        if(this.paciente === '' && this.desde === '' && this.hasta === '')
                        {
                            var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"";
                        } else {
                            var hospitalizacion = "/hospitalizacion/pacientes/fecha/egreso/"+this.desde+"/"+this.hasta+"/"+this.paciente+"";
                        }
                        axios.get(hospitalizacion ).then(response => {
                            this.listaPacientes  = response.data;
                        var num = this.listaPacientes.length;
                        if(num === 0) {
                            this.registro = false;
                            this.mensaje_registro = true;
                        } else {
                            this.registro = true;
                            this.mensaje_registro = false;
                        }
                    })
                    },
                    //-------------------------*CONSULTA*-----------------//

                    //-------------------------SEC_PASAR DATOS-----------------//
                    pasarDatos : function (codigo,descripcion) {
                        if(this.cargar === 1){
                            this.principal_codigo = codigo;
                            this.principal_descripcion = descripcion;
                            this.lista_diagnostico = [];
                        }else if(this.cargar === 2)
                        {
                            this.asociado1_codigo = codigo;
                            this.asociado1_descripcion = descripcion;
                            this.lista_diagnostico = [];
                        } else if(this.cargar === 3) {
                            this.asociado2_codigo = codigo;
                            this.asociado2_descripcion = descripcion;
                            this.lista_diagnostico = [];
                        }

                        $("#buscarDiagnostico").modal('hide');
                    },
                    pasarDatosConsulta : function (id,paciente) {
                        this.paciente_principal = paciente;

                        var url =  "consultarhospitalizacionid/"+id+'';

                        axios.get(url).then(response => {
                            this.datosConsulta  = response.data;

                        this.id = this.datosConsulta[0].id;
                        this.Procedencia = this.datosConsulta[0].Procedencias;
                        this.Nombre_Responsable = this.datosConsulta[0].Nombre_Responsable;
                        this.Direccion_Responsable = this.datosConsulta[0].Direccion_Responsable;
                        this.Documento_Responsable = this.datosConsulta[0].Documento_Responsable;
                        this.Telefono_Responsable = this.datosConsulta[0].Telefono_Responsable;
                        this.Observaciones = this.datosConsulta[0].Observaciones;
                        this.Servicios = this.datosConsulta[0].Servicios;
                        this.medico  = this.datosConsulta[0].medico;
                        this.principal  = this.datosConsulta[0].principal;
                        this.asociado_1 = this.datosConsulta[0].asociado_1;
                        this.asociado_2 = this.datosConsulta[0].asociado_2;
                        this.Medico_Observacion = this.datosConsulta[0].Medico_Observacion;
                        //this.fecha_modificacion  = this.datosConsulta[0].fecha_modificacion;
                        //this.usuario_modificacion = this.datosConsulta[0].usuario_modificacion;
                        //this.pcname = this.datosConsulta[0].pc;
                        this.derivacion = this.datosConsulta[0].derivacion;
                        this.hospital = this.datosConsulta[0].hospital;
                        this.status_documento = this.datosConsulta[0].status_documento;
                        this.observacion_documento = this.datosConsulta[0].observacion_documento;
                        this.status = this.datosConsulta[0].status;
                        //this.medico_anterior = this.datosConsulta[0].medico;
                        //alert(this.datosConsulta[0].medico);

                        this.fecha_hora_ingreso = this.datosConsulta[0].Fecha_Hora_Ingreso;
                        this.complicacion = this.datosConsulta[0].complicacion;
                        this.tipo_complicacion = this.datosConsulta[0].tipo_complicacion;
                        this.codigo_cama = this.datosConsulta[0].codigo_cama;
                        this.des_campo1 = this.datosConsulta[0].des_campo1;
                        this.des_campo2 = this.datosConsulta[0].des_campo2;
                        this.des_campo3 = this.datosConsulta[0].des_campo3;

                        this.sala = this.datosConsulta[0].habitacion;

                        this.cama = this.datosConsulta[0].codigo_cama;

                        var url2 ="consultarcamas/"+this.cama+"";
                        axios.get(url2).then(response => {
                            this.datosConsulta2  = response.data;
                        this.cama = this.datosConsulta2[0].numero_camas;
                        this.unidad = this.datosConsulta2[0].descripcion_camas;
                        this.sala = this.datosConsulta2[0].sala;
                    })


                    })
                        this.visibilidad = true;
                        $("#buscar").modal('hide');

                    },
                    //------------------------*PASAR DATOS*-----------------//

                    //-------------------------SEC_BOTONES-----------------//
                    limpiarDiagnostico : function(numero){
                        if(numero === 1) {
                            this.principal_codigo = "";
                            this.principal_descripcion = "";
                            this.principal_ = false;
                        }else if(numero === 2){
                            this.asociado1_codigo = "";
                            this.asociado1_descripcion = "";
                            this.asociado1 = false;
                        } else if(numero === 3){
                            this.asociado2_codigo = "";
                            this.asociado2_descripcion = "";
                            this.asociado2 = false;
                        }
                    },
                    limpiar : function () {
                        this.principal_descripcion = "";
                        this.codigo_cama = "";
                        this.asociado2_codigo = "";
                        this.asociado1_codigo = "";
                        this.asociado1_descripcion = "";
                        this.asociado2_descripcion = "";
                        this.principal_codigo = "";
                        this.observacion_egreso = "";
                        this.id = "";
                        this.medico = "";
                        this.fecha_hora_ingreso = "";
                        this.tipo_alta = "";
                        this.tipo_egreso = "";
                        this.codigo_cama = "";
                        this.Procedencia = "";
                        this.observacion_egreso = "";
                        this.paciente_principal = "";
                        this.cama = '';
                        this.sala = '';
                        this.listaPacientes = [];
                        this.unidad = [];
                        this.btn_guardar = true;
                        this.btn_imprimir = false;
                        this.principal_ = false;
                        this.asociado1 = false;
                        this.asociado2 = false

                    },
                    Imprimir : function() {


                        this.egreso2 = $("#id").val();
                        //alert(this.egreso2);
                    //    document.getElementById("ticketEgreso").submit();

                      var c = document.createElement("a");
                      var d = document.createElement("a");
                      c.target = "_blank";
                      c.href = "hospitalizacion/reporte/egreso/"+this.id+"/"+this.egreso2+"";
                      c.click();
                    /*
                      d.target = "_blank";
                      d.href = "hospitalizacion/garantia/admision/"+this.id+"/"+egreso2+"";
                      d.click();
                      */
                    },
                    //------------------------*PASAR DATOS*-----------------//

                    //--------------------------SEC_VALIDAR-----------------//
                    validarEgreso : function () {
                        if(this.paciente_principal === '' || this.paciente_principal === undefined)
                        {
                            this.errores.push("Carge la información del paciente hospitalizado");
                        }


                        if(this.medico === 0 || this.medico === undefined)
                        {
                            this.errores.push("Seleccione el medico de egreso");
                        }

                        if(this.principal_codigo === '' || this.principal_codigo=== undefined)
                        {
                            this.errores.push("Seleccione el diagnostico principal");
                        }

                        if(this.asociado1_codigo === '' || this.asociado1_codigo=== undefined)
                        {
                            this.errores.push("Seleccione el diagnostico asociado 1");
                        }

                        if(this.asociado2_codigo === '' || this.asociado2_codigo=== undefined)
                        {
                            this.errores.push("Seleccione el diagnostico asociado 1");
                        }

                        if(this.tipo_alta === 0 || this.tipo_alta === undefined)
                        {
                            this.errores.push("Seleccione el tipo de alta");
                        }

                        if(this.tipo_egreso === 0 || this.tipo_egreso=== undefined)
                        {
                            this.errores.push("Seleccione el tipo de egreso");
                        }

                        if(this.observacion_egreso === '' || this.observacion_egreso=== undefined)
                        {
                            this.errores.push("Ingrese la observación");
                        }
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "600",
                            "hideDuration": "3000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        if( this.errores.length === 0) {
                            // document.getElementById('registroTitular').submit()
                            this.insertEgreso()
                        } else {
                            var num = this.errores.length;
                            for(i=0; i<num;i++) {
                                toastr.error(this.errores[i]);
                            }
                        }
                        this.errores = [];


                    },
                    validarConsulta : function() {

                        if(this.desde === '' || this.desde === undefined) {
                            var date = new Date();
                            var y = date.getFullYear();
                            var m = date.getMonth() +1 ;
                            //date.setDate(date.getMonth() + 1);
                           if(m === 0){
                               m = 01;
                           }else if(m < 10) {
                               m = ''+0+m;
                           }

                            var d = date.getDate();
                            if(d < 10) {
                                d = ''+0+d;
                            }
                            this.desde = ''+y+'-'+m+'-'+d+'';

                        }

                        if(this.hasta === '' || this.hasta === undefined) {
                            var date = new Date();
                            var y = date.getFullYear();
                            var m = date.getMonth() +1 ;

                            //TuFecha.setDate(TuFecha.getDate() + dias);
                            var d = date.getDate();

                            if(m === 2 && d === 28 )
                            {
                                m= 3;
                                d= 1;
                            } else if(( m===4 || m===6 || m===9 || m===11 ) && d === 30) {
                                m = m+1;
                                d =1;
                            } else if(( m===1 || m===3 || m===5 || m===7 || m==8 || m===10 ) && d === 31)
                            {
                                m = m +1;
                                d="1";
                            } else if( m===12 && d === 31)
                            {
                                y= y+1;
                                m = 1;
                                d =1;
                            } else {
                                d = d +1;
                            }

                            if( m < 10) {
                                m = ''+0+m;
                            }
                            if(d < 10){
                                d = ''+0+d;
                            }

                            this.hasta = ''+y+'-'+m+'-'+d+'';
                        }


                    },

                    //-------------------------*VALIDAR*-----------------//

                    //--------------------------SEC_iNSERTAR-----------------//
                    insertEgreso :function () {
                        var parametros = {
                            "_token": "{{ csrf_token() }}",
                            //--
                            "id_hospitalizacion" : this.id, //
                            "medico" : this.medico,
                            "principal" : this.principal_codigo,
                            "asociado1" : this.asociado1_codigo,
                            "asociado2" : this.asociado2_codigo,
                            "cmbTipoEgreso" : this.tipo_egreso, //

                            "complicacion" : this.complicacion, //
                            "tipo_complicacion" : this.tipo_complicacion, //
                            "fechaingreso" : this.fecha_hora_ingreso, //

                            "listaTipoAlta" : this.tipo_alta, //
                            "cama" : this.codigo_cama, //
                            "procedencia" : this.Procedencia, //
                            "observacion" : this.observacion_egreso, //

                        };
                        $.ajax({
                            data : parametros,
                            url : "egresoshospitalizacion",
                            type : "post",
                            async : false,
                            success : function(d){
                                console.log(d);
                                if( d == 0){
                                    toastr.error('Error de DB.', 'Error', {timeOut: 5000});
                                } else {
                                    var b = JSON.parse(d);
                                    $("#id").val(b.id);
                                    toastr.success('Egreso realizado con exito.', 'Exito', {timeOut: 5000});
                                }
                            },
                            error : function (response,jqXHR) {
                                if(response.status === 422)
                                {
                                    var errors = $.parseJSON(response.responseText);
                                    $.each(errors, function (key, value) {
                                        if($.isPlainObject(value)) {
                                            $.each(value, function (key, value) {
                                                toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                                console.log(key+ " " +value);
                                            });
                                        }else{
                                            toastr.error('Error al momento de realizar el  egreso. '+''+'', 'Error', {timeOut: 5000});
                                        }
                                    });

                                } else {
                                    toastr.error('Error al momento de realizar el  egreso::: '+''+'', 'Error', {timeOut: 5000});
                                }

                            }
                        })

                        //  alert($("#id").val());

                        if($("#id").val() !== ''){
                            var egreso= $("#id").val();
                            //var a = document.createElement("a");
                            //var b = document.createElement("a");
                            //a.target = "_blank";
                            //a.href = "reporte/egreso/"+this.id+"/"+egreso+"";
                            //a.click();

                            //b.target = "_blank";
                            //b.href = "garantia/admision/"+this.id+"/"+egreso+"";
                            //b.click();

                            this.btn_imprimir = true;
                            this.btn_guardar = false;
                            this.btn_limpiar = true;
                        }


                    },
                    //-------------------------*iNSERTAR*-----------------//

                }
            });

     </script>


    <script type="text/javascript">
        $(document).ready(function() {

            $.get("medico", function (datos) {
                $.each(datos, function (key, value) {
                    $("#medicoList").append("<option value=" + value.id + ">" + value.Medico + "</option>");
                });
            });


            $.get("seguro", function (datos) {
                $.each(datos, function (key, value) {
                    $("#seguroList").append("<option value=" + value.codigo + ">" + value.Descripcion + "</option>");
                });
            });

            $.get("cmbegresoalta", function (datos) {
                $.each(datos, function (key, value) {
                    $("#egresoAltaList").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("cmbegreso", function (datos) {
                $.each(datos, function (key, value) {
                    $("#egresoList").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("sala", function (datos) {
                $.each(datos, function (key, value) {
                    $("#sala").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });

            $.get("unidad", function (datos) {
                $.each(datos, function (key, value) {
                    $("#unidad").append("<option value=" + value.codigo + ">" + value.descripcion + "</option>");
                });
            });



          //  tipodiagnostico/{descripcion}
        });
    </script>

@endsection
