@extends('layouts.principal')
@section('titulo')
 <h1>REGISTRO PACIENTES</h1>
@endsection
@section('title','registro de pacientes')
@section('css')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
     <link href="{{ asset('css/paper-bootstrap-wizard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet">
    <style>
        .body {
            font-size: 1.3rem;
        }
        .tab-content  {
            font-size: 1.3rem;
        }

        .form-control-control {
            display: block;
            width: 100%;
            padding: 7px;
            font-size: 1.59rem;
            line-height: 1.9;
            color: #495057;
            background-color: #FFF;
            background-clip: padding-box;
            border: 2px solid #ced4da;
            border-radius: 4px;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        .form-control-control::-ms-expand {
            background-color: transparent;
            border: 0;
        }

        .form-control-control:focus {
            color: #495057;
            background-color: #FFF;
            border-color: #009688;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .form-control-control::-webkit-input-placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control:-ms-input-placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control::-ms-input-placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control::placeholder {
            color: #6c757d;
            opacity: 1;
        }

        .form-control-control:disabled, .form-control-control[readonly] {
            background-color: #e9ecef;
            opacity: 1;
        }

        select.form-control-control:not([size]):not([multiple]) {
            /*height: calc(2.0625rem + 4px);*/
            padding: 12px;
        }

        select.form-control-control:focus::-ms-value {
            color: #495057;
            background-color: #FFF;
        }

        .form-control-control-file,
        .form-control-control-range {
            display: block;
            width: 100%;
        }

    </style>
@endsection
@section('body','"font-size:0.900rem"')
@section('contenido')

    {!! Toastr::message() !!}
     @include('flash::message')
    <div class="col-lg-12" id="aplicacion" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml"
         xmlns:v-on:key.enter="http://www.w3.org/1999/xhtml" xmlns:v-on.keyup="http://www.w3.org/1999/xhtml">
        @if(isset($id))
            <input type="hidden" value="{{$id}}" name="respuesta" id="Respuesta_controlador">
            {{$id}}
        @endif

        <div class="wizard-container">

            <div class="card wizard-card" id="wizardProfile" data-color="blue">
                <div class="container">

                    <div class="col-md-12 col-center">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <i v-if="nuevo === 'activo'"    class="fa fa-file-o fa-3x"   aria-hidden="true"   v-bind:class = "{'activo':'activo'}" v-on:click="Nuevo" ></i>
                                <i v-else   class="fa fa-file-o fa-3x"   aria-hidden="true"   v-bind:class = "{'inactivo':'inactivo'}" ></i>
                                <p >Nuevo</p>
                            </div>

                            <div class="col-md-2">
                                <i v-if="editar === 'activo'" v-on:click="Editar" class="fa fa-pencil-square-o fa-3x" aria-hidden="true" v-bind:class = "{'activo':'inactivo'}"  ></i>
                                <i v-else class="fa fa-pencil-square-o fa-3x" aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"  ></i>

                                <p>Editar</p>
                            </div>

                            <div class="col-md-2">
                                <i v-if="imprimir === 'activo'"   class="fa fa-print fa-3x" aria-hidden="true" v-bind:class = "{'activo':'activo'}"></i>
                                <i v-else class="fa fa-print fa-3x" aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"></i>
                                <div class="dropdown">
                                    <p   data-toggle="dropdown">
                                        Imprimir
                                    </p>
                                    <div   class="dropdown-menu">
                                        <form action="{{route('emergencia.registrorapido')}}" method="get"  class="dropdown-item" target="_blank">

                                            <input type="hidden" name="id"  v-model="id">
                                            <input type="hidden" name="apellido_paterno"  v-model="apellido_paterno">
                                            <input type="hidden" name="apellido_materno"  v-model="apellido_materno">
                                            <input type="hidden" name="primer_nombre"  v-model="primer_nombre">
                                            <input type="hidden" name="segundo_nombre"  v-model="segundo_nombre">
                                            <input type="hidden" name="fecha_nacimiento"  v-model="fecha_nacimiento">
                                            <input type="hidden" name="edad" v-model="edad">
                                            <input type="hidden" name="telefono"  v-model="telefono">
                                            <input type="hidden" name="direccion" v-model="direccion">
                                            <input type="hidden" name="ocupacion"  v-model="ocupacion">
                                            <input type="hidden" name="apellidos_titular"  v-model="apellidos">
                                            <input type="hidden" name="nombres_titular"  v-model="nombres">
                                            <input type="hidden" name="genero"  v-model="genero">
                                            <input type="hidden" name="tipo_seguro"  v-model="tipo_seguro">
                                            <input type="hidden" name="cedula"  v-model="in_cedula">
                                            <input type="hidden" name="estado_civil"  v-model="estado_civil">
                                            <input type="hidden" name="lugar_nacimiento"  v-model="lugar_nacimiento">

                                            <button type="submit" class="btn btn-link"> Registro rapido</button>
                                        </form>

                                        <form action="{{route('emergencia.reporteadmision')}}" method="get" class="dropdown-item" target="_blank">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="parroquia" v-model="parroquia">
                                            <input type="hidden" name="ciudad" v-model="TempCiudad">
                                            <input type="hidden" name="provincia" v-model="provincia">
                                            <input type="hidden" name="id"  v-model="id">
                                            <input type="hidden" name="apellido_paterno"  v-model="apellido_paterno">
                                            <input type="hidden" name="apellido_materno"  v-model="apellido_materno">
                                            <input type="hidden" name="primer_nombre"  v-model="primer_nombre">
                                            <input type="hidden" name="segundo_nombre"  v-model="segundo_nombre">
                                            <input type="hidden" name="cedula"  v-model="in_cedula">
                                            <input type="hidden" name="direccion" v-model="direccion">
                                            <input type="hidden" name="telefono"  v-model="telefono">
                                            <input type="hidden" name="celular"  v-model="celular">
                                            <input type="hidden" name="otro"  v-model="otro">
                                            <input type="hidden" name="fecha_nacimiento"  v-model="fecha_nacimiento">
                                            <input type="hidden" name="lugar_nacimiento"  v-model="lugar_nacimiento">
                                            <input type="hidden" name="nacionalidad" v-model="nacionalidad">
                                            <input type="hidden" name="etnia"  v-model="etnico">
                                            <input type="hidden" name="edad" v-model="edad">
                                            <input type="hidden" name="genero"  v-model="genero">
                                            <input type="hidden" name="estado_civil"  v-model="estado_civil">
                                            <input type="hidden" name="instruccion"  v-model="instruccion">

                                            <input type="hidden" name="ocupacion"  v-model="ocupacion">
                                            <input type="hidden" name="trabajo"  v-model="lugar_trabajo">
                                            <input type="hidden" name="tipo_seguro"  v-model="tipo_seguro">
                                            <!-- referido -->
                                            <input type="hidden" name="apellidos"  v-model="apellidos">
                                            <input type="hidden" name="nombre"  v-model="Titularprimer_nombre">
                                            <input type="hidden" name="fecha_emision"  v-model="fecha_modificacion">
                                            <input type="hidden" name="parentesco"  v-model="tipo_parentesco">
                                            <input type="hidden" name="Titulardireccion"  v-model="Titulardireccion">
                                            <input type="hidden" name="Titulartelefono"  v-model="Titulartelefono">
                                            <input type="hidden" name="Titularcelular"  v-model="Titularcelular">
                                            <input type="hidden" name="Titularotro"  v-model="Titularotro">






                                            <!-- parentesco -->
                                            <!-- dirrecion -->
                                            <!-- telefono -->
                                            <!-- admisionista -->

                                            <button type="submit" class="btn btn-link" > Admision </button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <i v-if="limpiar === 'activo'" class="fa fa-close fa-3x" aria-hidden="true" v-bind:class = "{'activo':'activo'}" v-on:click="Limpiar()"></i>
                                <i v-else class="fa fa-close fa-3x" aria-hidden="true" v-bind:class = "{'inactivo':'inactivo'}"></i>
                                <p>Limpiar</p>
                            </div>
                            <input class="form-control float-right col-md-4" v-model="verificar_titular_cedula" type="text" maxlength="15" placeholder="verificar titular por cedula" v-on:keyup.13="verificarTitular">
                        </div>
                    </div>
                </div>
                <hr>
                <form method="post" id="form1" action="{{route('emergencia.registropacientes.guardar')}}" >
                    {{ csrf_field() }}
                    <div class="wizard-header text-center">

                        <h3 class="wizard-title">Registro</h3>
                    </div>
                    <div class="wizard-navigation">
                        <div class="progress-with-circle">
                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="7" style="width: 21%;"></div>
                        </div>
                        <ul>
                            <li>
                                <a href="#basica" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-notepad" ></i>
                                    </div>
                                    Información basica
                                </a>
                            </li>
                            <li>
                                <a href="#domiciliaria" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-home" ></i>
                                    </div>
                                    Información domiciliaria
                                </a>
                            </li>
                            <li>
                                <a href="#seguro" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-lock" ></i>
                                    </div>
                                    Seguro
                                </a>
                            </li>
                            <li>
                                <a href="#dependiente" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-more" ></i>
                                    </div>
                                    Información adicional
                                </a>
                            </li>
                            <li>
                                <a href="#titular" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-user" ></i>
                                    </div>
                                    Titular
                                </a>
                            </li>
                            <li>
                                <a href="#escaner" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-printer" ></i>
                                    </div>
                                    Escanear
                                </a>
                            </li>
                            <li>
                                <a href="#cobrar" data-toggle="tab" v-on:click="setCampos">
                                    <div class="icon-circle">
                                        <i class="ti-wallet" ></i>
                                    </div>
                                    Cuentas por cobrar
                                </a>
                            </li>
                        </ul>

                    </div>

                    <div class="tab-content" style="font-size: 1.3rem;">
                        <div class="tab-pane" id="basica">
                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> Ingrese la información basica.</h5>
                                <div class="form-horizontal">
                                    <input type="hidden" name="accion" v-model="grabar_actualizar">
                                    <input type="hidden" name="campo1" v-model="des_campo1">
                                    <input type="hidden" name="campo2" v-model="des_campo2">
                                    <input type="hidden" name="campo3" v-model="des_campo3">
                                    <input type="hidden" name="status_otro_seguro" v-model="status_otro_seguro">
                                    <input type="hidden" name="descripcion_otro_seguro" v-model="descripcion_otro_seguro">
                                    <input type="hidden" name="campo1a" v-model="des_campo1_a">
                                    <input type="hidden" name="campo2a" v-model="des_campo2_a">
                                    <input type="hidden" name="campo3a" v-model="des_campo3_a">
                                    <input type="hidden" name="usuario_ingreso_a" v-model="usuario_ingreso_a">
                                    <input type="hidden" name="usuario_ingreso" v-model="usuario_ingreso">
                                    <input type="hidden" name="fecha_cedulacion" v-model="fecha_cedulacion">
                                    <input type="hidden" name="fecha_fallecimiento" v-model="fecha_fallecimiento">
                                    <input type="hidden" name="instruccion" v-model="instruccion">
                                    <input type="hidden" name="id" v-model="id">
                                    <input type="hidden" name="TempTipoSeguro" v-model="TempTipoSeguro">
                                    <input type="hidden" name="TempTitular" v-model="TempTitular ">
                                    <input type="hidden" name="pc" v-model="pcname">
                                    <input type="hidden" name="fecha_ingreso" v-model="fecha_ingreso">


                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Identificación</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control"   v-bind:readonly="visibilidad_IB3" :value="tipo_identificacion" name="tipo_identificacion" v-model="tipo_identificacion">
                                                <option v-for="dato in  cmbidentificacion" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <input class="form-control-control" type="text" placeholder="Cedula" v-bind:readonly="visibilidad_IB3" v-model="in_cedula" pattern="[A-Za-z0-9]{1,15}" maxlength="15" name=cedula v-on:keyup.13="consultarCedula">
                                        </div>
                                        <div class="col-md-1">
                                            <i v-if ="buscar_nombre ==='activo'" v-bind:class = "{'activo':'activo'}"  class="fa fa-search fa-x3"  aria-hidden="true" id="show-modal"  ></i>
                                            <i v-else  v-bind:class = "{'inactivo':'inactivo'}"  class="fa fa-search fa-x3" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Nombres</label>
                                        <div class="col-md-5">
                                            <input v-model="primer_nombre" name="primer_nombre" class="form-control-control" type="text" placeholder="Primer Nombre"  v-bind:readonly="visibilidad_IB" autocomplete="off" >
                                        </div>
                                        <div class="col-md-5">
                                            <input v-model="segundo_nombre" name="segundo_nombre" class="form-control-control" type="text" placeholder="Segundo Nombre" v-bind:readonly="visibilidad_IB" autocomplete="off" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Apellidos</label>
                                        <div class="col-md-5">
                                            <input v-model="apellido_paterno" name="apellido_paterno" class="form-control-control" type="text" placeholder="Paterno" v-bind:readonly="visibilidad_IB" autocomplete="off">
                                        </div>
                                        <div class="col-md-5">
                                            <input v-model="apellido_materno"  name="apellido_materno" class="form-control-control" type="text" placeholder="Materno" v-bind:readonly="visibilidad_IB" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Fecha Nac</label>
                                        <div class="col-md-4">
                                            <input v-model="fecha_nacimiento" name="fecha_nacimiento" v-bind:readonly="visibilidad_IB" class="form-control-control" type="date" v-on:change="calcular_edad"  >
                                        </div>
                                        <div class="col-md-2">
                                            <input v-model="edad" name="edad" class="form-control-control" type="text"  readonly >
                                        </div>
                                        <div class="col-md-4">
                                            <input v-model="lugar_nacimiento" name="lugar_nacimiento" v-bind:readonly="visibilidad_IB" maxlength="40" class="form-control-control" type="text" placeholder="Lugar de nacimiento">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Genero</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control"   :value="genero" name="genero" v-bind:disabled="visibilidad_IB" v-model="genero">
                                                <option v-for="dato in  cmbGenero" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                        <label class="control-label col-md-2">Ocupacion</label>
                                        <div class="control-label col-md-4">
                                            <input v-model="ocupacion" name="ocupacion" class="form-control-control" type="text" maxlength="30" placeholder="ocupacion" v-bind:readonly="visibilidad_IB2" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Estado Civil</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control"  v-bind:disabled="visibilidad_IB" :value="estado_civil" name="estado_civil" v-model="estado_civil" >
                                                <option v-for="dato in  cmbEstadoCivil" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>

                                        </div>
                                        <label class="control-label col-md-2">Nacionalidad</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control"   :value="nacionalidad" name="nacionalidad" v-model="nacionalidad" v-bind:disabled="visibilidad_IB">
                                                <option v-for="dato in  cmbNacionalidad" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Cultura Étnicas</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control"   :value="etnico" name="etnico" v-model="etnico" v-bind:disabled="visibilidad_IB">
                                                <option v-for="dato in  cmbEtnia" :value="dato.raza" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                        <label class="control-label col-md-2">Tipo Sangre</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control"  v-bind:disabled="visibilidad_IB2" v-model="tipo_sangre" name="sangre">
                                                <option v-for="dato in  cmbSangre" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="form-label col-md-2" >Discapacidad</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control" :value="status_discapacidad" v-model="status_discapacidad" name="status_discapacidad" v-bind:disabled="visibilidad_IB2">
                                                <option value="S"> S </option>
                                                <option value="N"> N</option>
                                            </select>
                                        </div>
                                        <div v-if="status_discapacidad == 'S'" class="col-md-6">
                                            <input class="form-control-control" name="carnet_conadis" v-model="carnet_conadis" v-bind:readonly="visibilidad_IB2" >
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="domiciliaria">
                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> Ingrese la información domiciliaria.</h5>
                                <div class="form-horizontal">

                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Provincia</label>
                                        <div class="col-md-4">
                                            <select class="form-control-control" :value="provincia" v-model="provincia"  name="provincia" v-on:change="CambioProvincia" required v-bind:disabled="visibilidad_ID">
                                                <option v-for="dato in  cmbProvincia" :value="dato.codigo"  > @{{dato.descripcion}} </option>
                                            </select>

                                        </div>
                                        <label class="control-label col-md-2">Ciudad</label>
                                        <div class="col-md-4" v-if="ciudad===undefined">
                                            <select class="form-control-control" :value="TempCiudad" v-model="TempCiudad"  name="ciudad"  v-on:change="CambioCiudad" required v-bind:disabled="visibilidad_ID">
                                                <option v-for="dato in  cmbCiudad" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                        <div class="col-md-4" v-else>
                                            <select class="form-control-control" :value="TempCiudad"  v-model="ciudad"   name="ciudad"  v-on:change="CambioCiudad" required v-bind:disabled="visibilidad_ID">
                                                <option v-for="dato in  cmbCiudad" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Parroquia</label>
                                        <div class="col-md-10">
                                            <select class="form-control-control"   v-model="parroquia"  name="parroquia" required  v-bind:disabled="visibilidad_ID">
                                                <option v-for="dato in  cmbParroquia" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Dirección</label>
                                        <div class="col-md-10">
                                            <textarea v-model="direccion" name="direccion" maxlength="50" placeholder="Dirección domiciliaria" class="form-control-control" v-bind:readonly="visibilidad_ID"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Télefono</label>
                                        <div class="col-md-4">
                                            <input v-model="telefono" type="text" class="form-control-control" minlength="9" maxlength="9" placeholder="codigo provincial + #telefono 043573483"  name="telefono" v-bind:readonly="visibilidad_ID">
                                        </div>
                                        <label class="control-label col-md-2">Celular</label>
                                        <div class="col-md-4">
                                            <input v-model="celular" type="text" class="form-control-control" minlength="10" maxlength="10" name="celular" placeholder="0952013224" v-bind:readonly="visibilidad_ID">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Otro</label>
                                        <div class="col-md-10">
                                            <input v-model="otro" maxlength="10" type="text" class="form-control-control" name="otro" v-bind:readonly="visibilidad_ID">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Lugar de Trabajo</label>
                                        <div class="col-md-10">
                                            <input v-model="lugar_trabajo" name="lugar_trabajo" v-bind:readonly="visibilidad_ID" type="text" class="form-control-control" maxlength="25"  placeholder="Lugar de trabajo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="seguro">
                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> INGRESE LA INFORMACIÓN ACERCA DEL SEGURO</h5>
                                <div class="form-horizontal">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Tipo de seguro</label>
                                        <div class="col-md-9">
                                            <select class="form-control-control"   v-model="tipo_seguro" name="seguro"  v-on:change="CambioTipoSeguro()" required v-bind:readonly="visibilidad_S">
                                                <option v-for="dato in  cmbSeguro" :value="dato.codigo" > @{{dato.Descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row" v-if="tipo_seguro== 1">
                                        <label class="control-label col-md-2">Seguro IESS</label>
                                        <div class="col-lg-10">
                                            <select class="form-control-control" :value="tipo_seguro_iess" v-bind:disabled="visibilidad_S" name="tipo_seguro_iess" v-model="tipo_seguro_iess">
                                                <option v-for="dato in  cmbOtroSeguro" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="animated-checkbox">
                                        <label>
                                            <input type="checkbox" v-bind:disabled="visibilidad_S" v-model="coberturas" name="cobertura" ><span class="label-text">Mas coberturas</span>
                                        </label>
                                    </div>
                                    <div v-if="coberturas">
                                        <div class="animated-checkbox">
                                            <label>
                                                <input type="checkbox" name="prepagada" v-model="prepagada" v-bind:disabled="visibilidad_S" ><span class="label-text">Prepagada</span>
                                            </label>
                                        </div>
                                        <div class="" v-if="prepagada===true">
                                            <select class="form-control-control"  name="prepagada" v-model="otro_seguro" v-bind:disabled="visibilidad_S">
                                                <option v-for="dato in  cmbIngresoCalificacion" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                        <div class="animated-checkbox">
                                            <label>
                                                <input type="checkbox" v-model="isspol" name="isspol" v-bind:disabled="visibilidad_S"><span class="label-text">ISSPOL</span>
                                            </label>
                                        </div>
                                        <div class="animated-checkbox">
                                            <label>
                                                <input type="checkbox" v-model="issfa" name="issfa" v-bind:disabled="visibilidad_S"><span class="label-text">ISSFA</span>
                                            </label>
                                        </div>
                                        <div class="animated-checkbox" >
                                            <label>
                                                <input type="checkbox"  name="iess" v-model="iess" v-bind:disabled="visibilidad_S"><span class="label-text">IESS</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="dependiente">
                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> INFORMACIÓN ADICIONAL</h5>
                                <div class="form-horizontal">
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Padre</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control-control" v-bind:readonly="visibilidad_IA" v-model="nombres_padre" name="nombres_padre" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">Madre</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control-control" v-bind:readonly="visibilidad_IA" v-model="nombres_madre" name="nombres_madre" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-2">cónyuge</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control-control" v-bind:readonly="visibilidad_IA2" v-model="conyugue" name="conyuge" >
                                        </div>
                                    </div>
                                     <div class="form-group row"  v-if="edad < 18">
                                        <label class="control-label col-md-3">Cedula seg. prog.</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control-control"  v-model="cedula_seg_progenitor" name="cedula_segundo_progenitor" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="titular">
                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> INGRESE LA INFORMACIÓN DEL TITULAR</h5>
                                <div class="form-horizontal">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Parentesco</label>
                                        <div class="col-md-9">
                                            <select class="form-control-control" v-bind:disabled="visibilidad_T1"   :value="tipo_parentesco" name="parentesco" v-model="tipo_parentesco"  v-on:change="Parentesco_beneficiario()"  >
                                                <option v-for="dato in cmbParentesco" :value="dato.codigo"> @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">T.Beneficiario</label>
                                        <div class="col-md-9" v-if="tipo_beneficiario===undefined">
                                            <select class="form-control-control" v-bind:disabled="visibilidad_T1" :value="tipo_beneficiario " name="tipo_beneficiario" v-model="TempTipoBeneficiario"  >
                                                <option v-for="dato in cmbBeneficiario" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                        <div class="col-md-9" v-if="tipo_beneficiario!=undefined">
                                            <select class="form-control-control" v-bind:disabled="visibilidad_T1" :value="tipo_beneficiario " name="tipo_beneficiario" v-model="tipo_beneficiario"   >
                                                <option v-for="dato in cmbBeneficiario" :value="dato.codigo" > @{{dato.descripcion}} </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Titular/Afiliado</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control-control"  v-model="cedula_titular" name="cedula_titular" placeholder="Cedula Afiliado/titualar" v-bind:readonly="visibilidad_T3" v-on:keyup.13="ConsultarTitularCedula">
                                        </div>
                                        <div class="col-md-1">
                                            <i v-if ="buscar_titular_nombre ==='activo'" v-bind:class = "{'activo':'activo'}"  class="fa fa-search fa-x3"  aria-hidden="true"  data-toggle="modal" data-target="#buscar_titular_nombre_modal" ></i>
                                            <i v-else  v-bind:class = "{'inactivo':'inactivo'}"  class="fa fa-search fa-x3" aria-hidden="true"></i>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Apellidos</label>
                                        <div class="col-md-9">
                                            <input  type="text"  v-model="apellidos" name="apellidos" class="form-control-control"  placeholder="Apellidos" v-bind:readonly="visibilidad_T2">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Nombres</label>
                                        <div class="col-md-9">
                                            <input type="text" v-model="nombres" class="form-control-control" name="nombres"  placeholder="Nombres" v-bind:readonly="visibilidad_T2" >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Observación</label>
                                        <div class="col-md-9">
                                            <textarea required v-model="observacionTitular" v-bind:readonly="visibilidad_paciente" name="observacion" class="form-control-control"></textarea>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="escaner">

                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> Escanee las información.</h5>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <div class="card-header"> Docuemento</div>
                                            <div class="card-body">
                                                <img style="height:300px; width:100%; display: block" alt="imagen escaneada">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-8">
                                        <p>Documento</p>
                                        <div class="col-md-12">

                                           <select class="js-example-basic-multiple js-states form-control-control" style="width: 105%" name="states[]" multiple="multiple">
                                             <option v-for="dato in cmbDocumentosIess" :value="dato.codigo" > @{{dato.descripcion}}</option>
                                            </select>
                                            <textarea class="form-control-control" placeholder="observación"></textarea>
                                            <br>
                                            <div>
                                                <button class="btn btn-primary" type="button" onclick="scanToJpg()"><i class="fa fa-refresh" aria-hidden="true"></i>Cargar </button>
                                                <button class="btn btn-primary" type="button"><i class="fa fa-floppy-o" aria-hidden="true"></i>Grabar</button>
                                                <button class="btn btn-primary" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i>Limpiar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="cobrar">
                            <div class="col-md-10 col-md-offset-1">
                                <h5 class="info-text"> Cuentas por cobrar</h5>
                                <div class="form-group row">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizard-footer">
                        <div class="pull-right">
                            <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Siguiente' v-on:click="setCampos" />
                            <button type='button' v-if="guardar===true" v-on:click="AgregarPaciente()" class='btn btn-finish btn-fill btn-warning btn-wd' name='finish'  > Guardar</button>

                        </div>

                        <div class="pull-left">
                            <input type='button'  class='btn btn-previous btn-default btn-wd' name='previous' value='anterior' />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
        <!-- modales -->

        <!-- busqueda de titular por nombre -->
        <div class="modal face" id="buscar_titular_nombre_modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta genreal de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <div class="form-group">
                            <input type="text" class="form-control-control"  placeholder="Ingrese los apellidos para la consulta"  v-model="apellido" v-on:keyup.13="getUsers">
                        </div>
                        <hr>
                        <div style="overflow: scroll">
                            <table class="table table-bordered" style="width: 100%;" v-if="listaAfiliados" >
                                <thead>
                                <tr>
                                    <th>Seleccionar</th>
                                    <th>id</th>
                                    <th>cedula</th>
                                    <th>primer nombre</th>
                                    <th>segundo nombre</th>
                                    <th>apellido paterno</th>
                                    <th>apellido materno</th>
                                    <th>genero</th>
                                    <th>fecha de nacimiento</th>
                                    <th>Nacionalidad </th>
                                    <th>Pais</th>
                                    <th>Provincia</th>
                                    <th>Ciudad</th>
                                    <th>Dirección</th>
                                    <th>Telefono</th>
                                    <th>Celular</th>
                                    <th> Tipo de seguro</th>
                                    <th>des_campo1</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="dato in listaAfiliados">
                                    <td> <button class="btn btn-link" v-on:click="PasarDatos(dato.cedula,'si')"> seleccionar</button> </td>
                                    <td> @{{dato.id}}</td>
                                    <td>@{{dato.cedula}}</td>
                                    <td> @{{dato.primer_nombre}} </td>
                                    <td> @{{dato.segundo_nombre}}</td>,
                                    <td> @{{dato.apellido_paterno}}</td>
                                    <td> @{{dato.apellido_materno}}</td>
                                    <td> @{{dato.genero}}</td>
                                    <td> @{{dato.fecha_nacimiento}}</td>
                                    <td> @{{dato.nacionalidad}} </td>
                                    <td>@{{dato.pais}}</td>
                                    <td>@{{dato.provincia}}</td>
                                    <td>@{{dato.ciudad}}</td>
                                    <td>@{{dato.direccion}}</td>
                                    <td>@{{dato.telefono}}</td>
                                    <td>@{{dato.celular}}</td>
                                    <td> @{{dato.tipo_seguro}}</td>
                                    <td>@{{dato.des_campo1}}</td>
                                </tr>
                                </tbody>
                            </table>


                        </div>

                        <div class="modal-footer">

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- busqueda de pacientes por nombre -->
        <div class="modal face" id="buscar_paciente_modal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Consulta genreal de pacientes
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <h4> BUSQUEDA POR NOMBRE </h4>
                        <hr>
                        <div class="form-group">
                            <input type="text" class="form-control-control"  v-model="apellido" v-on:keyup.13="getUsers">
                        </div>
                        <hr>
                        <div style="overflow: scroll">
                            <table class="table table-bordered" style="width: 100%;" v-if="listaAfiliados" >
                                <thead>
                                <tr>
                                    <th>Seleccionar</th>
                                    <th>id</th>
                                    <th>cedula</th>
                                    <th>primer nombre</th>
                                    <th>segundo nombre</th>
                                    <th>apellido paterno</th>
                                    <th>apellido materno</th>
                                    <th>genero</th>
                                    <th>fecha de nacimiento</th>
                                    <th>Nacionalidad </th>
                                    <th>Pais</th>
                                    <th>Provincia</th>
                                    <th>Ciudad</th>
                                    <th>Dirección</th>
                                    <th>Telefono</th>
                                    <th>Celular</th>
                                    <th> Tipo de seguro</th>
                                    <th>des_campo1</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="dato in listaAfiliados">
                                    <td> <button class="btn btn-link" v-on:click="PasarDatos(dato.cedula,'no')"> seleccionar</button> </td>
                                    <td> @{{dato.id}}</td>
                                    <td>@{{dato.cedula}}</td>
                                    <td> @{{dato.primer_nombre}} </td>
                                    <td> @{{dato.segundo_nombre}}</td>,
                                    <td> @{{dato.apellido_paterno}}</td>
                                    <td> @{{dato.apellido_materno}}</td>
                                    <td> @{{dato.genero}}</td>
                                    <td> @{{dato.fecha_nacimiento}}</td>
                                    <td> @{{dato.nacionalidad}} </td>
                                    <td>@{{dato.pais}}</td>
                                    <td>@{{dato.provincia}}</td>
                                    <td>@{{dato.ciudad}}</td>
                                    <td>@{{dato.direccion}}</td>
                                    <td>@{{dato.telefono}}</td>
                                    <td>@{{dato.celular}}</td>
                                    <td> @{{dato.tipo_seguro}}</td>
                                    <td>@{{dato.des_campo1}}</td>
                                </tr>
                                </tbody>
                            </table>


                        </div>

                        <div class="modal-footer">

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>


@endsection

@section('script')
    <!-- <script src="{/{asset('js/scanner.js')}}" ></script>

    <script>
        //
        // Please read scanner.js developer's guide at: http://asprise.com/document-scan-upload-image-browser/ie-chrome-firefox-scanner-docs.html
        //
        /** Initiates a scan */
        function scanToJpg() {
            scanner.scan(displayImagesOnPage,
                {
                    "output_settings": [
                        {
                            "type": "return-base64",
                            "format": "jpg"
                        }
                    ]
                }
            );
        }
        /** Processes the scan result */
        function displayImagesOnPage(successful, mesg, response) {
            if(!successful) { // On error
                console.error('Failed: ' + mesg);
                return;
            }
            if(successful && mesg != null && mesg.toLowerCase().indexOf('user cancel') >= 0) { // User cancelled.
                console.info('User cancelled');
                return;
            }
            var scannedImages = scanner.getScannedImages(response, true, false); // returns an array of ScannedImage
            for(var i = 0; (scannedImages instanceof Array) && i < scannedImages.length; i++) {
                var scannedImage = scannedImages[i];
                processScannedImage(scannedImage);
            }
        }
        /** Images scanned so far. */
        var imagesScanned = [];
        /** Processes a ScannedImage */
        function processScannedImage(scannedImage) {
            imagesScanned.push(scannedImage);
            var elementImg = scanner.createDomElementFromModel( {
                'name': 'img',
                'attributes': {
                    'class': 'scanned',
                    'src': scannedImage.src
                }
            });
            document.getElementById('images').appendChild(elementImg);
        }
    </script>  -->

    <script src="{{asset('js/Emergencia.js')}}"> </script>

    <script type="text/javascript">

            function enviar() {
                document.getElementById("form1").submit()
            }

            function validarForm() {
             alert("hola");
                return false;
            }

            $(document).ready(function() {
                 $('.js-example-basic-multiple').select2();
            });

    </script>
    <script src="{{asset('js/jquery.bootstrap.wizard.js')}}"></script>
    <script src="{{asset('js/paper-bootstrap-wizard.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    <script>
    $('#flash-overlay-modal').modal().delay(3000).fadeOut(350);
</script>
@endsection
