|@extends('layouts.principal')
@section('titulo','Asistencia Medica')
@section('title','Asistencia Medica')
@section('contenido')
    {!! Toastr::message() !!}
    <div class=" col-md-8 col-md-offset-3" id="asistenciaMedica" xmlns:v-on.keyup="http://www.w3.org/1999/xhtml">

        @include('flash::message')
        <div class="tile">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#formulario">Información de ingreso</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabla">Atenciones de emergencia</a>
                </li>
                <!-- v-if="imprimir_v === true"  -->
                <li class="nav-item dropdown" v-if="imprimir_v === true">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Imprimir</a>

                    <div class="dropdown-menu"   >
                        <form action="{{route('emergencia.asistenciamedica.actaservicio')}}" method="get"  class="dropdown-item" target="_blank">

                            <input type="hidden" name="id"  v-model="id">
                            <input type="hidden" name="id_paciente"  v-model="paciente">
                            <input type="hidden" name="nombre_responsable"  v-model="responsable">
                            <input type="hidden" name="observacion"  v-model="observacion">
                            <input type="hidden" name="nombre_paciente"  v-model="nombre_paciente">
                            <input type="hidden" name="nombre_afinidad"  v-model="nombre_afinidad">
                            <input type="hidden" name="admision"  v-model="id_admision">

                            <button type="submit" class="btn btn-link"> Acta Servicio</button>
                        </form>
                    </div>

                    <div class="dropdown-menu">
                        <form action="{{route('emergencia.asistenciamedica.actaservicio')}}" method="get"  class="dropdown-item" target="_blank">
                            {{ csrf_field() }}
                            <input type="hidden" name="id"  v-model="id">
                            <input type="hidden" name="codigo"  v-model="id">
                            <input type="hidden" name="atencion" v-model="atencion">
                            <input type="hidden" name="servicio">
                            <input type="hidden" name="visita">
                            <input type="hidden" name="documento">
                            <button type="submit" class="btn btn-link"> Acta de servico</button>
                        </form>
                    </div>
                </li>
            </ul>

            <input type="hidden" name="id_admision"  id="id_admision">
            <!-- Tab panes -->
            <div class="tab-content" >
                <div id="formulario" class="container tab-pane active"><br>
                    <form action="{{route('emergencia.asistenciamedica.store')}}" method="post" id="form1">
                        {{ csrf_field() }}
                        <div class="tile-body">
                            <div class="form-group row">
                                <label class="control-label col-md-2" for="atencion">Nro. atención:</label>
                                <input class="form-control col-md-3" readonly type="text"  id="prueba">
                               <!-- <input class="form-control col-md-3" readonly type="text"  id="atencion" placeholder="" v-model="atencion" value="" > -->
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-md-1" for="afiliado">Afiliado</label>
                                <div class="col-md-3">
                                    <input v-model="cedula" required name="cedula" id="cedula" value="" class="form-control" type="text" placeholder="cedula" v-on:keyup.13="ConsultarCedula"  >
                                </div>
                                <i class="col-md-1 fa fa-search-plus fa-3x" aria-hidden="true"  data-toggle="modal" data-target="#buscar_paciente_modal"></i>
                                <input type="hidden" required v-model="id"  name="afiliado" >
                                <div class="col-md-7">

                                    <input v-model="nombre" required value="" readonly  name="nombre" class="form-control" type="text" placeholder=""  >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">paciente</label>
                                <select class="form-control" v-model="paciente" name="paciente" required v-on:change="consultarPacienteId()" >
                                    <option v-for="dato in cmbpaciente" :value="dato.Id"> @{{dato.Nombres}}</option>
                                </select>
                            </div>

                            <div class="form-horizontal">
                                <label class="control-label">En caso de ser necesario avisar a :</label>
                                <div class="form-group row">
                                    <div class="col-md-8">
                                      <input class="form-control" required  value="" readonly v-model="responsable" name="responsable" placeholder="Nombre del responsable">
                                    </div>
                                    <div class="col-md-4">

                                    <select class="form-control" v-model="afinidad" name="afinidad" v-on:change="setAfinidad" >
                                        <option value="titular">Titular</option>
                                        <option value="padre">Padre</option>
                                        <option value="madre">Madre</option>
                                        <option value="abuelo/a">Abuelo/a</option>
                                    </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-md-2">Dirección</label>
                                <input class="form-control col-md-10" readonly  required value="" v-model="direccion" name="direccion" placeholder="Direccion domiciliaria">
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2" for="telefono">Teléfono</label>
                                <input class="form-control col-md-3" readonly required name="telefono" id="telefono" v-model="telefono"    placeholder="telefono">
                                <label class="control-label col-md-2" >Tipo de seguro de salud</label>
                                <div class="col-md-3">
                                    <select class="form-control" disabled  v-model="seguro" name="seguro" >
                                        <option v-for="dato in  cmbSeguro" :value="dato.codigo" > @{{dato.Descripcion}} </option>
                                    </select>
                                </div>
                            </div>

                            <div class="animated-checkbox">
                                <label>
                                    <input type="checkbox" v-model="derivacion" name="derivacion"><span class="label-text">Derivación</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <select class="col-md-8 form-control" v-model="hospital" name="hospital" id="hospital" v-show="derivacion === true">
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="animated-radio-button " >
                                    <label class="form-radio-label radio-inline">
                                        <input  type="radio" value="si" v-model="firmante" name="firma"><span class="label-text">Paciente firmante</span>
                                    </label>
                                </div>
                                <div class= "animated-radio-button ">
                                    <label class="radio-inline form-radio-label ">
                                        <input  type="radio" value="no" id="no" v-model="firmante" name="firma"><span  class="label-text">Paciente no firmante</span>
                                    </label>
                                </div>
                                <div class= "animated-radio-button radio-inline">
                                    <label class="radio-inline form-radio-label ">
                                        <input  type="radio" value="in" v-model="firmante" name="firma"><span class="label-text">Acompañante</span>
                                    </label>
                                </div>
                            </div>
                                <div class="form-group row" v-if="firmante == 'no' || firmante == 'in'">
                                    <label class="control-label col-md-2">Parentesco</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="parentesco" name="parentesco" v-model="parentesco">
                                            <option v-for="dato in  cmbparentesco" :value="dato.codigo"> @{{dato.descripcion}} </option>
                                       </select>
                                    </div>
                                </div>
                            <div v-if="firmante == 'in'" class=" form-group row">

                                <input class="form-control col-md-4"  value="" v-model="cedula_acompanate" name="dcedul_a" placeholder="Cedula del acompañante">
                                <input class="form-control col-md-6"  value="" v-model="nombre_acompanante" name="dnombre_a" placeholder="Nombres del acompañante">

                            </div>

                            <div class="form-group row">
                                <label class="control-label col-md-1" for="doctor">Doctor(a)</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="doctor" required name="doctor" v-model="doctor">
                                    <option v-for="dato in  cmbmedico" :value="dato.id"> @{{dato.Medico}} </option>
                                </select>
                                </div>
                                <i class="fa fa-refresh col-md-1" aria-hidden="true"></i>

                            </div>
                            <div class="form-group">

                                <label class="control-label" for="observacion">Observación</label>
                                <input class="form-control" type="text" id="observacion" name="observacion" v-model="observacion" autocomplete="off">
                            </div>


                        <div class="tile-footer">
                            <button v-if="guardar_var===true" class="btn btn-primary" type="button" v-on:click="validarInsert" ><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>
                            <button v-else class="btn btn-defaul" type="button"  ><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>
                            <input class="btn btn-danger" type="reset" value="Limpiar" @click="limpiar">
                        </div>
                        </div>
                    </form>
                </div>
                <div id="tabla" class="container tab-pane fade"><br>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Paciente</th>
                            <th>Tipo de seguro</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="dato in pacientesAdmision ">
                            <td>@{{dato.Usuario}}</td>
                            <td>@{{dato.Paciente}}</td>
                            <td>@{{dato.Seguro}}</td>
                            <td>@{{dato.Fecha}}</td>
                            <td>@{{dato.Hora}}</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="modal face" id="buscar_paciente_modal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            Consulta genreal de pacientes
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" >
                            <h4> BUSQUEDA POR NOMBRE </h4>
                            <hr>
                            <div class="form-group">
                                <input type="text" class="form-control"  v-model="apellido" v-on:keyup.13="getUsers">
                            </div>
                            <hr>
                            <div style="overflow: scroll">
                                <table class="table table-bordered" style="width: 100%; height:10px;" >
                                    <thead>
                                    <tr>
                                        <th>Elegir</th>
                                        <th>cedula</th>
                                        <th>primer nombre</th>
                                        <th>segundo nombre</th>
                                        <th>apellido paterno</th>
                                        <th>apellido materno</th>
                                        <th> Tipo de seguro</th>
                                    </tr>
                                    </thead>
                                    <tbody style="height:20px;">
                                    <tr v-for="dato in listaAfiliados">
                                        <td> <button class="btn btn-link" v-on:click="PasarDatos(dato.cedula,'no')"> <i class="fa fa-mouse-pointer fa-4x " aria-hidden="true"></i></button> </td>
                                        <td>@{{dato.cedula}}</td>
                                        <td> @{{dato.primer_nombre}} </td>
                                        <td> @{{dato.segundo_nombre}}</td>,
                                        <td> @{{dato.apellido_paterno}}</td>
                                        <td> @{{dato.apellido_materno}}</td>
                                        <td> @{{dato.tipo_seguro}}</td>
                                    </tr>
                                    </tbody>
                                </table>


                            </div>

                            <div class="modal-footer">

                            </div>
                        </div>

                    </div>

                </div>
            </div>



        </div>
    </div>
@endsection
@section('script')

   <!-- <script src="{/{asset('js/AsistenciaMedica.js')}}"> </script> -->
   <script type="text/javascript">
       var app = new Vue ({
           el:'#asistenciaMedica',
           data : {
               cmbpaciente_id : [],


               apellido : '',
               listaAfiliados : [],
               //

               errores : [],
               id2 : '',
               imprimir_v : false,

               id_admision : 0,
               atencion: 0,
               cedula : '' ,
               nombre : '',
               paciente : '',
               responsable: '',
               afinidad : '',
               direccion : '',
               telefono:'',
               seguro : '',
               firmante:'',
               acompanante : '',
               doctor: '',
               observacion : '',
               parentesco : '',
               ejemplo : '',
               cedula_acompanate : '',
               nombre_acompanante : '',
               derivacion : false,
               nombre_paciente:'',
               nombre_afinidad:'',
               cmbmedico : [],
               cmbparentesco: [],
               cmbSeguro : [],
               cmbpaciente : [],
               pacientesAdmision : [],

               //
               id:'',
               fecha_nacimiento : '',
               celular : '',
               otro : '',
               observacion2: '',
               primer_nombre: '',
               segundo_nombre : '',
               apelldio_paterno : '',
               apellido_materno : '',
               hospital:0,
               guardar_var : true,

           },


           created : function() {
               axios.get('seguro').then(response => {
                   this.cmbSeguro  = response.data
           })

               axios.get('consultarmedico').then(response => {
                   this.cmbmedico  = response.data
           })

               axios.get('consultarparentesco2').then(response => {
                   this.cmbparentesco  = response.data
           })

               var url = 'consultarpacientesadmision';
               axios.get(url).then(response => {
                   this.pacientesAdmision  = response.data;
           })

               toastr.options = {
                   "closeButton": true,
                   "debug": false,
                   "newestOnTop": false,
                   "progressBar": true,
                   "positionClass": "toast-top-right",
                   "preventDuplicates": false,
                   "onclick": null,
                   "showDuration": "600",
                   "hideDuration": "3000",
                   "timeOut": "5000",
                   "extendedTimeOut": "1000",
                   "showEasing": "swing",
                   "hideEasing": "linear",
                   "showMethod": "fadeIn",
                   "hideMethod": "fadeOut"
               };

               $.get("cmbderivacionhospital", function (datos) {
                   $.each(datos, function (key, value) {
                       $("#hospital").append("<option value=" + value.id + ">" + value.descripcion + "</option>");
                   });

               });

           },

           methods : {

               getUsers: function() {
                   var urlUsers = '/emergencia/registropacientes/TitularAfiliadoPorApellido/'+this.apellido+'';
                   axios.get(urlUsers).then(response => {
                       this.listaAfiliados  = response.data
                })
               },

               PasarDatos : function (cedula,titular) {

                   if(titular === 'si') {

                   } else  {
                       this.cedula = cedula;
                       this.ConsultarCedula();
                       $('#buscar_paciente_modal').modal('toggle');

                   }
               },

               consultarPacientes : function() {
                   var url = 'consultarpacientesadmision';
                   axios.get(url).then(response => {
                       this.pacientesAdmision  = response.data;
               })
               },

               ConsultarBeneficiario : function () {
                   var url = 'consultarbeneficiario/'+this.cedula+'';
                   axios.get(url).then(response => {
                       this.cmbpaciente  = response.data;
                    })
               },
               setAfinidad : function() {
                   this.nombre_afinidad = $('select[name="afinidad"] option:selected').text();

               },
               consultarPacienteId : function () {
                   var url = 'consultar/atenciones/'+this.paciente+'';
                   this.nombre_paciente =  $('select[name="paciente"] option:selected').text();
                   axios.get(url).then(response => {
                       this.cmbpaciente_id  = response.data;
                       var numero = this.cmbpaciente_id.length;
                        if(numero != 0){
                            this.errores.push('El paciente ya se encuentra registrado');
                            toastr.error('EL paciente ya se encuentra registrado.', 'Error', {timeOut: 5000});
                        }else {
                            this.errores = [];
                        }
                    })
               },
               ConsultarCedula : function() {
                   var urlUsers = 'registropacientes/TitularAfiliadoPorCedula/'+this.cedula+'';
                   var numero = 0;
                   axios.get(urlUsers).then(response => {
                       this.listaAfiliadosCedula  = response.data;
                       this.numero = this.listaAfiliadosCedula.length;
                           if (this.numero === 0) {
                               toastr.error("No existen datos asociados a ese tipo de identificación.")
                           } else {
                               this.id = this.listaAfiliadosCedula[0].id;

                               this.primer_nombre = this.listaAfiliadosCedula[0].primer_nombre;
                               this.segundo_nombre = this.listaAfiliadosCedula[0].segundo_nombre;
                               this.apellido_paterno = this.listaAfiliadosCedula[0].apellido_paterno;
                               this.apellido_materno = this.listaAfiliadosCedula[0].apellido_materno;

                               if(this.segundo_nombre === null)
                               {
                                   this.nombre = this.primer_nombre + ' '+this.apellido_paterno+' '+this.apellido_materno+'';
                                   this.responsable = this.nombre;
                               }else {
                                   this.nombre = this.primer_nombre + ' '+this.segundo_nombre+' '+this.apellido_paterno+' '+this.apellido_materno+'';
                                   this.responsable = this.nombre;
                               }

                               this.fecha_nacimiento = new Date(this.listaAfiliadosCedula[0].fecha_nacimiento);
                               this.direccion = this.listaAfiliadosCedula[0].direccion;
                               this.telefono = this.listaAfiliadosCedula[0].telefono;
                               this.seguro  = this.listaAfiliadosCedula[0].tipo_seguro;

                               this.celular = this.listaAfiliadosCedula[0].celular;
                               this.otro = this.listaAfiliadosCedula[0].otro;
                               this.observacion2 = this.listaAfiliadosCedula[0].observacion;
                               this.ConsultarBeneficiario(response.data);
                           }


                    })
               },

               validarInsert : function () {
                   this.espaciosBlancos();
                   this.validarCampos();
                   // mensajes de alerta
                   if( this.errores.length === 0) {
                       //document.getElementById('registroTitular').submit()
                       this.guardar();
                       //var formulario = document.getElementById("form1");
                       //formulario.submit();
                   } else {
                       var num = this.errores.length;
                       for(i=0; i<num;i++) {
                           toastr.error(this.errores[i]);
                       }
                   }
                   this.errores = [];

               },

               guardar : function() {
                   var temp1 = 0;
                   var temp2 = 0;

                   var parametros = {
                       "_token": "{{ csrf_token() }}",
                       //--
                       "afiliado" : this.id,
                       "paciente" : this.paciente,
                       "responsable": this.responsable,
                       "observacion" : this.observacion,
                       "doctor": this.doctor,
                       "seguro" : this.seguro,
                       //"fuente_informacion" : "fuente_informacion",
                       "derivacion" : this.derivacion,
                       "hospital" : this.hospital,
                       "dcedul_a" : this.cedula_acompanate,
                       "dnombre_a" : this.cedula_acompanate,
                       "parentesco" : this.parentesco,

                   };
                   $.ajax({
                       data : parametros,
                       url : "asistenciamedica/storepacientes",
                       type : "post",
                       async : false,
                       success : function(d){
                           var a = JSON.parse(d);
                           console.log(a.atencion);
                           $("#atencion").val(a.atencion);
                           $("#id_admision").val(a.id);
                           $("#prueba").val(a.atencion);
                           toastr.success('Registro ingresado con exito.', 'Exito', {timeOut: 5000});
                       },
                       error : function (response,jqXHR) {
                           toastr.error('Error al momento de ingresar el registro.', 'Error', {timeOut: 5000});

                       }
                   });
                   this.consultarPacientes();
                    this.atencion = $("#prueba").val();
                    this.id_admision = $("#id_admision").val();

                   if(this.atencion !==0){
                        //bloquea el boton de guardar
                       this.guardar_var = false;
                       this.imprimir_v = true;
                   }

               },

               limpiar : function () {
                   this.cmbpaciente = [];
                   this.pacientesAdmision = [];
                   this.imprimir_v = false;
                   this.guardar_var =true;
                   this.id = "";
                   this.paciente= "";
                   this.responsable= "";
                   this.observacion= "";
                   this.doctor= "";
                   this.seguro= "";
                       //"fuente_informacion" : "fuente_informacion"= "";
                   this.derivacion= "";
                   this.hospital= "";
                   this.cedula_acompanate= "";
                   this.cedula_acompanate= "";
                   this.parentesco= "";
               },

               validarCampos : function () {
                   var patt2 =  /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/; // para la afinidad
                   var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ,\s0-9]+$/; //para la observación


                   if (this.firmante !== 'si' && this.firmante !== 'no' && this.firmante !== 'in') {
                       this.errores.push("Seleccione si es 'Firmante','No firmante' o acompañante");
                   } else {
                       if (this.firmante === 'no') {
                           if (this.parentesco === '' || this.parentesco === null || this.parentesco === undefined)
                               this.errores.push("Seleccione el tipo de parentesco");

                       } else if (this.firmante === 'in') {
                           if (this.parentesco === '' || this.parentesco === null || this.parentesco === undefined) {
                               this.errores.push("Seleccione el tipo de parentesco");
                           } else {
                               if (this.cedula_acompanate === '' || this.nombre_acompanante === '') {
                                   this.errores.push("Introduzca  los datos del acompañante");
                               }
                               if(isNaN(this.cedula_acompanate)) {
                                   this.errores.push("Ingrese solo numeros en el numero de cedula del acompañante");
                               }
                           }

                       }

                   }
                   if(this.derivacion === true){
                       if(this.hospital === 0) {
                           this.errores.push("Seleccione el hospital del cual deriva el paciente");
                       }
                   }
                   if(this.cedula === '')
                       this.errores.push("Introduzca un numero de cedula");
                   if(this.nombre === '')
                       this.errores.push("Introduzca el nombre del paciente");
                   if(this.paciente === '')
                       this.errores.push("Seleccione el paciente");
                   if(this.responsable === ''){
                       this.errores.push("Introduzca el nombre del responsable");
                   }
                   if(this.direccion === ''){
                       this.errores.push("Introduzca la dirección");
                   }
                   if(this.telefono === ''){
                       this.errores.push("Introduzca un numero de telefono");
                   }
                   if(this.seguro === ''){
                       this.errores.push("Seleccione el tipo de seguro");
                   }
                   if(this.doctor === 0 || this.doctor === ''){
                       this.errores.push("Seleccione al medico");
                   }
                   if(this.afinidad === '')
                   {
                       this.errores.push("introduzca la afinidad");
                   }else{
                       if(patt2.test(this.afinidad) == false)
                       {
                           this.errores.push("El campo afinidad no puede contener numeros ni caracteres especiales");
                       }
                   }

                   if(this.observacion === '')
                   {
                       this.errores.push("introduzca la observación");
                   }else{
                       if(patt2.test(this.observacion ) == false)
                       {
                           this.errores.push("El campo observación no puede contener caracteres especiales");
                       }
                   }
               },

               espaciosBlancos : function () {
                   this.cedula = this.cedula.trim();
                   this.responsable = this.responsable.trim();
                   this.afinidad  = this.afinidad.trim();
                   this.direccion = this.direccion.trim();
                   this.telefono = this.telefono.trim();

               }

           }

       });

   </script>


@endsection
