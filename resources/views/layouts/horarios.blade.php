<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
<div id="app">
    <main>
        <div class="row">
        <div class="col-md-12" id="">
            <div class="tile">
                <h2>Horario Medico</h2>
                <div class="col-md-12">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>MEDICO</th>
                            <th>ESPECIALIZACION</th>
                            <th>LUNES</th>
                            <th>MARTES</th>
                            <th>MIERCOLES</th>
                            <th>JUEVES</th>
                            <th>VIERNES</th>
                            <th>SABADO</th>
                            <th>DOMINGO</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($datos as $dato)
                            <tr>
                                <th>{{$dato->NOMBRE_MEDICO}}</th>
                                <th>{{$dato->ESPECIALIZACION}}</th>
                                <th>{{$dato->LUNES}}</th>
                                <th>{{$dato->MARTES}}</th>
                                <th>{{$dato->Miercoles}}</th>
                                <th>{{$dato->Jueves}}</th>
                                <th>{{$dato->Viernes}}</th>
                                <th>{{$dato->Sabado}}</th>
                                <th>{{$dato->Domingo}}</th>
                            </tr>
                        @empty
                            <p> Error al momento de cargar la información </p>
                        @endforelse


                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        </div>
    </main>

</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">$('#sampleTable').DataTable(
        {
            "scrollX": true,
            "language": {
                search: "Buscar:",
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "Ningun registro encontrado ",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "Sin registros disponibles",
                "infoFiltered": "(filtrando de un total de  _MAX_ registros)",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Ultimo"
                },
            }
        }
    );
</script>
</body>
</html>
