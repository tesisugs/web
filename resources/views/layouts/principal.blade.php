<!DOCTYPE html>
<html lang="en" xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <meta name="description" content="Majoma control hospitalario">
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- icono -->
    <link rel="shorcut icon"    href="{{asset('logo.ico')}}">

     <style>
        .activo {
            color:black;
        }

        [v-cloak] {
            display: none;
        }

        .inactivo {
            color:grey;
        }

        .faltastock {
            background-color: yellow;
            color: blue;
            font-style: italic;
        }
    </style>
        @yield('css')
    <!-- Main CSS-->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/demo.css') }}" rel="stylesheet">
    <!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="app sidebar-mini rtl pace-done sidenav-toggled body">
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i> {{ Auth::user()->name }} <span class="caret"></span></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li>
                    <a class="dropdown-item"  href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                        Cerrar Sesión
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>

        <!--Notification Menu-->
        <!--Notification Menu-->
        <li class="dropdown" id="notificaciones_menu" v-cloak><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i><span class="badge">@{{numero}}</span></a>
            <ul class="app-notification dropdown-menu dropdown-menu-right">
                <!--titulo de las notificaciones-->
                <li class="app-notification__title" v-if="numero>0">Tienes nuevas notificaciones.</li>
                <!--contenido de las notificaciones-->
                <div class="app-notification__content">
                    <li v-for="dato in notificaciones">
                        <a class="app-notification__item" href="javascript:;">
                            <span class="app-notification__icon">
                                <span class="fa-stack fa-lg">
                                    <i v-if="dato.icono == '1'" class="fa fa-circle fa-stack-2x text-primary"></i>
                                    <i v-if="dato.icono == '2'" class="fa fa-circle fa-stack-2x text-warning"></i>
                                    <i v-if="dato.icono == '3'" class="fa fa-circle fa-stack-2x text-danger"></i>
                                    <i class="fa fa-envelope-open-o fa-stack-1x fa-inverse "></i>
                                </span>
                            </span>
                            <div>
                                <p class="app-notification__message">@{{dato.titulo}}</p>
                                <p class="app-notification__meta"></p>
                            </div>
                        </a>
                    </li>
                    <!--
                    <li>
                        <a class="app-notification__item" href="javascript:;">
                            <span class="app-notification__icon">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                    <i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i>
                                </span>
                            </span>
                            <div>
                                <p class="app-notification__message">Problemas con el servidor de correos</p>
                                <p class="app-notification__meta"> hace 5 minutos</p>
                            </div></a></li>
                    <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                            <div>
                                <p class="app-notification__message">Transaccion completa</p>
                                <p class="app-notification__meta">hace 2 minutos</p>
                            </div></a></li>
                    -->
                </div>
                <!--Pie de las notificaciones-->
                <li v-if="numero > 0" class="app-notification__footer"><a href="{{route('notificaciones.all')}}">Ver todas las notificaciones.</a></li>
                <li v-else class="app-notification__footer"><a href="#">No tienes nuevas notificaciones.</a></li>

            </ul>
        </li>

    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        @can('consulta_externa')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-medkit"></i><span class="app-menu__label">Consulta Externa</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                    <li><a class="treeview-item" href="{{route('agendapacientes.index')}}"> <i class="fa fa-address-book-o " aria-hidden="true"></i> Agenda de pacientes</a></li>
                    <li><a class="treeview-item" href="{{route('consultaexterna.horariosmedicodetalle')}}"> <i class="fa fa-calendar-o " aria-hidden="true"></i> Horarios medicos</a></li>
                  <!--  <li><a class="treeview-item" href="#"> <i class="fa fa fa-male " aria-hidden="true"></i> Medicos C/E</a></li> -->
                    <li><a class="treeview-item" href="{{route('consultaexterna.preparacionpacientes')}}"> <i class="fa fa fa-male " aria-hidden="true"></i> Consulta Pacientes Preparación</a></li>
               < <li><a class="treeview-item" href="{{route('consultaexterna.agendamedico')}}"> <i class="fa fa fa-male " aria-hidden="true"></i> Agenda Medico</a></li>
            </ul>
        </li>
        @endcan
        @can('emergencia')
        <li class="treeview"><a class="app-menu__item"  data-toggle="treeview" href="#"><i class="app-menu__icon fa fa-plus-square"></i><span class="app-menu__label">Emergencia</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{route('emergencia.registropacientes')}}"><i class="icon fa fa-users"></i> Registro de pacientes</a></li>
                <li><a class="treeview-item" href="{{route('emergencia.asistencia')}}"> <i class="icon fa fa fa-male"></i> Asistencia medica</a></li>

            </ul>
        </li>
        @endcan
        @can('hospitalizacion')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-h-square"></i><span class="app-menu__label">Hospitalización</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{route('hospitalizacion.ingreso')}}">                                         <i class="icon fa  fa-times"></i> Ingreso</a></li>
                <li><a class="treeview-item" href="{{route('hospitalizacion.egreso')}}"><i class="icon fa  fa-male"></i> Egreso</a></li>
                <li><a class="treeview-item" href="{{route('hospitalizacion.anulacion')}}"><i class="icon fa  fa-times"></i> Anulación</a></li>
                <li><a class="treeview-item" href="{{route('hospitalizacion.reasignacionmedico')}}"><i class="icon fa  fa-male"></i> Reasignación Medico/Seguro</a></li>
                <li><a class="treeview-item" href="{{route('hospitalizacion.traspasopacientes')}}"><i class="icon fa  fa-male"></i> Traspaso Pacientes</a></li>

            </ul>
        </li>
        @endcan
        @can('cirugia')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-heartbeat"></i><span class="app-menu__label">Cirugia</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
            </ul>
        </li>
        @endcan()
        @can('auditoria')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Auditoria</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">

            </ul>
        </li>
        @endcan
        @can('estadistica')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-area-chart"></i><span class="app-menu__label">Estadistica</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
            </ul>
        </li>
        @endcan
        @can('administracion')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-hospital-o"></i><span class="app-menu__label">Administración</span><i class="treeview-indicator  fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
              @can('administracion.logs')  <li><a class="treeview-item" href="{{route('logs')}}"><i class="icon fa  fa-times"></i>Logs</a></li> @endcan
                  @can('reportes.admision')  <li><a class="treeview-item" href="{{route('estadisticas.admision')}}"><i class="icon fa  fa-times"></i>Reportes admisión</a></li> @endcan
                  @can('administracion.notificaciones')<li><a class="treeview-item" href="{{route('administracion.notificaciones')}}"><i class="icon fa  fa-times"></i>Notificaciones</a></li> @endcan
                  @can('administracion.estado')<li><a class="treeview-item" href="{{route('administracion.estado')}}"><i class="icon fa  fa-times"></i>Indicadores</a></li> @endcan
                  @can('administracion.medicoalllamado')<li><a class="treeview-item" href="{{route('administracion.medicosalllamado.index')}}"><i class="icon fa fa-user-md"></i>Medicos al llamado</a></li> @endcan
            </ul>
        </li>
        @endcan
        @can('seguridad')
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-shield"></i><span class="app-menu__label">Seguridad</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{route('permisos.index')}}"><i class="fa fa-key" aria-hidden="true"></i>Permisos</a></li>
                <li><a class="treeview-item" href="{{route('roles.index')}}"><i class="fa fa-users" aria-hidden="true"></i> Roles</a></li>
                <li><a class="treeview-item" href="{{route('user.index')}}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Usuarios</a></li>

            </ul>
        </li>
        @endcan

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Otros</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
             <li><a  class="treeview-item" href="{{route('historico.notificaciones')}}"> <i class="fa fa-bell-o" aria-hidden="true"> </i>  Historial de notificaciones</a></li>
            </ul>
        </li>



    </ul>
</aside>
<main class="app-content">

    <div class="app-title">
        <div>
          <!--  <img src="{/{asset('logo.ico')}}"> -->
            <h1>@yield('titulo')</h1>
            @yield('breadcrumbs')
        </div>
    </div>
    <div class="row">
        @yield('contenido')
    </div>
</main>
<!-- Essential javascripts for application to work-->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/toastr.js')}}"></script>

<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('js/plugins/pace.min.js')}}"></script>
<!-- scanner -->
<script src="{{asset('js/axios.min.js')}}"></script>
<!--
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.5"></script> -->
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.0/vue.js"></script>

<script src="{{asset('js/plugins/pace.min.js')}}"></script>
<script src="{{asset('js/plugins/select2.min.js')}}"></script>


@yield('script')

<script type="text/javascript">

    $( "aside" )
        .mouseover(function() {
            $( "body" ).toggleClass( "sidenav-toggled" );
        })
        .mouseout(function() {
            $( "body" ).toggleClass( "sidenav-toggled" );
        });

</script>

<script type="text/javascript">
    var app = new Vue ({
        el:'#notificaciones_menu',
        data : {
            notificaciones : [],
            numero : 0,
            icono : 0,
        },

        created : function() {
            axios.get('/total/notificaciones').then(response => {
                this.notificaciones  = response.data;
                this.numero = this.notificaciones.length;
            })
            const self = this;
            setInterval(function(){ axios.get('/total/notificaciones').then(response => {
                self.$nextTick(function() {
                self.notificaciones  = response.data;
                self.numero = self.notificaciones.length
                })
            });
            }, 10000);
           // setInterval(function(){ axios.get('/total/notificaciones').then(response => {
            //    this.notificaciones  = response.data;
             //   this.numero = this.notificaciones.length;
           // });
           // }, 10000);
        },
        methods : {

            notificaciones : function(){
                axios.get('/total/notificaciones').then(response => {
                    this.notificaciones  = response.data
                    this.numero = this.notificaciones.length
                })
            },

        }

    });


</script>
</body>
</html>