<td v-for="dato in lista_usuarios">
    <tr> @{{dato.id}}</tr>
    <tr> @{{dato.apellidos}}</tr>
    <tr> @{{dato.nombres}}</tr>
    <tr> @{{dato.rol}}</tr>
</td>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Creacion de usuarios</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control"   placeholder="Apellidos" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control"  placeholder="Nombres" autocomplete="off">
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-2" >Tipo</label>
                    <div class=" col-md-10">
                        <select class="form-control" >
                            <option></option>
                        </select>
                    </div>

                </div>
                <div class="form-group">
                    <input type="password" class="form-control"  placeholder="Contraseña" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control"  placeholder="Confirmacion de Contraseña" autocomplete="off">
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-2" >Rol</label>
                    <div class=" col-md-10">
                        <select class="form-control">
                            <option></option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="myModal2" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="col-lg-12">
            <div class="bs-component">
                <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Creacion de usuarios</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control"   placeholder="Apellidos" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control"  placeholder="Nombres" autocomplete="off">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2" >Tipo</label>
                                    <div class=" col-md-10">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control"  placeholder="Contraseña" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control"  placeholder="Confirmacion de Contraseña" autocomplete="off">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2" >Rol</label>
                                    <div class=" col-md-10">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button">Guardar</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="Ver" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="col-lg-12">
            <div class="bs-component">
                <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Creacion de usuarios</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="apellidos"  placeholder="Apellidos" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="nommbres" placeholder="Nombres" autocomplete="off">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2" >Tipo</label>
                                    <div class=" col-md-10">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="" placeholder="Contraseña" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="" placeholder="Confirmacion de Contraseña" autocomplete="off">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2" >Rol</label>
                                    <div class=" col-md-10">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button">Guardar</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="Actualizar" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="col-lg-12">
            <div class="bs-component">
                <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Creacion de usuarios</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" id=""  placeholder="Apellidos" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="" placeholder="Nombres" autocomplete="off">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2" >Tipo</label>
                                    <div class=" col-md-10">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="" placeholder="Contraseña" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="" placeholder="Confirmacion de Contraseña" autocomplete="off">
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2" >Rol</label>
                                    <div class=" col-md-10">
                                        <select class="form-control">
                                            <option></option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button">Guardar</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


  <!-- <div class="col-md-12">
                <h3></h3>
                <div class="form-group row">
                    <label class="control-label col-md-2" >Rol</label>
                    <div class=" col-md-10">
                        <select class="form-control" id="roles" name="roles">
                            <option></option>
                        </select>
                    </div>
                </div>
                <h3>Permisos</h3>
                <hr>
                <ul>
                    
                    <li v-for="permiso in permisos">
                        <label> <input type="checkbox" name="@{{ permiso.id }}"> @{{permiso.name}}  </label>
                    </li>
                    
                    <form method="post" action="{{route('permission.store')}}">
                        {{ csrf_field() }} 
                        <input type="checkbox" name="c1" value="1"> 1
                        <input type="checkbox" name="c2"> 2
                        <input type="checkbox" name="c3"> 3
                        <button type="submit">enviar</button>
                    </form>

                </ul>
            </div> -->