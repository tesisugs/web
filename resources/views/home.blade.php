@extends('layouts.principal')
@section('titulo','Inicio')
@section('title','Inicio')
@section('contenido')

<div class="row" id="estadistica" v-cloak>
    @foreach ($resultado as $accesos)
        <div class="col-md-3 col-lg-3">
            <div class="widget-small {{$accesos->color}} coloured-icon"><i class="{{$accesos->icono}}"></i>
                <div class="info">
                    <p><a href="{{route($accesos->url)}}">{{$accesos->nombre}}</a> </p>
                    <p><b></b></p>
                </div>
            </div>
        </div>
    @endforeach
    @if($rol === 1)
        <div class="col-md-8" id="" >
            <div class="tile">
                <h3 class="tile-title">Estadisticas de Admisión<span></span></h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4" >
            <ul class="list-group">
                <li class="list-group-item list-group-item-success">Camas</li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Disponibles
                    <span class="badge badge-primary badge-pill">@{{habilitadas}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Ocupadas
                    <span class="badge badge-primary badge-pill">@{{ocupada}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Desinfección
                    <span class="badge badge-primary badge-pill">@{{ desinfectada }}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Averiadas
                    <span class="badge badge-primary badge-pill">@{{ averiada }}</span>
                </li>
            </ul>
        </div>
    @endif

    @if($rol == 2)
        <div class="col-md-8" id="" >
            <div class="tile">
                <h3 class="tile-title">Estado camas<span></span></h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="lineChartDemo2"></canvas>
                </div>
            </div>
        </div>
            <div class="col-md-4" >
                <div class="alert alert-primary">
                    <div class="form-group animated-radio-button">
                        <label><input type="radio" name="camassala" v-model="tipo_lista" value="cama"><span class="label-text">Camas</span> </label>
                        <label><input type="radio" name="camassala" v-model="tipo_lista" value="cuna"><span class="label-text">Cunas</span></label>
                    </div>
                </div>
                <table class="table table-bordered table-sm" v-show="tipo_lista === 'cama'">
                    <thead>
                    <tr class="table-success">
                        <td>sala</td>
                        <td>disponibles</td>
                        <td>ocupadas</td>
                        <td>Desinfección</td>
                        <td>dañadas</td>
                    </tr>
                    </thead>
                    <tbody>

                    <tr v-for="dato in cmbCamas" class="table-light">
                        <td > @{{dato.nombre_sala}} </td>
                        <td > @{{dato.disponibles}} </td>
                        <td > @{{dato.ocupadas}} </td>
                        <td >@{{dato.desinfección}} </td>
                        <td >@{{dato.averiada}} </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-sm" v-show="tipo_lista === 'cuna'">
                    <thead>
                    <tr class="table-success">
                        <td>sala</td>
                        <td>disponibles</td>
                        <td>ocupadas</td>
                        <td>Desinfección</td>
                        <td>dañadas</td>
                    </tr>
                    </thead>
                    <tbody>

                    <tr v-for="dato in cmbCunas" class="table-light">
                        <td > @{{dato.nombre_sala}} </td>
                        <td > @{{dato.disponibles}} </td>
                        <td > @{{dato.ocupadas}} </td>
                        <td >@{{dato.desinfección}} </td>
                        <td >@{{dato.averiada}} </td>
                    </tr>
                    </tbody>
                </table>
            </div>

    @endif
</div>

@endsection

@section('script')
    <script type="text/javascript"  src ="{{asset('js/plugins/chart.js')}}"></script>



    <script>
        var app = new Vue ({
            el:'#estadistica',
            data : {

                cmbEstadistica : [],
                pacientes : '',
                emergencias : '',
                hospitalizacion : '',
                egresos : '',
                CExterna : '',
                habilitadas : '' ,
                ocupada : '',
                desinfectada : '',
                averiada : '',
                tipo_lista : 'cama',
                cmbCamas : [] ,
                cmbCunas : [] ,

            },
            created : function() {
                axios.get('/indicadores/administracion').then(response => {
                    this.cmbEstadistica= response.data
                this.pacientes = this.cmbEstadistica[0].pacientes;
                this.emergencias = this.cmbEstadistica[0].emergencias;
                this.hospitalizacion = this.cmbEstadistica[0].hospitalizacion;
                this.egresos = this.cmbEstadistica[0].egresos;
                this.CExterna = this.cmbEstadistica[0].CExterna;

                var data = {
                    labels: ["Registro Pacientes", "Ingresos emergencia", "Hospitalización", "Egresos", "Registros Consulta Externa"],
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "#83cdff",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [this.pacientes, this.emergencias, this.hospitalizacion, this.egresos, this.CExterna]
                        }
                    ]
                };
                var pdata = [
                    {
                        value: 300,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Complete"
                    }
                ]

                var ctxl = $("#lineChartDemo").get(0).getContext("2d");
                var lineChart = new Chart(ctxl).Bar(data);

            })

                axios.get('/indicadores/admision').then(response => {
                    this.cmbEstadistica= response.data
                this.habilitadas = this.cmbEstadistica[0].habilitadas;
                this.ocupada = this.cmbEstadistica[0].ocupada;
                this.desinfectada = this.cmbEstadistica[0].desinfecta;
                this.averiada = this.cmbEstadistica[0].averiada;

                var data = {
                    labels: ["HABILITADAS", "OCUPADAS", "DESINFECTA", "AVERIADA"],
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "#83cdff",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [this.habilitadas, this.ocupada, this.desinfectada, this.averiada]
                        }
                    ]
                };
                var pdata = [
                    {
                        value: 300,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Complete"
                    }
                ]

                var ctxl = $("#lineChartDemo2").get(0).getContext("2d");
                var lineChart = new Chart(ctxl).Bar(data);

            })

                axios.get('/lista/camas').then(response => {
                    this.cmbCamas  = response.data
                });

                axios.get('/lista/cunas').then(response => {
                    this.cmbCunas  = response.data
                });

            },

            methods : {

            }

        });


    </script>
@endsection
