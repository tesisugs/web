@extends('layouts.principal')
@section('titulo','Usuarios')
@section('title','Usuarios')
@section('css')
@endsection
@section('contenido')
    <div class="col-md-12" id="usuarios" v-cloak >
        <div class="tile">
            <div class="col-md-12">
                <button class="btn btn-primary btn-lg pull-right col-md-2" data-toggle="modal" data-target="#crearUsuario"> Crear </button>
            </div>
            <div class="form-group-row colmd-8">

            </div>
            <div class="form-group row">
                <label class="form-control-label col-md-1 " for="name">Burcar:</label>
                <select class="col-md-4 form-control" v-model="rol_consulta" id="b_roles">
                    <option value="0"></option>
                </select>
                <input  type="text"   class="col-md-3 form-control" placeholder="nombre" v-model="consulta" v-on:keyup.13="consultarNombreRol">

            </div>
            <br>
            <div class="col-sm-12 col-sm-offset-2" style="background-color:white;">
                <table class="table table-hover table-bordered"  >
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Usuario</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr v-for="dato in usuarios">
                            <td>@{{dato.id}}</td>
                            <td>@{{dato.nombres}}</td>
                            <td>@{{dato.apellidos}}</td>
                            <td>@{{dato.name}}</td>
                            <td>@{{dato.rol}}</td>
                            <td>
                                    <button type="button" class="btn btn-info" v-on:click="ver(dato.id)">Ver</button>
                                    <button type="button" class="btn btn-success" v-on:click="editar(dato.id)">Actualizar</button>
                                    <button type="button" class="btn btn-danger" v-on:click="suprimir(dato.id)">Eliminar</button>

                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Usuario</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
        <!-- modales -->
        <div id="crearUsuario" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                          <form id="frmGuardar" name="frmGuardar" method="POST" action="{{ route('register') }}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Creacion de usuarios</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="apellidos" id="apellidos" name="apellidos"   placeholder="Apellidos" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="nombres" id="nombres" name="nombres"  placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="usuario" id="usuario"  name="usuario" placeholder="nombre_usuario" autocomplete="off">
                                        </div>

                                        <div class="form-group row">
                                            <label class="control-label col-md-3" >Departamento</label>
                                            <div class=" col-md-9">
                                                <select class="form-control" v-model="departamento" id="departamento">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" v-model="pass" id="pass" name="pass"  placeholder="Contraseña" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" v-model="pass2" id="pass2"  placeholder="Confirmacion de Contraseña" autocomplete="off">
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2" >Rol</label>
                                            <div class=" col-md-10">
                                                <select class="form-control"  id="roles" name="rol" v-model="rol">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">192.168.1.</button>
                                        </span>
                                            <input type="text" class="form-control" v-model="ip" id="ip" name="ip" aria-describedby="basic-addon3">
                                        </div>
                                        <div class="input-group">
                                       <!-- <label class="control-label col-md-7"> <input type="checkbox"> usuario desarrollo</label> -->
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button" v-on:click="validar">Guardar</button>
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="vistaUsuarios" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Usuarios</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <label class="label-text"> Apellidos:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_apellidos" id="v_apellidos"  placeholder="Apellidos" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Nombres:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_nombres" id="v_nommbres" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Usuario:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_usuario" id="v_usuario" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <!--
                                        <label class="label-text col-md-6"> Tipo:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" readonly v-model="v_tipo" id="v_tipo" placeholder="Nombres" autocomplete="off">
                                        </div> -->

                                        <label class="label-text col-md-6"> Rol:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="rol_view" v-model="v_role" name="" :value="v_role"></select>
                                        </div>
                                        <label class="label-text col-md-6"> Departamento:</label>
                                        <div class="form-group">
                                            <select class="form-control" id="depa_view" v-model="v_depa" name="" :value="v_depa"></select>
                                        </div>
                                        <label class="label-text"> ip:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_ip"  placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <!--
                                        <label class="label-text"> Usuario creacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_usuarioc" id="v_usuarioc" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Usuario modificacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_usuariom" id="v_usuariom" placeholder="Nombres" autocomplete="off">
                                        </div> -->
                                        <label class="label-text"> Fecha creacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_fechac" id="v_fechaoc" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Fecha modificacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_fecham" id="v_fecham" placeholder="Nombres" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="editarUsuarios" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Usuarios</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <label class="label-text"> Apellidos:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_apellidos" id="e_apellidos"  placeholder="Apellidos" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Nombres:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_nombres" id="e_nombres" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Usuario:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_usuario" id="e_usuario" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" v-model="e_pass" id="e_pass" name="pass"  placeholder="Contraseña" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" v-model="e_pass2" id="e_pass2"  placeholder="Confirmacion de Contraseña" autocomplete="off">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="label-text col-md-6"> Rol:</label>
                                        </div>
                                        <div class=" col-md-10">
                                            <select class="form-control" id="e_roles" name="rol" v-model="e_roles" :value="e_roles">
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="label-text col-md-6"> Departamento:</label>
                                        </div>
                                        <div class=" col-md-10">
                                            <select class="form-control" id="e_departamentos" name="rol" v-model="e_departamentos" :value="e_departamentos">
                                            </select>
                                        </div>
                                        <br>
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">192.168.1.</button>
                                        </span>
                                            <input type="text" class="form-control" v-model="e_ip" id="e_ip" name="ip_e" aria-describedby="basic-addon3">
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button" v-on:click="actualizar">Guardar</button>
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
@section('script')
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>

    <script>
        var app = new Vue ({
            el:'#usuarios',
            data : {
                rol :'',
                usuarios : [],
                usuarios_s : [],
                usuarios_e : [],
                // modelos
                //visualizar
                v_id  : '',
                v_apellidos : '',
                v_nombres : '',
                v_usuario : '',
                v_tipo : '',
                v_role : '',
                v_usuarioc : '',
                v_usuariom : '',
                v_fechac : '',
                v_fecham :'',
                v_depa : '',
                v_ip : '',
                v_fecha : '',
                //editar

                e_id  : '',
                e_apellidos : '',
                e_nombres : '',
                e_usuario : '',
                e_tipo : '',
                e_rol : '',
                e_usuarioc : '',
                e_usuariom : '',
                e_fechac : '',
                e_fecham :'',
                e_pass : '',
                e_pass2 : '',
                e_roles : '',
                e_ip : '',
                e_departamentos : '',
                e_fecha : '',

                nombres : '',
                apellidos : '',
                usuario : '',
                departamento : '',
                ip : '',
                pass : '',
                pass2 : '',

                errores : [],
                respuesta1 : 1,
                respuesta2 : 1,
                respuesta1_update : 1,
                respuesta2_update : 1,

                rol_consulta : 0,
                consulta : '',


            },

            created : function() {
                    axios.get('usuariostables').then(response => {
                        this.usuarios  = response.data
                    })

                    this.nombres = '';
                    this.apellidos = '';
                    this.usuario = '';
                    this.departamento = '';
                    this.ip = '';
                    this.pass = '';
                    this.pass2 = '';

            },

            methods : {

                consultarNombreRol: function () {
                    this.consulta = this.consulta.trim();
                    if(this.consulta !== '' || this.rol_consulta !== "0" ){
                        this.consulta = this.consulta.trim();
                        axios.get('/usuarios/nombre/'+this.rol_consulta+'/'+this.consulta).then(response => {
                            this.usuarios  = response.data
                    })
                    } else if(this.consulta === '' && this.rol_consulta === 0 ) {
                        this.recargar();
                    } else {
                        this.recargar();
                    }

                },

                validar : function () {
                    var patt1 = /^[0-9]+$/; // nombres apellidos
                    var patt2 =  /^[a-zA-Z_]+$/; // nombres de usuario
                    var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/; // nombres apellidos
                    if(this.nombres === ''){
                        this.errores.push("ingrese los nombres del usuario");
                    }else{
                        this.nombres = this.nombres.trim();
                        if(patt3.test(this.nombres) == false)
                        {
                            this.errores.push("Los nombres no pueden contener numeros ni caracteres especiales");
                        }
                    }

                    if(this.apellidos === ''){
                        this.errores.push("ingreso los apellidos del usuario");
                    }else{
                        this.apellidos= this.apellidos.trim();
                        if(patt3.test(this.apellidos) == false)
                        {
                            this.errores.push("Los apellidos no pueden contener numeros ni caracteres especiales");
                        }
                    }

                    if(this.usuario === ''){
                        this.errores.push("ingrese el nombre del usuario (Debe de ser unico)");
                    } else {
                        this.usuario= this.usuario.trim();
                        if(patt2.test(this.usuario) == false)
                        {
                            this.errores.push("El nombre de usuario no puede contener espaciones, numeros  o caracteres especiales distintots a: '_' ");
                        }else{
                            // consulta a la base de datos
                            axios.get('validar/usuario/'+this.usuario+'').then(response => {
                                this.respuesta1  = response.data;
                            })
                        }

                    }

                    if(this.departamento === ''){
                        this.errores.push("seleccione un departamento");
                    }

                    if(this.pass === ''){
                        this.errores.push("ingrese la contraseña");
                    } else {
                        if(this.pass.length < 8){
                            this.errores.push("La contraseña debe tener un minimo de 8 caracteres");
                        }
                    }
                    if(this.pass2 === ''){
                        this.errores.push("ingrese la confirmación de la contraseña");
                    }

                    if(this.pass !== '' && this.pass2 !== '' )
                    {
                        if(this.pass !== this.pass2){
                            this.errores.push("las contraseñas no coinciden");
                        }
                    }

                    if(this.rol === ''){
                        this.errores.push("seleccione el rol del nuevo usuario");
                    }

                    if(this.ip === ''){
                        this.errores.push("Ingrese la dirección ip, RECUERDE: DEBE DE SER UNICA");
                    } else {

                        if(patt1.test(this.ip) == false)
                        {
                            this.errores.push("LA ip selo acepta un digito");
                        }else{
                            // consulta a la base de datos
                            // consulta a la base de datos
                            axios.get('validar/ip/'+this.ip+'').then(response => {
                                this.respuesta2  = response.data;
                            })
                        }
                    }

                    if(this.respuesta1 === 0) {
                        this.errores.push("El nombre del usuario ya se encuentra registrado");
                    }

                    if(this.respuesta2 === 0) {
                        this.errores.push("La dirección ip se encuentra registrada");
                    }

                    if( this.errores.length === 0) {
                       this.crear();
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },
                // recarga la tabla
                recargar : function () {
                    axios.get('usuariostables').then(response => {
                        this.usuarios  = response.data
                    })
                },

                crear : function () {
                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "nombres" : this.nombres.toUpperCase(),
                        "apellidos" : this.apellidos.toUpperCase(),
                        "name" : this.usuario.toUpperCase(),
                        "estado" : 1,
                        "password" : $("#pass").val(),
                        "rol" : $("select[name=rol]").val(),
                        "ip" : this.ip,
                        "departamento" : this.departamento,
                        //"ip" : ,
                        //"departamento" ,
                    };
                    $.ajax({
                        data : parametros,
                        url : "register",
                        type : "post",
                        async : false,
                        success : function(response){

                            if(response[0] === "Error"){
                                toastr.error('Error: '+response[1]+'', 'Error', {timeOut: 5000});
                            }else{
                                $("#crearUsuario").modal('hide');
                                toastr.success('Usuario creado con exito.', 'Success Alert', {timeOut: 5000});
                            }
                        },
                        error : function (response,jqXHR) {
                            if(response.status === 422)
                            {
                                var errors = $.parseJSON(response.responseText);
                                $.each(errors, function (key, value) {
                                    if($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {
                                            toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                            console.log(key+ " " +value);
                                        });
                                    }else{
                                        toastr.error('Error  al momento de crear el usuario.', 'Error', {timeOut: 5000});
                                        toastr.warning('La contraseña debe ser de 8 digitos.', 'Error', {timeOut: 5000});
                                        toastr.warning('Revise que el nombre de usuario no se repita.', 'Error', {timeOut: 5000});
                                    }
                                });
                            } else {
                                toastr.error('Error: '+response.status+' al momento de crear el usuario.', 'Error', {timeOut: 5000});
                                toastr.warning('La contraseña debe ser de 8 digitos.', 'Error', {timeOut: 5000});
                                toastr.warning('Revise que el nombre de usuario no se repita.', 'Error', {timeOut: 5000});
                            }
                        }
                    })
                    this.limpiar();
                    this.recargar();
                },

                ver :function (id) {
                    this.v_id = id;

                    var show = 'usershow/'+id+'';
                    axios.get(show).then(response => {
                        this.usuarios_s  = response.data;
                    //this.v_id = this.usuarios_s[0].;
                    this.v_nombres = this.usuarios_s[0].nombres;
                    this.v_apellidos = this.usuarios_s[0].apellido;
                    this.v_usuario = this.usuarios_s[0].name;
                    this.v_fechac = this.usuarios_s[0].fecha_creacion;
                    this.v_fecham = this.usuarios_s[0].fecha_actualizacion;
                    this.v_ip = this.usuarios_s[0].ip;
                    this.v_depa = this.usuarios_s[0].departamento;
                    this.v_role = this.usuarios_s[0].rol;
                    this.v_fecha = this.usuarios_s[0].fecha;
                    })

                    $("#vistaUsuarios").modal('show');
                },

                editar :function (id) {
                    this.v_id = id;

                    var show = 'useredit/'+id+'';
                    axios.get(show).then(response => {
                        this.usuarios_e  = response.data;
                    this.e_id = this.usuarios_e[0].id;
                    this.e_nombres = this.usuarios_e[0].nombres;
                    this.e_apellidos = this.usuarios_e[0].apellidos;
                    this.e_usuario = this.usuarios_e[0].usuario;
                    this.e_roles = this.usuarios_e[0].rol;
                    this.e_ip = this.usuarios_e[0].ip;
                    this.e_departamentos = this.usuarios_e[0].departamento;
                    this.e_fecha = this.usuarios_e[0].fecha;

                })

                    $("#editarUsuarios").modal('show');
                },

                actualizar : function () {


                    if(this.e_nombres === ''){
                        this.errores.push("ingrese los nombres del usuario");
                    }

                    if(this.e_apellidos === ''){
                        this.errores.push("ingreso los apellidos del usuario");
                    }

                    if(this.e_usuario === ''){
                        this.errores.push("ingrese el nombre del usuario (Debe de ser unico)");
                    } else {
                        // consulta a la base de datos
                        axios.get('validar/usuario/update/'+this.e_usuario+'/'+this.v_id+'').then(response => {
                            this.respuesta1_update  = response.data;

                        })
                    }

                    if(this.e_departamentos === ''){
                        this.errores.push("seleccione un departamento");
                    }


                    if(this.e_pass !== '' && this.e_pass2 !== '' )
                    {
                        if(this.pass !== this.pass2){
                            this.errores.push("las contraseñas no coinciden");
                        }
                    }

                    if(this.e_roles === ''){
                        this.errores.push("seleccione el rol del nuevo usuario");
                    }

                    if(this.e_ip === ''){
                        this.errores.push("Ingrese la dirección ip, RECUERDE: DEBE DE SER UNICA");
                    } else {
                        // consulta a la base de datos
                        axios.get('validar/ip/update/'+this.e_ip+'/'+this.v_id+'').then(response => {
                            this.respuesta2_update  = response.data;
                        })
                    }

                    if(this.respuesta1_update === 0) {
                        this.errores.push("El nombre del usuario ya se encuentra registrado");
                    }

                    if(this.respuesta2_updateme === 0) {
                        this.errores.push("La dirección ip se encuentra registrada");
                    }

                    if( this.errores.length === 0) {
                        var parametros = {
                            "_token": "{{ csrf_token() }}",
                            "nombres" : $("#e_nombres").val(),
                            "apellidos" : $("#e_apellidos").val(),
                            "name" : $("#e_usuario").val(),
                            "estado" : 1,
                            "password" : $("#e_pass").val(),
                            "departamento" : $("#e_departamentos").val(),
                            "ip" : $("#e_ip").val(),
                            "rol" : $("#e_roles").val(),
                            "fecha" : this.e_fecha,

                        };
                        $.ajax({
                            data : parametros,
                            url : "/usuarios/"+this.v_id+"/update",
                            type : "post",
                            async : false,
                            success : function(response){
                                //response contiene la respuesta al llamado de tu archivo
                                //aqui actualizas los valores de inmediato llamando a sus respectivas id.
                                console.log("exito");
                                $("#editarUsuarios").modal('hide');
                                toastr.success('Usuario actualizado con exito.', 'Exito', {timeOut: 5000});
                            },
                            error : function (response,jqXHR) {
                                toastr.error('Error al momento de acutalizar el usuario.', 'Error', {timeOut: 5000});
                            }
                        })
                        this.recargar();
                        $("#editarUsuarios").modal('hide');
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];



                },

                suprimir : function (id) {

                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "id" : id
                    };
                    $.ajax({
                        data : parametros,
                        url : "usuarios/"+id+"/estado",
                        type : "post",
                        async:false,
                        success : function(response){
                            toastr.success('Usuario eliminado con exito.', 'Exito', {timeOut: 5000});
                            console.log("exito");
                        },
                        error : function (response,jqXHR) {
                            toastr.error('Error al momento de eliminar el usuario.', 'Error', {timeOut: 5000});
                            console.log(response);
                        }
                    })
                    this.recargar();
                },

                limpiar: function () {
                        this.nombres = '';
                        this.apellidos = '';
                        this.usuario = '';
                        this.departamento = '';
                        this.ip = '';
                        this.pass = '';
                        this.pass2 = '';
                }

            }

        });


    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $.get("roles", function (datos) {
                $.each(datos, function (key, value) {
                    $("#roles").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
            $.get("roles", function (datos) {
                $.each(datos, function (key, value) {
                    $("#rol_view").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
            $.get("roles", function (datos) {
                $.each(datos, function (key, value) {
                    $("#e_roles").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
            $.get("roles", function (datos) {
                $.each(datos, function (key, value) {
                    $("#b_roles").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });

            $.get("usuarios/departamentos", function (datos) {
                $.each(datos, function (key, value) {
                    $("#departamento").append("<option value=" + value.id + ">" + value.nombre+ "</option>");
                });
            });

            $.get("usuarios/departamentos", function (datos) {
                $.each(datos, function (key, value) {
                    $("#e_departamentos").append("<option value=" + value.id + ">" + value.nombre+ "</option>");
                });
            });


            $.get("usuarios/departamentos", function (datos) {
                $.each(datos, function (key, value) {
                    $("#depa_view").append("<option value=" + value.id + ">" + value.nombre+ "</option>");
                });
            });
        });
    </script>

@endsection