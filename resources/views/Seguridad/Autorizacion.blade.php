@extends('layouts.principal')
@section('titulo','Asignacion de permisos')
@section('title','Asignacion de permisos')
@section('css')
@endsection
@section('contenido')
    <div class="col-md-6" >
        <div class="tile">
            <div class="col-md-12">
                <h3> Permisos por rol</h3>
                <div class="form-group row">
                    <label class="control-label col-md-2" >Rol</label>
                    <div class=" col-md-10">
                        <select class="form-control" id="roles" name="roles">
                            <option></option>
                        </select>
                    </div>
                </div>

                <h3>Permisos</h3>
                <hr>
            </div>
        </div>
    </div>

    <div class="col-md-6" >
        <div class="tile">
            <div class="col-md-12">
                <h3>Permisos por usuario</h3>
                <div class="form-group row">
                    <label class="control-label col-md-2" > Usuario </label>
                    <div class=" col-md-10">
                        <select class="form-control" id="roles" name="roles">
                            <option></option>
                        </select>
                    </div>

                </div>
                <h3>Permisos</h3>
                <hr>
            </div>
        </div>
    </div>

@endsection
@section('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $.get("roles", function (datos) {
                $.each(datos, function (key, value) {
                    $("#roles").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
        });
    </script>

@endsection