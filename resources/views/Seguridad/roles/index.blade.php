@extends('layouts.principal')
@section('titulo','Roles')
@section('title','Roles')
@section('contenido')
    <div class="col-md-12" id="">
        <div class="tile">
            <a href="{{route('roles.create')}}"> <button class=" form-control col-md-2 btn btn-info">CREAR</button></a>
            <br>
            <br>
            <div class="col-md-12">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>NOMBRE</th>
                        <th>SLUG</th>
                        <th>DESCRIPCION</th>
                        <th>ESPECIAL</th>
                        <th>ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($resultado as $dato)
                        <tr>
                            <td>{{$dato->name}}</td>
                            <td>{{$dato->slug}}</td>
                            <td>{{$dato->description}}</td>
                            <td>
                                    @if($dato->special === "all-access")
                                        Acceso total
                                    @endif

                                    @if($dato->special === "no-access")
                                     Sin acceso
                                    @endif

                                    @if($dato->special != "no-access" and $dato->special != "all-access")
                                       Acceso restringido
                                    @endif
                            </td>
                            <th>
                                <a href="{{route('roles.edit',[$dato->id])}}"> <button class="btn btn-success" >EDITAR</button></a>
                                <a href="{{route('roles.show',[$dato->id])}}"> <button class="btn btn-primary">VER</button></a>
                            </th>
                        </tr>
                    @empty
                        <p> Error al momento de cargar la información </p>
                    @endforelse


                    </tbody>

                </table>
                <ul class="pagination">
                    <li > {{ $resultado->links()}} </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection