@extends('layouts.principal')
@section('titulo','Seguridad/permisos')
@section('title','Permisos')
@section('css')
@endsection
@section('contenido')
    <div class="col-md-12" id="permisos" >
        <div class="tile">
            <div class="col-md-12">
                <button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#crearPermiso"> Crear </button>
               <div>
                   <label class="form-control-label" for="name">Burcar:</label>
                   <input  type="text"   class="col-md-4 form-control" placeholder="nombre" name="name" v-model="consulta" v-on:keyup.13="consultarNombre">
               </div>
            </div>
            <br>
            <hr>
            <div class="col-sm-12 col-sm-offset-2" style="background-color:white;">
                <table class="table table-hover table-bordered"  >
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombres</th>
                        <th>Slug</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr v-for="dato in permisos">
                            <td>@{{dato.id}}</td>
                            <td>@{{dato.name}}</td>
                            <td>@{{dato.slug}}</td>
                            <td>@{{dato.description}}</td>
                            <td>
                                    <button type="button" class="btn btn-info" v-on:click="ver(dato.id)">Ver</button>
                                    <button type="button" class="btn btn-success" v-on:click="editar(dato.id)">Actualizar</button>
                                    <button type="button" class="btn btn-danger" v-on:click="suprimir(dato.id)">Eliminar</button>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nombres</th>
                        <th>Slug</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
        <!-- modales -->
        <div id="crearPermiso" class="modal fade" role="dialog" >
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                          <form id="frmGuardar" name="frmGuardar" method="post" action="">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Creacion de permisos</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                    
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="name" id="nombre" required name="nombre"   placeholder="nombre del permmiso" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" required class="form-control" v-model="slug" id="slug" name="slug"  placeholder="nombre de la ruta" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" required class="form-control" v-model="description" id="descripcion"  name="descripcion" placeholder="descripcion del permiso" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" id="guardar" type="button" v-on:click="crear">Guardar</button>
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="vistaPermisos" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Permisos</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                         <div class="form-group">
                                            <input type="text" class="form-control" readonly id="nombre" name="nombre" v-model="v_nombre"   placeholder="nombre del permmiso" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" readonly v-model="v_slug" id="slug" name="slug"  placeholder="nombre de la ruta" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="descripcion" readonly v-model="v_descripcion"  name="descripcion" placeholder="descripcion del permiso" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Fecha creacion:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" readonly v-model="v_fechac" id="v_fechaoc" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Fecha modificacion:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" readonly v-model="v_fecham" id="v_fecham" placeholder="Nombres" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="editarPermisos" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Permisos</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <label class="label-text"> Nombre:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_nombre" id="e_nombre"  placeholder="Nombre del permiso" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Slug:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_slug" id="e_slug" placeholder="" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Descripción:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_descripcion" id="e_descripcion" placeholder="Descripcion del permiso" autocomplete="off">
                                        </div>
                                        <input type="hidden" id="respuesta_guardado">
                                        <input type="hidden" id="respuesta_actualizacion">
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button" v-on:click="actualizar()">Guardar</button>
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
@section('script')
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('js/plugins/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>

    <script>
        var app = new Vue ({
            el:'#permisos',
            data : {

                errores : [],
                respuesta : 0,
                permisos : [],
                permisos_s : [],
                permisos_e : [],

                name : '',
                slug :'',
                description : '',

                id_edicion : 0,
                // modelos
                //visualizar
                v_id  : '',
                v_nombre : '',
                v_slug : '',
                v_descripcion : '',
                v_usuarioc : '',
                v_usuariom : '',
                v_fechac : '',
                v_fecham :'',
                //editar

                e_id  : '',
                e_nombre : '',
                e_slug : '',
                e_usuario : '',
               e_descripcion : '',
                e_usuarioc : '',
                e_usuariom : '',
                e_fechac : '',
                e_fecham :'',

                consulta : '',

            },
            created : function() {
                axios.get('permisostables').then(response => {
                    this.permisos  = response.data
                })

            },

            methods : {
                // recarga la tabla
                recargar : function () {
                    axios.get('permisostables').then(response => {
                        this.permisos  = response.data
                })
                },

                consultarNombre: function () {
                    this.consulta = this.consulta.trim();
                    if(this.consulta !== ''){
                        this.consulta = this.consulta.trim();
                        axios.get('/permisos/'+this.consulta+'/nombre').then(response => {
                            this.permisos  = response.data
                        })
                    } else {
                        this.recargar();
                    }

                },
                // valida la actualización y creación
                validar : function ($tipo) {
                    var patt2 =  /^[a-zA-Z.]+$/; // para la afinidad
                    var patt3 = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/; //para la observación
                    if($tipo === 'crear') {
                        if(this.name === ''){
                            this.errores.push("Introduzca el nombre del nuevo permiso");
                        } else{
                            this.name = this.name.trim();
                            if(patt3.test(this.name) == false)
                            {
                                this.errores.push("EL nombre no puede contener numeros ni caracetes especiales");
                            }
                        }
                        if(this.slug === ''){
                            this.errores.push("Introduzca el slug del nuevo permiso");
                        }else {
                            this.slug= this.slug.trim();
                            if(patt2.test(this.slug) == false)
                            {
                                this.errores.push("EL slug  no puede contener espacios,numeros ni caracetes especiales distitos a '.'");
                            }
                        }
                        if(this.description === ''){
                            this.errores.push("Introduzca la descripcion del nuevo permiso");
                        }else{
                            this.description= this.description.trim();
                            if(patt3.test(this.description ) == false)
                            {
                                this.errores.push("La descripción no puede contener numeros ni caracetes especiales");
                            }
                        }
                    }else {
                        if(this.e_name === ''){
                            this.errores.push("Introduzca el nombre del permiso");
                        } else{
                            this.e_name= this.e_name.trim();
                            if(patt3.test(this.e_name) == false)
                            {
                                this.errores.push("EL nombre no puede contener numeros ni caracetes especiales");
                            }
                        }
                        if(this.e_slug === ''){
                            this.errores.push("Introduzca el slug del permiso");
                        }else{
                            this.e_slug= this.e_slug.trim();
                            if(patt2.test(this.e_slug) == false)
                            {
                                this.errores.push("EL slug  no puede contener espacios,numeros ni caracetes especiales distitos a '.'");
                            }
                        }
                        if(this.e_description === ''){
                            this.errores.push("Introduzca la descripcion del  permiso");
                        }else{
                            this.e_description= this.e_description.trim();
                            if(patt3.test(this.e_description ) == false)
                            {
                                this.errores.push("La descripción no puede contener numeros ni caracetes especiales");
                            }
                        }
                    }

                },
                // metodo que revisa el numero de errores
                crear : function () {
                    this.validar('crear');
                    if( this.errores.length === 0) {
                        this.insertar();
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    this.errores = [];
                },
                // inserta el enuevo permiso
                insertar : function() {
                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "slug" : $("#slug").val(),
                        "name" : $("#nombre").val(),
                        "description" : $("#descripcion").val()
                    };
                    $.ajax({
                        data : parametros,
                        url : "permisosstore",
                        type : "post",
                        async:false,
                        success : function(response){

                            if(response[0] === "Error"){
                                toastr.error('Error: '+response[1]+'', 'Error', {timeOut: 5000});
                            }else{
                                $("#respuesta_guardado").val('respuesta');
                                $("#crearPermiso").modal('hide');
                                toastr.success('Permiso creado con exito.'+response+'', 'Exito', {timeOut: 5000});

                            }
                            //toastr.warning('Si la tabla no se muestra el nuevo registro de click en el boton recargar', {timeOut: 6000});
                        },
                        error : function (response,jqXHR) {
                            if(response.status === 422)
                            {
                                var errors = $.parseJSON(response.responseText);
                                $.each(errors, function (key, value) {
                                    if($.isPlainObject(value)) {
                                        $.each(value, function (key, value) {
                                            toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                            console.log(key+ " " +value);
                                        });
                                    }else{
                                        toastr.error('Error '+response+' al momento de crear el permiso.', 'Error', {timeOut: 5000});
                                    }
                                });
                            } else {
                                toastr.error('Error: '+response.status+' al momento de crear el permiso.', 'Error', {timeOut: 5000});
                            }
                        }
                    });

                    if($("#respuesta_guardado").val() !== ''){
                        this.recargar();
                        this.name = '';
                        this.slug ='';
                        this.description = '';
                        $("#respuesta_guardado").val();
                    }

                },

                ver :function (id) {
                    this.v_id = id;

                    var show = 'permisosshow/'+id+'';
                    axios.get(show).then(response => {
                        this.permisos_s  = response.data;
                    this.v_id = this.permisos_s[0].id;
                    this.v_slug = this.permisos_s[0].slug;
                    this.v_nombre = this.permisos_s[0].name;
                    this.v_descripcion = this.permisos_s[0].description;
                    this.v_fechac = this.permisos_s[0].created_at;
                    this.v_fecham = this.permisos_s[0].updated_at;
                    })

                    $("#vistaPermisos").modal('show');
                },

                editar :function (id) {
                    this.id_edicion = id;

                    var show = 'permisosedit/'+id+'';
                    axios.get(show).then(response => {
                        this.permisos_e  = response.data;
                    this.e_id = this.permisos_e[0].id;
                    this.e_slug = this.permisos_e[0].slug;
                    this.e_nombre = this.permisos_e[0].name;
                    this.e_descripcion = this.permisos_e[0].description;
                    this.e_fechac = this.permisos_e[0].created_at;
                    this.e_fecham = this.permisos_e[0].updated_at;
                });

                    $("#editarPermisos").modal('show');
                },

                actualizar : function () {
                    this.validar("actualizar");

                    if( this.errores.length === 0) {
                        var parametros = {
                            "_token": "{{ csrf_token() }}",
                            "slug" : $("#e_slug").val(),
                            "name" : $("#e_nombre").val(),
                            "description" : $("#e_descripcion").val()
                        };
                        $.ajax({
                            data : parametros,
                            url : "/permisos/"+this.id_edicion+"/update",
                            type : "post",
                            async : false,
                            success : function(response){
                                //response contiene la respuesta al llamado de tu archivo
                                //aqui actualizas los valores de inmediato llamando a sus respectivas id.
                                if(response[0] === "Error"){
                                    toastr.error('Error:'+response[1]+'', 'Error', {timeOut: 5000});
                                }else{
                                    $("#respuesta_actualizacion").val('respuesta');
                                    $("#editarPermisos").modal('hide');
                                    toastr.success('Permiso Actualizado con exito.', 'Exito', {timeOut: 5000});
                                }
                            },
                            error : function (response,jqXHR) {
                                if(response.status === 422)
                                {
                                    var errors = $.parseJSON(response.responseText);
                                    $.each(errors, function (key, value) {
                                        if($.isPlainObject(value)) {
                                            $.each(value, function (key, value) {
                                                toastr.error('Error en el controlador: '+value+'', 'Error', {timeOut: 5000});
                                                console.log(key+ " " +value);
                                            });
                                        }else{
                                            toastr.error('Error '+response+' al momento de actualizar el permiso.', 'Error', {timeOut: 5000});
                                        }
                                    });
                                } else {
                                    toastr.error('Error: '+response.status+' al momento de actualizar el permiso.', 'Error', {timeOut: 5000});
                                }

                            }
                        })
                        this.recargar();
                    } else {
                        var num = this.errores.length;
                        for(i=0; i<num;i++) {
                            toastr.error(this.errores[i]);
                        }
                    }
                    if($("#respuesta_actualizacion").val() !== ''){
                        this.recargar();
                        this.e_name = '';
                        this.e_slug ='';
                        this.e_description = '';
                        $("#respuesta_actualizacion").val()
                    }
                    this.errores = [];
                },

                deleted :function(id) {
                    var estado = 0;


                        var ciclo = 0;
                     while(ciclo === 0)
                     {
                         swal({
                             title: "Are you sure?",
                             text: "You will not be able to recover this imaginary file!",
                             type: "warning",
                             showCancelButton: true,
                             confirmButtonText: "Yes, delete it!",
                             cancelButtonText: "No, cancel plx!",
                             closeOnConfirm: false,
                             closeOnCancel: false
                         }, function(isConfirm) {

                             if (isConfirm) {
                                 /*
                                         var parametros = {
                                             "_token": " {/{ csrf_token() }}",
                                             "id" : id
                                         };
                                         $.ajax({
                                             data : parametros,
                                             url : "permisos/"+id+"/delete",
                                             type : "post",
                                             async:false,
                                             success : function(response){
                                                 console.log("exito");
                                                 estado = 1;
                                             },
                                             error : function (response,jqXHR) {
                                                 console.log(response);
                                                 toastr.error('Error al momento de crear el permiso.', 'Alerta', {timeOut: 8000});
                                             }
                                         }); */
                                 swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                 estado = 1;


                             } else {
                                 swal("Cancelled", "Your imaginary file is safe :)", "error");
                             }
                         });
                        if (estado === 1) {
                            console.log(estado);
                            ciclo = 1;
                           break;
                        }
                    }
                        /*
                            var parametros = {
                                "_token": " //{ csrf_token() }}",
                                "id" : id
                            };
                            $.ajax({
                                data : parametros,
                                url : "permisos/"+id+"/delete",
                                type : "post",
                                async:false,
                                success : function(response){
                                    console.log("exito");
                                },
                                error : function (response,jqXHR) {
                                    console.log(response);
                                }
                            });
                               */



                }, /// borrar o corregir

                suprimir : function (id) {

                    var parametros = {
                        "_token": " {{ csrf_token() }}",
                        "id" : id
                    };
                    $.ajax({
                        data : parametros,
                        url : "permisos/"+id+"/delete",
                        type : "post",
                        async:false,
                        success : function(response){
                            toastr.success('Registro eliminado con exito.', 'Alerta', {timeOut: 8000});
                        },
                        error : function (response,jqXHR) {
                            console.log(response);
                            toastr.error('Error al momento de crear el permiso.', 'Alerta', {timeOut: 8000});
                        }
                    });
                    this.recargar();

                }

            }

        });


    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.get("permisostables", function (datos) {
                $.each(datos, function (key, value) {
                    $("#lista_permisos").append("<option value=" + value.id + ">" + value.name + "</option>");
                    $("#lista_permisos").append("<option value=" + value.id + ">" + value.name + "</option>");
                });
            });
        });
    </script>
@endsection