@extends('layouts.principal')
@section('titulo','Roles')
@section('title','Roles')
@section('contenido')
    <div class="col-md-12" id="roles">
        <div class="tile">
            <div class="col-md-12">
                <button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#crearRoles"> Crear </button>
                <button class=" btn btn-warning" v-on:click="recargar"> <i class="fa fa-refresh" aria-hidden="true"></i> Recargar </button>

            </div>

            <hr>
            <div class="col-sm-12 col-sm-offset-2" style="background-color:white;">
                <table class="table table-hover table-bordered"  >
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombres</th>
                        <th>Slug</th>
                        <th>Descripcion</th>
                        <th>Especial</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="dato in roles">
                        <td>@{{dato.id}}</td>
                        <td>@{{dato.name}}</td>
                        <td>@{{dato.slug}}</td>
                        <td>@{{dato.description}}</td>
                        <td>@{{dato.special}}</td>
                        <td>
                            <button type="button" class="btn btn-info" v-on:click="ver(dato.id)">Ver</button>
                            <button type="button" class="btn btn-success" v-on:click="editar(dato.id)">
                               <a href= {{route('roles.edit')}} > </a> Actualizar
                            </button>
                            <button type="button" class="btn btn-danger" v-on:click="suprimir(dato.id)">Eliminar</button>

                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nombres</th>
                        <th>Slug</th>
                        <th>Descripción</th>
                        <th>Especial</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>
                </table>
            </div>

        </div>



        <!-- modales -->
        <div id="crearRoles" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <form id="frmGuardar" name="frmGuardar" method="post" action="{{route('roles.store')}}">
                                {{ csrf_token() }}
                                {{ csrf_field() }}
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Creacion de roles</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <label class="label-text"> Nombre:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" v-model="v_nombre" id="v_nombre"  placeholder="Nombre del rol" autocomplete="off">
                                            </div>
                                            <label class="label-text"> Slug:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="slug" v-model="v_slug" id="v_slug" placeholder="Nombre del rol" autocomplete="off">
                                            </div>
                                            <label class="label-text"> Description:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="description" v-model="v_descripcion" id="v_descripcion" placeholder="Descripción del rol" autocomplete="off">
                                            </div>

                                            <h3>Permisos especiales</h3>
                                            <label><input type="radio" name="special" value="all-access"> Acceso total</label>
                                            <label><input type="radio" name="special" value="no-access"> Acceso restringido</label>
                                            <ul>
                                                <li id="lista_permisos">
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" type="submit" >Guardar</button> <!-- v-on:click="crear" -->
                                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="vistaRoles" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Permisos</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" id="nombre" name="nombre" v-model="v_nombre"   placeholder="nombre del permmiso" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_slug" id="slug" name="slug"  placeholder="nombre de la ruta" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" id="descripcion" v-model="v_descripcion"  name="descripcion" placeholder="descripcion del permiso" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" id="special" v-model="v_special    "  name="descripcion" placeholder="descripcion del permiso" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Usuario creacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_usuarioc" id="v_descripcionc" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Usuario modificacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_usuariom" id="v_descripcionm" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Fecha creacion:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" readonly v-model="v_fechac" id="v_fechaoc" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Fecha modificacion:</label>
                                        <div class="form-group">
                                            <input type="text" readonly class="form-control" v-model="v_fecham" id="v_fecham" placeholder="Nombres" autocomplete="off">
                                        </div>
                                        <h2>Permisos</h2>
                                        <hr>
                                        <ul>
                                            <li v-for="dato in permisos_rol">
                                                <label>@{{dato.name}}</label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="editarRoles" class="modal fade" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Permisos</h5>
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <label class="label-text"> Nombre:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_nombre" id="e_nombre"  placeholder="Nombre del permiso" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Slug:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_slug" id="e_slug" placeholder="" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Descripción:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" v-model="e_descripcion" id="e_descripcion" placeholder="Descripcion del permiso" autocomplete="off">
                                        </div>
                                        <label class="label-text"> Permiso especial:</label>
                                        <div class="form-group">
                                            <select class="form-control" name="e_special" v-model="e_special" :value="e_special" id="e_special" >
                                                <option value="all-access" > acceso total</option>
                                                <option value="no-access"> acceso restringido</option>
                                            </select>
                                        </div>
                                        <hr>
                                        <label class="label-warning" > Permisos Actuales</label>
                                        <hr>
                                        <label class="label-success" > Permisos Restantes</label>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button" v-on:click="actualizar()">Actualizar</button>
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')

    <script>
        var app = new Vue ({
            el:'#roles',
            data : {
                permisos : [],
                permisos_rol : [],
                roles : [],
                roles_s : [],
                roles_e : [],

                id_edicion : 0,
                // modelos
                //visualizar
                v_id  : '',

                v_nombre : '',
                v_slug : '',
                v_descripcion : '',
                v_usuarioc : '',
                v_usuariom : '',
                v_fechac : '',
                v_fecham :'',
                v_special : '',
                //editar

                e_id  : '',
                e_nombre : '',
                e_slug : '',
                e_usuario : '',
                e_descripcion : '',
                e_usuarioc : '',
                e_usuariom : '',
                e_fechac : '',
                e_fecham :'',
                e_special : ''

            },
            created : function() {
                axios.get('rolestables').then(response => {
                    this.roles  = response.data
                })

                axios.get('permisostables').then(response => {
                        this.permisos  = response.data
                })

            },

            methods : {

                // recarga la tabla
                recargar : function () {
                    axios.get('rolestables').then(response => {
                        this.roles  = response.data
                    })

                    axios.get('permisostables').then(response => {
                        this.permisos  = response.data
                    })
                },

                table : function () {
                    axios.get('rolestables').then(response => {
                        this.roles  = response.data
                    })
                }, // eliminar si no se utiliza

                crear : function () {


                    var parametros = {
                        "_token": "{{ csrf_token() }}",
                        "slug" : $("#slug").val(),
                        "name" : $("#nombre").val(),
                        "description" : $("#descripcion").val(),
                        "permisos" : $("#descripcion").val()
                    };
                    $.ajax({
                        data : parametros,
                        url : "rolesstore",
                        type : "post",
                        success : function(response){
                            //response contiene la respuesta al llamado de tu archivo
                            //aqui actualizas los valores de inmediato llamando a sus respectivas id.

                            $("#crearRoles").modal('hide');
                            $('input[type="text"]').val('');
                            toastr.success('Rol creado con exito.', 'Success Alert', {timeOut: 5000});
                        },
                        error : function (response,jqXHR) {
                            toastr.error('Error al momento de crear el rol.', 'Success Alert', {timeOut: 5000});
                            // var errors = $.parseJSON(response.responseText);
                            // $.each(errors, function (key, value) {
                            //     console.log(value);
                            // });

                        }
                    });
                    this.table();
                }, // sin uso

                ver :function (id) {
                    this.v_id = id;

                    var show = 'rolesshow/'+id+'';
                    axios.get(show).then(response => {
                        this.roles_s  = response.data;
                    this.v_id = this.roles_s[0].id;
                    this.v_slug = this.roles_s[0].slug;
                    this.v_nombre = this.roles_s[0].name;
                    this.v_descripcion = this.roles_s[0].description;
                    this.v_fechac = this.roles_s[0].created_at;
                    this.v_fecham = this.roles_s[0].updated_at;
                    this.v_special = this.roles_s[0].special;
                    })
                    var permiso = 'rolespermisos/'+id+'';
                    axios.get(permiso).then(response => {
                        this.permisos_rol= response.data;

                        })

                    $("#vistaRoles").modal('show');

                },

                editar :function (id) {
                    this.id_edicion = id;

                    var show = 'rolesedit/'+id+'';
                    axios.get(show).then(response => {
                        this.roles_e  = response.data;
                    this.e_id = this.roles_e[0].id;
                    this.e_slug = this.roles_e[0].slug;
                    this.e_nombre = this.roles_e[0].name;
                    this.e_descripcion = this.roles_e[0].description;
                    this.e_fechac = this.roles_e[0].created_at;
                    this.e_fecham = this.roles_e[0].updated_at;
                    this.e_special = this.roles_e[0].special;
                });

                    $("#editarRoles").modal('show');
                },

                actualizar : function (id) {

                    var parametros = {
                            "_token": "{{ csrf_token() }}",
                            "slug" : $("#e_slug").val(),
                            "name" : $("#e_nombre").val(),
                            "description" : $("#e_descripcion").val(),
                            "special" : $("select[name=e_special]").val()
                        };
                    $.ajax({
                        data : parametros,
                        url : "/roles/"+this.id_edicion+"/update",
                        type : "post",
                        async : false,
                        success : function(response){
                            //response contiene la respuesta al llamado de tu archivo
                            //aqui actualizas los valores de inmediato llamando a sus respectivas id.
                            console.log("exito");
                            $("#editarRoles").modal('hide');
                            $('input[type="text"]').val('');
                            toastr.success('Rol actualizado con exito.', 'Exito', {timeOut: 5000});
                        },
                        error : function (response,jqXHR) {
                            toastr.error('Error al momento de actualizar el rol.', 'Error', {timeOut: 5000});
                            // var errors = $.parseJSON(response.responseText);
                            // $.each(errors, function (key, value) {
                            //     console.log(value);
                            // });

                        }
                    })
                    this.recargar();

                },

                suprimir : function (id) {

                    toastr.warning('Antes de eliminar debe actualizar a los usuarios con este rol.', 'Advertencia', {timeOut: 5000});
                    // mostrar una lista de usuarios con este rol.
                    // desea actualizar a un rol Generico

                }

            }

        });


    </script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    <script>
        $('#flash-overlay-modal').modal().delay(3000).fadeOut(350);
    </script>

   
     <script type="text/javascript">
        $(document).ready(function() {
            $.get("permisostables", function (datos) {
                $.each(datos, function (key, value) {
                    $("#lista_permisos").append("<label><input type='checkbox' name='permisos[]' value= " + value.id + ">"+value.description +"</label>");
                });
            });
        });
    </script>
@endsection