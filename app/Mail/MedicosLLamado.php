<?php

namespace App\Mail;
use App\Http\Controllers\Administracion\MediciosAlLLamadoController;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MedicosLLamado extends Mailable
{
    use Queueable, SerializesModels;

    public $medico;
    public $dia;
    public $fecha;
    public $paciente;
    public $seguro;
    public $id;
    public $token;


    public function __construct($medico,$dia,$fecha,$paciente,$seguro,$id,$token)
    {

        $this->medico = $medico;
        $this->fecha = $fecha;
        $this->paciente = $paciente;
        $this->seguro = $seguro;
        $this->dia = $dia;
        $this->id = $id;
        $this->token = $token;

    }

    public function build()
    {
        return  $this->from('example@example.com')
                    ->subject('HOSPITALIZACIÓN: MEDICO AL LLAMADO')
                    ->view('mail.medicosllamado')
                        ->with([
                            'medico' => $this->medico,
                            'fecha' => $this->fecha,
                            'paciente' => $this->paciente,
                            'seguro' => $this->seguro,
                            'id' => $this->id,
                            'token' => $this->token
                            ]);
    }
}
