<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //dd($data);
        return Validator::make($data, [
            'nombres' => 'required|max:30',
            'apellidos' => 'required',
            'name'=> 'required|unique:users|max:10',
            'password' => 'required|min:4|max:16',
            'rol' => 'required',
        ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $users = \Auth::user();
        $usuario_modificacion = $users->id;

        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H') - 5;
        $minutos = date('i');
        $segundos = date('s');

        $fecha_modficacion = $year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';



        try{
            $usuario = User::create([
                'name' => $data['name'],
                'nombres' => $data['nombres'],
                'apellidos' => $data['apellidos'],
                'password' => bcrypt($data['password']),
                'estado' => 1,
            ]);
            $id = $usuario->id;
            $rol = $data['rol'];
            DB::table('role_user')->insert([
                'role_id' => $rol,'user_id' => $id
            ]);

            DB::table('tbdireccionesip')->insert([
                'ip' => "192.168.1.".$data['ip'],
                'departamento_id' => $data['departamento'],
                'usuario_id' => $id,
                'fecha_ingreso' => $fecha_modficacion,
                'fecha_modificacion' => $fecha_modficacion,
                'usuario_ingreso' => $usuario_modificacion,
                'usuario_modificacion' => $usuario_modificacion,
                'estado' => 1
            ]);

            return $usuario;
        }catch (QueryException $e){
            $array = array("Error" , $e->errorInfo[1]);
            return $array;
        }


    }
}
