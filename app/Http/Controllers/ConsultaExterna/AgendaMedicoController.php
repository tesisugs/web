<?php

namespace App\Http\Controllers\ConsultaExterna;

use App\Core\Procedures\ConsultaExternaProcedure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgendaMedicoController extends Controller
{
    protected $ConsultaExternaProcedure;
    public function __construct(ConsultaExternaProcedure $consultaExternaProcedure)
    {
        $this->ConsultaExternaProcedure = $consultaExternaProcedure;
    }

    //
    public function index() {
        //$medico = 298;
        //$fecha = "2018/07/21";
        //$datos = $this->ConsultaExternaProcedure->ConsultarAgendaMedicoTurnos2(298);
        return view('ConsultaExterna.AgendaMedico');

    }

    public function consultarAgendaMedicoTurnos2(){
        // pensar como llamar al medico
        $datos = $this->ConsultaExternaProcedure->ConsultarAgendaMedicoTurnos2(41);
        return $datos;
    }


}
