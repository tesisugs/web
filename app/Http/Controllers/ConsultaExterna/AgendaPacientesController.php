<?php

namespace App\Http\Controllers\ConsultaExterna;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\ConsultaExternaProcedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
class AgendaPacientesController extends Controller
{

    protected $ConsultaExternaProcedure;


    public function __construct(ConsultaExternaProcedure $consultaExternaProcedures)
    {
        $this->ConsultaExternaProcedure = $consultaExternaProcedures;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('ConsultaExterna.AgendaPacientes');
    }

    public function horario()
    {
        //
        return view('ConsultaExterna.HorarioMedico');
    }

    public function consultarMedico () {
        $datos = $this->ConsultaExternaProcedure->consultarMedico();
        return $datos;
    }

      public function consultarHorarioMedicoDetalle () {
        $datos = $this->ConsultaExternaProcedure->consultarHorarioMedico();
        //dd($datos);
        return view('ConsultaExterna.HorarioMedico',compact('datos'));
       
      }

    public function consultarHorarioMedico ($medico) {
        $datos = $this->ConsultaExternaProcedure->consultarHorarioMedico2($medico);
        //dd($datos);
        return $datos;
    }

    public function consultarTurnosAgenda($medico,$fecha) {
        $datos = $this->ConsultaExternaProcedure->ConsultarTurnosAgenda($medico,$fecha);
        //dd($datos);
        return $datos;
    }

    public function consultarHorarioMedicoDetalleiframe () {
        $datos = $this->ConsultaExternaProcedure ->consultarHorarioMedico();
        //dd($datos);
        return view('layouts.horarios',compact('datos'));

    }

    public function consultarEspecializacion () {
        $datos = $this->ConsultaExternaProcedure->consultarEspecializacion();
        return $datos;
    }


    public function consultarDependencias () {
        $datos = $this->ConsultaExternaProcedure->consultarDependencias();
        return $datos;
    }

    public function consultarTipoConsulta () {
        $datos = $this->ConsultaExternaProcedure->consultarTipoConsulta();
        return $datos;
    }

    public function InsertConsultaExternaTurnosDiarios2 () {
        $datos = $this->ConsultaExternaProcedure->insertConsultaExternaTurnosDiarios2();
        return $datos;
    }
    public function consultarEspecialidadMedico ($especialidad) {
        $datos = $this->ConsultaExternaProcedure->consultarMedicoEspecialidad2($especialidad);
        return $datos;
    }

    public function consultarPacienteAgendado($medico,$fecha,$paciente){
        $datos = \DB::table('tbconsultaexternaturnosdiarios')
            ->select('turnos_diarios')
            ->where([
                ['paciente',$paciente],
                ['medico',$medico],
                ['fecha_registro_paciente',$fecha]
                ])

            ->get();

         $var = $datos->isNotEmpty();
         //echo $var;
        //dd($datos);
        if($var === true){
            return 1;
        }else {
            return 0;
        }
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //echo $request->input('hora_inicio');
        //echo $request->input('hora_fin');
        //dd($request);

        $turnos_diarios = 0; // variable de salida
        $consulta_externa  =0; // la proceso adentro
        $paciente = $request->input('paciente_id'); // se repite abajo
        $cedula_paciente = $request->input('cedula_paciente'); // se repite abajo
        $turnos =0; //  variable de salida
        $medico = $request->input('medico'); //se repite abajo
        $observacion = "."; // se repite abajo
        $fecha_registro_paciente =$request->input('fecha'); //se repite abajo
         // se repiten abajo
        $hora_registro_paciente = $request->input('hora_inicio');  // se repiten abajo // lo tomo de la base
        $des_campo1 = $request->input('hora_fin');
        $des_campo2 =".";
        $des_campo3 = "0";
        $user = Auth::user();
        $usuario_ingreso = $user->id ;
        $usuario_modificacion= $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];
        $status = 1;
        $titular_representante = $request->input('titular');

        $id = 0; // variables de salida
        $codigo  = 0; //variables de salida

        //$paciente = 0;
        //$cedula_paciente = 0;
        $fecha_nacimiento = '1995/02/19'; //$request->input('fecha_nacimiento_paciente');
        $telefono_paciente = $request->input('telefono_paciente');
        //$observacion = 0;
        $direccion_paciente= $request->input('direccion_paciente');;

        $dependencias = $request->input('dependencias');
        //$medico= 0; nombre_representante
        $nombre_representante= $request->input('nombre_representante');
        $cedula_representante= $request->input('titular');
         $direccion_representante = $request->input('direccion_representante');;
        $telefono_representante = $request->input('telefono_representante');;
        $observacion_eliminada = ".";
        //$fecha_registro_paciente = 0;
        //$hora_registro_paciente = 0;

        $titular_representante= $request->input('titular');
        $principal = "000000";
        $asociado_1 = "000000";
        $asociado_2= "000000";

        $NumeroAtencion= 0; // se procesa adentro
        $transaccion  = 0; // se procesa adentro

        $factura = 0;
        $tipo_seguro = $request->input('seguro');

        // procesamiento
       $tipo_consulta = $request->input('tipo_consulta');
        $id_consulta = 0;

        //valores que van en null
        $fuente_informacion  = null;
        $persona_entrega  = null;
        $cedula_persona_entrega  = null;
        $tipo_acompanante = null;
        $fecha_ingreso = "1900/01/02";
        $fecha_modificacion = "1900/01/02";




        $datos2 = $this->ConsultaExternaProcedure ->insertIngresoPacienteConsultaExterna2($id  , $codigo  , $paciente ,
            $cedula_paciente , $fecha_nacimiento , $telefono_paciente ,
            $observacion , $direccion_paciente , $dependencias , $medico , $nombre_representante ,
            $cedula_representante , $direccion_representante , $telefono_representante , $observacion_eliminada ,
            $fecha_registro_paciente , $hora_registro_paciente , $des_campo1 , $des_campo2 , $des_campo3  ,
            $usuario_ingreso  , $fecha_ingreso , $usuario_modificacion , $fecha_modificacion , $pcname ,
            $status , $titular_representante , $principal  , $asociado_1 , $asociado_2 , $NumeroAtencion  ,
            $transaccion  , $factura , $tipo_seguro , $tipo_consulta , $id_consulta , $fuente_informacion,
            $persona_entrega, $cedula_persona_entrega , $tipo_acompanante);



        $datos = $this->ConsultaExternaProcedure ->insertConsultaExternaTurnosDiarios2($turnos_diarios, $consulta_externa,
            $paciente, $cedula_paciente, $turnos, $medico,$observacion ,
            $fecha_registro_paciente, $hora_registro_paciente,$des_campo1,$des_campo2 ,$des_campo3,$usuario_ingreso,
            $usuario_modificacion ,$pcname,$status, $titular_representante);


        $datos3 = $this->ConsultaExternaProcedure->insertConsultaExternaPreparacion(0,$datos[0]->_consulta_externa,$paciente,
            $titular_representante,0,0,0,0,0,0,
            0,'n','.','.','.',0, $usuario_ingreso,$fecha_ingreso,$usuario_modificacion,
            $fecha_modificacion,$pcname,$status);


       // echo $datos2[0]->id;
       // $datos[0]->_turnos_diarios;


        //dd($datos);
        if ( isset($datos[0]->_turnos_diarios)) {
            $turno = $datos[0]->_turnos_diarios;

            $resultado = array( 'turno' => $turno);
            return json_encode($resultado);

        } else {

            return 0;
        }

    }

    public function imprimirTicket(Request $request){

        //dd($request);

        $datos_consulta = \DB::table('tbconsultaexternaturnosdiarios')->select('turnos','consulta_externa')
            ->where('turnos_diarios',$request->input('id'))->get();

        $paciente = $request->input('paciente');
        $medico = \DB::table('tbmedico')->select('nombres','apellidos')->where('id',$request->input('medico'))->get();
        $admision =  \DB::table('tbTipoingreso')->where('codigo',$request->input('seguro'))->value('descripcion');
        $medico = $medico[0]->nombres.' '.$medico[0]->apellidos;
        $especialidad = \DB::table('tbespecializacion')->select('descripcion')->where('codigo',$request->input('especialidad'))->get();
        $especialidad = $especialidad[0]->descripcion;
        $tipo_consulta = \DB::table('tbtipoconsultaexterna')->where('codigo',$request->input('tipo_consulta'))->value('descripcion');
        $consultorio = "C".\DB::table('tbCamasPorSala')->where('codigo',(\DB::table('tbMedico')->where('id',41)->value('consultorio_default')))->value('numero_camas');
        $turno = $datos_consulta[0]->turnos;
        $fecha = $request->input('fecha').' '.$request->input('hora');

        return view('ConsultaExterna.ticket.turnos', compact('paciente','medico','especialidad','admision','tipo_consulta','consultorio','turno','fecha'));
    }

    public function consultarPacienteHospitalizacionEgresado($paciente) {
        $datos = $this->ConsultaExternaProcedure->consultarPacienteHospitalizacionEgresado($paciente);
        return $datos;
    }

    public function consultarPacientesCartaIess($paciente) {
        $datos = $this->ConsultaExternaProcedure->consultarPacientesCartaIess($paciente);
        return $datos;
    }

    public function consultarIngresosPacientesConsultaExternaNumeroAtencionMedicos($medico) {
        $datos = $this->ConsultaExternaProcedure->consultarIngresosPacientesConsultaExternaNumeroAtencionMedicos($medico);
        return $datos;
    }

    public function consultarIngresosPacientesCENumeroAtencionMedicosDia($medico,$dia) {
        $datos = $this->ConsultaExternaProcedure->consultarIngresosPacientesCENumeroAtencionMedicosDia($medico,$dia);
        return $datos;
    }



    public function consultarMedicoPorCodigo($medico) {
        $datos = $this->ConsultaExternaProcedure->consultarMedicoPorCodigo($medico);
        return $datos;
    }

    public function horaUltimaAtencion($medico) {
        $datos = $this->ConsultaExternaProcedure->horaUltimaAtencion($medico);
        return $datos;
    }

    public function tempConsultarPacienteId($id) {
        $datos = $this->ConsultaExternaProcedure->tempConsultarPacienteId($id);
        return $datos;
    }

    public function horaUltimaAtencionDia($medico,$fecha) {
        $datos = $this->ConsultaExternaProcedure->horaUltimaAtencionDia($medico,$fecha);
        return $datos;
    }



}
