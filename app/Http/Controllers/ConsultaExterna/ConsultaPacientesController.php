<?php

namespace App\Http\Controllers\ConsultaExterna;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\ConsultaExternaProcedure;
use Illuminate\Support\Facades\Auth;

class ConsultaPacientesController extends Controller
{
    protected $ConsultaExternaProcedure;

    public function __construct(ConsultaExternaProcedure $consultaExternaProcedures)
    {
        $this->ConsultaExternaProcedure = $consultaExternaProcedures;
    }

    public function index() {
        return view('ConsultaExterna.PreparacionPacientes');
    }

    public function consultarPreparacion($fecha_i,$fecha_f,$paciente ="",$medico="",$dependencias="") {

        $datos = $this->ConsultaExternaProcedure->consultarPreparacion($paciente,$medico,$dependencias,$fecha_i,$fecha_f);
        return $datos;
    }

    public function consultarPreparacion2() {
        $datos = $this->ConsultaExternaProcedure->consultarPreparacion2();
        return $datos;
    }

    public function consultarPreparacionPacientes($consulta){

        $datos = $this->ConsultaExternaProcedure->consultarPreparacionPacientes($consulta);
        return $datos;
    }

    public function update(Request $request, $id){

             \DB::table('tbconsultaexternapreparacion')->where('id',$id)
            ->update([
                'temperatura' => $request->input('temperatura'),
                'pulso' => $request->input('pulso'),
                'presion_arterial'  => $request->input('presion_arterial'),
                'respiracion'  => $request->input('respiracion'),
                'peso'  => $request->input('peso'),
                'estatura'  => $request->input('estatura'),
                'talla'  => $request->input('talla'),
            ]);
    }
}
