<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $user = Auth::user();
        $usario_ingreso = (integer) $user->id;
        $dir = $_SERVER['REMOTE_ADDR'];

        try{
            // reviso si la ip existe en la base de datos
            $ip = DB::table('tbdireccionesip')
                ->whereRaw('usuario_id ='.$usario_ingreso.' and ip ="'.$dir.'"')
                ->get();

                $rol = DB::table('role_user')
                    ->select('role_id')
                    ->where('user_id',$usario_ingreso)
                    ->get();
                $rol = (object) $rol;
                $result = DB::table('tbaccesodirectos')
                    ->where('rol_id',$rol[0]->role_id)
                    ->get();
                $indicadores = \DB::select('call spIndicadoresRol(?)',array($rol[0]->role_id));
                return view('home')->with('resultado',$result)->with('indicadores',$indicadores)->with('rol',$rol[0]->role_id);
        }catch (QueryException $e){
            echo $e;
        }
    }

    public function IndicadoresAdministracion() {
         return \DB::select('Call spIndicadoresAdministracion');
    }
    public function IndicadoresAdmision() {
        return \DB::select('Call spIndicadoresAdmision');
    }

    public function estadoCamasSala() {
        return \DB::select('Call spStatusSalasCamas');
    }

    public function estadoCunasSala() {
        return \DB::select('Call spStatusSalasCunas');
    }
}
