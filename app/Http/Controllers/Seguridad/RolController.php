<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;
use Illuminate\Database\QueryException;

class RolController extends Controller
{
    public function index()
    {
        //
        $resultado = DB::table('roles')
            ->select('id','name','slug','description','special')
            ->paginate(10);
        return view('Seguridad.Roles.index',compact('resultado'));
    }

    public function create()
    {
        return view('Seguridad.Roles.create');
    }

    public function getFechaActual(){
        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H');
        $minutos = date('i');
        $segundos = date('s');

        $fecha_actualizacion =$year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';
        return $fecha_actualizacion;
    }


    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'slug' => 'required|max:30|unique:roles|string',
            'name' => 'required|max:30|unique:roles|string',
            'description'=> 'required|max:100|string',

        ],['slug.required' => 'El slug es requerido','slug.max' => 'El slug solo puede tener un maximo de 30 caracteres','slug.unique' => "El nombre del slug debe ser unico",
            'name.required' => 'El nommbre es requerido','name.max' => 'El nombre solo puede tener un maximo de 30 caracteres','name.unique' => "El nombre  debe ser unico",
            'descripcion.required' => 'La descripción es requerida','slug.max' => 'La descripción solo puede tener un maximo de 100 caracteres',
        ]);


                $name = $request->input('name');
                $slug = $request->input('slug');
                $description = $request->input('description');
                $permisos = $request->input('permisos');

                if($request->input('special') == 'all-access' )
                {
                    $special = $request->input('special');
                } elseif ($request->input('special') == 'all-access' ){
                    $special = $request->input('special');
                } else {
                    $special = null;
                }

                $fecha = $this->getFechaActual();

                try{

                    $id =DB::table('roles')->insertGetId(
                        [   'name' => $name,
                            'slug' => $slug,
                            'description' => $description,
                            'special' => $special,
                            'created_at' => $fecha ,
                            'updated_at' => $fecha
                        ]
                    );
                    if($permisos != null || $permisos != ''){
                        foreach ($permisos as $items) {
                            DB::table('permission_role')->insert([
                                'permission_id' => $items,
                                'role_id' => $id,
                                'created_at' => $fecha ,
                                'updated_at' => $fecha
                            ]);
                        }
                    }
                    flash()->success('se ha registrado el rol con exito');
                    return redirect()->route('roles.create');
                }catch (QueryException $e){
                    flash()->error("Error".$e->errorInfo[1]);
                    return redirect()->route('roles.create');

                }


    }
    public function show($id)
    {
        //
        $resultado = DB::table('roles')->where('id',$id)->get();
        $permisos = \DB::select('Call spPermisosRol(?)',array($id));
        //dd($permisos);
        return view('seguridad.roles.show',compact('resultado','permisos'));
    }

    public function edit($id)
    {
        $resultado = DB::table('roles')->where('id',$id)->get();
        $permisos = \DB::select('Call spPermisosRol(?)',array($id));
        $permisosF = \DB::select('Call spPermisosRolFaltantes(?)',array($id));
        //dd($resultado);
        return view('seguridad.roles.edit',compact('resultado','permisos','permisosF'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        // validaciones del lado del controlador

        $request->validate([
            'slug' => 'required|max:30|string',
            'name' => 'required|max:30|string',
            'description'=> 'required|max:100|string',

        ],['slug.required' => 'El slug es requerido','slug.max' => 'El slug solo puede tener un maximo de 30 caracteres','slug.unique' => "El nombre del slug debe ser unico",
            'name.required' => 'El nommbre es requerido','name.max' => 'El nombre solo puede tener un maximo de 30 caracteres','name.unique' => "El nombre  debe ser unico",
            'descripcion.required' => 'La descripción es requerida','slug.max' => 'La descripción solo puede tener un maximo de 100 caracteres',
        ]);


        // obtenemos los valores del formulario
        $actuales1 = \DB::table('permission_role')->select('id')->where('role_id',$id)->get();
        $actuales = $request->input('actuales');
        $eliminar = [];
        $name = $request->input('name');
        $slug = $request->input('slug');
        $description = $request->input('description');
        $permisos = $request->input('permisos');

        if($request->input('especial') === "null"){
            $special = null;
        } else {
           $special = $request->input('especial');
        }
        $nuevos =  $request->input('nuevos');

        // validamos que el rol no se encuentre en la base de datos
        $result =DB::table('roles')->select('id')->where([['name',$name],['slug',$slug],['id', '!=' ,$id]])->get();
        //dd($result->first());
        if($result->first() == null){
            try{

                // agregamos a un array  los permisos que han sido desmarcados de la lista de actuales
                if($request->input('actuales') == null)
                {
                    \DB::table('permission_role')->where('role_id', '=', $id)->delete();
                } else {
                    foreach ($actuales1 as $a) {
                        //echo  $a->id; echo "<br>";
                        foreach ($actuales as $item) {
                            if($a->id == $item) {

                            }else {
                                array_push($eliminar,$a->id);
                            }
                        }
                    }
                }
                // comenzamos con la eliminación
                if($eliminar != [])
                {
                    foreach ($eliminar as $item){
                        \DB::table('permission_role')->where('id', '=', $item)->delete();
                    }
                }

                $fecha = $this->getFechaActual();

                DB::table('roles')->where('id',$id)
                    ->update([ 'name' => $name,
                        'slug' => $slug,
                        'description' => $description,
                        'special' => $special,
                        'updated_at' => $fecha
                    ]);

                // agregamos los permisos nuevos
                if($nuevos != null || $nuevos != ''){
                    foreach ($nuevos as $items) {
                        DB::table('permission_role')->insert([
                            'permission_id' => $items,
                            'role_id' => $id,
                        ]);
                    }
                }
                flash()->success('se ha actualizado el rol con exito');
                return redirect()->route('roles.edit',['edit'=>$id]);
            }catch (QueryException $e){
                echo "error";
                //flash()->error("Error".$e->errorInfo[1]);
               // return redirect()->route('roles.create');
            }

        }else{
            echo "me fui al else";
            flash()->error('El nombre y el slug deben de ser unicos');
            return redirect()->route('roles.edit',['edit'=>$id]);
        }

    }


    public function destroy($id)
    {
        //
    }

    public function table()
    {
        $result = DB::table('roles')
            ->select('id','name','slug','description','special')
            ->get();

        return $result;
    }

    public function rolesPermisos($id) {

        $result = DB::table('permission_role')
            ->join('permissions','permissions.id','=','permission_role.permission_id')
            ->select('permissions.name')
            ->where('role_id',$id)
            ->get();
        return $result;
    }

    public function uniqueName($name) {
        $resultado = DB::table('roles')->select('id')->where('name',$name)->get();
        $resultado = (object) $resultado;
        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }

    }

    public function uniqueSlug($name) {
        $resultado = DB::table('roles')->select('id')->where('slug',$name)->get();
        $resultado = (object) $resultado;
        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }
    }
    public function uniqueNameUpdate($name,$id) {
        $resultado = DB::table('roles')->select('id')->where([['name',$name],['id','<>',$id]])->get();
        $resultado = (object) $resultado;
        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }
    }

    public function uniqueSlugUpdate($name,$id) {
        $resultado = DB::table('roles')->select('id')->where([['slug',$name],['id','<>',$id]])->get();
        $resultado = (object) $resultado;
        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }
    }
}
