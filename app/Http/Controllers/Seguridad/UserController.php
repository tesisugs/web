<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Seguridad.Usuarios');
    }

    //
    public function table()
    {
       $result = DB::table('users')
            ->join('role_user','users.id','=','role_user.user_id')
            ->join('roles','role_user.role_id','=','roles.id')
            ->select('users.id','users.nombres','users.apellidos','users.name','roles.name as rol')
            ->where('users.estado',1)
            ->get();
        //return \Response::json($response);
        return $result;
        //return view('Seguridad.Usuarios');
    }



    public function store(Request $request)
    {

         $request->validate([
            'nombres' => 'required|max:30',
            'apellidos' => 'required',
            'name'=> 'required|unique:users|max:10',
            'password' => 'required|min:8|max:16',
            'rol' => 'required'
        ]);
        //
        $nombres = $request->input('nombres');
        $apellidos = $request->input('apellidos');
        $name = $request->input('name');
        $estado = 1;
        $rol = $request->input('rol');
        $password = bcrypt($request->input('pass')) ;
        //$token =  'remember_token' => str_random(10);
       // $remember =
        //$created_at = (string)getdate();
        //$updated_at =  (string)getdate();
        //dd($request);
        //$user = new User();
        //$user->nombres="$nombres";
        //$user->apellidos="$apellidos";
        //$user->name = $name;
        //$user->estado = 1;
        //$user->password = bcrypt($password);
        //$user->save();

        $id =DB::table('users')->insertGetId(
            ['nombres' => $nombres,
                'apellidos' => $apellidos,
                'name' => $name,
                'estado' => $estado,
                'password' => $password,
                'remember_token' => str_random(60),
                'created_at' => null,
                'updated_at' => null]
        );

        DB::table('role_user')->insert([
            'role_id' => $rol,'user_id' => $id
            ]);
         User::create($request->input());
        return response()->json(['success'=>'Informacion guardada con exito']);

    }

    public function show($id)
    {
        //
       $result = DB::select('call spUsuariosShow(?)',array($id));
       return $result;
    }

    public function edit($id)
    {
        //
        $result = DB::select('Call spDatosUsuario(?)',array($id));
        return $result;
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $request->validate([
            'nombres' => 'required|max:30',
            'name' => 'required|max:30',
            'apellidos'=> 'required|max:30',
            'rol'=> 'required',
            'ip'=> 'required',
            'departamento'=> 'required|max:100',
        ],['nombre.required' => 'Los nombres es requeridos',
            'name.required' => 'El nombre de usuarios es requerido',
            'apellidos.required' => 'Los apellidos es requeridos',
            'rol.required' => 'El rol es requerido',
            'rol.departamento' => 'El departamentos es requerido',
        ]);

        $nombres = $request->input('nombres');
        $apellidos = $request->input('apellidos');
        $name = $request->input('name');
        $rol = $request->input('rol');
        $password = $request->input('password') ;
        $ip = $request->input('ip');
        $departamento = $request->input('departamento');
        $fecha_creacion = $request->input('fecha');
        $user = \Auth::user();
        $usuario_modificacion = $user->id;
        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H') ;
        $minutos = date('i');
        $segundos = date('s');

        $fecha_modficacion = $year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';

        try{
            if($password===null || $password === ''){
                $user = DB::table('users')->where('id',$id)
                    ->update(['nombres' => $nombres,
                        'apellidos' => $apellidos,
                        'name'  => $name,
                        'created_at' => $fecha_creacion,
                        'updated_at' => $fecha_modficacion
                    ]);

                DB::table('role_user')->where('user_id',$id)
                    ->update(['role_id' => $rol]);

                DB::table('tbdireccionesip')->where('usuario_id',$id)
                    ->update([  'ip' => $ip,
                        'departamento_id' => $departamento,
                        'fecha_modificacion'=>$fecha_modficacion,
                        'usuario_modificacion' => $usuario_modificacion
                    ]);


            }else{
                $user = DB::table('users')->where('id',$id)
                    ->update(['nombres' => $nombres,
                        'apellidos' => $apellidos,
                        'name'  => $name,
                        'password' => bcrypt($password),
                        'updated_at' => $fecha_modficacion,
                        'created_at' => $fecha_creacion
                    ]);

                DB::table('role_user')->where('user_id',$id)
                    ->update(['role_id' => $rol]);

                DB::table('tbdireccionesip')->where('usuario_id',$id)
                    ->update([  'ip' => $ip,
                        'departamento_id' => $departamento,
                        'fecha_modificacion'=>$fecha_modficacion,
                        'usuario_modificacion' => $usuario_modificacion
                    ]);
            }
        }catch (QueryException $e){
            $array = array("Error" , $e->errorInfo[1]);
            return $array;
        }



    }

    public function destroy($id)
    {
        //
    }

    public function cmbRoles()
    {
           $resultado = DB::table('roles')->get();
        return $resultado;
    }

    public function  estado($id) {
         DB::table('users')->where('id',$id)
            ->update(['estado' => 0
            ]);
    }

    public function departamentos() {
        return DB::select('call spConsultarDepartamentosAll');
    }

    public function ip($ip){
        $resultado = DB::table('tbdireccionesip')->select('id')->where('ip',$ip)->get();
        $resultado = (object) $resultado;

        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }

    }

    public function usuario($user){
        $resultado = DB::table('users')->select('id')->where('name',$user)->get();
        $resultado = (object) $resultado;
        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }
    }

    public function ipUpdate($ip,$id){
        $resultado = DB::table('tbdireccionesip')->select('id')->where([['ip',$ip],['id','<>',$id]])->get();
        $resultado = (object) $resultado;

        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }

    }

    public function usuarioUpdate($user,$id){
        $resultado = DB::table('users')->select('id')->where([['name',$user],['id','!=',$id]])->get();
        $resultado = (object) $resultado;
        if(isset($resultado[0]->id))
        {
            return 0;
        }else {
            return 1;
        }
    }

    public function consularUsuarioName($rol=0,$usuario='')
    {
        return \DB::select('call spconsultarUsuariosName(?,?)',array($usuario,$rol));
    }
}
