<?php

namespace App\Http\Controllers\seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Caffeinated\Shinobi\Models\Permission;

class PermissionController extends Controller
{

    public function index()
    {
        return view('Seguridad.permisos');
    }

    public function store(Request $request)
    {
        //

       $request->validate([
            'slug' => 'required|max:30|unique:permissions',
            'name' => 'required|unique:permissions|max:30',
            'description'=> 'required|max:100',

        ],['slug.required' => 'El slug es requerido','slug.max' => 'El slug solo puede tener un maximo de 30 caracteres','slug.unique' => "El nombre del slug debe ser unico",
            'name.required' => 'El nommbre es requerido','name.max' => 'El nombre solo puede tener un maximo de 30 caracteres','name.unique' => "El nombre  debe ser unico",
            'descripcion.required' => 'La descripción es requerida','slug.max' => 'La descripción solo puede tener un maximo de 100 caracteres',
        ]);
        //
        $name = $request->input('name');
        $slug = $request->input('slug');
        $description = $request->input('description');

        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H');
        $minutos = date('i');
        $segundos = date('s');

        $fecha_actualizacion =$year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';

        try{
            $id =DB::table('permissions')->insertGetId(
                [   'name' => $name,
                    'slug' => $slug,
                    'description' => $description,
                    'create_at' => $fecha_actualizacion,
                    'updated_at' => $fecha_actualizacion
                ]
            );
            // User::create($request->input());
            return response()->json(['success'=>'Informacion guardada con exito']);
        }catch (QueryException $e){
            $array = array("Error" , $e->errorInfo[1]);
            //$array = (object) $array;
            return $array;
        }
    }

    public function show($id)
    {
        $result = DB::table('permissions')->where('id',$id)->get();
        return $result;
    }

    public function edit($id)
    {
        $result = DB::table('permissions')->where('id', $id)->get();
        return $result;
    }

    public function update(Request $request, $id)
    {
           $request->validate([
               'slug' => 'required|max:30',
               'name' => 'required|max:30',
               'description'=> 'required|max:100',

           ],['slug.required' => 'El slug es requerido','slug.max' => 'El slug solo puede tener un maximo de 30 caracteres','slug.unique' => "El nombre del slug debe ser unico",
               'name.required' => 'El nommbre es requerido','name.max' => 'El nombre solo puede tener un maximo de 30 caracteres','name.unique' => "El nombre  debe ser unico",
               'descripcion.required' => 'La descripción es requerida','slug.max' => 'La descripción solo puede tener un maximo de 100 caracteres',
           ]);
        //
        //

        $name = $request->input('name');
        $slug = $request->input('slug');
        $description = $request->input('description');

        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H');
        $minutos = date('i');
        $segundos = date('s');

        $fecha_actualizacion =$year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';
        $id = (integer) $id;
        $result =DB::table('permissions')->select('id')->where([['name',$name],['slug',$slug],['id', '!=' ,$id]])->get();
        if($result === []){
            try{
                DB::table('permissions')->where('id',$id)
                    ->update([ 'name' => $name,
                        'slug' => $slug,
                        'description' => $description,
                        'updated_at' =>$fecha_actualizacion
                    ]);
                return response()->json(['success'=>'Informacion guardada con exito']);
            }catch (QueryException $e){
                $array = array("Error" , $e->errorInfo[1]);
                return $array;
            }
        } else {
            $array = array("Error"," EL nombre y el slug deben de se unicos");
            return $array;
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
           $result = db::table('permissions')->where('id',$id)->delete();
           return $result;
    }

    public function table()
    {
       $result = DB::table('permissions')
        ->select('id','name','slug','description')
        ->get();

       return $result;
    }

    public function consularPermisosName($permiso='')
    {
        return \DB::select('call spConsultarPermisosNombre(?)',array($permiso));
    }
}
