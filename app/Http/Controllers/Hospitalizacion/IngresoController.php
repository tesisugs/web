<?php

namespace App\Http\Controllers\Hospitalizacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\HospitalizacionnProcedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class IngresoController extends Controller
{
    protected $HospitalizacionnProcedure;


    public function __construct(HospitalizacionnProcedure $hospitalizacionProcedures)
    {
        $this->HospitalizacionnProcedure = $hospitalizacionProcedures;
    }

    public function index()
    {
        return view('Hospitalizacion.IngresoPacientes');
    }

    public function consultarPacientesNombre($paciente, $estado)
    {
        $datos = $this->HospitalizacionnProcedure->consultarPacientesNombre($paciente, $estado);
        return $datos;
    }

    public function consultarUnidad()
    {
        $datos = $this->HospitalizacionnProcedure->consultarTipoCama();
        return $datos;
    }

    public function consultarCamaPorSalaCodigoSala($id)
    {
        $datos = $this->HospitalizacionnProcedure->consultarCamaPorSalaCodigoSala($id);
        return $datos;
    }

    public function consultarCamaPorSalaCodigo($id)
    {
        $datos = $this->HospitalizacionnProcedure->consultarCamaPorSalaCodigo($id);
        return $datos;
    }

    public function consultarCamaPorSalaSalaHabilitadaCunas($sala, $camas)
    {
        $datos = $this->HospitalizacionnProcedure->consultarCamaPorSalaSalaHabilitadaCunas($sala, $camas);
        return $datos;
    }

    public function consultarPacienteId($id)
    {

        $datos = $this->HospitalizacionnProcedure->consultarPacienteId($id);
        return $datos;
    }

    public function consultarDerivacionHospital()
    {
        $datos = $this->HospitalizacionnProcedure->consultarDerivacionHospital();
        return $datos;
    }

    public function consultarMedicosLlamadoIngresoHospitalario($paciente,$medico,$fecha)
    {
        $datos = $this->HospitalizacionnProcedure->consultarMedicosLlamadoIngresoHospitalario($paciente,$medico,$fecha);
        return $datos;
    }

    public function consultarHospitalizacionTodos($paciente) {

        $datos = $this->HospitalizacionnProcedure->consultarHospitalizacionTodos($paciente);
        return $datos;
    }

    public function validarInsert($paciente)
    {

        try {
            $result = \DB::table('tbhospitalizacion')
                ->select('id')->where('paciente', $paciente)->wherein('status', [1, 3])->get();
            if (isset($result[0]->id)) {
                return 1;
            } else {
                return 0;
            }
        } catch (QueryException $e) {

            return $e;
        }

    }

    public function insertHospitalizacion(Request $request)
    {

       $this->validate($request, [
            'fecha_nacimiento' => 'required', 'id' => 'required', 'sala' => 'required', 'procedencia' => 'required',
            'nombre_responsable' => 'required', 'documento_responsable' => 'required', 'fecha_medico' => 'required',
            'servicio' => 'required', 'medico' => 'required', 'principal' => 'required',
            'asociado1' => 'required', 'asociado2' => 'required', 'observacion_medico' => 'required', 'codigo_cama' => 'required',
            'genero' => 'required', 'titular_id' => 'required', 'observacion_documentos' => 'required',
        ],[
            'fecha_nacimiento.required' => 'La fecha de nacimiento es requerida', 'id.required' => 'La información del paciente es requerida',
            'sala.required.required' => 'la sala es requerida', 'procedencia' => 'La información del paciente es requerida',
            'nombre_responsable.required' => 'El nombre del titular es requerido',
            'documento_responsable.required' => 'EL documento del responsable es requerido',
            'fecha_medico.required' => 'La fecha de la pestaña medico es requerida',
            'servicio.required' => 'La especialidad es requerida',
            'medico.required' => 'El medico es requerido', 'principal.required' => 'El diagnostico proncipal es requerido',
            'asociado1.required' => 'El diagnostico asociado es requerido', 'asociado2.required' => 'El diagnostico asociado 2 es requerido',
            'observacion_medico.required' => 'la observación del medico es requerida',
            'codigo_cama.required' => 'El numero de cama es requerido',
            'genero.required' => 'El genero del paciente es requerido', 'titular_id.required' => 'La información del responsable es requerida',
             'observacion_documentos.required' => 'La observación de documentos completos es requerida',
        ]);

        $id = 0;
        $SecuenciaHO = 0;
        $No_Historial = 0;
        $Fecha_Hora_Ingreso = '1900/01/01';
        $cobertura_Maxima = 0;
        $fecha_nacimiento = $request->input('fecha_nacimiento');

        if ($request->input('tipo') === 'normal') {
            $Normal = 1;
            $Cirugia_Dia = 0;
        } else {
            $Normal = 0;
            $Cirugia_Dia = 1;
        }

        $Fecha_de_Registro = '1900/01/01';
        $Presupuesto = 0;
        $Pre_Factura = 0;
        $paciente = $request->input('id');
        $NumeroAtencion = 0;
        $habitacion = $request->input('sala');
        $Procedencias = $request->input('procedencia');
        $Paquete_Atencion = 0;
        $Nombre_Responsable = $request->input('nombre_responsable');
        $Direccion_Responsable = $request->input('direccion_responsable');
        $Documento_Responsable = $request->input('documento_responsable');
        $Telefono_Responsable = $request->input('telefono_responsable');
        $Plan_Atencion = 0;
        $CoaSeguro = 0;
        $Deducible = 0;
        $Observaciones = $request->input('observacion');
        $Siniestro = 0;
        $Referencia_1 = 0;
        $Referencia_2 = 0;
        $Medico_Fecha = $request->input('fecha_medico');
        $Servicios = $request->input('servicio');
        $medico = $request->input('medico');
        $principal = $request->input('principal');
        $asociado_1 = $request->input('asociado1');
        $asociado_2 = $request->input('asociado2');
        $Medico_Observacion = $request->input('observacion_medico');
        $garantia = 0; // saber de donde obtengo este dato
        $Garantia_plan = 0;
        $Garantia_Coaseguros = 0;
        $Garantia_Deducible = 0;
        $Garantia_Moneda = 0;
        $Garantia_siniestro = 0;
        $Garantia_referencia_1 = 0;
        $Garantia_referencia_2 = 0;
        $Dieta = 0;
        $Dieta_Observaciones = 0;
        $Dieta_Contenido = 0;
        $Varios_Fecha = '1900/01/01 00:00:00';
        $Varios_Musica = 1;
        $Varios_Musica_desde = '1900/01/01 00:00:00';
        $Varios_Musica_hasta = '1900/01/01 00:00:00';
        $Varios_Televisor = 1;
        $Varios_Televisor_desde = '1900/01/01 00:00:00';
        $Varios_Televisor_hasta = '1900/01/01 00:00:00';
        $Varios_Periodico = 1;
        $Varios_Acompanante = 1;
        $Varios_Acepta_Visitas_si = 1;
        $Varios_Acepta_Visitas_no = 1;
        $Varios_Acepta_Visitas_restringido = 1;
        $Varios_Acepta_llamadas = 1;
        $Varios_Dieta_aconpanante = 0;
        $fecha_egreso = '1900/01/01 00:00:00';
        $medico_egreso = 0;
        $principal_egreso = 0;
        $asociadoEgreso1 = 0;
        $asociadoEgreso2 = 0;
        $tipo_egreso = 0;
        $complicacion = 0;
        $tipo_complicacion = 0;
        $des_campo1 = ".";
        $des_campo2 = ".";
        $des_campo3 = 0;
        $fecha_registro = '1900/01/01';
        $user = Auth::user();
        $usuario_registro = $user->id;
        $fecha_modificacion = '1900/01/01';
        $usuario_modificacion = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];;
        $status = 1;
        $codigo_cama = $request->input('codigo_cama');
        $traspaso = 0;
        $fecha_hora_traspaso = '1900/01/01 00:00:00';
        $genero = $request->input('genero');
        $observacion_traspaso = ".";
        $transaccion = 0;
        $titular = $request->input('titular_id'); // pasar el id del titular
        $tipo_egreso_alta = 0;
        $derivacion = ($request->input('derivacion') != null) ? 1 : 0;
        $hospital = ($request->input('hospital') != null) ? $request->input('hospital') : 0;
        $status_documento = ($request->input('documentos_completos') == 'on') ? 1 : 0;
        //$status_documento = 1;
        $osbservacion_documento = $request->input('observacion_documentos');
        $observacion_eliminada = '.';
        //$telefono = ($request->input('telefono') != null) ? $request->input('telefono') : ".";


        $datosP = array('id' => $id, 'SecuenciaHO' => $SecuenciaHO,
            'No_Historial  ' => $No_Historial,
            'Fecha_Hora_Ingreso ' => $Fecha_Hora_Ingreso,
            'cobertura_Maxima  ' => $cobertura_Maxima,
            'fecha_nacimiento ' => $fecha_nacimiento,
            'Normal ' => $Normal,
            'Cirugia_Dia ' => $Cirugia_Dia,
            'Fecha_de_Registro' => $Fecha_de_Registro,
            'Presupuesto ' => $Presupuesto,
            'Pre_Factura ' => $Pre_Factura,
            'paciente ' => $paciente,
            'NumeroAtencion  ' => $NumeroAtencion,
            'habitacion ' => $habitacion,
            'Procedencias ' => $Procedencias,
            'Paquete_Atencion ' => $Paquete_Atencion,
            'Nombre_Responsable ' => $Nombre_Responsable,
            'Direccion_Responsable ' => $Direccion_Responsable,
            'Documento_Responsable ' => $Documento_Responsable,
            'Telefono_Responsable ' => $Telefono_Responsable,
            'Plan_Atencion ' => $Plan_Atencion,
            'CoaSeguro ' => $CoaSeguro,
            'Deducible ' => $Deducible,
            'Observaciones ' => $Observaciones,
            'Siniestro ' => $Siniestro,
            'Referencia_1 ' => $Referencia_1,
            'Referencia_2 ' => $Referencia_2,
            'Medico_Fecha  ' => $Medico_Fecha,
            'Servicios ' => $Servicios,
            'medico  ' => $medico,
            'principal  ' => $principal,
            'asociado_1 ' => $asociado_1,
            'asociado_2 ' => $asociado_2,
            'Medico_Observacion ' => $Medico_Observacion,
            'garantia' => $garantia,
            'Garantia_plan' => $Garantia_plan,
            'Garantia_Coaseguros ' => $Garantia_Coaseguros,
            'Garantia_Deducible ' => $Garantia_Deducible,
            'Garantia_Moneda ' => $Garantia_Moneda,
            'Garantia_siniestro ' => $Garantia_siniestro,
            'Garantia_referencia_1 ' => $Garantia_referencia_1,
            'Garantia_referencia_2 ' => $Garantia_referencia_2,
            '$Dieta' => $Dieta,
            '$Dieta_Observaciones  ' => $Dieta_Observaciones,
            'Dieta_Contenido ' => $Dieta_Contenido,
            'Varios_Fecha ' => $Varios_Fecha,
            'Varios_Musica ' => $Varios_Musica,
            'Varios_Musica_desde ' => $Varios_Musica_desde,
            'Varios_Musica_hasta' => $Varios_Musica_hasta,
            'Varios_Televisor  ' => $Varios_Televisor,
            'Varios_Televisor_desde ' => $Varios_Televisor_desde,
            'Varios_Televisor_hasta ' => $Varios_Televisor_hasta,
            'Varios_Periodico ' => $Varios_Periodico,
            'Varios_Acompanante ' => $Varios_Acompanante,
            'Varios_Acepta_Visitas_si ' => $Varios_Acepta_Visitas_si,
            'Varios_Acepta_Visitas_no ' => $Varios_Acepta_Visitas_no,
            'Varios_Acepta_Visitas_restringido ' => $Varios_Acepta_Visitas_restringido,
            'Varios_Acepta_llamadas ' => $Varios_Acepta_llamadas,
            'Varios_Dieta_aconpanante ' => $Varios_Dieta_aconpanante,
            'fecha_egreso  ' => $fecha_egreso,
            'medico_egreso ' => $medico_egreso,
            'principal_egreso ' => $principal_egreso,
            'asociadoEgreso1 ' => $asociadoEgreso1,
            'asociadoEgreso2 ' => $asociadoEgreso2,
            'tipo_egreso ' => $tipo_egreso,
            'complicacion ' => $complicacion,
            'tipo_complicacion ' => $tipo_complicacion,
            'des_campo1 ' => $des_campo1,
            'des_campo2 ' => $des_campo2,
            'des_campo3 ' => $des_campo3,
            'fecha_registro  ' => $fecha_registro,
            '$usuario_registro  ' => $usuario_registro,
            'fecha_modificacion  ' => $fecha_modificacion,
            'usuario_modificacion  ' => $usuario_modificacion,
            'pcname ' => $pcname,
            'status ' => $status,
            'codigo_cama ' => $codigo_cama,
            'traspaso ' => $traspaso,
            'fecha_hora_traspaso ' => $fecha_hora_traspaso,
            'genero ' => $genero,
            'observacion_traspaso' => $observacion_traspaso,
            'transaccion  ' => $transaccion,
            'titular ' => $titular,
            'tipo_egreso_alta ' => $tipo_egreso_alta,
            'derivacion ' => $derivacion,
            'hospital ' => $hospital,
            'status_documento ' => $status_documento,
            'osbservacion_documento' => $osbservacion_documento,
            'observacion_eliminada' => $observacion_eliminada);


            try {
                $datos = $this->HospitalizacionnProcedure->insertHospitalizacion(
                    $id, $SecuenciaHO, $No_Historial, $Fecha_Hora_Ingreso, $cobertura_Maxima, $fecha_nacimiento, $Normal, $Cirugia_Dia,
                    $Fecha_de_Registro, $Presupuesto, $Pre_Factura, $paciente, $NumeroAtencion, $habitacion, $Procedencias,
                    $Paquete_Atencion, $Nombre_Responsable, $Direccion_Responsable, $Documento_Responsable, $Telefono_Responsable,
                    $Plan_Atencion, $CoaSeguro, $Deducible, $Observaciones, $Siniestro, $Referencia_1, $Referencia_2,
                    $Medico_Fecha, $Servicios, $medico, $principal, $asociado_1, $asociado_2, $Medico_Observacion, $garantia,
                    $Garantia_plan, $Garantia_Coaseguros, $Garantia_Deducible, $Garantia_Moneda, $Garantia_siniestro,
                    $Garantia_referencia_1, $Garantia_referencia_2, $Dieta, $Dieta_Observaciones, $Dieta_Contenido,
                    $Varios_Fecha, $Varios_Musica, $Varios_Musica_desde, $Varios_Musica_hasta, $Varios_Televisor, $Varios_Televisor_desde,
                    $Varios_Televisor_hasta, $Varios_Periodico, $Varios_Acompanante, $Varios_Acepta_Visitas_si, $Varios_Acepta_Visitas_no,
                    $Varios_Acepta_Visitas_restringido, $Varios_Acepta_llamadas, $Varios_Dieta_aconpanante, $fecha_egreso,
                    $medico_egreso, $principal_egreso, $asociadoEgreso1, $asociadoEgreso2, $tipo_egreso, $complicacion, $tipo_complicacion,
                    $des_campo1, $des_campo2, $des_campo3, $fecha_registro, $usuario_registro, $fecha_modificacion, $usuario_modificacion,
                    $pcname, $status, $codigo_cama, $traspaso, $fecha_hora_traspaso, $genero, $observacion_traspaso, $transaccion,
                    $titular, $tipo_egreso_alta, $derivacion, $hospital, $status_documento, $osbservacion_documento, $observacion_eliminada
                );
                $array = $this->fecha();

                if (isset($datos[0]->_id)) {
                    $id = $datos[0]->_id;
                    $secuencia = $datos[0]->_SecuenciaHo;
                    $historial = $datos[0]->_No_Historial;
                    $atencion = $datos[0]->_NumeroAtencion;
                    $transaccion = $datos[0]->_transaccion;

                    $resultado = array('id' => $id, 'secuencia' => $secuencia,
                        'historial' => $historial, 'atencion' => $atencion,
                        'transaccion' => $transaccion, 'fecha_registro' => $array->fecha,
                        'fecha_hora_registro' => $array->fecha_hora);
                    return json_encode($resultado);

                } else {
                    $error = 0;
                    return $error;
                }

            } catch (QueryException $e){
                return $e->errorInfo[1];
            }
    }

    public function fecha() {
        $fecha = getdate();
        $day = $fecha['mday'];

        if ($day < 10) {
            $day = "0" . $day;
        }

        if ($fecha['mon'] < 10) {
            $mes = "0" . $fecha['mon'];
        } else {
            $mes = $fecha['mon'];
        }
        $hora = $fecha['hours'] - 5;

        if ($hora < 10) {
            $hora = "0" . $hora;
        }

        if ($fecha['minutes'] < 10) {
            $minutos = "0" . $fecha['minutes'];
        } else {
            $minutos = $fecha['minutes'];
        }

        if ($fecha['seconds'] < 10) {
            $segundos = "0" . $fecha['seconds'];
        } else {
            $segundos = $fecha['seconds'];
        }

        $fecha_registro = $fecha['year'] . "/" . $mes . "/" . $day;
        $fecha_hora_registro = $fecha['year'] . "/" . $mes . "/" . $day . " " . $hora . ":" . $minutos . ":" . $segundos;
        $array = array("fecha_hora" => $fecha_hora_registro, "fecha" => $fecha_registro );
        $array = (object) $array;
        return $array;
    }
    // reportes

/*    public function reporteHospitalizacion(Request $request)
    {

        //$url = Storage::url('RptAdmision001');
        //$url = Storage::url('RptAdmision001');
        /*
                $datosUser = [
                    'id' => $request->input('id'),
                    'parroquia' => $parroquia,
                    'ciudad' => $ciudad,
                    'provincia' => $provincia,
                    'apellido_paterno' => $request->input('apellido_paterno'),
                    'apellido_materno' => $request->input('apellido_materno'),
                    'primer_nombre' => $request->input('primer_nombre'),
                    'segundo_nombre' => $request->input('segundo_nombre'),
                    'cedula' => $request->input('cedula'),
                    'direccion' => $request->input('direccion'),
                    'telefono' => $telefono,
                    'fecha_nacimiento' => $request->input('fecha_nacimiento'),
                    'lugar_nacimiento' => $request->input('lugar_nacimiento'),
                    'nacionalidad' => $nacionalidad,
                    'etnia' => $etnia,
                    'edad' => $request->input('edad'),
                    'genero' => $request->input('genero'),
                    'estado_civil' => $request->input('estado_civil'),
                    'ocupacion' => $request->input('ocupacion'),
                    'lugar_trabajo' => $request->input('trabajo'),
                    'seguro' => $seguro,
                    'instruccion' => $request->input('instruccion'),
                    'fecha_emision' => $request->input('fecha_emision'),
                    'titular' => $titular,
                    'parentesco' => $parentesco,
                    'direccion2' => $direccion2,
                    'telefono2' => $telefono2,

                ];
                */
        //$datos = (object)$datosUser;
        /*<!-- referido --><!-- avisar --><!-- parentesco --><!-- dirrecion --><!-- telefono --><!-- admisionista --> */

   //     $pdf = \PDF::loadView('Hospitalizacion.PDF.RptRegistroDeHospitalizacion1');
     //   return $pdf->stream();
/*

    } */

    public function rotuloHospitalizacion(Request $request)
    {
        $datos = (object)$request->all();
        return view('Hospitalizacion.PDF.rptHospitalizacionRotuloPuerta1',compact('datos'));
    }

    public function consultarTipoDiagnosticoCodigo($codigo)
    {
        $datos = $this->HospitalizacionnProcedure->consultarTipoDiagnosticoCodigo($codigo);
        return $datos;

    }

    public function imprimirIngreso($hospitalizacion){

        try{

            $datos_hospitalizacion = $this->HospitalizacionnProcedure->consultarHospitalizacion(17453);
            $datos_paciente = $this->HospitalizacionnProcedure->consultarInformacionPaciente($datos_hospitalizacion[0]->Paciente);
            $datos_camas = $this->HospitalizacionnProcedure->consultarDatosCama($datos_hospitalizacion[0]->codigo_cama);
            $fecha_nacimiento_array = explode(" ", $datos_paciente[0]->fecha_nacimiento);
            $fecha_ingreso_array = explode(" ", $datos_hospitalizacion[0]->Fecha_de_Registro);


            /*-------------------------------------------------------*/
            list($Y,$m,$d) = explode("-",$fecha_nacimiento_array[0]);
            $edad = ( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
            date_default_timezone_set('America/Lima');
            $year = date('Y');
            $mes = date('m');
            $dia = date('d');
            $hora= date('H') ;
            $minutos = date('i');
            $segundos = date('s');
            $fecha_impresion  = $dia."/".$mes."/".$year;
            $hora_impresion =   $hora."-".$minutos."-".$segundos;
            $user = Auth::user();
            /*-------------------------------------------------------*/

            $datos_egreso= [
                'historia_clinica' => $datos_hospitalizacion[0]->No_Historial,
                'fecha_ingreso' => $fecha_ingreso_array[0],
                'paciente' => $datos_paciente[0]->paciente,
                'cedula' => $datos_paciente[0]->cedula,
                'fecha_nacimiento' => $fecha_nacimiento_array[0],
                'edad' => $edad,
                'estado_civil' => $datos_paciente[0]->estado_civil,
                'genero' => $datos_paciente[0]->genero  ,
                'observacion' => $datos_paciente[0]->observacion,
                'discapacidad' => $datos_paciente[0]->status_discapacidad,
                'conadis' => $datos_paciente[0]->carnet_conadis,
                'etnia' => $datos_paciente[0]->raza,
                'seguro' => $datos_paciente[0]->seguro,
                'nombre' =>  $datos_paciente[0]->titular,
                'cedula2' => $datos_paciente[0]->cedula_titular,
                'direccion' => $datos_paciente[0]->dir_titular,
                'parentesco' => $datos_paciente[0]->parentesco,
                'telefono' => $datos_paciente[0]->tel_titular,

                'medico_tratante' => $datos_hospitalizacion[0]->medico ,
                'principal' => $datos_hospitalizacion[0]->principal,
                'asociado' => $datos_hospitalizacion[0]->diagnostico1,

                'area' => $datos_camas[0]->sala ,
                'cama' => $datos_camas[0]->tipo_cama,
                'unidad' => $datos_camas[0]->numero_camas,

                'numero' => 1,
                'medico_egreso' => '' , //$datos_medico_egreso[0]->nombres.' '.$datos_medico_egreso[0]->apellidos,
                'principal_egreso' => '', // $principal_egreso,
                'asociado_egreso' => '', // $asociado_egreso,
                'fecha_egreso' => '' ,// $fecha_egreso_array[0],
                'dias_estancia' => '' , // $egresos[0]->dias_trascurridos,
                'clinica' => '' , //$clinica,
                'cirugia' => '' , //$cirugia,
                'tipo_egreso' => '' , //$tipo_egreso,
                'hora_egreso' => '' , //$egresos[0]->hora_salida,
                'transaccion' =>$datos_hospitalizacion[0]->transaccion,
                'fecha_impresion' => $fecha_impresion,
                'hora_impresion' => $hora_impresion,
                'usuario_impresion' => $user->name,

            ];
            $datos = (object)$datos_egreso;
            //return view('Hospitalizacion.PDF.RptRegistroDeHospitalizacion1',compact('datos'));
            //$datos = (object)$datos_egreso;
            $pdf = \PDF::loadView('Hospitalizacion.PDF.RptRegistroDeHospitalizacion1',compact('datos'));
            return $pdf->stream();


        } catch (QueryException $e) {
            echo $e;
        }


    }

    public function garantia($hospitalizacion){

        try{
            $datos_hospitalizacion = $this->HospitalizacionnProcedure->consultarHospitalizacion(17453);
            $datos_paciente = $this->HospitalizacionnProcedure->consultarInformacionPaciente($datos_hospitalizacion[0]->Paciente);
            $datos_camas = $this->HospitalizacionnProcedure->consultarDatosCama($datos_hospitalizacion[0]->codigo_cama);
            $fecha_ingreso_array = explode(" ", $datos_hospitalizacion[0]->Fecha_de_Registro);

            $datos_egreso= [
                'historia_clinica' => $datos_hospitalizacion[0]->No_Historial,
                'fecha_ingreso' => $fecha_ingreso_array[0],
                'paciente' => $datos_paciente[0]->paciente,
                'cedula' => $datos_paciente[0]->cedula,
                'seguro' => $datos_paciente[0]->seguro,
                'direccion' => $datos_paciente[0]->dir_paciente,
                'telefono' => $datos_paciente[0]->tel_paciente,
                'telefono_medico' => $datos_hospitalizacion[0]->tel_medico ,
                'direccion_medico' => $datos_hospitalizacion[0]->dir_medico ,
                'lugar_trabajo' => $datos_paciente[0]->trabajo_paciente,
                'medico_tratante' => $datos_hospitalizacion[0]->medico ,
                'area' => $datos_camas[0]->sala ,
            ];


            $datos = (object)$datos_egreso;

            //dd($datos);
            //$pdf = \PDF::loadView('Hospitalizacion.PDF.RptGarantiaAdmision1',compact('datos'));
            return view('Hospitalizacion.PDF.RptGarantiaAdmision1',compact('datos'));
            //return $pdf->stream();

        } catch (QueryException $e) {
            echo $e;
        }


    }

    public function ReporteAdmision(Request $request) {

        //dd($request->all());
        $datos_paciente = $this->HospitalizacionnProcedure->consultarInformacionPaciente('147343');
        //dd($datos_paciente);



        if($datos_paciente[0]->tel_paciente == null || $datos_paciente[0]->tel_paciente== "." || $datos_paciente[0]->tel_paciente== ",")
        {
            if ($datos_paciente[0]->cel_paciente == null || $datos_paciente[0]->cel_paciente == "." || $datos_paciente[0]->cel_paciente == ",")
            {
                if ($datos_paciente[0]->otro_paciente == null || $datos_paciente[0]->otro_paciente == "." || $datos_paciente[0]->otro_paciente == ",")
                {
                    $telefono = "";
                } else {
                    $telefono = $datos_paciente[0]->otro_paciente;
                }
            } else {
                $telefono = $datos_paciente[0]->cel_paciente;
            }
        } else  {
            $telefono = $datos_paciente[0]->tel_paciente;
        }


           // $titular = $request->input('apellidos') ." ". $request->input('nombre');
           // $direccion2 = $request->input('Titulardireccion');
            if($datos_paciente[0]->tel_titular== null || $datos_paciente[0]->tel_titular== "." || $datos_paciente[0]->tel_titular== ",")
            {
                if ($datos_paciente[0]->cel_titular == null || $datos_paciente[0]->cel_titular == "." || $datos_paciente[0]->cel_titular == ",")
                {
                    $telefono2 = "";
                } else {
                    $telefono2 = $datos_paciente[0]->cel_titular;
                }
            } else  {
                $telefono2 = $datos_paciente[0]->tel_titular;
            }
            $nombre_array = [];
            $nombre_array = explode(" ",$datos_paciente[0]->paciente);
        $fecha_array = [];
        $fecha_array = explode(" ",$datos_paciente[0]->fecha_nacimiento);

        $cumpleanos = new \DateTime($fecha_array[0]);
        $hoy = new \DateTime();
        $annos = $hoy->diff($cumpleanos);

        //$url = Storage::url('RptAdmision001');
        //$url = Storage::url('RptAdmision001');
        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H') ;
        $minutos = date('i');
        $segundos = date('s');
        $fecha_impresion  = $dia."/".$mes."/".$year;

        $datosUser = [
            'id' => "147343",
            'parroquia' => $datos_paciente[0]->parroquia,
            'ciudad' => $datos_paciente[0]->ciudad,
            'provincia' => $datos_paciente[0]->provincia,
            'apellido_paterno' => $nombre_array[0],
            'apellido_materno' => $nombre_array[1],
            'primer_nombre' => $nombre_array[2],
            'segundo_nombre' => $nombre_array[3],
            'cedula' => $datos_paciente[0]->cedula,
            'direccion' => $datos_paciente[0]->dir_paciente,
            'telefono' => $telefono,
            'fecha_nacimiento' => $fecha_array[0],
            'lugar_nacimiento' => $datos_paciente[0]->lugar_nacimiento,
            'nacionalidad' => $datos_paciente[0]->pais,
            'etnia' => $datos_paciente[0]->raza,
            'edad' => $annos->y,
            'genero' => $datos_paciente[0]->ini_genero,
            'estado_civil' => $datos_paciente[0]->nun_estado_civil,
            'ocupacion' => $datos_paciente[0]->ocupacion,
            'lugar_trabajo' => $datos_paciente[0]->trabajo_paciente,
            'seguro' => $datos_paciente[0]->seguro,
            'instruccion' => $datos_paciente[0]->instruccion,
            'fecha_emision' => $fecha_impresion,
            'titular' => $datos_paciente[0]->titular,
            'parentesco' => $datos_paciente[0]->parentesco,
            'direccion2' => $datos_paciente[0]->dir_titular,
            'telefono2' => $telefono2,

        ];
        $datos = (object)$datosUser;
        /*<!-- referido --><!-- avisar --><!-- parentesco --><!-- dirrecion --><!-- telefono --><!-- admisionista --> */
        //dd($datos);

        $pdf = \PDF::loadView('pdf.admision001',compact('datos'));
       return $pdf->stream();
    }


}
