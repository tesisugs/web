<?php

namespace App\Http\Controllers\Hospitalizacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\HospitalizacionnProcedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class TraspasoController extends Controller
{
    protected $HospitalizacionnProcedure;


    public function __construct(HospitalizacionnProcedure $hospitalizacionProcedures)
    {
        $this->HospitalizacionnProcedure = $hospitalizacionProcedures;
    }
    public function  index(){
        return view('Hospitalizacion.TraspasoPacientes');
    }

    public function  insertTrasladoHospitalizacion(Request $request){

        $this->validate($request, [
            'hospitacion_id' => 'required', 'fecha_registro' => 'required', 'paciente_id' => 'required', 'n_unidad_anterior' => 'required',
            'observacion_traslado' => 'required', 'codigo_cama_traslado' => 'required',
        ],[
            'hospitacion_id.required' => 'Ingrese la información del paciente.',
            'fecha_registro.required' => 'Ingrese la información del paciente',
            'paciente_id.required' => 'Ingrese la información del paciente',
            'n_unidad_anterior.required' => 'Ingrese la información del paciente',
            'observacion_traslado.required' => 'Ingrese la información del traslado',
            'codigo_cama_traslado.required' => 'Ingrese la información de la nueva habitación del paciente'
        ]);


        $id = 0;
        $id_hospitalizacion = $request->input('hospitacion_id');
        $paciente_traspaso = $request->input('paciente_id');
        $fecha_ingreso = $request->input('fecha_registro');


        // ************calculo de  los dias y horas trascurridos**************

        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H');
        $minutos = date('i');
        $segundos = date('s');

        $fecha_registro =new \DateTime($fecha_ingreso);

        $fecha_actual = $year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';
        $fecha_fin = new \DateTime($fecha_actual);
        $interval = $fecha_registro->diff($fecha_fin);
        $dias_trasncurridos = (int) $interval->format('%d');
        $horas_trascurridas = (int) $interval->format('%h');

        $total_horas_trascurridas = (($dias_trasncurridos*24) + $horas_trascurridas) - 5;
        //echo $dias_trasncurridos;
        //echo "\n";
        //echo $total_horas_trascurridas;

        // ***********campos calculados*********************
        $dias_transcurridos= (string) $dias_trasncurridos;
        $horas_transcurridas= (string) $total_horas_trascurridas;
        // *********** fin campos calculados*********************

        // ************ campos desglozados **********
        $fecha_salida= $year.'-'.$mes.'-'.$dia.'';

        $fecha_array = [];
        $fecha_array= explode(' ',$fecha_ingreso);

        $hora_ingreso= $fecha_array[1];

        $hora_salida = $hora .':'.$minutos.':'.$segundos.'';
        //echo $hora_ingreso;
        // ************ campos desglozados **********
        $fecha_salida= $year.'-'.$mes.'-'.$dia.'';
        $hora_ingreso= $fecha_array[1];
        $hora_salida = $hora .':'.$minutos.':'.$segundos.'';
        $dias_transcurridos= (string) $dias_trasncurridos;
        $horas_transcurridas= (string) $total_horas_trascurridas;
         $valor = 0 ;

         $des_campo1 = "."  ;
         $des_campo2 = ".";
         $des_campo3 = 0 ;
         $user = Auth::user();
         $usuario_ingreso = $user->id;
         $usuario_modificacion = $user->id;
         $pcname = $_SERVER['REMOTE_ADDR'];
         $status =1;

         $observacion_traslado = $request->input('observacion_traslado');
         $codigo_cama_anterior = $request->input('n_unidad_anterior');
         $codigo_cama_traslado = $request->input('codigo_cama_traslado');
         $numero_traslado = 0;
         $secuencia = 0;

         $array = array(
            'id' => $id,
             'id_hospitalizacion ' =>$id_hospitalizacion ,
             'paciente_traspaso ' =>$paciente_traspaso ,
             'fecha_ingreso' =>$fecha_ingreso,

             'fecha_salida' => $fecha_salida,
             'hora_ingreso' => $hora_ingreso,
             'hora_salida ' => $hora_salida ,
             'dias_transcurridos' => $dias_transcurridos,
             'horas_transcurridas' => $horas_transcurridas,
             'valor' => $valor,


             'des_campo1' => $des_campo1 ,
             'des_campo2' =>$des_campo2 ,
             'des_campo3 ' =>$des_campo3 ,

             'usuario_ingreso ' =>$usuario_ingreso ,
             'usuario_modificacion ' =>$usuario_modificacion ,
             'pcname  ' =>$pcname  ,
             'status ' =>$status ,

             'observacion_traslado ' =>$observacion_traslado ,
             'codigo_cama_anterior ' =>$codigo_cama_anterior ,
             'codigo_cama_traslado ' =>$codigo_cama_traslado ,
             'numero_traslado ' =>$numero_traslado ,
             'secuencia ' =>$secuencia,

    );

        //dd($array);

        try{
            $datos = $this->HospitalizacionnProcedure->insertHospitalizacionTraspaso($id,$id_hospitalizacion ,$paciente_traspaso ,
                $fecha_ingreso , $fecha_salida  , $hora_ingreso , $hora_salida ,
                $dias_transcurridos , $horas_transcurridas , $valor , $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
                $usuario_modificacion,$pcname, $status, $observacion_traslado,$codigo_cama_anterior, $codigo_cama_traslado,
                $numero_traslado,$secuencia);
            return $datos;
        }catch (QueryException $e){
            return $e->errorInfo[1];
        }



      //if (empty($datos)) {
        //  return  redirect()->route('hospitalizacion.traspasopacientes');
      //} else {
        // return $datos;
     // }
    }



}
