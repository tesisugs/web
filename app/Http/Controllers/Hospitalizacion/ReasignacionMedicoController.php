<?php

namespace App\Http\Controllers\Hospitalizacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\HospitalizacionnProcedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
class ReasignacionMedicoController extends Controller
{
    protected $HospitaliazacionProcedure;


    public function __construct(HospitalizacionnProcedure $hospitalizacionProcedures)
    {
        $this->HospitaliazacionProcedure = $hospitalizacionProcedures;
    }
    
    public function  index(){
        return view('Hospitalizacion.ReasignacionMedico');
    }

    public function  consultarSala(){

        $datos = $this->HospitaliazacionProcedure->cmbAreas();
        return $datos;
    }

    public function  consultarPaciente($estado,$desde,$hasta,$paciente=''){
        $datos = $this->HospitaliazacionProcedure->consultarHospitalizacionfechas($paciente,$desde,$hasta,$estado);
        return $datos;
    }
    public function  consultarRegistroAdmision(){
        $datos = $this->HospitaliazacionProcedure->consultarRegistroAdmision();
         return $datos;
    }

    public function  consultarHospitalizacionId($id){
        $datos = [];
        $datos = $this->HospitaliazacionProcedure->consultarHospitalizacionId($id);
        return $datos;
    }

    public function updateIngresosHospitalizacionMedico(Request $request)
    {
        $this->validate($request, [
            'id' => 'required', 'Procedencias' => 'required', 'Nombre_Responsable' => 'required', 'Direccion_Responsable' => 'required',
            'Documento_Responsable' => 'required', 'Telefono_Responsable' => 'required','Observaciones' => 'required',
            'Servicios' => 'required','medico' => 'required','medico' => 'required','medico' => 'required',
        ],[
            'hospitacion_id.required' => 'Ingrese la información del paciente.',
            'fecha_registro.required' => 'Ingrese la información del paciente',
            'paciente_id.required' => 'Ingrese la información del paciente',
            'n_unidad_anterior.required' => 'Ingrese la información del paciente',
            'observacion_traslado.required' => 'Ingrese la información del traslado',
            'codigo_cama_traslado.required' => 'Ingrese la información de la nueva habitación del paciente'
        ]);
        $id = $request->input('id');
        $Procedencias = $request->input('Procedencias');
        $Nombre_Responsable = $request->input('Nombre_Responsable');
        $Direccion_Responsable = $request->input('Direccion_Responsable');
        $Documento_Responsable = $request->input('Documento_Responsable');
        $Telefono_Responsable = $request->input('Telefono_Responsable');
        $Observaciones = $request->input('Observaciones');
        $Servicios = $request->input('Servicios');
        $medico = $request->input('medico');
        $principal = $request->input('principal');
        $asociado_1 = $request->input('asociado_1');
        $asociado_2 = $request->input('asociado_2');
        $Medico_Observacion = $request->input('Medico_Observacion');

        $fecha_modificacion  = '1900/01/01';
        $user = Auth::user();

        $usario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];

        $derivacion  = (integer) $request->input('derivacion');
        $hospital = (integer) $request->input('hospital');
        $status_documento = (integer) $request->input('status_documento');
        $observacion_documento = $request->input('observacion_documento');
        $datos = $this->HospitaliazacionProcedure->updateIngresosHospitalizacion(
            $id,
            $Procedencias, $Nombre_Responsable,$Direccion_Responsable,
            $Documento_Responsable,$Telefono_Responsable, $Observaciones,
            $Servicios,$medico,$principal,$asociado_1,$asociado_2,$Medico_Observacion
            ,$fecha_modificacion,$usario_ingreso,$pcname,0,$hospital,
            0, 0);
        return $datos;

    }

    public function updateIngresosHospitalizacionSeguro(Request $request)
    {
        //  dd($request);
        $id = $request->input('id');
        $Procedencias = $request->input('Procedencias');
        $Nombre_Responsable = $request->input('Nombre_Responsable');
        $Direccion_Responsable = $request->input('Direccion_Responsable');
        $Documento_Responsable = $request->input('Documento_Responsable');
        $Telefono_Responsable = $request->input('Telefono_Responsable');
        $Observaciones = $request->input('Observaciones');
        $Servicios = $request->input('Servicios');
        $medico = $request->input('medico');
        $principal = $request->input('principal');
        $asociado_1 = $request->input('asociado_1');
        $asociado_2 = $request->input('asociado_2');
        $Medico_Observacion = $request->input('Medico_Observacion');

        $fecha_modificacion  = '1900/01/01';
        $user = Auth::user();

        $usario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];

        $derivacion  = (integer) $request->input('derivacion');
        $hospital = (integer) $request->input('hospital');
        $status_documento = (integer) $request->input('status_documento');
        $observacion_documento = $request->input('observacion_documento');

        $datos = $this->HospitaliazacionProcedure->updateIngresosHospitalizacion(
            $id,
            $Procedencias, $Nombre_Responsable,$Direccion_Responsable,
            $Documento_Responsable,$Telefono_Responsable, $Observaciones,
            $Servicios,$medico,$principal,$asociado_1,$asociado_2,$Medico_Observacion
            ,$fecha_modificacion,$usario_ingreso,$pcname,0,$hospital,
            0, 0);
        return $datos;

    }

    public function updateHospitalizacionSeguro(Request $request) {

        $this->validate($request, [
            'id' => 'required', 'Procedencias' => 'required',
        ],[
            'id.required' => 'Ingrese la información del paciente.',
            'Procedencias.required' => 'Ingrese el nuevo seguro',
        ]);

        //dd($request->all());

        $id = $request->input('id');
        $fecha_hora_ingreso = '1900/01/01';
        $user = Auth::user();
        $usuario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];
        $status = 3;
        $Procedencias = $request->input('Procedencias');
        //dd($request->all());
        try{
            $datos = $this->HospitaliazacionProcedure->updateHospitalizacionSeguro(
                $id,$fecha_hora_ingreso, $usuario_ingreso,$pcname, $status,$Procedencias);
            return $datos;
        }catch (QueryException $e){
            return $e->errorInfo[1];
        }
    }

    public function updateHospitalizacionMedico(Request $request) {

        $this->validate($request, [
            'id' => 'required', 'Servicios' => 'required', 'medico' => 'required',
        ],[
            'id.required' => 'Ingrese la información del paciente.',
            'Servicios.required' => 'Ingrese el servicio del médico',
            'medico.required' => 'Ingrese el nombre del médico',
        ]);



        $id = $request->input('id');
        $fecha_hora_ingreso = '1900/01/01';
        $user = Auth::user();
        $usuario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];
        $status = $request->input('status');
        $Servicios = $request->input('Servicios');
        $medico = $request->input('medico');

        try{
            $datos = $this->HospitaliazacionProcedure->updateHospitalizacionMedico(
                $id,$fecha_hora_ingreso, $usuario_ingreso,$pcname, $status,$medico,$Servicios);
            return $datos;
        }catch (QueryException $e){
            return $e->errorInfo[1];
        }

    }


}
