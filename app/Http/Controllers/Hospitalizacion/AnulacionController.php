<?php

namespace App\Http\Controllers\Hospitalizacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\HospitalizacionnProcedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class AnulacionController extends Controller
{
    public function __construct(HospitalizacionnProcedure $hospitalizacionProcedures)
    {
        $this->HospitalizacionnProcedure = $hospitalizacionProcedures;
    }

    public function  index(){
        return view('Hospitalizacion.AnulacionPacientes');
    }

    public function SpAnulacionHospitalizacion(Request $request)
    {
        $this->validate($request, [
            'observacion_eliminada' => 'required', 'id' => 'required', 'paciente_id' => 'required', 'N_unidad_asignada' => 'required',
        ],[
            'observacion_eliminada.required' => 'La observación de la eliminación es requerida.',
            'id.required' => 'Ingrese la información del paciente',
            'paciente_id.required' => 'Ingrese la información del paciente',
            'N_unidad_asignada.required' => 'Ingrese la información del paciente',
        ]);

        $id= $request->input('id');;
        $id_hospitalizacion = $request->input('id');
        $paciente  = $request->input('paciente_id');
        $observacion = "."; //$request->input('observacion_eliminada');
        $user = Auth::user();
        $usuario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];
        $fecha_modificacion= '1900/01/01';
        $codigo= (integer) $request->input('N_unidad_asignada');
        $status_camas= $request->input('status_camas');
        $ocupante= (integer) $request->input('paciente_id');
        $usuario_modificacion =  $user->id;
        $observacion_eliminada = $request->input('observacion_eliminada');

        try{
            $datos = $this->HospitalizacionnProcedure
                ->SpAnulacionHospitalizacion($id, $id_hospitalizacion,$paciente, $observacion, $usuario_ingreso,
                    $fecha_modificacion,$codigo, $status_camas, $ocupante,
                    $usuario_modificacion, $observacion_eliminada,$pcname);
            return $datos;
        }catch(QueryException $e){
            return $e->errorInfo[1];
        }

    }
}
