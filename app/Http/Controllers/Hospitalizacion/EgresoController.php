<?php

namespace App\Http\Controllers\Hospitalizacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\HospitalizacionnProcedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;


class EgresoController extends Controller
{
    protected $HospitalizacionnProcedure;


    public function __construct(HospitalizacionnProcedure $hospitalizacionProcedures)
    {
        $this->HospitalizacionnProcedure = $hospitalizacionProcedures;
    }
    public function  index(){
        return view('Hospitalizacion.EgresoPacientes');
    }

    public function estadosHospitalizacion() {

        $datos = $this->HospitalizacionnProcedure->cmbEstadosHospitalizacion();
        return $datos;
//        return json_encode($datos);
    }

    public function hospitalizacionFechas($estado,$desde,$hasta,$paciente='') {

        $datos = $this->HospitalizacionnProcedure->consultarHospitalizacionfechas($paciente,$desde,$hasta,$estado);
         return $datos;
    }
    public function hospitalizacionFechasEgreso ($desde,$hasta,$paciente='') {

        $datos = $this->HospitalizacionnProcedure->consultarHospitalizacionfechasEgreso($paciente,$desde,$hasta);
        return $datos;
    }


    public function tipoEgreso(){
        $datos = $this->HospitalizacionnProcedure->ConsultarTipoEgreso();
        return $datos;

    }

    public function tipoEgresoAlta(){
        $datos = $this->HospitalizacionnProcedure->consultarTipoEgresoAlta();
        return $datos;
    }

    public function tipoDiagnosticoNombre($descripcion){
        $datos = $this->HospitalizacionnProcedure->consultarTipoDiagnosticoNombre($descripcion);
        return $datos;

    }

    public function  insertHospitalizacionEgresos(Request $request) {

        $this->validate($request, [
            'id_hospitalizacion' => 'required', 'medico' => 'required', 'principal' => 'required', 'asociado1' => 'required',
            'asociado2' => 'required', 'cmbTipoEgreso' => 'required', 'complicacion' => 'required',
            'tipo_complicacion' => 'required', 'fechaingreso' => 'required', 'listaTipoAlta' => 'required',
            'cama' => 'required', 'procedencia' => 'required', 'observacion' => 'required',
        ],[
            'id_hospitalizacion.required' => 'La Información de la hospitalización es necesaria',
            'medico.required' => 'La información del médico de egreso es requerida',
            'principal.required' => 'El diagnostico proncipal es requerido',
            'asociado1.required' => 'El diagnostico asociado es requerido', 'asociado2.required' => 'El diagnostico asociado 2 es requerido',
            'cmbTipoEgreso.required' => 'El tipo de egreso es requerido', 'complicacion.required' => 'El tipo de complicacion es requerida',
            'tipo_complicacion.required' => 'El tipo de complicación es requerido',
            'fechaingreso.required' => 'La fecha de el ingreso es requerida',
            'listaTipoAlta.required' => 'El tipo de alta es requerido',
            'cama.required' => 'El numero de cama es requerido',
            'procedencia' => 'La información del paciente es requerida',
            'observacion.required' => 'la observación del medico es requerida',
        ]);

        $id= 0;
        $id_hospitalizacion= (integer) $request->input('id_hospitalizacion');
        $medico_egreso= (integer)  $request->input('medico');
        $principal_egreso= $request->input('principal');
        $asociadoEgreso1= $request->input('asociado1');
        $asociadoEgreso2= $request->input('asociado2');
        $tipo_egreso= (integer) $request->input('cmbTipoEgreso');
        $complicacion= (integer) $request->input('complicacion');
        $tipo_complicacion= (integer) $request->input('tipo_complicacion');
        $fecha_ingreso= $request->input('fechaingreso');
        $tipo_egreso_alta= (integer) $request->input('listaTipoAlta');
        $codigo_cama= (integer) $request->input('cama');
        $status_camas= "1";
        $Procedencias= $request->input('procedencia');
        $valor= "0";
        $des_campo1= ".";
        $des_campo2= ".";
        $des_campo3= 0;
        $user = Auth::user();
        $usuario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];
        $status= 1;
        $observacion = $request->input('observacion');
        // ************calculo de  los dias y horas trascurridos**************
        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H') ;
        $minutos = date('i');
        $segundos = date('s');
        $fecha_registro =new \DateTime($fecha_ingreso);
        $fecha_actual = $year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';
        $fecha_fin = new \DateTime($fecha_actual);
        $interval = $fecha_registro->diff($fecha_fin);
        $dias_trasncurridos = (int) $interval->format('%d');
        $horas_trascurridas = (int) $interval->format('%h');
        $total_horas_trascurridas = (($dias_trasncurridos*24) + $horas_trascurridas) - 5;
        //echo $dias_trasncurridos;
        //echo "\n";
        //echo $total_horas_trascurridas;
        // ***********campos calculados*********************
        $dias_transcurridos= (string) $dias_trasncurridos;
        $horas_transcurridas= (string) $total_horas_trascurridas;
        // *********** fin campos calculados*********************
        // ************ campos desglozados **********
        $fecha_salida= '1900/01/01';
        $fecha_array = [];
        $fecha_array= explode(' ',$fecha_ingreso);
        $hora_ingreso= $fecha_array[1];
        $hora_salida = $hora .':'.$minutos.':'.$segundos.'';
        //echo $hora_ingreso;
        // ************ campos desglozados **********

        $datos_p = array( 'id' =>
            $id,
            'id_hospitalizacion' => $id_hospitalizacion,
            'medico_egreso' => $medico_egreso,
            'principal_egreso' => $principal_egreso,
            'asociadoEgreso1' =>$asociadoEgreso1,
            'asociadoEgreso2' =>$asociadoEgreso2,
            'tipo_egreso' =>$tipo_egreso,
            'complicacion' =>$complicacion,
            'tipo_complicacion' =>$tipo_complicacion,
            'fecha_ingreso' =>$fecha_ingreso,
            'fecha_salida' =>$fecha_salida,
            'hora_ingreso' =>$hora_ingreso,
            'hora_salida' =>$hora_salida,
            'tipo_egreso_alta' =>$tipo_egreso_alta,
            'codigo_cama' => $codigo_cama,
            'status_camas' => $status_camas,
            'Procedencias' => $Procedencias,
            'dias_transcurridos' =>$dias_transcurridos,
            'horas_transcurridas' =>$horas_transcurridas,'valor' =>$valor,'des_campo1' =>$des_campo1,
            'des_campo2' =>$des_campo2,'des_campo3' =>$des_campo3,
            'usuario_ingreso' =>$usuario_ingreso,'pcname' =>$pcname,'status' =>$status,'observacion' =>$observacion
        );


        try {
            $datos = $this->HospitalizacionnProcedure->InsertHospitalizacionEgresos($id,$id_hospitalizacion,$medico_egreso,$principal_egreso,
                $asociadoEgreso1,$asociadoEgreso2,$tipo_egreso,$complicacion,$tipo_complicacion,$fecha_ingreso,
                $fecha_salida,$hora_ingreso,$hora_salida,$tipo_egreso_alta,$codigo_cama,$status_camas,
                $Procedencias,$dias_transcurridos,$horas_transcurridas,$valor,$des_campo1,$des_campo2,$des_campo3,
                $usuario_ingreso,$pcname,$status,$observacion);

            //return 0;
            if (isset($datos[0]->_id)) {
                $id = $datos[0]->_id;
                $resultado = array( 'id' => $id);
                return json_encode($resultado);
            } else {
                $error = 0;
                return $error;
            }

        }catch(QueryException $e){
            return $e->errorInfo[1];
        }


    }

    public function imprimirTicket(Request $request){

        dd($request);

        $datos_consulta = \DB::table('tbconsultaexternaturnosdiarios')->select('turnos','consulta_externa')
            ->where('turnos_diarios',$request->input('id'))->get();

        $paciente = $request->input('paciente');
        $medico = \DB::table('tbmedico')->select('nombres','apellidos')->where('id',$request->input('medico'))->get();
        $admision =  \DB::table('tbTipoingreso')->where('codigo',$request->input('seguro'))->value('descripcion');
        $medico = $medico[0]->nombres.' '.$medico[0]->apellidos;
        $especialidad = \DB::table('tbespecializacion')->select('descripcion')->where('codigo',$request->input('especialidad'))->get();
        $especialidad = $especialidad[0]->descripcion;
        $tipo_consulta = \DB::table('tbtipoconsultaexterna')->where('codigo',$request->input('tipo_consulta'))->value('descripcion');
        $consultorio = "C".\DB::table('tbCamasPorSala')->where('codigo',(\DB::table('tbMedico')->where('id',41)->value('consultorio_default')))->value('numero_camas');
        $turno = $datos_consulta[0]->turnos;
        $fecha = $request->input('fecha').' '.$request->input('hora');

        return view('ConsultaExterna.ticket.turnos', compact('paciente','medico','especialidad','admision','tipo_consulta','consultorio','turno','fecha'));
    }


    public function imprimirEgreso($hospitalizacion,$egreso){

        try{

            $datos_hospitalizacion = $this->HospitalizacionnProcedure->consultarHospitalizacion($hospitalizacion);
            $datos_paciente = $this->HospitalizacionnProcedure->consultarInformacionPaciente($datos_hospitalizacion[0]->Paciente);
            $datos_camas = $this->HospitalizacionnProcedure->consultarDatosCama($datos_hospitalizacion[0]->codigo_cama);
            $fecha_nacimiento_array = explode(" ", $datos_paciente[0]->fecha_nacimiento);
            $fecha_ingreso_array = explode(" ", $datos_hospitalizacion[0]->Fecha_de_Registro);


            /*-------------------------------------------------------*/
            list($Y,$m,$d) = explode("-",$fecha_nacimiento_array[0]);
            $edad = ( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
            date_default_timezone_set('America/Lima');
            $year = date('Y');
            $mes = date('m');
            $dia = date('d');
            $hora= date('H') ;
            $minutos = date('i');
            $segundos = date('s');
            $fecha_impresion  = $dia."/".$mes."/".$year;
            $hora_impresion =   $hora."-".$minutos."-".$segundos;
            $user = Auth::user();
            /*-------------------------------------------------------*/

            $datos_egreso= [
                'historia_clinica' => $datos_hospitalizacion[0]->No_Historial,
                'fecha_ingreso' => $fecha_ingreso_array[0],
                'paciente' => $datos_paciente[0]->paciente,
                'cedula' => $datos_paciente[0]->cedula,
                'fecha_nacimiento' => $fecha_nacimiento_array[0],
                'edad' => $edad,
                'estado_civil' => $datos_paciente[0]->estado_civil,
                'genero' => $datos_paciente[0]->genero  ,
                'observacion' => $datos_paciente[0]->observacion,
                'discapacidad' => $datos_paciente[0]->status_discapacidad,
                'conadis' => $datos_paciente[0]->carnet_conadis,
                'etnia' => $datos_paciente[0]->raza,
                'seguro' => $datos_paciente[0]->seguro,
                'nombre' =>  $datos_paciente[0]->titular,
                'cedula2' => $datos_paciente[0]->cedula_titular,
                'direccion' => $datos_paciente[0]->dir_titular,
                'parentesco' => $datos_paciente[0]->parentesco,
                'telefono' => $datos_paciente[0]->tel_titular,

                'medico_tratante' => $datos_hospitalizacion[0]->medico ,
                'principal' => $datos_hospitalizacion[0]->principal,
                'asociado' => $datos_hospitalizacion[0]->diagnostico1,

                'area' => $datos_camas[0]->sala ,
                'cama' => $datos_camas[0]->tipo_cama,
                'unidad' => $datos_camas[0]->numero_camas,

                'numero' => 1,
                'medico_egreso' => '' , //$datos_medico_egreso[0]->nombres.' '.$datos_medico_egreso[0]->apellidos,
                'principal_egreso' => '', // $principal_egreso,
                'asociado_egreso' => '', // $asociado_egreso,
                'fecha_egreso' => '' ,// $fecha_egreso_array[0],
                'dias_estancia' => '' , // $egresos[0]->dias_trascurridos,
                'clinica' => '' , //$clinica,
                'cirugia' => '' , //$cirugia,
                'tipo_egreso' => '' , //$tipo_egreso,
                'hora_egreso' => '' , //$egresos[0]->hora_salida,
                'transaccion' =>$datos_hospitalizacion[0]->transaccion,
                'fecha_impresion' => $fecha_impresion,
                'hora_impresion' => $hora_impresion,
                'usuario_impresion' => $user->name,

            ];
            $datos = (object)$datos_egreso;
            return view('Hospitalizacion.PDF.RptRegistroDeHospitalizacion1',compact('datos'));
            //$datos = (object)$datos_egreso;
            //$pdf = \PDF::loadView('Hospitalizacion.PDF.RptRegistroDeHospitalizacion1',compact('datos'));
            //return $pdf->stream();


        } catch (QueryException $e) {
            echo $e;
        }


    }

    public function garantia($hospitalizacion,$egreso){

        try{

            $result = \DB::table('tbhospitalizacion')
                ->select('id','SecuenciaHO','No_Historial','fecha_nacimiento','Fecha_de_Registro','Paciente','habitacion',
                    'Procedencias','medico','principal','asociado_1','fecha_egreso',
                    'medico_egreso','principal_egreso','asociadoEgreso1','tipo_egreso',
                    'codigo_cama','transaccion','titular','principal_egreso','asociadoEgreso1','Normal','Cirugia_Dia')
                ->where('id',$hospitalizacion)->get();
            //dd($result);


                if($result[0]->Normal === 1){
                    $clinica = "X";
                } else {
                    $clinica = "";
                }

            if($result[0]->Cirugia_Dia === 1){
                $cirugia = "X";
            } else {
                $cirugia = "";
            }

            $datosPaciente = \DB::table('tbpaciente')->select('cedula','genero','estado_civil','status_discapacidad','carnet_conadis','etnico',
                'primer_nombre','segundo_nombre','apellido_paterno','apellido_materno','observacion','cedula_titular','tipo_parentesco','lugar_trabajo')
                ->where('id',$result[0]->Paciente)->get();

            $datosCama = \DB::table('tbcamasporsala')->select('numero_camas','descripcion_camas','sala')
                ->where('codigo',$result[0]->codigo_cama)->get();



            $datosResponsable = \DB::table('tbpaciente')->select('primer_nombre','segundo_nombre','apellido_paterno','apellido_materno',
                'direccion','telefono')
                ->where('cedula',$datosPaciente[0]->cedula_titular)->get();

            $egresos = \DB::table('tbegresos')->select('id','dias_trascurridos','horas_transcurridas','fecha_salida','hora_salida')
                ->where('id',$egreso)->get();



            $principal = \DB::table('tbdiagnostico')->where('codigo',$result[0]->principal)->value('descripcion');
            $asociado = \DB::table('tbdiagnostico')->where('codigo',$result[0]->asociado_1)->value('descripcion');
            $principal_egreso = \DB::table('tbdiagnostico')->where('codigo',$result[0]->principal_egreso)->value('descripcion');
            $asociado_egreso = \DB::table('tbdiagnostico')->where('codigo',$result[0]->asociadoEgreso1)->value('descripcion');
            $seguro = \DB::table('tbTipoingreso')->where('codigo',$result[0]->Procedencias)->value('descripcion');
            $etnia = \DB::table('tbTipoCultura')->where('raza',$datosPaciente[0]->etnico)->value('descripcion');
            $estado_civil = \DB::table('tbestadocivil')->where('codigo',$datosPaciente[0]->estado_civil)->value('descripcion');
            $parentesco = \DB::table('tbparentesco')->where('codigo',$datosPaciente[0]->tipo_parentesco)->value('descripcion');
            $datos_medico =  \DB::table('tbmedico')->select('nombres','apellidos')->where('id',$result[0]->medico)->get();
            $genero = \DB::table('tbgenero')->where('abreviatura',$datosPaciente[0]->genero)->value('descripcion');

            $datos_medico_egreso =  \DB::table('tbmedico')->select('nombres','apellidos')->where('id',$result[0]->medico_egreso)->get();
            $sala = \DB::table('tbsala')->where('codigo',$datosCama[0]->sala)->value('descripcion');
            $cama = \DB::table('tbtipocama')->where('codigo',$datosCama[0]->descripcion_camas)->value('descripcion');
            $tipo_egreso =  \DB::table('tbtipoegreso')->where('codigo',$result[0]->tipo_egreso)->value('descripcion');


            $fecha_nacimiento_array = explode(" ", $result[0]->fecha_nacimiento);
            $fecha_ingreso_array = explode(" ", $result[0]->Fecha_de_Registro);
            $fecha_egreso_array = explode(" ",$result[0]->fecha_egreso);

            list($Y,$m,$d) = explode("-",$fecha_nacimiento_array[0]);
            $edad = ( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );


            $datos_egreso= [
                'historia_clinica' => $result[0]->No_Historial,
                'fecha_ingreso' => $fecha_ingreso_array[0],
                'paciente' => $datosPaciente[0]->primer_nombre.' '.$datosPaciente[0]->segundo_nombre.' '.$datosPaciente[0]->apellido_paterno.' '.$datosPaciente[0]->apellido_materno,
                'cedula' => $datosPaciente[0]->cedula,
                'fecha_nacimiento' => $fecha_nacimiento_array[0],
                'edad' => $edad,
                'estado_civil' => $estado_civil,
                'genero' => $genero,
                'observacion' => $datosPaciente[0]->observacion,
                'discapacidad' => $datosPaciente[0]->status_discapacidad,
                'conadis' => $datosPaciente[0]->carnet_conadis,
                'etnia' => $etnia,
                'seguro' => $seguro,
                'nombre' => $datosResponsable[0]->primer_nombre.' '.$datosResponsable[0]->segundo_nombre.' '.$datosResponsable[0]->apellido_paterno.' '.$datosResponsable[0]->apellido_materno,
                'cedula2' => $datosPaciente[0]->cedula_titular,
                'direccion' => $datosResponsable[0]->direccion,
                'parentesco' => $parentesco,
                'telefono' => $datosResponsable[0]->telefono,
                'medico_tratante' => $datos_medico[0]->nombres.' '.$datos_medico[0]->apellidos,
                'principal' => $principal,
                'asociado' => $asociado,
                'area' => $sala,
                'unidad' => $datosCama[0]->numero_camas,
                'numero' => 1,
                'medico_egreso' => $datos_medico_egreso[0]->nombres.' '.$datos_medico_egreso[0]->apellidos,
                'principal_egreso' => $principal_egreso,
                'asociado_egreso' => $asociado_egreso,
                'fecha_egreso' => $fecha_egreso_array[0],
                'dias_estancia' => $egresos[0]->dias_trascurridos,
                'clinica' => $clinica,
                'cirugia' => $cirugia,
                'tipo_egreso' => $tipo_egreso,
                'hora_egreso' => $egresos[0]->hora_salida,
                'transaccion' =>005,
                'lugar_trabajo' => $datosPaciente[0]->lugar_trabajo

            ];


            $datos = (object)$datos_egreso;

            return view('Hospitalizacion.PDF.RptGarantiaAdmision1',compact('datos'));
            //$pdf = \PDF::loadView('Hospitalizacion.PDF.RptGarantiaAdmision1',compact('datos'));
            //return $pdf->stream();

        } catch (QueryException $e) {
            echo $e;
        }


    }

}
