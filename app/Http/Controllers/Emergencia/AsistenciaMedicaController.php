<?php

namespace App\Http\Controllers\Emergencia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\EmergenciasProcedure;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;
//use Brian2694\Toastr\Toastr;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;

class AsistenciaMedicaController extends Controller
{
    //
    protected $EmergenciaProcedure;
    protected $CuentasPorCobrar;


    public function __construct(EmergenciasProcedure $emergenciaProcedures)
    {
        $this->EmergenciaProcedure = $emergenciaProcedures;
    }

    public function index() {
        return view('emergencia.AsistenciaMedica');
    }

    public function  consultarAtencionPaciente($pacienteId){
        $datos = $this->EmergenciaProcedure->consultarAtencionPaciente($pacienteId);
        return json_encode($datos);
    }

    public function cmbMedico() {
        $datos = $this->EmergenciaProcedure->CmbMedico();
        return json_encode($datos);
    }

    public function ConsultarBeneficiarioTitularAfiliado($cedula)
    {
        $datos = $this->EmergenciaProcedure->ConsultarBeneficiarioTitularAfiliado($cedula);
        return json_encode($datos);
    }
        public function ConsultarRegistroAdmision() {
            $datos = $this->EmergenciaProcedure->ConsultarRegistroAdmision();
            return json_encode($datos);
        }

    public function ConsultarPacienteId($id)
    {
        $datos = $this->EmergenciaProcedure->ConsultarPacienteId($id);
        return json_encode($datos);
    }
    public function Consultarfirmas($atencion,$servicio,$visita,$documentos)
    {
        $datos = $this->EmergenciaProcedure->Consultarfirmas($atencion,$servicio,$visita,$documentos);
        return json_encode($datos);
    }
    public function cmbParentesco()
    {
        $datos = $this->EmergenciaProcedure->cmbParentesco();
        return json_encode($datos);
    }

    public function  StoreAsistencia(Request $request) {
       //return dd($request);
        $request->validate([
            'afiliado' => 'required|max:30',
        ],['afiliado.required' => 'El nombre del afiliado es requerido',
        ]);

        //dd($request);
        $id = 0;
        $afiliado = $request->input('afiliado');
        $paciente = $request->input('paciente');
        $fecha = '1900/02/02';
        $hora = 'hora';
        $responsable = $request->input('responsable');
        $observacion = $request->input('observacion');
        $doctor = $request->input('doctor');
        $user = Auth::user();
        $usuario_ingreso = $user->id;
        $pc_name = $_SERVER['REMOTE_ADDR'];;
        $tipo_ingreso = $request->input('seguro');
        $n_atencion = 0 ;
        $campo_3 = ".";
        $fuente_informacion = $request->input('fuente_informacion ');
        $persona_entrega = $request->input('dnombre_a_a');
        $derivacion = ($request->input('derivacion')== true)? 1 : null;
        $hospital = ($request->input('hospital') === 0)? null : $request->input('hospital');
        $cedula_entrega = $request->input('dcedul_a');
        $tipo_acompanante = $request->input('parentesco');

        $result = $this->EmergenciaProcedure->inserRegistroAdmision($id,$afiliado,$paciente,$fecha,$hora,
                    $responsable,$observacion,$doctor,$usuario_ingreso,$pc_name,$tipo_ingreso,
                    $n_atencion,$campo_3,$fuente_informacion,$persona_entrega,$derivacion,
                    $hospital,$cedula_entrega,$tipo_acompanante);

        //dd($result);
        //$resultado = json_decode($result);

        $id_e = $result[0]->_id;
        $atencion_ = $result[0]->_numero_atencion;

        //echo $id_e;
        //echo $nt;
        //dd($resultado);
        //dd($result);
        //flash()->overlay('Registro Guardado con exito. No_atención:'.$nt.'', 'Exito');
        //Toastr::info('Messages in here', 'Title', ["positionClass" => "toast-top-center"]);
        //return redirect()->route('emergencia.registropacientes',compact('id_e','nt'));
        $resultado = array( 'id' => $id_e, 'atencion' => $atencion_);
        return json_encode($resultado);

        //return view('emergencia.AsistenciaMedica');

/*
        $id_e = 5;
        $nt = 10;

        $array = ["id_e" => $id_e,
            "nt" => $nt];

        return ($array);
*/
    }

    public function  consultarpacienteAdmision() {
        $datos = $this->EmergenciaProcedure->consultarIngresosPacientesAdmision();
        return $datos;
    }



    public function printActaServicio (Request $request) {
         //dd($request->all());

        $datos = $this->EmergenciaProcedure->ConsultarRegistroAdmision($request->input('admision')) ;
        $datos2 = $this->EmergenciaProcedure->ConsultarPacienteId($request->input('id_paciente'));
       // $firma = $this->EmergenciaProcedure->Consultarfirmas(94062,2,0,5);

        $fecha = $datos[0]->fecha;
        date_default_timezone_set('America/Lima');
        setlocale(LC_ALL,"es_ES");

        $fecha_array = [];
        $fecha_array= explode('-',$fecha);
        $year = $fecha_array[0];
        $mes = strftime('%B',strtotime($fecha));
        $dia = $fecha_array[2];
        $dia_array = [];
        $dia_array= explode(' ',$dia);
        $dia = $dia_array[0];

        $datosConsulta = [
           'mes_year' => $mes."/".$year,
            'codigo' => "",
            'hc' => $request->input('id_paciente'),
            'servicio' => "",
            'codigo_cie' => "",
            'cedula' => $datos2[0]->cedula,
            'beneficiario' => $request->input('nombre_paciente'),
            'observacion' => $request->input('observacion'),
            'dia' => $dia,
            'mes' => $mes,
            'year' => $year,
            'afinidad'  => $request->input('nombre_afinidad'),
            'responsable' => $request->input('nombre_responsable'),
        ];

        $datos = (object)$datosConsulta;
        //echo strftime('%B',strtotime($fecha));
        $pdf = \PDF::loadView('emergencia.Pdf.ActaDeServicio',compact('datos'));
        return $pdf->stream();
    }

    public  function  prueba($id) {
       dd ($this->EmergenciaProcedure->prueba()) ;
        //dd($datos);
       //dd( DB::select('exec prueba '));
    }
}
