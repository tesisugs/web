<?php namespace App\Http\Controllers\Emergencia;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Core\Procedures\EmergenciasProcedure;
use App\Core\Procedures\CuentasPorCobrarProcedure;
//use Codedge\Fpdf\Facades\Fpdf; el facade no funciono como debía
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade;
use Brian2694\Toastr\Facades;
use Brian2694\Toastr;




class RegistroPacientesController extends Controller {

    protected $EmergenciaProcedure;
    protected $CuentasPorCobrar;
    

    public function __construct(EmergenciasProcedure $emergenciaProcedures, CuentasPorCobrarProcedure $cuentasPorCobrarProcedure)
    {
        $this->EmergenciaProcedure = $emergenciaProcedures;
        $this->CuentasPorCobrar = $cuentasPorCobrarProcedure;
    }

    public function index($id = null) {
        return view('emergencia.registroPacientes',compact('id'));
    }

    public function  prueba(){
        $prueba = $this->EmergenciaProcedure->getPrueba();

        return json_encode($prueba);
    }

    public function TitularAfiliadoPorApellido($apellido){

        $datos = $this->EmergenciaProcedure->ConsultarTitularAfiliadoPorApellidoAll($apellido);
        //return view('tables.titularApellido',compact('datos'));

        return json_encode($datos) ;
    }

    public function ConsultaCedula($cedula){

        $datos = $this->EmergenciaProcedure->ConsultarTitularAfiliadoPorCedula($cedula);
        //return view('tables.titularApellido',compact('datos'));

        return $datos;
    }

    public function ConsultaCedulaAll($cedula){

        $datos = $this->EmergenciaProcedure->ConsultarTitularAfiliadoPorCedulaAll($cedula);
        //return view('tables.titularApellido',compact('datos'));

        return $datos;
    }

    public function cmbTipoIdentificacion() {
        $datos = $this->EmergenciaProcedure->cmbTipoIdentificacion();
        return json_encode($datos);
    }

    public function cmbGeneroPaciente() {
        $datos = $this->EmergenciaProcedure->cmbGeneroPaciente();
        return json_encode($datos);
    }

    public function cmbNacionalidad() {
        $datos = $this->EmergenciaProcedure->cmbNacionalidad();
        return json_encode($datos);
    }

    public function cmbParentescoPaciente() {
        $datos = $this->EmergenciaProcedure->cmbParentescoPaciente();
        return json_encode($datos);
    }

    public function cmbTipoSangre() {
        $datos = $this->EmergenciaProcedure->cmbTipoSangre();
        return json_encode($datos);
    }

    public function cmbEstadoCivil() {
        $datos = $this->EmergenciaProcedure->cmbEstadoCivil();
        return json_encode($datos);
    }

    public function cmbEtnia() {
        $datos = $this->EmergenciaProcedure->cmbEtnia();
        return json_encode($datos);
    }


    function cmbProvincia($id) {
        $datos = $this->EmergenciaProcedure->cmbProvincia($id);
        return json_encode($datos);
    }

    public function cmbCiudad($id) {
        $datos = $this->EmergenciaProcedure->cmbCiudad($id);
        return json_encode($datos);
    }

    public function cmbParroquia($id) {
        if ($id == null)
        {
            $id = 1;
            $datos = $this->EmergenciaProcedure->cmbParroquia($id);
            return json_encode($datos);
        } else {

            $datos = $this->EmergenciaProcedure->cmbParroquia($id);
            return json_encode($datos);
        }

    }

    public function cmbSeguro() {
        $datos = $this->EmergenciaProcedure->cmbSeguro();
        return $datos;
    }

    public function cmbOtroSeguro() {
        $datos = $this->EmergenciaProcedure->cmbOtroSeguro();
        return json_encode($datos);
    }

    public function ConsultarInformacionAdicional($id_paciente) {
        $datos = $this->EmergenciaProcedure->ConsultarInformacionAdicional($id_paciente);
        return json_encode($datos);
    }


    public function cmbBeneficiarioPorParentesco($parentesco) {
        $datos = $this->EmergenciaProcedure->CmbIdentificacionBeneficiarioPorParentesco($parentesco);
        return json_encode($datos);
    }

    public function RegistroRapido(Request $request) {
        //dd($request);
        // tomar variables de entrada
        $id = $request->input('id');
        $apellido_paterno = $request->input('apellido_paterno');
        $apellido_materno = $request->input('apellido_materno');
        $primer_nombre = $request->input('primer_nombre');
        $segundo_nombre = $request->input('segundo_nombre');
        $fecha = $request->input('fecha_nacimiento');
        $telefono= $request->input('telefono');
        $ocupacion = $request->input('ocupacion');
        $apellidos = $request->input('apellidos_titular');
        $nombres = $request->input('nombres_titular');
        $cedula = $request->input('cedula');
        $edad = $request->input('edad');
        $direccion =$request->input('direccion');
        $genero = DB::table('tbgenero')->where('abreviatura', $request->input('genero'))->value('descripcion');
        $seguro = DB::table('tbTipoingreso')->where('codigo', $request->input('tipo_seguro'))->value('descripcion');
        $estado_civil = DB::table('tbestadocivil')->where('codigo',$request->input('estado_civil'))->value('descripcion');
        $lugar_nacimiento  = $request->input('lugar_nacimiento');
        $paciente = $apellido_paterno.' '.$apellido_materno.' '.$primer_nombre.' '.$segundo_nombre.' ';
        $titular = $apellidos.' '.$nombres.'';

        // Crear pdf
        $pdf = new Fpdf();
        $pdf->AddPage();
        $pdf->Image('storage/LogoHospital.png',10,10,20,20);
        //encabezado
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(192,8,'Benemerita Sociedad Protectora de la Infancia',0,1,'C',0);
        $pdf->Cell(192,8,'Hospital "LEON BECERRA DE GUAYAQUIL',0,1,'C',0);

        $contents = Storage::get('LogoHospital.png');
        // inicio del ped

        $pdf->Ln(8);
        $pdf->Cell(140);
        $pdf->SetTextColor(0,16,159);
        $pdf->SetDrawColor(0,16,159);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(70,8,'HISTORIA N',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetFillColor(254,200,2);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(27,8,$id,0,1,'I',1);
        $pdf->SetDrawColor(22,22,22);
        ////LINEA ////
        //$pdf->Cell(191,0.1,'',1,1,'T',1);
        ///REGISTRO////
        $pdf->SetTextColor(0,0,0);
        //$pdf->Cell(50);
        $pdf->Ln(2);
        $pdf->SetFillColor(232,232,232);
        $pdf->Cell(192,8,'REGISTRO DE PACIENTE',1,1,'C',1);
        $pdf->Ln(10);
        ///PACIENTE////
        $pdf->SetTextColor(0,16,159);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(70,8,'PACIENTE',0,0,'BI',0);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(-45);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$paciente,0,1,'L',1);
        ///////FECH NAC///////////
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(140);
        $pdf->Cell(70,8,'FECHA',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(25,8,$fecha,0,1,'L',1);
        $pdf->Ln(-5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(140);
        $pdf->Cell(70,8,'NACIMIENTO',0,0,'BI',0);
        ///////LUG NAC/////
        $pdf->Ln(6.5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(140);
        $pdf->Cell(70,8,'LUGAR',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(25,8,$lugar_nacimiento,0,1,'L',1);
        $pdf->Ln(-5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(140);
        $pdf->Cell(70,8,'NACIMIENTO',0,0,'BI',0);
        ///EDAD///////
        $pdf->Ln(6.5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(140);
        $pdf->Cell(70,8,'EDAD',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(25,8,$edad,0,1,'L',1);
        //////////CEDULA/////////
        $pdf->Ln(-27);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'CEDULA',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$cedula,0,1,'L',1);
        ////GENERO///
        $pdf->Ln(-1);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'GENERO',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$genero,0,1,'L',1);
        ////ESTAD CIVIL/////
        $pdf->Ln(-1);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'ESTADO',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$estado_civil,0,1,'L',1);
        $pdf->Ln(-5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'CIVIL',0,0,'BI',0);
        //////direcc/////////
        $pdf->Ln(6.5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'DIRECCION',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$direccion,0,1,'L',1);
        ///////// TELEF////////
        $pdf->Ln(-1);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'TELEFONO',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$telefono,0,1,'L',1);
        ////TITULAR/////
        $pdf->Ln(-1);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'TITULAR',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$titular,0,1,'L',1);
        ////////ADMIN////////
        $pdf->Ln(-1);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'VIA',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$seguro,0,1,'L',1);
        $pdf->Ln(-5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'ADMISION',0,0,'BI',0);

        //////OCUPACION
        $pdf->Ln(6.5);
        $pdf->SetTextColor(0,16,159);
        $pdf->Cell(70,8,'OCUPACION',0,0,'BI',0);
        $pdf->Cell(-45);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFillColor(245,245,245);
        $pdf->Cell(72,8,$ocupacion,0,1,'L',1);

        // pasar los datos a la vista
        $headers = ['Content-Type' => 'application/pdf'];
        return Response( $pdf->Output(),200,$headers);
        // return response()->file($fpdf->Output(),$headers); revisar como hacerlo esta en el manual

    }

    public function ReporteAdmision(Request $request) {


        $seguro = DB::table('tbTipoingreso')->where('codigo', $request->input('tipo_seguro'))->value('descripcion');
        $nacionalidad = DB::table('tbpais')->where('codigo',$request->input('nacionalidad'))->value('descripcion');
        $etnia = DB::table('tbTipoCultura')->where('raza',$request->input('etnia'))->value('descripcion');
        $parroquia = DB::table('tbparroquia')->where('codigo',$request->input('parroquia'))->value('descripcion');
        $ciudad = DB::table('tbciudad')->where('codigo',$request->input('ciudad'))->value('descripcion');
        $provincia = DB::table('tbprovincia')->where('codigo',$request->input('provincia'))->value('descripcion');
        $parentesco = DB::table('tbidentificacionbeneficiario')->where('codigo',$request->input('parentesco'))->value('descripcion');


        if($request->input('telefono') == null || $request->input('telefono') == "." || $request->input('telefono') == ",")
        {
            if ($request->input('celular') == null || $request->input('celular') == "." || $request->input('celular') == ",")
            {
                if ($request->input('otro') == null || $request->input('otro') == "." || $request->input('otro') == ",")
                {
                    $telefono = "";
                } else {
                    $telefono = $request->input('otro');
                }
            } else {
                $telefono = $request->input('celular');
            }
        } else  {
            $telefono = $request->input('telefono');
        }

        if($request->input('parentesco') == "AA"){
            $titular = $request->input('apellido_paterno') ." ". $request->input('apellido_materno') ." ". $request->input('primer_nombre');
            $telefono2 = $telefono;
            $direccion2 = $request->input('direccion');
        } else {
            $titular = $request->input('apellidos') ." ". $request->input('nombre');
            $direccion2 = $request->input('Titulardireccion');
            if($request->input('Titulartelefono') == null || $request->input('Titulartelefono') == "." || $request->input('Titulartelefono') == ",")
            {
                if ($request->input('Titularcelular') == null || $request->input('Titularcelular') == "." || $request->input('Titularcelular') == ",")
                {
                    if ($request->input('Titularotro') == null || $request->input('Titularotro') == "." || $request->input('Titularotro') == ",")
                    {
                        $telefono2 = "";
                    } else {
                        $telefono2 = $request->input('otro');
                    }
                } else {
                    $telefono2 = $request->input('celular');
                }
            } else  {
                $telefono2 = $request->input('telefono');
            }
        }
        //$url = Storage::url('RptAdmision001');
        //$url = Storage::url('RptAdmision001');

        $datosUser = [
            'id' => $request->input('id'),
            'parroquia' => $parroquia,
            'ciudad' => $ciudad,
            'provincia' => $provincia,
            'apellido_paterno' => $request->input('apellido_paterno'),
            'apellido_materno' => $request->input('apellido_materno'),
            'primer_nombre' => $request->input('primer_nombre'),
            'segundo_nombre' => $request->input('segundo_nombre'),
            'cedula' => $request->input('cedula'),
            'direccion' => $request->input('direccion'),
            'telefono' => $telefono,
            'fecha_nacimiento' => $request->input('fecha_nacimiento'),
            'lugar_nacimiento' => $request->input('lugar_nacimiento'),
            'nacionalidad' => $nacionalidad,
            'etnia' => $etnia,
            'edad' => $request->input('edad'),
            'genero' => $request->input('genero'),
            'estado_civil' => $request->input('estado_civil'),
            'ocupacion' => $request->input('ocupacion'),
            'lugar_trabajo' => $request->input('trabajo'),
            'seguro' => $seguro,
            'instruccion' => $request->input('instruccion'),
            'fecha_emision' => $request->input('fecha_emision'),
            'titular' => $titular,
            'parentesco' => $parentesco,
            'direccion2' => $direccion2,
            'telefono2' => $telefono2,

        ];

        $datos = (object)$datosUser;
        /*<!-- referido --><!-- avisar --><!-- parentesco --><!-- dirrecion --><!-- telefono --><!-- admisionista --> */


        $pdf = \PDF::loadView('pdf.admision001',compact('datos'));
        return $pdf->stream();
    }

//
    public function CmbDpcumentosIess() {
        $datos = $this->EmergenciaProcedure->CmbDpcumentosIess();
        return json_encode($datos);
    }

    Public function GuardarPaciente(Request $request) {

        // pasamos los datos a mayuscula
        //$str = strtoupper($str);



        if($request->input('accion') == 'Nuevo'){
           $resultado = $this->StorePaciente($request);

               if($resultado !=""  || $resultado != null ) {
                   flash()->overlay('PACIENTE N°'.$resultado."", 'Registro Guardado');
                   return redirect()->route('emergencia.registropacientes');
               } else {
                   flash()->overlay("Registro no ingresado", 'Error');
                   return redirect()->route('emergencia.registropacientes');
               }
        }else {
            $resultado = $this->updatePaciente($request);
                if($resultado == 0) {
                    flash()->overlay('Actualización registrada con exito', 'Actualizado');
                    return redirect()->route('emergencia.registropacientes');
                } else {
                    flash()->overlay('Error al momento de realizar la actualización', 'ERROR');
                    return redirect()->route('emergencia.registropacientes');
                }
        }
    }

    public function updatePaciente(Request $request) {

        $campo1 = $request->input('campo1');
        $campo2 = $request->input('campo2');
        $campo3 = $request->input('campo3');
        $status_otro_seguro = $request->input('status_otro_seguro');
        $descripcion_otro_seguro = $request->input('descripcion_otro_seguro');
        $campo1a = $request->input('campo1a');
        $campo2a = $request->input('campo2a');
        $campo3a = $request->input('campo3a');
        $usuario_ingreso = $request->input('usuario_ingreso');
        $usuario_ingreso_a = $request->input('usuario_ingreso_a');
        $fecha_ingreso = $request->input('fecha_ingreso');
        $fecha_cedulacion = $request->input('fecha_cedulacion');
        $fecha_fallecimiento = $request->input('fecha_fallecimiento');
        $instruccion = $request->input('instruccion');
        $TempTipoSeguro = $request->input('TempTipoSeguro');
        $TempTitular = $request->input('TempTitular');
        $id= $request->input('id');
        $cedula = $request->input('cedula');
        $tipo_identificacion = $request->input('tipo_identificacion');
        $primer_nombre = $request->input('primer_nombre');
        $segundo_nombre = $request->input('segundo_nombre');
        $apellido_paterno = $request->input('apellido_paterno');
        $apellido_materno = $request->input('apellido_materno');
        $genero = $request->input('genero');
        $fecha_nacimiento = $request->input('fecha_nacimiento');
        $lugar_nacimiento = $request->input('lugar_nacimiento');
        $ocupacion = $request->input('ocupacion');
        $estado_civil = $request->input('estado_civil');
        $tipo_sangre = $request->input('sangre');
        $nacionalidad = $request->input('nacionalidad');
        $etnico = $request->input('etnico');
        $pais = $request->input('nacionalidad');
        $provincia = $request->input('provincia');
        $ciudad = $request->input('ciudad');
        $direccion = $request->input('direccion');
        $telefono = $request->input('telefono');
        $celular = $request->input('celular');
        $otro = $request->input('otro');
        /// tipo de ingreso = tipo_seguro en la tabla de pacientes
        // variables
       // $tipo_seguro = 0;
       // $TemTipoInreso = 0;
       // $cedula_titular = '';
       // $TempTitular = '';
        $observacion = $request->input('observacion');
        $lugar_trabajo  = $request->input('lugar_trabajo');
        $tipo_parentesco = $request->input('parentesco');
        $tipo_beneficiario = $request->input('tipo_beneficiario');
        $tipo_seguro = $request->input('seguro');
        $user = Auth::user();
        $usuario_modificacion = $user->id;
        $pcname = $request->input('pc');
        $status= 1;
        if ($request->input('status_discapacidad') != null ) {
            $status_discapacidad = $request->input('status_discapacidad');
        } else
        {
            $status_discapacidad = "NO";
        }
        if ($request->input('carnet_conadis')!= null) {
            $carnet_conadis = $request->input('carnet_conadis');
        } else
        {
            $carnet_conadis= null;
        }
        if ($request->input('tipo_seguro_iess') != null) {
            $tipo_seguro_iess = $request->input('tipo_seguro_iess');
        } else
        {
            $tipo_seguro_iess = null;
        }
        $parroquia = $request->input('parroquia');
        $nombres_padre = $request->input('nombres_padre');
        $nombres_madre = $request->input('nombres_madre');
        $conyugue= $request->input('conyuge');
        $cedula_seg_progenitor = $request->input('cedula_segundo_progenitor');

        if ($request->input('cobertura_compartida' ) != null) {
            $cobertura_compartida= ($request->input('cobertura_compartida') == "on") ? "S" : "N";
        } else {
            $cobertura_compartida= null;

        }
        $prepagada= $request->input('prepagada');
        if ($request->input('issfa' ) != null) {
            $issfa= ($request->input('issfa') == "on") ? "S" : "N";
        } else {
            $issfa= null;

        }

        if ($request->input('isspol' ) != null) {
            $isspol=  ($request->input('isspol') == "on") ? "S" : "N";
        } else {
            $isspol= null;

        }

        if ($request->input('iess') != null) {
            $iess= ($request->input('iess') == "on") ? "S" : "N";
        } else {
            $iess= null;

        }
        $cedula_titular = $request->input('cedula_titular');
        ///revisa que datos han sido modificados
        $cambio = "N";
            if($tipo_seguro != $TempTipoSeguro)
            {
                $cambio = "S";
                if($cedula_titular != $TempTitular) {
                    $cambio = "TS";
                }
            } else if ($cedula_titular != $TempTitular) {
                $cambio = "T";
            }
        ///
        // grabar log
        //echo $tipo_seguro;
            //dd($request);
        $this->EmergenciaProcedure->InsertPacientesLog($id,
            $cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,
            $apellido_materno,$genero,$fecha_nacimiento,$lugar_nacimiento,$ocupacion ,
            $estado_civil,$tipo_sangre ,$nacionalidad,$pais,$provincia,
            $ciudad,$direccion,$telefono,$celular,$otro,
            $cedula_titular,$observacion,$lugar_trabajo,$tipo_parentesco,$tipo_beneficiario,
            $tipo_seguro = 1,$cambio,$campo2,$campo3,$usuario_ingreso,
            $fecha_ingreso,$pcname,$status,$status_discapacidad ,$carnet_conadis ,
            $status_otro_seguro ,$tipo_seguro_iess ,$descripcion_otro_seguro,$etnico);
        // update paciente

        $result = $this->EmergenciaProcedure->UpdatePacientes( $id,$cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,$apellido_materno,
            $genero,$fecha_nacimiento,$lugar_nacimiento,$ocupacion,$estado_civil,$tipo_sangre ,$nacionalidad,$pais,
            $provincia,$ciudad,$direccion,$telefono,$celular,$otro,$cedula_titular,$observacion,$lugar_trabajo,
            $tipo_parentesco,$tipo_beneficiario,$tipo_seguro,$campo1,$campo2,$campo3,
            $usuario_modificacion,$pcname,$status,$status_discapacidad,$carnet_conadis,$status_otro_seguro,
            $tipo_seguro_iess,$descripcion_otro_seguro ,$parroquia,$nombres_padre ,$nombres_madre ,
            $conyugue,$campo1a,$campo2a,$campo3a,$cedula_seg_progenitor ,
            $fecha_cedulacion ,$fecha_fallecimiento ,$instruccion ,$cobertura_compartida ,
            $prepagada ,$issfa,$isspol,$iess);

           $respuesta = $result[0]->error;
           //dd($respuesta);
           return $respuesta;
        //dd($result);
        //echo $result->error;
    }


    public function StorePaciente(Request $request) {

        // hacer una ultima busqueda con el CI que pasan
        // variables
        $cliCiudad = "090100";
        $cliProvincia = "090000";
        $clizon  = "CEN";
        $tipoId  = "001";
        $CodigoInstitucion = "18";
        $CodigoAgencia = "18";
        $Usuario = "hleonb1";
        $Contrasenia = "Pt456n@Qye";
        $des_campo1 = ".";
        $des_campo1InformacionAdicional = ".";
        $cedulado = "";
        $nivel_intruccion ="";
        $fecha_cedulacion = "01/01/1900";
        $fecha_fallecimiento = "01/01/1900";
        //
        //dd($request);
        $cedula = $request->input('cedula');
        $tipo_identificacion = $request->input('tipo_identificacion');
        $primer_nombre = $request->input('primer_nombre');
        $segundo_nombre = $request->input('segundo_nombre');
        $apellido_paterno = $request->input('apellido_paterno');
        $apellido_materno = $request->input('apellido_materno');
        $genero = $request->input('genero');
        $fecha_nacimiento = $request->input('fecha_nacimiento');
        $lugar_nacimiento = $request->input('lugar_nacimiento');
        $ocupacion = $request->input('ocupacion');
        $estado_civil = $request->input('estado_civil');
        $tipo_sangre = $request->input('sangre');
        $nacionalidad = $request->input('nacionalidad');
        $etnico = $request->input('etnico');
        $pais = $request->input('nacionalidad');
        $provincia = $request->input('provincia');
        $ciudad = $request->input('ciudad');
        $direccion = $request->input('direccion');
        $telefono = ($request->input('telefono_') != null) ? $request->input('telefono_') : ".";
        $celular = ($request->input('celular') != null) ? $request->input('celular') : ".";
        $otro = ($request->input('otro') != null) ? $request->input('otro') : ".";
        $cedula_titular = $request->input('cedula_titular');
        $observacion = ($request->input('observacion') != null) ? $request->input('observacion') : ".";
        $lugar_trabajo  = ($request->input('lugar_trabajo') != null) ? $request->input('lugar_trabajo') : null;
        $tipo_parentesco = $request->input('parentesco');
        $tipo_beneficiario = $request->input('tipo_beneficiario');
        $tipo_seguro = $request->input('seguro');
        $des_campo1 = ($request->input('des_campo1') != null) ? $request->input('des_campo1') : ".";
        $des_campo2 = ".";
        $des_campo3 = 0.0;
        $user = Auth::user();
        $usario_ingreso = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];
        $status= 1;
        if ($request->input('status_discapacidad') != null ) {
            $status_discapacidad = $request->input('status_discapacidad');
        } else
            {
            $status_discapacidad = "NO";
        }
        if ($request->input('carnet_conadis')!= null) {
            $carnet_conadis = $request->input('carnet_conadis');
        } else
            {
            $carnet_conadis= null;
        }
        if ($request->input('status_otro_seguro')!= null) {
            $status_otro_seguro= $request->input('status_otro_seguro');
        } else {
            $status_otro_seguro = null;
        }
        if ($request->input('tipo_seguro_iess') != null) {
            $tipo_seguro_iess = $request->input('tipo_seguro_iess');
        } else
            {
            $tipo_seguro_iess = null;
        }
        if ($request->input('descripcion_otro_seguro') != null) {
            $descripcion_otro_seguro = $request->input('descripcion_otro_seguro');
        } else {
            $descripcion_otro_seguro= null;
        }
        $parroquia = $request->input('parroquia');
        $nombres_padre = $request->input('nombres_padre');
        $nombres_madre = $request->input('nombres_madre');
        if ($request->input('conyuque') != null) {
            $conyugue= $request->input('conyuque');
        } else {
            $conyugue= null;
        }
        $des_campo1IA = ".";
        $des_campo2IA= ".";
        $des_campo3IA= 0.0;


        if ($request->input('cedula_segundo_progenitor' ) != null) {
            $cedula_seg_progenitor = $request->input('cedula_segundo_progenitor');
        } else {
            $cedula_seg_progenitor = null;

        }
        if ($request->input('fecha_cedulacion' ) != null) {
            $fecha_cedulacion = $request->input('fecha_cedulacion');
        } else {
            $fecha_cedulacion = "1900-01-01";

        }

        if ($request->input('fecha_fallecimiento' ) != null) {
            $fecha_fallecimiento = $request->input('fecha_fallecimiento');
        } else {
            $fecha_fallecimiento = "1900-01-01";

        }

        if ($request->input('instruccion' ) != null) {
            $instruccion= $request->input('instruccion');
        } else {
            $instruccion= null;

        }

            $cobertura_compartida= ($request->input('cobertura') == "on") ? "S" : "N";

        if ($request->input('prepagada' ) != null) {
            $prepagada= $request->input('prepagada');
        } else {
            $prepagada= null;

        }

        if ($request->input('issfa' ) != null) {
            $issfa= ($request->input('issfa') == "on") ? "S" : "N";
        } else {
            $issfa= null;

        }

        if ($request->input('isspol' ) != null) {
            $isspol=  ($request->input('isspol') == "on") ? "S" : "N";
        } else {
            $isspol= null;

        }

        if ($request->input('iess') != null) {
            $iess= ($request->input('iess') == "on") ? "S" : "N";
        } else {
            $iess= null;

        }
        // parametros de salida

         $r = 0;

        $datos = [
            'cedula' => $cedula ,
            'identificacion' => $tipo_identificacion,
            'primer' => $primer_nombre,
            'segundo' => $segundo_nombre,
            'patenro' => $apellido_paterno,
            'materno' => $apellido_materno,
            'genero' => $genero,
            'fecha' => $fecha_nacimiento,
            'lugar' => $lugar_nacimiento,
            'ocupacion' => $ocupacion,
            'estado' => $estado_civil,
            'sangre' => $tipo_sangre ,
            'nacionalidad' => $nacionalidad,
            'pais' => $pais,
            'provincia' => $provincia,
            'ciudad' => $ciudad,
            'direccion' => $direccion,
            'telefono' => $telefono,
            'celular' => $celular,
            'otro' => $otro,
            'tiular' =>$cedula_titular,
            'observacion' => $observacion,
            'lugar_trabajo' => $lugar_trabajo,
            'parentesco' => $tipo_parentesco,
            'beneficiario' => $tipo_beneficiario,
            'tipo_seguro' => $tipo_seguro,
            'des1' => $des_campo1,
            'des2' => $des_campo2,
            'des3' => $des_campo3,
            'usuario' => $usario_ingreso,
            'pc' => $pcname,
            'estado_' => $status,
            'estado discapacidad' => $status_discapacidad,
            'conadis' => $carnet_conadis,
            'status_otro_seguro' => $status_otro_seguro,
            'tipo_seguro_iess' => $tipo_seguro_iess,
            'descripcion_otro_seguro' => $descripcion_otro_seguro ,
            'etnico' => $etnico,
            'parroquia' => $parroquia,
            'padre' => $nombres_padre ,
            'madre' => $nombres_madre ,
            'conyugue' => $conyugue,
            'des1a' => $des_campo1IA,
            'des2a' =>$des_campo2IA,
            'des3a' =>$des_campo3IA,
            'segundo_Ce' =>$cedula_seg_progenitor,
            'cedulacion' =>$fecha_cedulacion,
            'fallecimiento' =>$fecha_fallecimiento,
            'instruccion' =>$instruccion,
            'cobertura' =>$cobertura_compartida,
            'prepagada' =>$prepagada ,
            'issfa' =>$issfa,
            'isspol' =>$isspol,
            'iess' =>$iess,
            'r' =>$r
        ];

        //dd($datos);

        $result = $this->EmergenciaProcedure->InsertPacientes(
            $cedula ,
            $tipo_identificacion= (integer)$tipo_identificacion,
            $primer_nombre,
            $segundo_nombre,
            $apellido_paterno,
            $apellido_materno,
            $genero ,
            $fecha_nacimiento,
            $lugar_nacimiento,
            $ocupacion,
            $estado_civil= (integer)$estado_civil,
            $tipo_sangre = (integer)$tipo_sangre,
            $nacionalidad= (integer)$nacionalidad,
            $pais= (integer)$pais,
            $provincia= (integer)$provincia,
            $ciudad= (integer)$ciudad,
            $direccion,
            $telefono,
            $celular,
            $otro,
            $cedula_titular,
            $observacion,
            $lugar_trabajo,
            $tipo_parentesco,
            $tipo_beneficiario,
            $tipo_seguro= (integer)$tipo_seguro,
            $des_campo1,
            $des_campo2,
            $des_campo3,
            $usario_ingreso,
            $pcname,
            $status,
            $status_discapacidad,
            $carnet_conadis,
            $status_otro_seguro,
            $tipo_seguro_iess,
             $descripcion_otro_seguro ,
            $etnico= (integer)$etnico,
            $parroquia= (integer)$parroquia,
            $nombres_padre ,
            $nombres_madre ,
            $conyugue,
            $des_campo1IA,
            $des_campo2IA,
            $des_campo3IA,
            $cedula_seg_progenitor,
            $fecha_cedulacion,
            $fecha_fallecimiento,
            $instruccion,
            $cobertura_compartida ,
            $prepagada= (integer)$prepagada ,
            $issfa,
            $isspol,
            $iess,
            $r
        );



        //$resultado = DB::table('tbpaciente')->where('cedula', $cedula)->value('id');
        //dd($resultado);
        $resultado = DB::table('tbpaciente')->where('cedula', $cedula)->value('id');

        return $resultado;


    }

    public function cmbIngresoCalificacion() {
        $datos = $this->EmergenciaProcedure->cmbIngresoCalificacion();
        return json_encode($datos);
    }

    public function GrabarCuentasPorCobrar() {

    }
}
