<?php
namespace App\Http\Controllers\Administracion;
use Maatwebsite\Excel\Concerns\FromCollection;

class InvoicesExport implements FromCollection
{
    protected $fecha_ini;
    protected $fecha_fin;

    public function __construct($desde,$hasta)
    {
        $this->fecha_ini = $desde;
        $this->fecha_fin = $hasta;
    }

    public function collection()
    {
        return \DB::select('Call spConsultarMedicosLLamados2(?,?)',array($this->fecha_ini ,$this->fecha_fin));

    }
}