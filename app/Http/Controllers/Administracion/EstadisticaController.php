<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\AdministracionProcedure;
use Illuminate\Support\Facades\Auth;
use App\Exports\InvoiceExportEstadistica;
use App\Exports\InvoiceExportEstadisticaEgreso;
use App\Exports\InvoiceExportEstadisticaEmergencia;
use App\Exports\InvoiceExportEstadisticaPacientes;
use App\Exports\InvoiceExportEstadisticaCe;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class EstadisticaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(AdministracionProcedure $administracionProcedure)
    {
        $this->AdministracionProcedure  = $administracionProcedure;
    }
    public function index()
    {
        return view('modulos.administracion.Estadistica');
    }

    public function estadisticaRegistrosIngresoHospitalizacion(){
       $datos = $this->AdministracionProcedure->estadisticaRegistrosIngresoHospitalizacion();
        return $datos;
    }
    public function estadisticaRegistrosPacientes(){
        $datos = \DB::select('Call spEstadisticaRegistroPacientes');
        return $datos;
    }
    public function estadisticaRegistrosAtencionesE(){
        $datos = \DB::select('Call spEstadisticaRegistrosEmergencia');
        return $datos;
    }
    public function estadisticaRegistrosIngresoAtencionesCE(){
        $datos = \DB::select('Call spEstadisticaRegistrosPacientesCE');
        return $datos;
    }


    public function estadisticaRegistrosIngresoHospitalizacionYears(){
        $datos = $this->AdministracionProcedure->estadisticaRegistrosIngresoHospitalizacionYears();
        return $datos;
    }
    public function estadisticaRegistrosCeYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->estadisticaRegistrosCeYears($fecha1,$fecha2);
        return $datos;
    }
    public function estadisticaRegistrosEgresosYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->estadisticaRegistrosEgresosYears($fecha1,$fecha2);
        return $datos;
    }
    public function estadisticaRegistrosEmergenciaYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->estadisticaRegistrosEmergenciaYears($fecha1,$fecha2);
        return $datos;
    }
    public function estadisticaRegistrosPacientesYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->estadisticaRegistrosPacientesYears($fecha1,$fecha2);
        return $datos;
    }


    public function estadisticaRegistrosEgresosHospitalizacion(){
        $datos = $this->AdministracionProcedure->estadisticaRegistrosEgresosHospitalizacion();
        return $datos;
    }

    public function IndexDetalle($detalles){
        if($detalles === "ingreso"){
            return view('modulos.administracion.detalles')->with('detalles',$detalles);
        }else if($detalles === "egreso"){
            return view('modulos.administracion.detallesegreso');
        }else if($detalles === "paciente"){
            return view('modulos.administracion.detallesregistrop');
        }else if($detalles === "emergencia"){
            return view('modulos.administracion.detallesatenciones');
        }else if($detalles === "ce"){
            return view('modulos.administracion.detallesce');
        }

        echo $detalles;
        //return view('modulos.administracion.detalles')->with('detalles',$detalles);
        //return view('modulos.administracion.estado');
    }

    // hospitalizacion
    public function consultarHospitaliacionesYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarHospitaliacionesYears($fecha1,$fecha2);
        return $datos;
    }
    public function consultarHospitaliacionesMes($fecha1,$fecha2){
        $fecha1 = $fecha1.'-01';
        $fecha2 = $fecha2.'-01';
        $datos = $this->AdministracionProcedure->consultarHospitaliacionesMes($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarHospitaliacionSemanas($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure-> consultarHospitaliacionSemanas($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarHospitaliacionRangos($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure-> consultarHospitaliacionRangos($fecha1,$fecha2);
        return $datos;
    }

    public function  consultarHospitaliacionExcel($desde,$hasta,$tipo){

        return Excel::download(new InvoiceExportEstadistica ($desde,$hasta,$tipo), 'Ingesos hospitalarios.xlsx');
        //$datos = $this->AdministracionProcedure-> consultarHospitaliacionRangos($fecha1,$fecha2);
        //return $datos;
    }

    public function  consultarHospitaliacionPdf($desde,$hasta,$tipo){
        return view('modulos.administracion.pdf.hospitalizacion.periodo',compact('desde','hasta','tipo'));
        //return Excel::download(new InvoiceExportEstadistica ($desde,$hasta,$tipo), 'Ingesos hospitalarios.xlsx');
    }

    // egresos
    public function consultarEgresosYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarEgresosYears($fecha1,$fecha2);
        return $datos;
    }
    public function consultarEgresosMes($fecha1,$fecha2){
        $fecha1 = $fecha1.'-01';
        $fecha2 = $fecha2.'-01';
        $datos = $this->AdministracionProcedure->consultarEgresosMes($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarEgresosSemanas($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarEgresosSemanas($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarEgresosRangos($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarEgresosRangos($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarEgresosExcel($desde,$hasta,$tipo){
        //echo "Egresos";
        return Excel::download(new InvoiceExportEstadisticaEgreso($desde,$hasta,$tipo), 'Egresos hospitalarios.xlsx');
    }
    public function  consultarEgresoPdf($desde,$hasta,$tipo){
        return view('modulos.administracion.pdf.egreso.periodo',compact('desde','hasta','tipo'));
        //return Excel::download(new InvoiceExportEstadistica ($desde,$hasta,$tipo), 'Ingesos hospitalarios.xlsx');
    }

    // registro pacientes
    public function consultarRegistroPacientesYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarRegistroPacientesYears($fecha1,$fecha2);
        return $datos;
    }
    public function consultarRegistroPacientesMes($fecha1,$fecha2){
        $fecha1 = $fecha1.'-01';
        $fecha2 = $fecha2.'-01';
        $datos = $this->AdministracionProcedure->consultarRegistroPacientesMes($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarRegistroPacientesSemanas($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarRegistroPacientesSemanas($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarRegistroPacientesRangos($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarRegistroPacientesRangos($fecha1,$fecha2);
        return $datos;
    }

    public function  consultarRegistroPacientesExcel($desde,$hasta,$tipo){

        return Excel::download(new InvoiceExportEstadisticaPacientes($desde,$hasta,$tipo), 'Registro Pacientes.xlsx');
    }

    public function  consultarRegistroPacientesPdf($desde,$hasta,$tipo){
        return view('modulos.administracion.pdf.hospitalizacion.periodo',compact('desde','hasta','tipo'));
    }

    //  atenciones por emergencia
    public function consultarEmergenciasYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarEmergenciasYears($fecha1,$fecha2);
        return $datos;
    }
    public function consultarEmergenciasMes($fecha1,$fecha2){
        $fecha1 = $fecha1.'-01';
        $fecha2 = $fecha2.'-01';
        $datos = $this->AdministracionProcedure->consultarEmergenciasMes($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarEmergenciasSemanas($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarEmergenciasSemanas($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarEmergenciasRangos($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarEmergenciasRangos($fecha1,$fecha2);
        return $datos;
    }

    public function  consultarEmergenciasExcel($desde,$hasta,$tipo){

        return Excel::download(new InvoiceExportEstadisticaEmergencia($desde,$hasta,$tipo), 'Emergencia.xlsx');
        //$datos = $this->AdministracionProcedure-> consultarHospitaliacionRangos($fecha1,$fecha2);
        //return $datos;
    }

    public function  consultarEmergenciasPdf($desde,$hasta,$tipo){
        return view('modulos.administracion.pdf.hospitalizacion.periodo',compact('desde','hasta','tipo'));
        //return Excel::download(new InvoiceExportEstadistica ($desde,$hasta,$tipo), 'Ingesos hospitalarios.xlsx');
    }

    // consulta externa
    public function consultarCeYears($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarCeYears($fecha1,$fecha2);
        return $datos;
    }
    public function consultarCeMes($fecha1,$fecha2){
        $fecha1 = $fecha1.'-01';
        $fecha2 = $fecha2.'-01';
        $datos = $this->AdministracionProcedure->consultarCeMes($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarCeSemanas($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarCeSemanas($fecha1,$fecha2);
        return $datos;
    }
    public function  consultarCeRangos($fecha1,$fecha2){
        $datos = $this->AdministracionProcedure->consultarCeRangos($fecha1,$fecha2);
        return $datos;
    }

    public function  consultarCeExcel($desde,$hasta,$tipo){

        return Excel::download(new InvoiceExportEstadisticaCe($desde,$hasta,$tipo), 'Ingesos hospitalarios.xlsx');
        //$datos = $this->AdministracionProcedure-> consultarHospitaliacionRangos($fecha1,$fecha2);
        //return $datos;
    }

    public function  consultarCePdf($desde,$hasta,$tipo){
        return view('modulos.administracion.pdf.hospitalizacion.periodo',compact('desde','hasta','tipo'));
        //return Excel::download(new InvoiceExportEstadistica ($desde,$hasta,$tipo), 'Ingesos hospitalarios.xlsx');
    }
}
