<?php

namespace App\Http\Controllers\Administracion;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\AdministracionProcedure;
use Illuminate\Support\Facades\Auth;


class NotificacionesController extends Controller
{
    protected $AdministracionProcedure;

    public function __construct(AdministracionProcedure $administracionProcedure)
    {
        $this->AdministracionProcedure  = $administracionProcedure;

    }

    public function index()
    {
        //
        return view('modulos.administracion.notification');
    }

    public function viewNotificaciones() {
        $user = Auth::user();
        $id = $user->id;
        $rol = DB::table('role_user')
            ->select('role_id')
            ->where('user_id',$id)->get();

        $notificacion_id = DB::table('tbnotificaciones')
            ->select('id')
            ->where('rol_id',$rol)->get();

        foreach($notificacion_id  as $valor) {

            $notificaciones = DB::table('tbuser_notification')
                ->select('id')
                ->where('rol_id',$rol)->get();

        }

    }

    public function historialNotificaciones(){
        $user = \Auth::user();
        $usuario = $user->id;
        $resultado = \DB::select('Call spNotificacionesHistoricasUsuario(?)',array($usuario));
        //dd($resultado);
        return view('modulos.otros.notificacionesHistorial',compact('resultado'));
    }


    public function storeNotificacionesPorRol(Request $request)
    {
        //dd($request->all());
        $user = Auth::user();
        $usuario_ingreso = $user->id;

        $fecha = getdate();

        $day = $fecha['mday'];
        if($fecha['mon'] < 10){
            $mes = "0".$fecha['mon'];
        } else {
            $mes = $fecha['mon'];
        }
        $fecha_ingreso = $fecha['year']."/".$mes."/".$day;
        $fecha_modificacion = $fecha_ingreso;


            //
        $titulo = $request->input('titulo_nr');
        $descripcion = $request->input('descripcion_nr');

        $icono = $request->input('icono_nr');

        if($icono === 1)
        {
            $icono = "text-primary";
        }else if($icono === 2) {
            $icono = "text-primary";
        }else if($icono === 3) {
            $icono = "text-danger";
        }

        $todos = $request->input('todos','off');
        //dd($request->all());
        // pregunta si la norificación es para todos los roles
        if( $todos === 'on')
        {
            $rol = \DB::table('roles')->select('id')->get();

            foreach ($rol as $roles) {

                $notificacion_id =\DB::table('tbnotificaciones')->insertGetId(
                    ['rol_id' => $roles->id,
                        'titulo' => $titulo,
                        'descripcion' => $descripcion,
                        'icono' => $icono,
                        'estado' => 1,
                        'fecha_ingreso' => $fecha_ingreso,
                        'usuario_ingreso' => $usuario_ingreso ,
                        'fecha_modificacion' => $fecha_modificacion,
                        'usuario_modificacion' => $usuario_ingreso ]
                );

                // guardamos en un array todos los los usuaios que tengan el rol de ingreso
                $user_rol = \DB::table('role_user')->select('user_id')->where('role_id',$roles->id)->get();

                //echo $user_rol[0]->user_id;
                //dd($user_rol);
                foreach ($user_rol as $users )
                {
                    \DB::table('tbuser_notification')->insert(
                        [   'user_id' => $users->user_id,
                            'notification_id' => $notificacion_id,
                            'estado' => 1,
                            'fecha_ingreso' => $fecha_ingreso,
                            'usuario_ingreso' => $usuario_ingreso
                        ]
                    );
                }
            }
        }
        else {
            // obtiene los roles para los cuales es la notificación
            $rol =$request->input('rol');

            // va a insertar el registo para los diferentes roles
            foreach ($rol as $roles) {

                $notificacion_id =\DB::table('tbnotificaciones')->insertGetId(
                    ['rol_id' => $roles,
                        'titulo' => $titulo,
                        'descripcion' => $descripcion,
                        'icono' => $icono,
                        'estado' => 1,
                        'fecha_ingreso' => $fecha_ingreso,
                        'usuario_ingreso' => $user->id,
                        'fecha_modificacion' => $fecha_ingreso,
                        'usuario_modificacion' => $user->id]
                    );
                // guardamos en un array todos los los usuaios que tengan el rol de ingreso
                $user_rol = \DB::table('role_user')->select('user_id')->where('role_id',$roles)->get();


                foreach ($user_rol as $users )
                {

                    \DB::table('tbuser_notification')->insert(
                        [   'user_id' => $users->user_id,
                            'notification_id' => $notificacion_id,
                            'estado' => 1,
                            'fecha_ingreso' => "2018/07/19",
                            'usuario_ingreso' => 1
                        ]
                    );
                }


            }
        }


        flash()->success("Registro realizado con exito");
        return redirect()->route('administracion.notificaciones');

        //$user_rol = \DB::table('role_user')->select('user_id')->get();

    }

    public function storeNotificacionesRapidas(Request $request)
    {
        dd($request);
    }

    public function showNotificacionesRapidas($id)
    {
        //
    }

    public function showNotificacionesPorRol($id)
    {
        //
    }

    public function roles()
    {
        $resultado = $this->AdministracionProcedure->consultarRolesAll();

        return $resultado;
    }

    public function notificacionesUsuario() {

        $user = Auth::user();
        $usuario = (integer) $user->id;
        return \DB::select('call spNotificacionesUsuario(?)',array($usuario));
    }
}
