<?php

namespace App\Http\Controllers\Administracion;

use App\User;
use App\Exports\InvoicesExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\AdministracionProcedure;
use Illuminate\Support\Facades\Mail;
use App\Mail\MedicosLLamado;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Administracion;
class MediciosAlLLamadoController  extends Controller
{
    protected $AdministracionProcedure;
    public function __construct(AdministracionProcedure $administracionProcedure)
    {
        $this->AdministracionProcedure = $administracionProcedure;
      //  $this->InvoicesExport = new InvoicesExport();
    }
    public function exportarLista($desde,$hasta)
    {
        return Excel::download(new InvoicesExport($desde,$hasta), 'Lista Medicos al llamado.xlsx');
    }


    public function index()
    {
        //
        return view('modulos.administracion.medicosalllamado');
    }

    public function getFechaActual(){
        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $hora= date('H');
        $minutos = date('i');
        $segundos = date('s');

        $fecha_actualizacion =$year.'-'.$mes.'-'.$dia.' '.$hora .':'.$minutos.':'.$segundos.'';
        return $fecha_actualizacion;
    }


    public function getDiaSemana(){
        //$resultado = ;
        date_default_timezone_set('America/Lima');
        $year = date('Y');
        $mes = date('m');
        $dia = date('d');
        $fecha =$year.'-'.$mes.'-'.$dia;
        $fechats = strtotime($fecha); //pasamos a timestamp

//el parametro w en la funcion date indica que queremos el dia de la semana
//lo devuelve en numero 0 domingo, 1 lunes,....
        switch (date('w', $fechats)){
            case 0: return "Domingo"; break;
            case 1: return "Lunes"; break;
            case 2: return "Martes"; break;
            case 3: return "Miercoles"; break;
            case 4: return "Jueves"; break;
            case 5: return "Viernes"; break;
            case 6: return "Sabado"; break;
            default: return "";
        }
    }
    public function consultarMedicosLLamados2($fechaini,$fechafin ){

        $resultado = $this->AdministracionProcedure->consultarMedicosLLamados2($fechaini,$fechafin);
        return $resultado;
    }
   public function consultarEspecializacionMedico($id) {
       $resultado = $this->AdministracionProcedure->consultarEspecializacionMedico($id);
       return $resultado;
   }
    public function store(Request $request)
    {

        $request->validate([
            'medico' => 'required', 'paciente_id' => 'required', 'seguro'=> 'required', 'correo'=> 'required|email', 'medico_name'=> 'required',
            'seguro_name'=> 'required',

        ],['medico.required' => 'El nombre del médico es requerido',
            'paciente_id.required' => 'La información del paciente es requerida',
            'seguro.required' => 'La información del paciente es requerida',
            'correo.required' => 'El correo del médico es requerido', 'correo.email' => 'El correo electronico no es valido',
            'medico_name.required' => 'EL nombre del médico es requerido',
            'seguro_name.required' => 'EL nombre del seguro es requerido',
        ]);

        //dd($request);
        $medico = $request->input('medico');
        $dia = $this->getDiaSemana();
        $fecha = $this->getFechaActual();
        $paciente = $request->input('paciente_id');
        $paciente_name = $request->input('paciente');
        $seguro = $request->input('seguro');
        $correo = $request->input('correo');
        $medico_name = $request->input('medico_name');
        $seguro_name = $request->input('seguro_name');
        $user = Auth::user();
        $usuario = $user->id;
        $pcname = $_SERVER['REMOTE_ADDR'];

        try{
            $resultado = $this->AdministracionProcedure->insertCorreoMedicos(0,$paciente,$medico,$seguro,$correo,$usuario,$pcname);

            if(isset($resultado[0]->id)){
                $id_n = $resultado[0]->id;
                $token = str_random(25);


                DB::table('tbtokenemail')->insert([
                    'id' => $id_n,'token' => $token,
                    'estado' => 0
                ]);

                $text = "Estimado Doctor/a ".$medico_name.' se le ha enviado un correo con la información de hospitalización responda a la mayor brevedad posible.';

                \Telegram::sendMessage([
                    'chat_id' => env('TELEGRAM_CHANNEL_ID', '-1001333339812'),
                    'parse_mode' => 'HTML',
                    'text' => $text
                ]);

                Mail::to('bryansilva021@hotmail.com')
                    ->cc('joselynthebest_1995@hotmail.com')
                    ->send(new MedicosLLamado($medico_name,$dia,$fecha,$paciente_name,$seguro_name,$id_n,$token));


                \Telegram::sendMessage([
                    'chat_id' => env('TELEGRAM_CHANNEL_ID', '-1001333339812'),
                    'parse_mode' => 'HTML',
                    'text' => "RECUERDE ESTE MEDIO NO ES UN CANAL VALIDO DE RESPUESTA."
                ]);
            }
        }catch (\Exception $e){
            $array = array("Error: " , $e->getCode().'=>'.$e->getMessage());
            return $array;
        }




        //return view('mail.medicosllamado',compact("medico","fecha","paciente","seguro"));

        //dd($resultado);
    }


    public function update(Request $request, $id)
    {
        //dd($request);
        $descripcion = $request->input('descripcion');
        //$respuesta = $request->input('respuesta');
        //echo $request->input('estado');
        if($request->input('estado') === "true")
        {
            $respuesta = 1;
        } else {
            $respuesta = 0;
        }
        $user = Auth::user();
        $usuario = $user->id;
        //echo $respuesta;
        $resultado = $this->AdministracionProcedure->updateMedicollamados($id,$respuesta,$descripcion,$usuario);
        return $resultado;
    }
}
