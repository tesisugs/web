<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Procedures\AdministracionProcedure;
use Illuminate\Support\Facades\Auth;

class EstadoController extends Controller
{
    public function __construct(AdministracionProcedure $administracionProcedure)
    {
        $this->AdministracionProcedure  = $administracionProcedure;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modulos.administracion.estado');
    }

    public function camasHabilitadas(){

        $datos = $this->AdministracionProcedure->camasHabilitadas();
        return $datos;

    }



}
