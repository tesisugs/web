<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Core\Procedures\EmergenciasProcedure;
//use Codedge\Fpdf\Facades\Fpdf; el facade no funciono como debía
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EmergenciaController extends Controller
{

    protected $EmergenciaProcedure;

        public function __construct(EmergenciasProcedure $emergenciaProcedures)
        {
            $this->EmergenciaProcedure = $emergenciaProcedures;
        }

    public function index() {
        return view('emergencia.registroPacientes');
    }

    public function  prueba(){
        $prueba = $this->EmergenciaProcedure->getPrueba();

        return json_encode($prueba);
    }

    public function TitularAfiliadoPorApellido($apellido){

        $datos = $this->EmergenciaProcedure->ConsultarTitularAfiliadoPorApellidoAll($apellido);
        //return view('tables.titularApellido',compact('datos'));

        return json_encode($datos) ;
    }

    public function ConsultaCedula($cedula){

        $datos = $this->EmergenciaProcedure->ConsultarTitularAfiliadoPorCedulaAll($cedula);
        //return view('tables.titularApellido',compact('datos'));

        return $datos;
    }

    public function cmbTipoIdentificacion() {
        $datos = $this->EmergenciaProcedure->cmbTipoIdentificacion();
        return json_encode($datos);
    }

    public function cmbGeneroPaciente() {
        $datos = $this->EmergenciaProcedure->cmbGeneroPaciente();
        return json_encode($datos);
    }

    public function cmbNacionalidad() {
        $datos = $this->EmergenciaProcedure->cmbNacionalidad();
        return json_encode($datos);
    }

    public function cmbParentescoPaciente() {
        $datos = $this->EmergenciaProcedure->cmbParentescoPaciente();
        return json_encode($datos);
    }

    public function cmbTipoSangre() {
        $datos = $this->EmergenciaProcedure->cmbTipoSangre();
        return json_encode($datos);
    }

    public function cmbEstadoCivil() {
        $datos = $this->EmergenciaProcedure->cmbEstadoCivil();
        return json_encode($datos);
    }

    public function cmbEtnia() {
        $datos = $this->EmergenciaProcedure->cmbEtnia();
        return json_encode($datos);
    }


    function cmbProvincia($id) {
         $datos = $this->EmergenciaProcedure->cmbProvincia($id);
        return json_encode($datos);
    }

    public function cmbCiudad($id) {
        $datos = $this->EmergenciaProcedure->cmbCiudad($id);
        return json_encode($datos);
    }

    public function cmbParroquia($id) {
        if ($id == null)
        {
            $id = 1;
            $datos = $this->EmergenciaProcedure->cmbParroquia($id);
            return json_encode($datos);
        } else {

            $datos = $this->EmergenciaProcedure->cmbParroquia($id);
            return json_encode($datos);
        }

    }

    public function cmbSeguro() {
        $datos = $this->EmergenciaProcedure->cmbSeguro();
        return json_encode($datos);
    }

    public function cmbOtroSeguro() {
        $datos = $this->EmergenciaProcedure->cmbOtroSeguro();
        return json_encode($datos);
    }



    public function ConsultarInformacionAdicional($id_paciente) {
        $datos = $this->EmergenciaProcedure->ConsultarInformacionAdicional($id_paciente);
        return json_encode($datos);
    }


    public function cmbBeneficiarioPorParentesco($parentesco) {
        $datos = $this->EmergenciaProcedure->CmbIdentificacionBeneficiarioPorParentesco($parentesco);
        return json_encode($datos);
    }


    public function RegistroRapido(Request $request) {
        $id = $request->input('id');
       $apellido_paterno = $request->input('apellido_paterno');
       $apellido_materno = $request->input('apellido_materno');
       $primer_nombre = $request->input('primer_nombre');
       $segundo_nombre = $request->input('segundo_nombre');
       $fecha_nacimiento = $request->input('fecha_nacimiento');
       $telefono= $request->input('telefono');
       $ocupacion = $request->input('ocupacion');
       $apellidos = $request->input('apellidos_titular');
       $nombres = $request->input('nombres_titular');
       $cedula = $request->input('in_cedula');
       $genero = DB::table('tbgenero')->where('abreviatura', $request->input('genero'))->value('descripcion');
       $seguro = DB::table('tbTipoingreso')->where('codigo', $request->input('tipo_seguro'))->value('descripcion');
       $estado_civil = DB::table('tbestadocivil')->where('codigo',$request->input('tipo_seguro'))->value('descripcion');
        $lugar_nacimiento  = DB::table('tbCiudad')->where('codigo',$request->input('ciudad'))->value('descripcion');
       $paciente = $apellido_paterno.' '.$apellido_materno.' '.$primer_nombre.' '.$segundo_nombre.' ';
       $titular = $apellidos.' '.$nombres.'';
       //echo $genero;
       //echo $seguro;
       //echo $estado_civil;

        //echo $name;
        //dd($request);
        //$genero = DB::table('tbgenero')->where('abreviatura', 'm')->value('descripcion');
        //$seguro = DB::table('tbTipoingreso')->where('codigo', 'm')->value('descripcion');
        //$estado_civil = DB::table('tbestadocivil')->where('codigo', 'm')->value('descripcion');

        //echo $genero;



        //
        $fpdf = new Fpdf();

        $contents = Storage::get('LogoHospital.png');
        // inicio del ped
        $fpdf->AddPage();

        // encabezado
            // Logo
            // $fpdf->Image($contents,10,8,33);

            $fpdf->SetFont('Arial','B',16);
            // Movernos a la derecha
            $fpdf->Cell(80);
            // Título
            $fpdf->Cell(30,10,'Benemerita Sociedad Protectora de la Infancia',0,10,'C');
            $fpdf->Cell(30,10,'HOSPITAL "LEON BECERRA"',0,10,'C');
            $fpdf->Cell(30,10,'GUAYAQUIL',0,1,'C');
            $fpdf->ln(3);
            $fpdf->SetFont('Arial','B',12);
            $fpdf->Cell(190,10,'REGISTRO DE PACIENTES ',0,1,'C');
            $fpdf->line(0,53,210,53);
            //cuerpo
            $fpdf->SetFont('Arial','U',14);
            $fpdf->ln(5);
            $fpdf->Cell(95,5,'PACIENTE:'.$paciente.'',0,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'HISTORIA N°:'.$id.'',0,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'FECHA DE NACIMIENTO°:'.$fecha_nacimiento.'',0,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'LUGAR DE NACIMIENTO:',0   ,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'GENERO:'.$genero.'',0,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'ESTADO CIVIL:'.$estado_civil.'',0   ,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'DIRECCION:',0   ,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'TELEFONO:'.$telefono.'',0   ,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'TITULAR:'.$titular.'',0   ,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'VIA ADMISION:'.$seguro.'',0   ,1,'L');
            $fpdf->ln(2);
            $fpdf->Cell(95,5,'OCUPACION:'.$ocupacion.'',0   ,1,'L');

        $headers = ['Content-Type' => 'application/pdf'];
        return Response($fpdf->Output(),200,$headers);
       // return response()->file($fpdf->Output(),$headers); revisar como hacerlo esta en el manual

    }



    public function ReporteAdmision(Request $request) {
        //$datos = $this->EmergenciaProcedure->CmbIdentificacionBeneficiarioPorParentesco($parentesco);
        //return json_encode($datos);
        //return view('emergencia.admision001');

        //dd($request);

        if ($request->has('id')) {
            echo "existe el valor";
        }

    }




    public function CmbDpcumentosIess() {
        $datos = $this->EmergenciaProcedure->CmbDpcumentosIess();
        return json_encode($datos);
    }

    public function cmbIngresoCalificacion() {
        $datos = $this->EmergenciaProcedure->cmbIngresoCalificacion();
        return json_encode($datos);
    }

    public function updatePaciente(Request $request) {

        $user = Auth::user();
        /*
        $id = $request->input('id');
        $cedula = $request->input('cedula');
        $tipo_identificacion = $request->input('identificacion');
        $primer_nombre = $request->input('primer_nombre');
        $segundo_nombre = $request->input('segundo_nombre');
        $apellido_paterno = $request->input('apellido_paterno');
        $apellido_materno = $request->input('apellido_materno');
        $genero = $request->input('genero');
        $fecha_nacimiento = $request->input('fecha_nacimiento');
        $lugar_nacimiento = $request->input('lugar_nacimiento');
        $ocupacion = $request->input('ocupacion');
        $estado_civil = $request->input('estado_civil');
        $tipo_sangre = $request->input('sangre');
        $nacionalidad = $request->input('nacionalidad');
        $pais = $request->input('pais');
        $provincia = $request->input('provincia');
        $ciudad = $request->input('ciudad');
        $direccion = $request->input('direccion');
        $telefono = $request->input('telefono');
        $celular = $request->input('celular');
        $otro = $request->input('otro');
        $cedula_titular = $request->input('cedula_titular');
        $observacion = $request->input('observacion');
        $lugar_trabajo  = $request->input('lugar_trabajo');
        $tipo_parentesco = $request->input('parentesco');
        $tipo_beneficiario  = $request->input('tipo_beneficiario');
        $tipo_seguro = $request->input('seguro');
        $des_campo1 = null;
        $des_campo2 = null;
        $des_campo3 = null;
        $usuario_modificacion = $user->name;
        $pcname = $_SERVER['REMOTE_ADDR']; 
        $status = 1;
        $status_discapacidad = $request->input('status_discapacidad');
        $carnet_conadis,
        $status_otro_seguro= $request->input('status_otro_seguro');
        $tipo_seguro_iess =  if(isset($request->input('status_otro_seguro'))) ? return $request->input('status_otro_seguro') : null;
        $descripcion_otro_seguro ,
        $parroquia = $request->input('parroquia');
        $nombres_padre = $request->input('nombres_padre');
        $nombres_madre = $request->input('nombres_madre');
        $conyugue = $request->input('conyuque');
        $des_campo1IA,
        $des_campo2IA,
        $des_campo3IA,
        $cedula_seg_progenitor = $request->input('cedula_segundo_progenitor');
        $fecha_cedulacion,
        $fecha_fallecimiento,
        $instruccion,
        $cobertura_compartida,
        $prepagada,
        $issfa,
        $sspol,
        $iess,
        
        $this->EmergenciaProcedure->UpdatePacientes($id,$cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,$apellido_materno,$genero,$fecha_nacimiento,$lugar_nacimiento,$ocupacion,$estado_civil,$tipo_sangre ,$nacionalidad,$pais,$provincia,$ciudad,$direccion,$telefono,$celular,$otro,$cedula_titular,$observacion,$lugar_trabajo,$tipo_parentesco,$tipo_beneficiario,$tipo_seguro,$des_campo1,$des_campo2,$des_campo3,$usuario_modificacion,$pcname,$status,$status_discapacidad,$carnet_conadis,$status_otro_seguro,$tipo_seguro_iess,$descripcion_otro_seguro ,$parroquia,$nombres_padre ,$nombres_madre ,$conyugue,$des_campo1IA,$des_campo2IA,$des_campo3IA,$cedula_seg_progenitor ,$fecha_cedulacion ,$fecha_fallecimiento ,$instruccion ,$cobertura_compartida ,$prepagada ,$issfa,$sspol,$iess); */
        dd($request);

    }
}



