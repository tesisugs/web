<?php

namespace App\Core\Procedures;

use Illuminate\Database\Eloquent\Model;

class AdministracionProcedure extends Model
{
    public function consultarRolesAll(){
        return \DB::select('Call spConsultarRolesAll');
    }

    public function estadisticaRegistrosIngresoHospitalizacion()
    {
        return \DB::select('Call spEstadisticaRegistrosIngresoHospitalizacion');
    }


    public function estadisticaRegistrosIngresoHospitalizacionYears(){
        return \DB::select('Call spEstadisticaRegistrosIngresoHospitalizacionYears');
    }
    public function estadisticaRegistrosCeYears($fecha1,$fecha2){
        return \DB::select('Call spEstadisticaRegistroCeYears(?,?)',array($fecha1,$fecha2));
    }
    public function estadisticaRegistrosEgresosYears($fecha1,$fecha2){
        return \DB::select('Call spEstadisticaRegistrosEgresosYears(?,?)',array($fecha1,$fecha2));
    }
    public function estadisticaRegistrosEmergenciaYears($fecha1,$fecha2){
        return \DB::select('Call spEstadisticaRegistrosEmergenciaYears(?,?)',array($fecha1,$fecha2));
    }
    public function estadisticaRegistrosPacientesYears($fecha1,$fecha2){
        return \DB::select('Call spEstadisticaRegistrosPacientesYears(?,?)',array($fecha1,$fecha2));
    }




    public function estadisticaRegistrosEgresosHospitalizacion(){
        return \DB::select('Call spEstadisticaRegistrosEgresosHospitalizacion');
    }

    public function camasHabilitadas(){
        return \DB::select('Call spCamasHabilitadas');
    }

    // comunes

    // medicos al llamado
    public function consultarMedicosLLamados2($fechaini,$fechafin ){
        return \DB::select('Call spConsultarMedicosLLamados2(?,?)',array($fechaini,$fechafin));
    }

    public function insertCorreoMedicos($id,$paciente,$medico,$tipo_seguro,$correo,$usuario,$pcname){
        return \DB::select('Call spInsertCorreoMedicos(?,?,?,?,?,?,?)',array($id,$paciente,$medico,$tipo_seguro,$correo,$usuario,$pcname));
    }

    public function updateMedicollamados($id,$respuesta,$descripcion,$usuario){
        return \DB::select('Call SpUpdateMedicollamado(?,?,?,?)',array($id,$respuesta,$descripcion,$usuario));
    }

    public function consultarEspecializacionMedico($id){
        return \DB::select('Call spConsultarEspecializacionMedico(?)',array($id));
    }
    //  estadisticas
        //hospitalización
        public function consultarHospitaliacionesMes($fecha1,$fecha2){
            return \DB::select('Call SpConsultarHospitaliacionMes(?,?)',array($fecha1,$fecha2));
        }
        public function consultarHospitaliacionesYears($fecha1,$fecha2){
            return \DB::select('Call SpConsultarHospitaliacionanios(?,?)',array($fecha1,$fecha2));
        }
        public function consultarHospitaliacionSemanas($fecha1,$fecha2){
            return \DB::select('Call SpConsultarHospitaliacionSemanas(?,?)',array($fecha1,$fecha2));
        }
        public function consultarHospitaliacionRangos($fecha1,$fecha2){
            return \DB::select('Call SpConsultarHospitaliacionRangos(?,?)',array($fecha1,$fecha2));
        }
    //egresos
        public function consultarEgresosMes($fecha1,$fecha2){
            return \DB::select('Call SpconsultarEgresosMes(?,?)',array($fecha1,$fecha2));
        }
        public function consultarEgresosYears($fecha1,$fecha2){
            return \DB::select('Call spConsultarEgresosanios(?,?)',array($fecha1,$fecha2));
        }
        public function consultarEgresosSemanas($fecha1,$fecha2){
            return \DB::select('Call spConsultarEgresoSemanas(?,?)',array($fecha1,$fecha2));
        }
        public function consultarEgresosRangos($fecha1,$fecha2){
            return \DB::select('Call  spConsultarEgresoRangos(?,?)',array($fecha1,$fecha2));
        }
        // registro pacientes
        public function consultarRegistroPacientesMes($fecha1,$fecha2){
            return \DB::select('Call spconsultarRegistroPacientesMes(?,?)',array($fecha1,$fecha2));
        }
        public function consultarRegistroPacientesYears($fecha1,$fecha2){
            return \DB::select('Call SpConsultarHospitaliacionanios(?,?)',array($fecha1,$fecha2));
        }
        public function consultarRegistroPacientesSemanas($fecha1,$fecha2){
            return \DB::select('Call spRegistroPacientesSemanas(?,?)',array($fecha1,$fecha2));
        }
        public function consultarRegistroPacientesRangos($fecha1,$fecha2){
            return \DB::select('Call spRegistroPacientesRangos(?,?)',array($fecha1,$fecha2));
        }

    // Emergencia
        public function consultarEmergenciasMes($fecha1,$fecha2){
            return \DB::select('Call spConsultarEmergenciasMes(?,?)',array($fecha1,$fecha2));
        }
        public function consultarEmergenciasYears($fecha1,$fecha2){
            return \DB::select('Call spConsultarEmergenciasanios(?,?)',array($fecha1,$fecha2));
        }
        public function consultarEmergenciasSemanas($fecha1,$fecha2){
            return \DB::select('Call spConsultarEmergenciasSemanas(?,?)',array($fecha1,$fecha2));
        }
        public function consultarEmergenciasRangos($fecha1,$fecha2){
            return \DB::select('Call spconsultarEmergenciasRangos(?,?)',array($fecha1,$fecha2));
        }

    // consulta externa
        public function consultarCeMes($fecha1,$fecha2){
            return \DB::select('Call spConsultarCeMes(?,?)',array($fecha1,$fecha2));
        }
        public function consultarCeYears($fecha1,$fecha2){
            return \DB::select('Call spConsultarCeanios(?,?)',array($fecha1,$fecha2));
        }
        public function consultarCeSemanas($fecha1,$fecha2){
            return \DB::select('Call spConsultarCeSemanas(?,?)',array($fecha1,$fecha2));
        }
        public function consultarCeRangos($fecha1,$fecha2){
            return \DB::select('Call spConsultarCeRangos(?,?)',array($fecha1,$fecha2));
        }

}
