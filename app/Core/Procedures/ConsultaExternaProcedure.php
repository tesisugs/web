<?php

namespace App\Core\Procedures;

use Illuminate\Database\Eloquent\Model;

class ConsultaExternaProcedure extends Model
{
    // llamada a los procedimientos que se encargan de cargar combos


    public function cmbEstadosHospitalizacion(){
        return \DB::select('Call SpConsultarStatusHospitalizacionTodos2');
    }

    public function ConsultarHospitalizacionfechas($paciente,$desde,$hasta,$estado){
        return \DB::select('Call SpConsultarHospitalizacionfechas(?,?,?,?)',array($paciente,$desde,$hasta,$estado));
    }

    public function consultarEspecializacion(){
        return \DB::select('Call spConsultaEspecializacion');
    }

    public function consultarMedico(){
        return \DB::select('Call spConsultarMedico');
    }

    public function consultarDependencias(){
        return \DB::select('Call SpConsultarCmbDependencias');
    }

    public function consultarTipoConsulta(){
        return \DB::select('Call SpConsultarCmbTipoConsulta');
    }

     public function consultarHorarioMedico(){
        return \DB::select('Call spConsultarHorarioMedicoDetalles();');
    }

    // consulta de turnos fechas
    public function ConsultarTurnosAgenda($medico,$fecha){
        return \DB::select('Call SpConsultarAgendaMedicoTurnosAgenda(?,?)',array($medico,$fecha));
    }

    public function ConsultarAgendaMedicoTurnos2($medico){
        return \DB::select('Call SpConsultarAgendaMedicoTurnos2(?)',array($medico));
    }
//majomacontrolhospitalario.('298');

    public function insertConsultaExternaTurnosDiarios2
    ($turnos_diarios, $consulta_externa, $paciente, $cedula_paciente, $turnos, $medico,$observacion ,
     $fecha_registro_paciente, $hora_registro_paciente,$des_campo1,$des_campo2 ,$des_campo3,$usuario_ingreso,
     $usuario_modificacion ,$pcname,$status, $titular_representante){
        return \DB::select('Call spInsertConsultaExternaTurnosDiarios2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',array($turnos_diarios, $consulta_externa,
            $paciente, $cedula_paciente, $turnos, $medico,$observacion ,
            $fecha_registro_paciente, $hora_registro_paciente,$des_campo1,$des_campo2 ,$des_campo3,$usuario_ingreso,
            $usuario_modificacion ,$pcname,$status, $titular_representante));
    }
//


    public function insertIngresoPacienteConsultaExterna2
    (
        $id  , $codigo  , $paciente , $cedula_paciente , $fecha_nacimiento , $telefono_paciente , $observacion , $direccion_paciente ,
        $dependencias , $medico , $nombre_representante , $cedula_representante , $direccion_representante , $telefono_representante ,
        $observacion_eliminada , $fecha_registro_paciente , $hora_registro_paciente , $des_campo1 , $des_campo2 , $des_campo3  ,
        $usuario_ingreso  , $fecha_ingreso ,  $usuario_modificacion , $fecha_modificacion , $pcname , $status , $titular_representante ,
        $principal  , $asociado_1 , $asociado_2 , $NumeroAtencion  , $transaccion  , $factura , $tipo_seguro , $tipo_consulta ,
        $id_consulta , $fuente_informacion, $persona_entrega, $cedula_persona_entrega , $tipo_acompanante
    ) {
        return \DB::select('Call spInsertIngresoPacienteConsultaExterna2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id  , $codigo  , $paciente , $cedula_paciente , $fecha_nacimiento , $telefono_paciente ,
                $observacion , $direccion_paciente , $dependencias , $medico , $nombre_representante ,
                $cedula_representante , $direccion_representante , $telefono_representante , $observacion_eliminada ,
                $fecha_registro_paciente , $hora_registro_paciente , $des_campo1 , $des_campo2 , $des_campo3  ,
                $usuario_ingreso  , $fecha_ingreso , $usuario_modificacion , $fecha_modificacion , $pcname ,
                $status , $titular_representante , $principal  , $asociado_1 , $asociado_2 , $NumeroAtencion  ,
                $transaccion  , $factura , $tipo_seguro , $tipo_consulta , $id_consulta , $fuente_informacion,
                $persona_entrega, $cedula_persona_entrega , $tipo_acompanante));
    }

    public function consultarMedicoEspecialidad2($especialidad){

        return \DB::select('Call spConsultarMedicoEspecialidad2(?)',array($especialidad));
    }

    // consultar los medicos por especialidad
    public function insertConsultaExternaPreparacion(
      $id,$consulta_externa,$paciente,$titular,$temperatura,$pulso,$presion_arterial,$respiracion,$peso,$estatura,
      $talla,$discapacidad,$carnet_conais,$des1,$des2,$des3, $usuario_ingreso,$fecha_ingreso,$usuario_modificacion,
      $fecha_modificacion,$pcname,$status
    ){

        return \DB::select('Call spInsertConsultaExternaPreparacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',array($id,$consulta_externa,$paciente,$titular,$temperatura,$pulso,$presion_arterial,$respiracion,$peso,$estatura,
            $talla,$discapacidad,$carnet_conais,$des1,$des2,$des3, $usuario_ingreso,$fecha_ingreso,$usuario_modificacion,
            $fecha_modificacion,$pcname,$status ));
    }
    public function consultarHorariomedico2($medico){

        return \DB::select('Call spConsultarHorarioMedico(?)',array($medico));
    }
    public function consultarPreparacion($paciente,$medico,$dependencias,$fecha_i,$fecha_f){

        return \DB::select('Call SpConsultarIngresosPacientesConsultaExternaPreparacion_0(?,?,?,?)',array($paciente,$medico,$fecha_i,$fecha_f));
    }
    public function consultarPreparacion2(){

        return \DB::select('Call SpConsultarIngresosPacientesConsultaExternaPreparacionHoy');
    }
    public function consultarPreparacionPacientes($consulta){

        return \DB::select('Call spConsultarPreparacionPacientes(?)',array($consulta));
    }

    public function consultarPacienteHospitalizacionEgresado($paciente) {
        return \DB::select('Call spConsultarPacienteHospitalizacionEgresado(?)',array($paciente));
    }

    public function consultarPacientesCartaIess($paciente) {
        return \DB::select('Call spConsultarPacienteCartaIESS(?)',array($paciente));
    }

    public function consultarIngresosPacientesConsultaExternaNumeroAtencionMedicos($medico) {
        return \DB::select('Call SpConsultarIngresosPacientesConsultaExternaNumeroAtencionMedicos(?)',array($medico));
    }

    public function consultarIngresosPacientesCENumeroAtencionMedicosDia($medico,$dia) {
        return \DB::select('Call SpConsultarIngresosPacientesCENumeroAtencionMedicosDia(?,?)',array($medico,$dia));
    }



    public function consultarMedicoPorCodigo($medico) {
        return \DB::select('Call spConsultarMedicoPorCodigo(?)',array($medico));
    }

    public function horaUltimaAtencion($medico) {
        return \DB::select('Call spHoraUltimaAtencion(?)',array($medico));
    }

    public function horaUltimaAtencionDia($medico,$fecha) {
        return \DB::select('Call spHoraUltimaAtencionDia(?,?)',array($medico,$fecha));
    }



    public function tempConsultarPacienteId($id) {
        return \DB::select('Call spTempSpConsultarPacienteId(?)',array($id));
    }

    /*
    |--------------------------------------------------------------------------
    | Medicos consulta Externa
    |--------------------------------------------------------------------------
    |Metodos de insert de consulta externa
    |SpInsertFirmaPorAtencion
    |spInsertIngresoConsultaExternaAnamnesis1
    |spInsertIngresoConsultaExternaAnamnesis2
    |spInsertIngresoConsultaExternaAnamnesis3
    |spInsertConsultaExternaDiagnosticoPrincipal
    |spInsertConsultaExternaEvolucionPrescripcion
    |spInsertConsultaExternaEvolucionRayox
    |spInsertConsultaExternaEvolucionPrescripcionLaboratorio
    |spInsertConsultaExternaDiagnostico
    |SpUpdateConsultaExternaStatusAtendido
    |
    */

    // form
    public function insertFirmaPorAtencion(
        $id,$tipo_servicio,
        $id_atencion, $id_visita, $id_tipo_documento, $fecha_atencion, $firma, $status,
        $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname
    ){
        return \DB::select('Call SpInsertFirmaPorAtencion(?,?,?,?,?,?,?,?,?,?,?)',
            array($id,$tipo_servicio,
                $id_atencion, $id_visita, $id_tipo_documento, $fecha_atencion, $firma, $status,
                $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname
            ));
    }

    public function insertConsultaExternaAnamnesis1(
        $id,
        $consulta_externa, $paciente, $titular, $motivoConsulta, $antecedentePersonal, $afam1, $afam2, $afam3, $afam4,
        $afam5, $afam6, $afam7, $afam8, $afam9, $afam10, $afam10_otro, $enfermedad, $des_campo1, $des_campo2,
        $des_campo3, $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status
    ){

        return \DB::select('Call spInsertIngresoConsultaExternaAnamnesis1(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id,
            $consulta_externa, $paciente, $titular, $motivoConsulta, $antecedentePersonal, $afam1, $afam2, $afam3, $afam4,
            $afam5, $afam6, $afam7, $afam8, $afam9, $afam10, $afam10_otro, $enfermedad, $des_campo1, $des_campo2,
            $des_campo3, $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status ));
    }

    public function insertConsultaExternaAnamnesis2(
        $id,
        $consulta_externa, $paciente, $titular, $chk10,$chk11,$chk20,$chk21,$chk30,$chk31,$chk40,$chk41,$chk50,$chk51,$chk60, $chk61,
        $ExamenFisico1, $ExamenFisico2, $ExamenFisico3, $ExamenFisico4, $ExamenFisico5, $ExamenFisico6,
        $des_campo1, $des_campo2,
        $des_campo3, $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status
    ){

        return \DB::select('Call spInsertIngresoConsultaExternaAnamnesis2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id,
                $consulta_externa, $paciente, $titular, $chk10,$chk11,$chk20,$chk21,$chk30,$chk31,$chk40,$chk41,$chk50,$chk51,$chk60, $chk61,
                $ExamenFisico1, $ExamenFisico2, $ExamenFisico3, $ExamenFisico4, $ExamenFisico5, $ExamenFisico6,
                $des_campo1, $des_campo2,
                $des_campo3, $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status ));
    }

    public function insertConsultaExternaAnamnesis3(
        $id,$consulta_externa, $paciente, $titular,
        $chkRevision10, $chkRevision11, $chkRevision20, $chkRevision21, $chkRevision30,
        $chkRevision31, $chkRevision40, $chkRevision41, $chkRevision50, $chkRevision51,
        $chkRevision60, $chkRevision61, $chkRevision70, $chkRevision71, $chkRevision80,
        $chkRevision81, $chkRevision90, $chkRevision91, $chkRevision100, $chkRevision101,
        $txtRevisionOrganos, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
        $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status

    ){

        return \DB::select('Call spInsertIngresoConsultaExternaAnamnesis3(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array(
                $id,$consulta_externa, $paciente, $titular,
                $chkRevision10, $chkRevision11, $chkRevision20, $chkRevision21, $chkRevision30,
                $chkRevision31, $chkRevision40, $chkRevision41, $chkRevision50, $chkRevision51,
                $chkRevision60, $chkRevision61, $chkRevision70, $chkRevision71, $chkRevision80,
                $chkRevision81, $chkRevision90, $chkRevision91, $chkRevision100, $chkRevision101,
                $txtRevisionOrganos, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
                $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status
            ));
    }

    public function insertConsultaExternaDiagnosticoPrincipal(
    $id,$consulta_externa, $paciente, $titular, $principal,
    $asociado, $asociado2, $principalCiap2, $asociadoCiap,
    $asociadoCiap2, $observacion, $des_campo1,
    $des_campo2, $des_campo3, $usuario_ingreso, $fecha_ingreso,
    $usuario_modificacion, $fecha_modificacion, $pcname, $status

){

    return \DB::select('Call spInsertConsultaExternaDiagnosticoPrincipal(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        array(
            $id,$consulta_externa, $paciente, $titular, $principal,
            $asociado, $asociado2, $principalCiap2, $asociadoCiap,
            $asociadoCiap2, $observacion, $des_campo1,
            $des_campo2, $des_campo3, $usuario_ingreso, $fecha_ingreso,
            $usuario_modificacion, $fecha_modificacion, $pcname, $status
        ));
}

    public function insertConsultaExternaEvolucionPrescripcion (
        $id,$consulta_externa, $paciente, $titular, $codigo_far, $descripcion_far,
        $linea_far, $observacion, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
        $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status, $numero

    ){

        return \DB::select('Call spInsertConsultaExternaEvolucionPrescripcion (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array(
                $id,$consulta_externa, $paciente, $titular, $codigo_far, $descripcion_far,
                $linea_far, $observacion, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
                $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status, $numero
            ));
    }



    public function insertConsultaExternaEvolucionRayox(
        $id,$consulta_externa, $paciente, $titular, $codigo_far,
        $descripcion_far, $linea_far, $observacion,
        $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso, $fecha_ingreso,
        $usuario_modificacion, $fecha_modificacion, $pcname, $status

    ){
        return \DB::select('Call spInsertConsultaExternaEvolucionRayox(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array(
                $id,$consulta_externa, $paciente, $titular, $codigo_far,
                $descripcion_far, $linea_far, $observacion,
                $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso, $fecha_ingreso,
                $usuario_modificacion, $fecha_modificacion, $pcname, $status
            ));
    }

    public function insertConsultaExternaEvolucionPrescripcionLaboratorio(
        $id,$consulta_externa, $paciente, $titular, $codigo_far,
        $descripcion_far, $linea_far, $observacion, $des_campo1, $des_campo2, $des_campo3,
        $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status

    ){

        return \DB::select('Call spInsertConsultaExternaEvolucionPrescripcionLaboratorio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array(
                $id,$consulta_externa, $paciente, $titular, $codigo_far,
                $descripcion_far, $linea_far, $observacion, $des_campo1, $des_campo2, $des_campo3,
                $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status
            ));
    }


    public function insertConsultaExternaDiagnostico(
        $id,$consulta_externa, $paciente, $titular, $principal, $asociado, $asociado2,
        $principalCiap2, $asociadoCiap, $asociadoCiap2, $observacion, $des_campo1, $des_campo2, $des_campo3,
        $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status

    ){
        return \DB::select('Call spInsertConsultaExternaDiagnostico(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array(
                $id,$consulta_externa, $paciente, $titular, $principal, $asociado, $asociado2,
                $principalCiap2, $asociadoCiap, $asociadoCiap2, $observacion, $des_campo1, $des_campo2, $des_campo3,
                $usuario_ingreso, $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status
            ));
    }

    public function updateConsultaExternaStatusAtendido(
        $id, $des_campo1, $usuario_modificacion,$pcname){
        return \DB::select('Call SpUpdateConsultaExternaStatusAtendido(?,?,?,?)',
            array($id, $des_campo1, $usuario_modificacion,$pcname));
    }


    //FrmFromatoLaboratorio

    public function insertOrdenLaboratorio(
        $tipo_servicio, $id_atencion, $id_visita,
        $paciente, $titular, $hemograma_completo, $formula_schilling, $hematies, $hemoglobina,
        $hematocrito, $hematocrito_capilar, $reticulocitos, $plaquetas, $eritrosedimentacion,
        $plasmodium, $grupo_sanguineo_rh, $t_sangre, $t_coagulacion, $retraccion_coagulo,
        $t_protombina, $t_tromboplastina, $fibrinogeno, $dimero_d, $haptoglobina, $vitamina_b12,
        $acido_folico, $hemoglobina_glicosilada, $glucosa, $acido_urico, $creatinina, $urea, $bum, $amonio,
        $bilirrubina, $colesterol, $colesterol_dhl, $colesterol_ldl, $trigliceridos, $fructosamina,
        $gluco_hemoglobina, $apolipoproteina_a1, $apolipoproteina_b, $lipoproteina_lp, $proteinas_totales,
        $hierro, $ferritina, $tiroglobulina, $shbg, $peptido_c, $peptido_c_indice, $homa_ir,
        $beta_2_microglobulina, $homocisteina, $bnp, $sodio, $potasio, $cloro, $reserva_alcania,
        $calcio, $fosforo, $magnesio, $litio, $got, $gpt, $ggtp, $fij_hierro, $fosfatasa_alcalina,
        $deshidrogenasa_lactica, $colinesterasa, $amilasa, $cpk, $lipasa, $ck_mb, $fosfatasa_acida_total,
        $fosfatasa_acida_prostatica, $gasometria, $troponina_I, $troponina_T, $curva_glucosa,
        $tsh, $t3_total, $t3_libre, $t4_total, $t4_libre, $pth, $hgh, $igf_1, $igfbp_3, $hcg_embarazo,
        $hcg_cuantitativa, $fsh, $lh, $prolactina, $prolactina_2tomas, $progesterona, $beta_estradiol,
        $estriol_no_conjugado, $testosterona, $testosterona_libre, $dheas, $androstenediona, $acth,
        $cortisol_am, $cortisol_pm, $insulina, $eritropoyetina, $vdrl, $reaccion_widal,
        $well_felix, $brusela, $monotest, $ac_anti_dengue, $antiestreptolisina,
        $proteina_c_reaciva_latex, $proteina_c_reaciva_cuantitativa, $factor_reumatoide,
        $ra_test, $le_test, $celulas_le, $ac_anti_nucleares, $ac_anti_dna, $ac_anti_tpo,
        $ac_anti_tiroglobulina, $ig_g, $ig_m, $ig_a, $ig_e, $complemento_c3, $complemento_c4,
        $ac_anti_toxoplasma_igm, $ac_anti_toxoplasma_igg, $ac_anti_rubeola_igm,
        $ac_anti_rubeola_igg, $ac_anti_citomegalovirus_igm, $ac_anti_citomegalovirus_igg,
        $ac_anti_herpes1y2_igm, $ac_anti_herpes_tipo1_igg, $ac_anti_herpes_tipo2_igg,
        $ac_anti_ameba, $ac_anti_c_trachomatis_igm, $ac_anti_h_pylori_igg, $ac_anti_hepatitis_a_igm,
        $ac_anti_hepatitis_a_igg, $hbsag, $ac_anti_hbs, $hbeag, $ac_anti_hbc_igm, $ac_anti_hbc_igg,
        $ac_anti_hepatitis_c, $ac_anti_hiv_1y2, $p_coombs_directa, $p_coombs_indirecta,
        $ag_carcinoembrionario, $alfafetoproteina, $psa, $psa_libre, $ca_72_4, $ca_15_3, $ca_125,
        $ca_19_9, $cyfra_21_1, $digoxina, $fenitoina, $carbamazepina, $acido_valproico,
        $fenobarbital, $teofilina, $benzodiazepina, $cultivo_secrecion_faringea,
        $cultivo_secreciones, $hemocultivo, $gram_y_fresco, $examen_koh, $estreptococo_grupo_a,
        $cultivo_baar, $fisico_quimico_y_sedimento, $micro_albumina, $na_urinario,
        $k_urinario, $ca_urinario, $proteina_24horas, $pyrilinks_d, $prueba_embarazo,
        $cultivo_orina, $aclaramiento_creatinina, $creatinina_orina, $parasitologico,
        $tincion_wrigth, $sangre_oculta, $h_pylori_antigeno, $cultivo_heces,
        $rotavirus, $adenovirus, $origen, $contaje_celular, $proteinas, $ldh,
        $glucosa_liq_biologicos, $otros, $codigo_far, $descripcion_far, $linea_far,
        $observacion, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
        $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status){
        return \DB::select('Call SpInsertOrdenLaboratorio
                (?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?,
                ?,?)',
            array($tipo_servicio, $id_atencion, $id_visita,
                $paciente, $titular, $hemograma_completo, $formula_schilling, $hematies, $hemoglobina,
                $hematocrito, $hematocrito_capilar, $reticulocitos, $plaquetas, $eritrosedimentacion,
                $plasmodium, $grupo_sanguineo_rh, $t_sangre, $t_coagulacion, $retraccion_coagulo,
                $t_protombina, $t_tromboplastina, $fibrinogeno, $dimero_d, $haptoglobina, $vitamina_b12,
                $acido_folico, $hemoglobina_glicosilada, $glucosa, $acido_urico, $creatinina, $urea, $bum, $amonio,
                $bilirrubina, $colesterol, $colesterol_dhl, $colesterol_ldl, $trigliceridos, $fructosamina,
                $gluco_hemoglobina, $apolipoproteina_a1, $apolipoproteina_b, $lipoproteina_lp, $proteinas_totales,
                $hierro, $ferritina, $tiroglobulina, $shbg, $peptido_c, $peptido_c_indice, $homa_ir,
                $beta_2_microglobulina, $homocisteina, $bnp, $sodio, $potasio, $cloro, $reserva_alcania,
                $calcio, $fosforo, $magnesio, $litio, $got, $gpt, $ggtp, $fij_hierro, $fosfatasa_alcalina,
                $deshidrogenasa_lactica, $colinesterasa, $amilasa, $cpk, $lipasa, $ck_mb, $fosfatasa_acida_total,
                $fosfatasa_acida_prostatica, $gasometria, $troponina_I, $troponina_T, $curva_glucosa,
                $tsh, $t3_total, $t3_libre, $t4_total, $t4_libre, $pth, $hgh, $igf_1, $igfbp_3, $hcg_embarazo,
                $hcg_cuantitativa, $fsh, $lh, $prolactina, $prolactina_2tomas, $progesterona, $beta_estradiol,
                $estriol_no_conjugado, $testosterona, $testosterona_libre, $dheas, $androstenediona, $acth,
                $cortisol_am, $cortisol_pm, $insulina, $eritropoyetina, $vdrl, $reaccion_widal,
                $well_felix, $brusela, $monotest, $ac_anti_dengue, $antiestreptolisina,
                $proteina_c_reaciva_latex, $proteina_c_reaciva_cuantitativa, $factor_reumatoide,
                $ra_test, $le_test, $celulas_le, $ac_anti_nucleares, $ac_anti_dna, $ac_anti_tpo,
                $ac_anti_tiroglobulina, $ig_g, $ig_m, $ig_a, $ig_e, $complemento_c3, $complemento_c4,
                $ac_anti_toxoplasma_igm, $ac_anti_toxoplasma_igg, $ac_anti_rubeola_igm,
                $ac_anti_rubeola_igg, $ac_anti_citomegalovirus_igm, $ac_anti_citomegalovirus_igg,
                $ac_anti_herpes1y2_igm, $ac_anti_herpes_tipo1_igg, $ac_anti_herpes_tipo2_igg,
                $ac_anti_ameba, $ac_anti_c_trachomatis_igm, $ac_anti_h_pylori_igg, $ac_anti_hepatitis_a_igm,
                $ac_anti_hepatitis_a_igg, $hbsag, $ac_anti_hbs, $hbeag, $ac_anti_hbc_igm, $ac_anti_hbc_igg,
                $ac_anti_hepatitis_c, $ac_anti_hiv_1y2, $p_coombs_directa, $p_coombs_indirecta,
                $ag_carcinoembrionario, $alfafetoproteina, $psa, $psa_libre, $ca_72_4, $ca_15_3, $ca_125,
                $ca_19_9, $cyfra_21_1, $digoxina, $fenitoina, $carbamazepina, $acido_valproico,
                $fenobarbital, $teofilina, $benzodiazepina, $cultivo_secrecion_faringea,
                $cultivo_secreciones, $hemocultivo, $gram_y_fresco, $examen_koh, $estreptococo_grupo_a,
                $cultivo_baar, $fisico_quimico_y_sedimento, $micro_albumina, $na_urinario,
                $k_urinario, $ca_urinario, $proteina_24horas, $pyrilinks_d, $prueba_embarazo,
                $cultivo_orina, $aclaramiento_creatinina, $creatinina_orina, $parasitologico,
                $tincion_wrigth, $sangre_oculta, $h_pylori_antigeno, $cultivo_heces,
                $rotavirus, $adenovirus, $origen, $contaje_celular, $proteinas, $ldh,
                $glucosa_liq_biologicos, $otros, $codigo_far, $descripcion_far, $linea_far,
                $observacion, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
                $fecha_ingreso, $usuario_modificacion, $fecha_modificacion, $pcname, $status));
    }

    // '========= VISUALIZAR 005 =========
        public function consultarPacienteId($id){
            return \DB::select('Call spConsultarPacienteId (?)',array($id));
        }

        public function consultarIngresosPacientesConsultaExternaPreparacionForm005_1($id){
            return \DB::select('Call SpConsultarIngresosPacientesConsultaExternaPreparacionForm005_1(?)',array($id));
        }

        public function consultar005PrescripcionConsultaExterna_rpt($paciente,$desde){
            return \DB::select('Call spConsultar005PrescripcionConsultaExterna_rpt(?)',array($paciente,$desde));
        }

        public function rpFirmas($atencion,$servicio,$vista,$documento){
            return \DB::select('Call SpRpFirmas(?,?,?,?)',array($atencion,$servicio,$vista,$documento));
        }
    //'========= DATOS DE PREPARACIÓN =========
    public function consultarPreparacionConsultaExternaPorConsultaExterna($Consulta_externa){
        return \DB::select('Call SpConsultarPreparacionConsultaExternaPorConsultaExterna(?)',array($Consulta_externa));
    }

    // '========= ACTUALIZAR ESTADO EN TABLA DE TURNOS DE CONSULTA EXTERNA Y EN VISOR DE CONSULTA EXTERNA
    public function insertVisor($codigo,$medico,$consultorio,$turno){
        return \DB::select('Call SpInsertVisor(?)',array($codigo,$medico,$consultorio,$turno));
    }

}
