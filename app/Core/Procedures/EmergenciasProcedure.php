<?php

namespace App\Core\Procedures;

use Illuminate\Database\Eloquent\Model;

class EmergenciasProcedure extends Model
{
    // llamada a los procedimientos que se encargan de cargar combos


    public function cmbTipoIdentificacion(){
     return \DB::select('Call SpConsultarCmbTipoIdentificacionPersona');
    }

    public function cmbGeneroPaciente(){
        return \DB::select('Call SpConsultarCmbGeneroPaciente');
    }
   public function cmbNacionalidad(){        
    return \DB::select('Call SpConsultarCmbNacionalidad');
    }

    public function cmbParentescoPaciente(){
        return \DB::select('Call SpConsultarCmbParentescoPaciente');
  }
    public function cmbTipoSangre(){
        return \DB::select('Call SpConsultarCmbTipoSangre');
    }

    public function cmbEstadoCivil(){
        return \DB::select('Call SpConsultarEstadoCivil');
 }

    public function cmbEtnia(){
        return \DB::select('Call spConsultarTipoCultura');
    }

    public function cmbSeguro(){
        return \DB::select('Call SpConsultarTipoSeguro');
    }

    public function cmbOtroSeguro(){
        return \DB::select('Call SpConsultarTipoSeguroIESS');
    }

    public function cmbIngresoCalificacion(){
        return \DB::select('Call SpConsultarTipoIngresoPorClasificacion(?)',array(2));
    }


    public function cmbProvincia($pais){
        return \DB::select('Call SpConsultarGeneralProvinciaPorPais(?)',array($pais));
    }

    public function cmbCiudad($ciudad){
        return \DB::select('Call SpConsultarGeneralCiudad(?)',array($ciudad));
    }

    public function cmbParroquia($parroquia){
        return \DB::select('Call SpConsultarGeneralParroquia(?)',array($parroquia));
    }
    public function CmbIdentificacionBeneficiarioPorParentesco($parentesco) {
        return \DB::select('call SpConsultarIdentificacionBeneficiarioPorParentesco(?)',array($parentesco));
    }

    public function CmbDpcumentosIess() {
        return \DB::select('call SpConsultarTipoDocumentoIess');
    }

    // procedimientos de consulta de datos

    public function ConsultarTitularAfiliadoPorApellidoAll($apellido) {
        return \DB::select('call SpConsultarTitularAfiliadoPorApellidoAll(?)',array($apellido));
    }

    public function ConsultarTitularAfiliadoPorCedulaAll($cedula) {
        return \DB::select('call SpConsultarEspecificaTitularAfiliadoPorCedulaAll(?)',array($cedula));
    }

    public function ConsultarTitularAfiliadoPorCedula($cedula) {
        return \DB::select('call SpConsultarEspecificaTitularAfiliadoPorCedula(?)',array($cedula));
    }

    public function ConsultarInformacionAdicional($id_paciente) {
        return \DB::select('call SpConsultarPacienteInformacionAdicional(?)',array($id_paciente));
    }

    // procedimeitnos de actualización
    public function UpdatePacientes(
            $id,
            $cedula,
            $tipo_identificacion,
            $primer_nombre,$segundo_nombre,$apellido_paterno,$apellido_materno,
            $genero,$fecha_nacimiento,
            $lugar_nacimiento,
            $ocupacion,
            $estado_civil,$tipo_sangre ,
            $nacionalidad,$pais,
            $provincia,$ciudad,$direccion,
            $telefono,$celular,$otro,
            $cedula_titular,$observacion,$lugar_trabajo,
            $tipo_parentesco,$tipo_beneficiario,
            $tipo_seguro,
            $des_campo1,$des_campo2,$des_campo3,
            $usuario_modificacion,$pcname,$status,
            $status_discapacidad,
            $carnet_conadis,
            $status_otro_seguro,
            $tipo_seguro_iess,$descripcion_otro_seguro ,
            $parroquia,$nombres_padre ,$nombres_madre ,
            $conyugue,$des_campo1IA,$des_campo2IA,$des_campo3IA,
            $cedula_seg_progenitor ,
            $fecha_cedulacion ,$fecha_fallecimiento ,$instruccion ,$cobertura_compartida ,
            $prepagada ,$issfa,$sspol,$iess)
            {
                return \DB::select('call SpUpdatePaciente2
                (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                 ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                 ?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                    array($id,$cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,
                        $apellido_materno,$genero,$fecha_nacimiento,$lugar_nacimiento,$ocupacion,$estado_civil,
                        $tipo_sangre ,$nacionalidad,$pais,$provincia,$ciudad,$direccion,$telefono,$celular,
                        $otro,$cedula_titular,$observacion,$lugar_trabajo,$tipo_parentesco,$tipo_beneficiario,
                        $tipo_seguro,$des_campo1,$des_campo2,$des_campo3,$usuario_modificacion,$pcname,$status,
                        $status_discapacidad,$carnet_conadis,$status_otro_seguro,$tipo_seguro_iess,
                        $descripcion_otro_seguro ,$parroquia,$nombres_padre ,$nombres_madre ,$conyugue,
                        $des_campo1IA,$des_campo2IA,$des_campo3IA,$cedula_seg_progenitor ,$fecha_cedulacion ,
                        $fecha_fallecimiento ,$instruccion ,$cobertura_compartida ,$prepagada ,$issfa,
                        $sspol,$iess));
            }
    // procedimientos de inserción de datos
    //
    public function InsertPacientes(
        $cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,$apellido_materno,$genero,
        $fecha_nacimiento, $lugar_nacimiento,$ocupacion,$estado_civil,$tipo_sangre ,$nacionalidad,$pais,$provincia,
        $ciudad,$direccion,$telefono, $celular,$otro,$cedula_titular,$observacion,$lugar_trabajo,$tipo_parentesco,
        $tipo_beneficiario,$tipo_seguro, $des_campo1,$des_campo2,$des_campo3,$usuario_ingreso,$pcname,$status,
        $status_discapacidad,$carnet_conadis, $status_otro_seguro,$tipo_seguro_iess,$descripcion_otro_seguro ,
        $etnico, $parroquia,$nombres_padre , $nombres_madre ,$conyugue,$des_campo1IA,$des_campo2IA,$des_campo3IA,
        $cedula_seg_progenitor ,$fecha_cedulacion ,$fecha_fallecimiento ,$instruccion ,$cobertura_compartida ,$prepagada ,
        $issfa,$isspol,$iess,$rt
    )
    {
        return \DB::select ('call SpInsertPaciente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', array(
            $cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,
            $apellido_materno, $genero, $fecha_nacimiento, $lugar_nacimiento, $ocupacion,
            $estado_civil, $tipo_sangre , $nacionalidad, $pais, $provincia,
            $ciudad, $direccion, $telefono, $celular, $otro,
            $cedula_titular, $observacion, $lugar_trabajo, $tipo_parentesco, $tipo_beneficiario,
            $tipo_seguro, $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
            $pcname, $status, $status_discapacidad, $carnet_conadis, $status_otro_seguro,
            $tipo_seguro_iess, $descripcion_otro_seguro,$etnico, $parroquia , $nombres_padre,
            $nombres_madre , $conyugue, $des_campo1IA, $des_campo2IA, $des_campo3IA,
            $cedula_seg_progenitor , $fecha_cedulacion , $fecha_fallecimiento , $instruccion , $cobertura_compartida ,
            $prepagada , $issfa, $isspol, $iess,$rt));
    }

    public function consultarPrueba() {
        return \DB::select('call  prueba');
    }


    public function InsertPacientesLog(
        $paciente,$cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,
        $apellido_paterno,$apellido_materno,$genero,$fecha_nacimiento,$lugar_nacimiento,
        $ocupacion ,$estado_civil,$tipo_sangre ,$nacionalidad,$pais,
        $provincia,$ciudad,$direccion,$telefono,$celular,
        $otro,$cedula_titular,$observacion,$lugar_trabajo,$tipo_parentesco,
        $tipo_beneficiario,$tipo_seguro,$des_campo1,$des_campo2,$des_campo3,
        $usuario_ingreso,$fecha_ingreso,$pcname,$status,$status_discapacidad ,
        $carnet_conadis ,$status_otro_seguro ,$tipo_seguro_iess ,$descripcion_otro_seguro ,$etnico
        ) {
        return \DB::select('call SpInsertPacienteLog(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', array(
            $paciente,
            $cedula,$tipo_identificacion,$primer_nombre,$segundo_nombre,$apellido_paterno,
            $apellido_materno,$genero,$fecha_nacimiento,$lugar_nacimiento,$ocupacion ,
            $estado_civil,$tipo_sangre ,$nacionalidad,$pais,$provincia,
            $ciudad,$direccion,$telefono,$celular,$otro,
            $cedula_titular,$observacion,$lugar_trabajo,$tipo_parentesco,$tipo_beneficiario,
            $tipo_seguro,$des_campo1,$des_campo2,$des_campo3,$usuario_ingreso,
            $fecha_ingreso,$pcname,$status,$status_discapacidad ,$carnet_conadis ,
            $status_otro_seguro ,$tipo_seguro_iess ,$descripcion_otro_seguro,$etnico));
    }

    // procedimiento nuevos usados en asitencia medica
    public function CmbMedico() {
        return \DB::select('call spConsultarMedico');
    }

    public function ConsultarBeneficiarioTitularAfiliado($cedula) {
        return \DB::select('call SpConsultarBeneficiarioPorTitularAfiliado (?)',array($cedula));
    }

    public function ConsultarRegistroAdmision($id) {
        return \DB::select('call spConsultar1RegistroAdmision(?)',array($id));
    }

    public function consultarAtencionPaciente($id) {
        return \DB::select('call spConsultarAtencionPaciente(?)',array($id));
    }

    public function ConsultarPacienteId($id) {
        return \DB::select('call spConsultarPacienteId(?)',array($id));
    }
    public function Consultarfirmas($atencion,$servicio,$visita,$documentos) {
        return \DB::select('call SpRpFirmas(?,?,?,?)',array($atencion,$servicio,$visita,$documentos));
    }

    public function cmbParentesco() {
        return \DB::select('call SpConsultarCmbParentescoPaciente2');
    }

    public function consultarIngresosPacientesAdmision() {
        return \DB::select('call SpConsultarIngresosPacientesAdmisionPorUsuario');
    }

    public function inserRegistroAdmision(
        $id,$afiliado,$paciente,$fecha,$hora,$responsable,$observacion,
        $doctor,$usuario_ingreso,$pcname,$tipo_ingreso,$numero_atencion,$campo3,
        $fuente_informacion, $persona_entrega,$derivacion,$hospital,
        $cedula_entrega,$tipo_acompanante
    ) {
        return \DB::select('call SpInsertRegistroDeAdmision(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', array(
            $id,$afiliado,$paciente,$fecha,$hora,
            $responsable,$observacion,
            $doctor,$usuario_ingreso,$pcname,$tipo_ingreso,
            $numero_atencion,$campo3,
            $fuente_informacion, $persona_entrega,
            $derivacion,$hospital,
            $cedula_entrega,$tipo_acompanante));
    }

    // procedimientos para llenar el acta de servicio firmas //
    public function prueba() {
        return \DB::select('call prueba');
    }

}
