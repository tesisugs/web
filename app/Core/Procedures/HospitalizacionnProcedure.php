<?php

namespace App\Core\Procedures;

use Illuminate\Database\Eloquent\Model;

class HospitalizacionnProcedure extends Model
{
    //
    public function consultarMedicosLlamadoIngresoHospitalario($paciente,$medico,$fecha){
        return \DB::select('Call spConsultarMedicosLlamadoIngresoHospitalario(?,?,?)',array($paciente,$medico,$fecha));
    }

    public function cmbEstadosHospitalizacion(){
        return \DB::select('Call SpConsultarStatusHospitalizacionTodos2');
    }

    public function cmbAreas(){
        return \DB::select('Call spConsultarSala');
    }


    public function consultarHospitalizacionfechas($paciente,$desde,$hasta,$estado){
        return \DB::select('Call SpConsultarHospitalizacionfechas(?,?,?,?)',array($paciente,$desde,$hasta,$estado));
    }

    public function consultarHospitalizacionTodos($paciente){
        return \DB::select('Call SpConsultarHospitalizacionTodos(?)',array($paciente));
    }

    public function consultarHospitalizacion($id) {
        return   \DB::select('Call spConsultarHospitalizacion(?)',array($id));
    }

    public function consultarInformacionPaciente($id_paciente) {
        return \DB::select('Call spConsultarDatosPaciente(?)',array($id_paciente));
    }
    public function consultarDatosCama($codigo_cama) {
        return \DB::select('Call spConsultardatosCama(?)',array($codigo_cama));
    }


    public function consultarHospitalizacionfechasEgreso($paciente,$desde,$hasta){
        return \DB::select('Call SpConsultarHospitalizacionfechasEgreso(?,?,?)',array($paciente,$desde,$hasta));
    }


    public function consultarRegistroAdmision(){
        return \DB::select('Call spConsultar1RegistroAdmision');
    }

    public function consultarTipoEgreso(){
        return \DB::select('Call spConsultarTipoEgreso');
    }

    public function consultarTipoEgresoAlta(){
        return \DB::select('Call spConsultarTipoEgrsoAlta');
    }

    public function consultarTipoDiagnosticoNombre($descripcion){
        return \DB::select('Call spConsultarDiagnosticoNombre(?)',array($descripcion));
    }

    public function consultarTipoDiagnosticoCodigo($codigo){
        return \DB::select('Call spConsultarDiagnosticoCodigo(?)',array($codigo));
    }

    public function consultarPacientesNombre($apellido,$estado){
        $desde = '1900/01/01';
        $hasta = '1900/01/01';
        return \DB::select('Call spConsultarPacienteNombres2(?,?,?,?)',array($apellido,$desde,$hasta,$estado));
    }

    public function updateIngresosHospitalizacion(
        $id,
        $Procedencias ,
        $Nombre_Responsable ,
        $Direccion_Responsable ,
        $Documento_Responsable ,
        $Telefono_Responsable ,
        $Observaciones ,
        $Servicios ,
        $medico ,
        $principal ,
        $asociado_1 ,
        $asociado_2 ,
        $Medico_Observacion ,
        $fecha_modificacion ,
        $usuario_modificacion ,
        $pcname ,
        $derivacion ,
        $hospital ,
        $status_documento ,
        $observacion_documento
    )
    {
        return \DB::select('Call spUpdateIngresosHospitalizacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array( $id,
            $Procedencias ,
            $Nombre_Responsable ,
            $Direccion_Responsable ,
            $Documento_Responsable ,
            $Telefono_Responsable ,
            $Observaciones ,
            $Servicios ,
            $medico ,
            $principal ,
            $asociado_1 ,
            $asociado_2 ,
            $Medico_Observacion ,
            $fecha_modificacion ,
            $usuario_modificacion ,
            $pcname ,
            $derivacion ,
            $hospital ,
            $status_documento ,
            $observacion_documento ));
    }

    public function consultarTipoCama(){
        return \DB::select('Call SpConsultarTipoCama');
    }
    public function consultarCamaPorSalaCodigo($id){
        return \DB::select('Call spConsultarCamaPorSalaCodigo(?)',array($id));
    }

    public function consultarCamaPorSalaCodigoSala($id){
        return \DB::select('Call spConsultarCamaPorSalaCodigoSala2(?)',array($id));
    }

    public function consultarCamaPorSalaSalaHabilitadaCunas($sala,$camas){
        return \DB::select('Call spConsultarCamaPorSalaSalaHabilitadaCunas(?,?)',array($sala,$camas));
    }


    public function consultarHospitalizacionId($id){
        //dd("hola");
         return \DB::select('Call SpConsultarHospitalizacionId(?)',array($id));
        //dd(\DB::select( \DB::raw('Call SpConsultarHospitalizacionId(?)',array($id))));
        //dd(\DB::selectRaw('Call SpConsultarHospitalizacionId(?)',array($id)));

    }

    public function UpdateHospitalizacionMedico($id,$Fecha_Hora_Ingreso,$usuario_modificacion,$pcname,$status,$medicoTraspaso,
            $servicios_medico){

        return \DB::select('Call spUpdateHospitalizacionMedico(?,?,?,?,?,?,?)',
            array($id,$Fecha_Hora_Ingreso,$usuario_modificacion,$pcname,$status,$medicoTraspaso,
            $servicios_medico));
    }

    public function consultarPacienteId($id){

        return \DB::select('Call spConsultarPacienteId(?)',
            array($id));
    }

    public function consultarDerivacionHospital(){

        return \DB::select('Call spConsultarDerivacionHospital');
    }



    public function UpdateHospitalizacionSeguro($id,$Fecha_Hora_Ingreso,$usuario_modificacion,$pcname,$status,$seguroTraspaso){

        return \DB::select('Call spUpdateHospitalizacionSeguro(?,?,?,?,?,?)',
            array($id,$Fecha_Hora_Ingreso,$usuario_modificacion,$pcname,$status,$seguroTraspaso));
    }

    public function insertHospitalizacionEgresos($id,$id_hospitalizacion,$medico_egreso,$principal_egreso,
                                                 $asociadoEgreso1,$asociadoEgreso2,$tipo_egreso,$complicacion,$tipo_complicacion,$fecha_ingreso,
                                                 $fecha_salida,$hora_ingreso,$hora_salida,$tipo_egreso_alta,$codigo_cama,$status_camas,
                                                 $Procedencias,$dias_transcurridos,$horas_transcurridas,$valor,$des_campo1,$des_campo2,$des_campo3,
                                                 $usuario_ingreso,$pcname,$status,$observacion){

        return \DB::select('Call spInsertHospitalizacionEgresos(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id,$id_hospitalizacion,$medico_egreso,$principal_egreso,
                $asociadoEgreso1,$asociadoEgreso2,$tipo_egreso,$complicacion,$tipo_complicacion,
                $fecha_ingreso, $fecha_salida,$hora_ingreso,$hora_salida,
                $tipo_egreso_alta,$codigo_cama,$status_camas,
                $Procedencias,$dias_transcurridos,$horas_transcurridas,$valor,$des_campo1,$des_campo2,$des_campo3,
                $usuario_ingreso,$pcname,$status,$observacion));
    }

    public function SpAnulacionHospitalizacion($id, $id_hospitalizacion,$paciente, $observacion, $usuario_ingreso,
                                                $fecha_modificacion,$codigo, $status_camas, $ocupante,
                                                $usuario_modificacion, $observacion_eliminada,$pcname)
    {
        return \DB::select('Call SpAnulacionHospitalizacion(?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id, $id_hospitalizacion,$paciente, $observacion, $usuario_ingreso,
                $fecha_modificacion,$codigo, $status_camas, $ocupante,
                $usuario_modificacion, $observacion_eliminada,$pcname));
    }

    public function insertHospitalizacionTraspaso(
        $id,$id_hospitalizacion ,$paciente_traspaso , $fecha_ingreso , $fecha_salida  , $hora_ingreso , $hora_salida ,
        $dias_transcurridos , $horas_transcurridas , $valor , $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
        $usuario_modificacion,$pcname, $status, $observacion_traslado,$codigo_cama_anterior, $codigo_cama_traslado,
        $numero_traslado,$secuencia
    )
    {
        return \DB::select('Call SpInsertHospitalizacionTraspaso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id,$id_hospitalizacion ,$paciente_traspaso , $fecha_ingreso , $fecha_salida  , $hora_ingreso , $hora_salida ,
                $dias_transcurridos , $horas_transcurridas , $valor , $des_campo1, $des_campo2, $des_campo3, $usuario_ingreso,
                $usuario_modificacion,$pcname, $status, $observacion_traslado,$codigo_cama_anterior, $codigo_cama_traslado,
                $numero_traslado,$secuencia ));
    }



    public function  insertHospitalizacion
        ($id  ,
        $SecuenciaHO  ,
        $No_Historial  ,
        $Fecha_Hora_Ingreso ,
        $cobertura_Maxima  ,
        $fecha_nacimiento ,
        $Normal ,
        $Cirugia_Dia ,
        $Fecha_de_Registro,
        $Presupuesto ,
        $Pre_Factura ,
        $paciente ,
        $NumeroAtencion  ,
        $habitacion ,
        $Procedencias ,
        $Paquete_Atencion ,
        $Nombre_Responsable ,
        $Direccion_Responsable ,
        $Documento_Responsable ,
        $Telefono_Responsable ,
        $Plan_Atencion ,
        $CoaSeguro ,
        $Deducible ,
        $Observaciones ,
        $Siniestro ,
        $Referencia_1 ,
        $Referencia_2 ,
        $Medico_Fecha  ,
        $Servicios ,
        $medico  ,
        $principal  ,
        $asociado_1 ,
        $asociado_2 ,
        $Medico_Observacion ,
        $garantia,
        $Garantia_plan,
        $Garantia_Coaseguros ,
        $Garantia_Deducible ,
        $Garantia_Moneda ,
        $Garantia_siniestro ,
        $Garantia_referencia_1 ,
        $Garantia_referencia_2 ,
        $Dieta,
        $Dieta_Observaciones  ,
        $Dieta_Contenido ,
        $Varios_Fecha ,
        $Varios_Musica ,
        $Varios_Musica_desde ,
        $Varios_Musica_hasta ,
        $Varios_Televisor  ,
        $Varios_Televisor_desde ,
        $Varios_Televisor_hasta ,
        $Varios_Periodico ,
        $Varios_Acompanante ,
        $Varios_Acepta_Visitas_si ,
        $Varios_Acepta_Visitas_no ,
        $Varios_Acepta_Visitas_restringido ,
        $Varios_Acepta_llamadas ,
        $Varios_Dieta_aconpanante ,
        $fecha_egreso  ,
        $medico_egreso ,
        $principal_egreso ,
        $asociadoEgreso1 ,
        $asociadoEgreso2 ,
        $tipo_egreso ,
        $complicacion ,
        $tipo_complicacion ,
        $des_campo1 ,
        $des_campo2 ,
        $des_campo3 ,
        $fecha_registro  ,
        $usuario_registro  ,
        $fecha_modificacion  ,
        $usuario_modificacion  ,
        $pcname ,
        $status ,
        $codigo_cama ,
        $traspaso ,
        $fecha_hora_traspaso ,
        $genero ,
        $observacion_traspaso,
        $transaccion  ,
        $titular ,
        $tipo_egreso_alta ,
        $derivacion ,
        $hospital ,
        $status_documento ,
        $osbservacion_documento,
        $observacion_eliminada
        )
    {
        return \DB::select('Call spInsertHospitalizacion
        (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            array($id  ,
                $SecuenciaHO  ,
                $No_Historial  ,
                $Fecha_Hora_Ingreso ,
                $cobertura_Maxima  ,
                $fecha_nacimiento ,
                $Normal ,
                $Cirugia_Dia ,
                $Fecha_de_Registro,
                $Presupuesto ,
                $Pre_Factura ,
                $paciente ,
                $NumeroAtencion  ,
                $habitacion ,
                $Procedencias ,
                $Paquete_Atencion ,
                $Nombre_Responsable ,
                $Direccion_Responsable ,
                $Documento_Responsable ,
                $Telefono_Responsable ,
                $Plan_Atencion ,
                $CoaSeguro ,
                $Deducible ,
                $Observaciones ,
                $Siniestro ,
                $Referencia_1 ,
                $Referencia_2 ,
                $Medico_Fecha  ,
                $Servicios ,
                $medico  ,
                $principal  ,
                $asociado_1 ,
                $asociado_2 ,
                $Medico_Observacion ,
                $garantia,
                $Garantia_plan,
                $Garantia_Coaseguros ,
                $Garantia_Deducible ,
                $Garantia_Moneda ,
                $Garantia_siniestro ,
                $Garantia_referencia_1 ,
                $Garantia_referencia_2 ,
                $Dieta,
                $Dieta_Observaciones  ,
                $Dieta_Contenido ,
                $Varios_Fecha ,
                $Varios_Musica ,
                $Varios_Musica_desde ,
                $Varios_Musica_hasta ,
                $Varios_Televisor  ,
                $Varios_Televisor_desde ,
                $Varios_Televisor_hasta ,
                $Varios_Periodico ,
                $Varios_Acompanante ,
                $Varios_Acepta_Visitas_si ,
                $Varios_Acepta_Visitas_no ,
                $Varios_Acepta_Visitas_restringido ,
                $Varios_Acepta_llamadas ,
                $Varios_Dieta_aconpanante ,
                $fecha_egreso  ,
                $medico_egreso ,
                $principal_egreso ,
                $asociadoEgreso1 ,
                $asociadoEgreso2 ,
                $tipo_egreso ,
                $complicacion ,
                $tipo_complicacion ,
                $des_campo1 ,
                $des_campo2 ,
                $des_campo3 ,
                $fecha_registro  ,
                $usuario_registro  ,
                $fecha_modificacion  ,
                $usuario_modificacion  ,
                $pcname ,
                $status ,
                $codigo_cama ,
                $traspaso ,
                $fecha_hora_traspaso ,
                $genero ,
                $observacion_traspaso,
                $transaccion  ,
                $titular ,
                $tipo_egreso_alta ,
                $derivacion ,
                $hospital ,
                $status_documento ,
                $osbservacion_documento,
                $observacion_eliminada));
    }


}
