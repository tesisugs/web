<?php
namespace App\Exports;
use App\Core\Procedures\AdministracionProcedure;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use  Illuminate\Support\Collection as Collection;

class InvoicesExport implements FromView
{
    protected $fecha_ini;
    protected $fecha_fin;

    public function __construct($desde,$hasta)
    {
        $this->fecha_ini = $desde;
        $this->fecha_fin = $hasta;
    }

    public function view(): View
    {
        $resultados = \DB::select('Call spConsultarMedicosLLamados2(?,?)',array($this->fecha_ini ,$this->fecha_fin));
        //dd($resultado);
        return view('exports.invoices', [
            'resultados' => $resultados
        ]);
    }

/*
    public function collection()
    {
        $resultado = \DB::select('Call spConsultarMedicosLLamados2(?,?)',array('2018/07/13','2018/07/13'));
        $resultado = (object) $resultado ;
        $result = \DB::table('users')->get();
        $var = ['numbers' => [1,2,3,4,5] , 'names' => ['jhon', 'doe', 'jane', 'dann'] ];
        $collection  = Collection::make($var);
        $collection2  = Collection::make($resultado);
        dd($collection);
        //dd($collection);
        //dd(User::all());


        return $collection;
    }
*/
}