<?php
/**
 * Created by PhpStorm.
 * User: bryan
 * Date: 28/8/2018
 * Time: 15:39
 */
namespace App\Exports;
use App\Core\Procedures\AdministracionProcedure;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use  Illuminate\Support\Collection as Collection;

class InvoiceExportEstadistica implements FromView
{

    protected $fecha_ini;
    protected $fecha_fin;

    public function __construct($desde, $hasta, $tipo)
    {
        $this->fecha_fin = $hasta;
        $this->fecha_ini = $desde;
        $this->tipo = $tipo;
    }

    public function view(): View
    {
        if ($this->tipo === 'mes') {
            $this->fecha_ini = $this->fecha_ini . '-01';
            $this->fecha_fin = $this->fecha_fin . '-01';
            $resultados = \DB::select('Call spConsultarHospitalizacionesMes(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('modulos.administracion.Excel.hospitalizacion.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'periodo') {
            $resultados = \DB::select('Call spConsultarHospitalizacionesPeriodos(?,?)', array($this->fecha_ini, $this->fecha_fin));
            //dd($resultados);
            return view('modulos.administracion.Excel.hospitalizacion.periodo', [
                'resultados' => $resultados
            ]);
        }

        if ($this->tipo === 'year') {
            $resultados = \DB::select('Call spConsultarHospitalizacionesEYears(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('exports.invoices', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'rango') {
            $resultados = \DB::select('Call spConsultarHospitalizacionesRango(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('modulos.administracion.Excel.hospitalizacion.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'semana') {
             $resultados = \DB::select('Call SpConsultarHospitaliacionesSemanas(?,?)',array($this->fecha_ini ,$this->fecha_fin));
             return view('modulos.administracion.Excel.hospitalizacion.periodo', [
                 'resultados' => $resultados
             ]);
            echo "semana";
        }

    }


    }