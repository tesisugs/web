<?php
/**
 * Created by PhpStorm.
 * User: bryan
 * Date: 2/9/2018
 * Time: 12:34
 */

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InvoiceExportEstadisticaCe implements FromView
{

    protected $fecha_ini;
    protected $fecha_fin;

    public function __construct($desde, $hasta, $tipo)
    {
        $this->fecha_fin = $hasta;
        $this->fecha_ini = $desde;
        $this->tipo = $tipo;
    }

    public function view(): View
    {
        if ($this->tipo === 'mes') {
            $this->fecha_ini = $this->fecha_ini . '-01';
            $this->fecha_fin = $this->fecha_fin . '-01';
            $resultados = \DB::select('Call spConsultarCeMesAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('modulos.administracion.Excel.consultaexterna.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'periodo') {
            $resultados = \DB::select('Call spConsultarCePeriodosAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            //dd($resultados);
            return view('modulos.administracion.Excel.consultaexterna.periodo', [
                'resultados' => $resultados
            ]);
        }

        if ($this->tipo === 'year') {
            $resultados = \DB::select('Call spConsultarCeYearAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('exports.invoices', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'rango') {
            $resultados = \DB::select('Call spConsultarCeRangoAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('modulos.administracion.Excel.consultaexterna.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'semana') {
             $resultados = \DB::select('Call SpConsultarCeSemanasAll(?,?)',array($this->fecha_ini ,$this->fecha_fin));
            return view('modulos.administracion.Excel.consultaexterna.periodo', [
                 'resultados' => $resultados
             ]);
            //echo "semana";
        }

    }

}