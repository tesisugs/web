<?php
/**
 * Created by PhpStorm.
 * User: bryan
 * Date: 2/9/2018
 * Time: 12:33
 */

namespace App\Exports;
use App\Core\Procedures\AdministracionProcedure;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use  Illuminate\Support\Collection as Collection;


class InvoiceExportEstadisticaEgreso implements FromView
{

    protected $fecha_ini;
    protected $fecha_fin;

    public function __construct($desde, $hasta, $tipo)
    {
        $this->fecha_fin = $hasta;
       $this->fecha_ini = $desde;
        $this->tipo = $tipo;
    }

    public function view(): View
    {


        if ($this->tipo === 'mes') {
            $this->fecha_ini = $this->fecha_ini . '-01';
            $this->fecha_fin = $this->fecha_fin . '-01';
            $resultados = \DB::select('Call spConsultarEgresosMesAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            //dd($resultados);
            return view('modulos.administracion.Excel.egreso.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'periodo') {
            $resultados = \DB::select('Call spConsultarEgresosPeriodosAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            //dd($resultados);
            return view('modulos.administracion.Excel.egreso.periodo', [
                'resultados' => $resultados
            ]);
        }

        if ($this->tipo === 'year') {
            $resultados = \DB::select('Call spConsultarEgresosYearAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('modulos.administracion.Excel.egreso.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'rango') {
            $resultados = \DB::select('Call spConsultarEgresosRangoAll(?,?)', array($this->fecha_ini, $this->fecha_fin));
            return view('modulos.administracion.Excel.egreso.periodo', [
                'resultados' => $resultados
            ]);
        }
        if ($this->tipo === 'semana') {
             $resultados = \DB::select('Call SpConsultarEgresosSemanasAll(?,?)',array($this->fecha_ini ,$this->fecha_fin));
             return view('modulos.administracion.Excel.egreso.periodo', [
                 'resultados' => $resultados
             ]);
            echo "semana";
        }

    }

}