<?php
Route::middleware(['auth'])->group(function() {

    Route::group(['prefix' => 'hospitalizacion' , 'as' => 'hospitalizacion.' ], function() {
        // consultas para los index
        route::get('/ingresos','Hospitalizacion\IngresoController@index')->name('ingreso')->middleware('permission:hospitalizacion');
        route::get('/egresos','Hospitalizacion\EgresoController@index')->name('egreso');
        route::get('/anulaciones','Hospitalizacion\AnulacionController@index')->name('anulacion');
        route::get('/reasignacionmedico','Hospitalizacion\ReasignacionMedicoController@index')->name('reasignacionmedico');
        route::get('/reasignacionseguro','Hospitalizacion\ReasignacionSeguroContoller@index')->name('reasignacionseguro');
        route::get('/trapasopacientes','Hospitalizacion\TraspasoController@index')->name('traspasopacientes');

        route::get('/tipodiagnostico/{descripcion}','Hospitalizacion\EgresoController@tipoDiagnosticoNombre')->name('diagnostico');
        route::get('/tipodiagnostico/{codigo}/codigo','Hospitalizacion\IngresoController@consultarTipoDiagnosticoCodigo')->name('diagnostico.codigo');
        route::get('/validarhospitalizacion/{paciente}','Hospitalizacion\IngresoController@validarInsert')->name('validarinsert');


        // rutas de carga de informacion
        route::get('/pacientesporfecha/{estado}/{desde}/{hasta}/{paciente?}','Hospitalizacion\EgresoController@hospitalizacionFechas')->name('hospitalizacionfechas');
        route::get('/pacientes/fecha/egreso/{desde}/{hasta}/{paciente?}','Hospitalizacion\EgresoController@hospitalizacionFechasEgreso')->name('hospitalizacionfechas.egreso');
        route::get('/pacientes/fecha/todos/{paciente}','Hospitalizacion\IngresoController@consultarHospitalizacionTodos');


        route::get('/pacientes/{paciente}','Hospitalizacion\EgresoController@hospitalizacionFechas')->name('hospitalizacionfechas');
        route::get('/registroadmision','Hospitalizacion\ReasignacionMedicoController@consultarRegistroAdmision')->name('reasignacion.admision');
        route::get('/consultarPacientesNombre/{apellido}/{estado}','Hospitalizacion\IngresoController@consultarPacientesNombre');
        route::get('/consultar/paciente/medico/{paciente}/{medico}/{fecha}','Hospitalizacion\IngresoController@consultarMedicosLlamadoIngresoHospitalario');
        route::get('/consultarhospitalizacionid/{id}','Hospitalizacion\ReasignacionMedicoController@consultarHospitalizacionId');
        route::get('/consultarcamasporsala/{id}','Hospitalizacion\IngresoController@consultarCamaPorSalaCodigoSala');
        route::get('/consultarcamas/{id}','Hospitalizacion\IngresoController@consultarCamaPorSalaCodigo');
        route::get('/consultarcamashabilitadas/{sala}/{cama}','Hospitalizacion\IngresoController@consultarCamaPorSalaSalaHabilitadaCunas');
        route::get('/consultarpaciente/{id}','Hospitalizacion\IngresoController@consultarPacienteId');
        route::get('registropacientes/TitularAfiliadoPorCedulaAll/{cedula}','Emergencia\RegistroPacientesController@ConsultaCedulaAll');
        route::get('registropacientes/TitularAfiliadoPorCedula/{cedula}','Emergencia\RegistroPacientesController@ConsultaCedula');



        // combos
        route::get('/estadoshospitalizacion','Hospitalizacion\EgresoController@estadosHospitalizacion')->name('estadohospitalizacion');
        route::get('/cmbegreso','Hospitalizacion\EgresoController@tipoEgreso')->name('tipoegreso');
        route::get('/cmbegresoalta','Hospitalizacion\EgresoController@tipoEgresoAlta')->name('tipoegresoalta');
        route::get('/cmbderivacionhospital','Hospitalizacion\IngresoController@consultarDerivacionHospital')->name('tipoegresoalta');



        // metodos de store, update and delete
        route::post('/cambiomedico','Hospitalizacion\ReasignacionMedicoController@updateHospitalizacionMedico')->name('updatemedico');
        route::post('/cambioseguro','Hospitalizacion\ReasignacionMedicoController@updateHospitalizacionSeguro')->name('updateseguro');
        route::post('/egresoshospitalizacion','Hospitalizacion\EgresoController@insertHospitalizacionEgresos')->name('egresopacientes');
        route::post('/anulacionhospitalizacion','Hospitalizacion\AnulacionController@SpAnulacionHospitalizacion')->name('anulacionpaciente');
        route::post('/ingresohospitalizacion','Hospitalizacion\IngresoController@insertHospitalizacion')->name('ingresopacientes');
        route::post('/ingresotraslado','Hospitalizacion\TraspasoController@insertTrasladoHospitalizacion')->name('trasladopacientes');


        // combos
        route::get('/seguro', 'Emergencia\RegistroPacientesController@cmbSeguro');
        route::get('/medico','ConsultaExterna\AgendaPacientesController@consultarMedico')->name('medico');
        route::get('/especializacion','ConsultaExterna\AgendaPacientesController@consultarEspecializacion')->name('especiaizacion');
        route::get('/sala','Hospitalizacion\ReasignacionMedicoController@consultarSala');
        route::get('/unidad','Hospitalizacion\IngresoController@consultarUnidad');
        route::get('/especialidadmedicos/{id}','ConsultaExterna\AgendaPacientesController@consultarEspecialidadMedico')->name('consultaexterna.especialidadmedico');

        // rutas para imprimir
        route::get('/reporte/egreso/{hosp}/{egreso}','Hospitalizacion\EgresoController@imprimirEgreso')->name('reporte.ingreso');


        route::post('/garantia/admision','Hospitalizacion\EgresoController@imprimirTicket')->name('ticket.egreso');

        route::get('/rotulo','Hospitalizacion\IngresoController@rotuloHospitalizacion')->name('rotulo.ingreso');
        route::get('/reporte/ingreso/{hospitalizacion}','Hospitalizacion\IngresoController@imprimirIngreso')->name('reporte.ingreso');
        route::get('/garantia/admision/{hospitalizacion}','Hospitalizacion\IngresoController@garantia')->name('garantia.egreso');
        route::get('/registro/admision/{hospitalizacion}','Hospitalizacion\IngresoController@ReporteAdmision')->name('garantia.egreso');


    });
});
?>