<?php

Route::middleware(['auth'])->group(function () {
    //agenda pacientes

    route::get('/agendapacientes','ConsultaExterna\AgendaPacientesController@index')->name('agendapacientes.index');
    route::get('/horariosmedicos','ConsultaExterna\AgendaPacientesController@horario')->name('horariomedico');

    // combos
    route::get('/medico','ConsultaExterna\AgendaPacientesController@consultarMedico')->name('consultaexterna.medico');
    route::get('/especializacion','ConsultaExterna\AgendaPacientesController@consultarEspecializacion')->name('consultaexterna.especiaizacion');
    route::get('/tipoconsulta','ConsultaExterna\AgendaPacientesController@consultarTipoConsulta')->name('consultaexterna.tipoconsulta');
    route::get('/dependencias','ConsultaExterna\AgendaPacientesController@consultarDependencias')->name('consultaexterna.tipoconsulta');
    route::get('/especialidadmedicos/{id}','ConsultaExterna\AgendaPacientesController@consultarEspecialidadMedico')->name('consultaexterna.especialidadmedico');


    route::get('consultar/turno/agenda/{medico}/{fecha}','ConsultaExterna\AgendaPacientesController@consultarTurnosAgenda');
    route::get('/horariosmedicosdetalle','ConsultaExterna\AgendaPacientesController@consultarHorarioMedicoDetalle')->name('consultaexterna.horariosmedicodetalle');
    route::get('/horariosmedico/{medico}','ConsultaExterna\AgendaPacientesController@consultarHorarioMedico')->name('consultaexterna.horariosmedico');

    route::get('/horariosmedicosdetallefr','ConsultaExterna\AgendaPacientesController@consultarHorarioMedicoDetalleiframe')->name('consultaexterna.horariosmedicodetallefr');

    route::post('/insertconsultaexternaturnos','ConsultaExterna\AgendaPacientesController@InsertConsultaExternaTurnosDiarios2')->name('consultaexterna.insertturnosd');

    route::get('/consultarPacienteHospitalizacionEgresado/{paciente}','ConsultaExterna\AgendaPacientesController@consultarPacienteHospitalizacionEgresado');
    route::get('/consultarPacientesCartaIess/{paciente}','ConsultaExterna\AgendaPacientesController@consultarPacientesCartaIess');
    route::get('/atencionesmedico/{medico}','ConsultaExterna\AgendaPacientesController@consultarIngresosPacientesConsultaExternaNumeroAtencionMedicos');
    route::get('/atencionesmedico/{medico}/{fecha}','ConsultaExterna\AgendaPacientesController@consultarIngresosPacientesCENumeroAtencionMedicosDia');
    route::get('/consultar/paciente/{medico}/{fecha}/{paciente}','ConsultaExterna\AgendaPacientesController@consultarPacienteAgendado');


    route::get('/consultar/paciente/{id}','ConsultaExterna\AgendaPacientesController@tempConsultarPacienteId');
    route::get('/consultarmedico/{codigo}','ConsultaExterna\AgendaPacientesController@consultarMedicoPorCodigo');
    route::get('/consultar/atencion/{medico}','ConsultaExterna\AgendaPacientesController@horaUltimaAtencion');
    route::get('/consultar/atencion/{medico}/{dia}','ConsultaExterna\AgendaPacientesController@horaUltimaAtencionDia');
    route::get('/ticket','ConsultaExterna\AgendaPacientesController@imprimirTicket')->name('ticket');



    // preparacion pacientes
    route::get('/preparacionpacientes','ConsultaExterna\ConsultaPacientesController@index')->name('consultaexterna.preparacionpacientes');
    route::get('/consultarpreparacion/{desde}/{hasta}/{paciente?}/{medico?}/{dependencias?}','ConsultaExterna\ConsultaPacientesController@consultarPreparacion');
    route::get('/consultarpreparacionhoy','ConsultaExterna\ConsultaPacientesController@consultarPreparacion2');
    route::get('/consultar/pacientes/{consulta}','ConsultaExterna\ConsultaPacientesController@consultarPreparacionPacientes');

    route::post('/preparacion/{id}/update','ConsultaExterna\ConsultaPacientesController@update');

    // agenda medico
    route::get('/agendamedico','ConsultaExterna\AgendaMedicoController@index')->name('consultaexterna.agendamedico');
    route::get('/consulta/agenda/medico','ConsultaExterna\AgendaMedicoController@consultarAgendaMedicoTurnos2');

    route::post('/agendapacientes/store','ConsultaExterna\AgendaPacientesController@store')->name('agendapacientes.store');

    route::get('/form',function (){
        return view('ConsultaExterna.prueba');
    });

    // Medicos Consulta Externa
    route::get('/medicos/consulta','ConsultaExterna\MedicosController@index')->name('consultaexterna.medicos');
    route::get('/tipodiagnostico/{descripcion}','Hospitalizacion\EgresoController@tipoDiagnosticoNombre')->name('diagnostico');
    route::post('/medicos/store/ce','ConsultaExterna\MedicosController@store')->name('medicos.consulta.externa.store');


});


?>