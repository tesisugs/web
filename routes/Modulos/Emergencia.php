<?php

    Route::middleware(['auth'])->group(function() {

            Route::group(['prefix' => 'emergencia' , 'as' => 'emergencia.' ], function() {
                // consultas
                route::get('/registropacientes/{id?}','Emergencia\RegistroPacientesController@index')->name('registropacientes')->middleware('permission:emergencia.registropacientes');

                route::get('registropacientes/TitularAfiliadoPorApellido/{apellido}','Emergencia\RegistroPacientesController@TitularAfiliadoPorApellido')->name('consultarApellido');
                route::get('registropacientes/TitularAfiliadoPorCedula/{cedula}','Emergencia\RegistroPacientesController@ConsultaCedula');
                route::get('registropacientes/TitularAfiliadoPorCedulaAll/{cedula}','Emergencia\RegistroPacientesController@ConsultaCedulaAll');
                route::get('registropacientes/informacionadicional/{id_paciente}','Emergencia\RegistroPacientesController@ConsultarInformacionAdicional');
                route::get('registropacientes/admision','Emergencia\RegistroPacientesController@ReporteAdmision')->name('reporteadmision');
                route::get('registropacientes/registrorapido','Emergencia\RegistroPacientesController@RegistroRapido')->name('registrorapido');
                //combos
                route::get('/tipoidentificacion', 'Emergencia\RegistroPacientesController@cmbTipoIdentificacion');
                route::get('/nacionalidad', 'Emergencia\RegistroPacientesController@cmbNacionalidad');
                route::get('/sangre', 'Emergencia\RegistroPacientesController@cmbTipoSangre');
                route::get('/genero', 'Emergencia\RegistroPacientesController@cmbGeneroPaciente');
                route::get('/parentescoPaciente', 'Emergencia\RegistroPacientesController@cmbParentescoPaciente');
                route::get('/estadoCivil', 'Emergencia\RegistroPacientesController@cmbEstadoCivil');
                route::get('/etnia', 'Emergencia\RegistroPacientesController@cmbEtnia');
                route::get('/provincia/{id}', 'Emergencia\RegistroPacientesController@cmbProvincia');
                route::get('/ciudad/{id}', 'Emergencia\RegistroPacientesController@cmbCiudad');
                route::get('/parroquia/{ciudad}', 'Emergencia\RegistroPacientesController@cmbParroquia');
                route::get('/seguro', 'Emergencia\RegistroPacientesController@cmbSeguro');
                route::get('/otroseguro', 'Emergencia\RegistroPacientesController@cmbOtroSeguro');
                route::get('/parentesco/{parentesco}', 'Emergencia\RegistroPacientesController@cmbBeneficiarioPorParentesco');
                route::get('/documentosiess', 'Emergencia\RegistroPacientesController@CmbDpcumentosIess');
                route::get('/tipoingreso', 'Emergencia\RegistroPacientesController@cmbIngresoCalificacion');
                // acciones
                route::post('/registropacientes/storepacientes', 'Emergencia\RegistroPacientesController@StorePaciente')->name('registropacientes.storePacientes');
                route::post('/registropacientes/guardarpacientes', 'Emergencia\RegistroPacientesController@GuardarPaciente')->name('registropacientes.guardar');

                route::get('/asistenciamedica','Emergencia\AsistenciaMedicaController@index')->name('asistencia');
                // asistencia medica
                route::get('/consultarmedico', 'Emergencia\AsistenciaMedicaController@cmbMedico');
                route::get('/consultarbeneficiario/{cedula}', 'Emergencia\AsistenciaMedicaController@ConsultarBeneficiarioTitularAfiliado');
                route::get('/ConsultarRegistroAdmision', 'Emergencia\AsistenciaMedicaController@ConsultarRegistroAdmision');
                route::get('/consultarpacienteid/{id}', 'Emergencia\AsistenciaMedicaController@ConsultarPacienteId');
                route::get('/consultarfirmas/{atencion}/{servicio}/{visita}/{documento}', 'Emergencia\AsistenciaMedicaController@Consultarfirmas');
                route::get('/consultarparentesco2', 'Emergencia\AsistenciaMedicaController@cmbParentesco');
                route::post('/asistenciamedica/storepacientes', 'Emergencia\AsistenciaMedicaController@StoreAsistencia')->name('asistenciamedica.store');
                route::get('/consultarpacientesadmision', 'Emergencia\AsistenciaMedicaController@consultarpacienteAdmision');
                route::get('/actadeservicio','Emergencia\AsistenciaMedicaController@printActaServicio')->name('asistenciamedica.actaservicio');
                route::get('/cmbderivacionhospital','Hospitalizacion\IngresoController@consultarDerivacionHospital')->name('tipoegresoalta');

                route::get('/consultar/atenciones/{id}', 'Emergencia\AsistenciaMedicaController@consultarAtencionPaciente');

                // rutas de prueba
                route::get('/prueba/{id}','Emergencia\AsistenciaMedicaController@prueba');

            });
    });
?>