<?php

Route::middleware(['auth'])->group(function () {

    Route::group(['prefix' => 'administracion' , 'as' => 'administracion.' ], function() {

        // pantallas
        route::get('/notificaciones','Administracion\NotificacionesController@index')->name('notificaciones');
        route::get('/estado','Administracion\EstadoController@index')->name('estado');
        route::get('/estadistica','Administracion\EstadisticaController@index')->name('estadistica');

        //consultas
        route::get('/roles','Administracion\NotificacionesController@roles')->name('roles');

        route::get('/estadisticasingresohospitalario','Administracion\EstadisticaController@estadisticaRegistrosIngresoHospitalizacion')->name('hospitalizacion.ingreso');
        route::get('/estadisticas/registrospacientes','Administracion\EstadisticaController@estadisticaRegistrosPacientes');
        route::get('/estadisticas/atenciones/emergencia','Administracion\EstadisticaController@estadisticaRegistrosAtencionesE');
        route::get('/estadisticas/atenciones/ce','Administracion\EstadisticaController@estadisticaRegistrosIngresoAtencionesCE');

        route::get('/estadocamas','Administracion\EstadoController@camasHabilitadas')->name('estado.camas');
        route::get('/estadisticasegresohospitalario','Administracion\EstadisticaController@estadisticaRegistrosEgresosHospitalizacion')->name('hospitalizacion.egreso');


        // metodos store y update
        route::post('/notificacionesrol','Administracion\NotificacionesController@storeNotificacionesPorRol')->name('storenotificacionesrol');
        route::post('/notificacionesrapidas','Administracion\NotificacionesController@storeNotificacionesRapidas')->name('storenotificacionespush');


        /**
         * Medicos al llamado.
         */
            //pantallas
        route::get('/medicosallllamado','Administracion\MediciosAlLLamadoController@index')->name('medicosalllamado.index');


            // consultas
        route::get('/consultarPacientesNombre/{apellido}/{estado}','Hospitalizacion\IngresoController@consultarPacientesNombre');
        route::get('/consultarhospitalizacionid/{id}','Hospitalizacion\ReasignacionMedicoController@consultarHospitalizacionId');
        route::get('/consultarpaciente/{id}','Hospitalizacion\IngresoController@consultarPacienteId');
        route::get('/correosenviados/{f_ini}/{f_fin}','Administracion\MediciosAlLLamadoController@consultarMedicosLLamados2');

        route::get('/especialidadmedico/{id}','Administracion\MediciosAlLLamadoController@consultarEspecializacionMedico')->name('especialidadmedico');

        //update - insert
        route::post('/enviocorreo','Administracion\MediciosAlLLamadoController@store')->name('store.enviocorreos');
        route::post('/updatemedicoalllamado/{id}','Administracion\MediciosAlLLamadoController@update')->name('update.medicoalllamado');
        route::get('/excel/{desde}/{hasta}','Administracion\MediciosAlLLamadoController@exportarLista')->name('descargar.excel');

        /**
         * General.
         */
            //combos
            route::get('/seguro', 'Emergencia\RegistroPacientesController@cmbSeguro');
            route::get('/medico','ConsultaExterna\AgendaPacientesController@consultarMedico')->name('medico');
            route::get('/especializacion','ConsultaExterna\AgendaPacientesController@consultarEspecializacion')->name('especiaizacion');
            route::get('/especialidadmedicos/{id}','ConsultaExterna\AgendaPacientesController@consultarEspecialidadMedico')->name('consultaexterna.especialidadmedico');

        /**
         * Detalles-Estadisticas.
         */
        route::get('/detalles{grafico}','Administracion\EstadisticaController@IndexDetalle')->name('graficos.detalles');

        //Hospitalizacion
        route::get('/estadisticasingresohospitalario/years','Administracion\EstadisticaController@estadisticaRegistrosIngresoHospitalizacionYears');
        route::get('/estadisticas/ingresos/hospitalario/mes/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarHospitaliacionesMes');
        route::get('/estadisticas/ingresos/hospitalario/años/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarHospitaliacionesYears');
        route::get('/estadisticas/ingresos/hospitalario/semanas/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarHospitaliacionSemanas');
        route::get('/estadisticas/ingresos/hospitalario/rangos/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarHospitaliacionRangos');
        route::get('/estadisticas/excel/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarHospitaliacionExcel');
        route::get('/estadisticas/pdf/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarHospitaliacionPdf');

        // egresos
        route::get('/estadisticasegresohospitalario/years/{fecha1}/{fecha2}','Administracion\EstadisticaController@estadisticaRegistrosEgresosYears');

        route::get('/estadisticas/egreso/hospitalario/mes/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEgresosMes');
        route::get('/estadisticas/egreso/hospitalario/años/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEgresosYears');
        route::get('/estadisticas/egreso/hospitalario/semanas/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEgresosSemanas');
        route::get('/estadisticas/egreso/hospitalario/rangos/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEgresosRangos');
        route::get('/estadisticas/excel/egreso/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarEgresosExcel');
        route::get('/estadisticas/pdf/egreso/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarEgresoPdf');

        // registro pacientes
        route::get('/estadisticas/registro/pacientes/years/{fecha1}/{fecha2}','Administracion\EstadisticaController@estadisticaRegistrosPacientesYears');

        route::get('/estadisticas/registro/pacientes/mes/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarRegistroPacientesMes');
        route::get('/estadisticas/registro/pacientes/años/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarRegistroPacientesYears');
        route::get('/estadisticas/registro/pacientes/semanas/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarRegistroPacientesSemanas');
        route::get('/estadisticas/registro/pacientes/rangos/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarRegistroPacientesRangos');
        route::get('/estadisticas/excel/registro/pacientes/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarRegistroPacientesExcel');
        route::get('/estadisticas/pdf/registro/pacientes/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarRegistroPacientesPdf');

        // emergencia
        route::get('/estadisticas/emergencias/years/{fecha1}/{fecha2}','Administracion\EstadisticaController@estadisticaRegistrosEmergenciaYears');


        route::get('/estadisticas/emergencias/mes/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEmergenciasMes');
        route::get('/estadisticas/emergencias/años/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEmergenciasYears');
        route::get('/estadisticas/emergencias/semanas/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEmergenciasSemanas');
        route::get('/estadisticas/emergencias/rangos/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarEmergenciasRangos');
        route::get('/estadisticas/excel/emergencias/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarEmergenciasExcel');
        route::get('/estadisticas/pdf/emergencias/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarEmergenciasPdf');

        // consulta externa
        route::get('/estadisticas/ce/years/{fecha1}/{fecha2}','Administracion\EstadisticaController@estadisticaRegistrosCeYears');

        route::get('/estadisticas/ce/mes/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarCeMes');
        route::get('/estadisticas/ce/años/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarCeYears');
        route::get('/estadisticas/ce/semanas/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarCeSemanas');
        route::get('/estadisticas/ce/rangos/{fecha1}/{fecha2}','Administracion\EstadisticaController@consultarCeRangos');
        route::get('/estadisticas/excel/ce/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarCeExcel');
        route::get('/estadisticas/pdf/ce/{fecha1}/{fecha2}/{tipo}','Administracion\EstadisticaController@consultarCePdf');

    });

});


?>