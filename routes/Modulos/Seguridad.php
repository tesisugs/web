<?php


	Route::middleware(['auth'])->group(function (){

        // usuarios
        route::get('/usuarios','Seguridad\UserController@index')->name('user.index')->middleware('permission:user.index');
        route::get('/usuariostables','Seguridad\UserController@table')->name('user.table')->middleware('permission:user.index');
        route::post('/usuariosstore','Seguridad\UserController@store')->name('user.store')->middleware('permission:user.index');
        route::get('/roles','Seguridad\UserController@cmbRoles')->name('user.roles')->middleware('permission:user.index');
        route::get('/usershow/{id}','Seguridad\UserController@show')->name('user.show')->middleware('permission:user.index');
        route::get('/useredit/{id}','Seguridad\UserController@edit')->name('user.edit')->middleware('permission:user.index');
        route::post('/usuarios/{id}/update','Seguridad\UserController@update')->name('user.update')->middleware('permission:user.index');
        route::post('/usuarios/{id}/estado','Seguridad\UserController@estado')->name('user.estado')->middleware('permission:user.index');
        route::get('/usuarios/departamentos','Seguridad\UserController@departamentos')->name('user.departamentos')->middleware('permission:user.index');
        route::get('/validar/ip/{ip}','Seguridad\UserController@ip')->name('validar.ip')->middleware('permission:user.index');
        route::get('/validar/usuario/{usuario}','Seguridad\UserController@usuario')->name('validar.usuario')->middleware('permission:user.index');
        route::get('/validar/ip/update/{ip}/{id}','Seguridad\UserController@ipUpdate')->name('validar.ip.update')->middleware('permission:user.index');
        route::get('/validar/usuario/update/{usuario}/{id}','Seguridad\UserController@usuarioUpdate')->name('validar.usuario.update')->middleware('permission:user.index');
        route::get('/usuarios/nombre/{rol}/{nombre?}','Seguridad\UserController@consularUsuarioName')->name('usuario.comsulta')->middleware('permission:usuario.consulta');


        // permisos
        route::get('/permisos','Seguridad\PermissionController@index')->name('permisos.index');
        route::get('/permisostables','Seguridad\PermissionController@table')->name('permisos.table');
        route::post('/permisosstore','Seguridad\PermissionController@store')->name('permisos.store');
        route::get('/permisosshow/{id}','Seguridad\PermissionController@show')->name('permisos.show')->middleware('permission:permisos.index');
        route::get('/permisosedit/{id}','Seguridad\PermissionController@edit')->name('permisos.edit')->middleware('permission:permisos.index');
        route::post('/permisos/{id}/update','Seguridad\PermissionController@update')->name('permisos.update')->middleware('permission:permisos.index');
        route::post('/permisos/{id}/delete','Seguridad\PermissionController@destroy')->name('permisos.estado')->middleware('permission:permisos.index');
        route::get('/permisos/{permiso?}/nombre','Seguridad\PermissionController@consularPermisosName')->name('permisos.comsulta')->middleware('permission:permisos.consulta');

        //roles
        route::get('/rolestables','Seguridad\RolController@table')->name('roles.table');
        route::get('/rolcreate','Seguridad\RolController@create')->name('roles.create');
        route::get('/rol','Seguridad\RolController@index')->name('roles.index');
        route::post('/rolesstore','Seguridad\RolController@store')->name('roles.store');
        route::get('/rolesshow/{id}','Seguridad\RolController@show')->name('roles.show')->middleware('permission:roles.index');
        route::get('/rolesedit/{id}','Seguridad\RolController@edit')->name('roles.edit')->middleware('permission:roles.index');
        route::post('/roles/{id}/update','Seguridad\RolController@update')->name('roles.update')->middleware('permission:roles.index');
        route::get('/validar/rol/{name}/name','Seguridad\RolController@uniqueName')->name('validar.rol.name')->middleware('permission:roles.index');
        route::get('/validar/rol/{name}/slug','Seguridad\RolController@uniqueSlug')->name('validar.rol.slug')->middleware('permission:roles.index');
        route::get('/validar/update/rol/{name}/{id}/name','Seguridad\RolController@uniqueNameUpdate')->name('validar.rol.update.name')->middleware('permission:roles.index');
        route::get('/validar/update/rol/{name}/{id}/slug','Seguridad\RolController@uniqueSlugUpdate')->name('validar.rol.update.slug')->middleware('permission:roles.index');

        route::post('/roles/{id}/delete','Seguridad\RolController@destroy')->name('roles.estado')->middleware('permission:roles.index');
        route::get('/rolespermisos/{id}','Seguridad\RolController@rolesPermisos')->name('roles.permisos');

    });





?>