<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/Modulos/Emergencia.php';
require __DIR__ . '/Modulos/Hospitalizacion.php';
require __DIR__ . '/Modulos/Seguridad.php';
require __DIR__ .  '/Modulos/ConsultaExterna.php';
require __DIR__ .  '/Modulos/Administracion.php';

Route::get('/emergencia', function () {
    return view('emergencia.index');
});
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/telegram',function (){
    //$activity = Telegram::getUpdates();
    //dd($activity);
    $text = 'Estimado Doctor se le ha enviado un correo para la realización de una hospitalizacion por favor responda a la menor brevedad posible.';
/*
    Telegram::sendMessage([
        'chat_id' => env('TELEGRAM_CHANNEL_ID', '-1001313292076'),
        'parse_mode' => 'HTML',
        'text' => $text
    ]); */

    Telegram::sendMessage([
        'chat_id' => env('TELEGRAM_CHANNEL_ID', '-1001333339812'),
        'parse_mode' => 'HTML',
        'text' => $text
    ]);
});
route::get('/prueba',function (){

    $mysqli = new mysqli("localhost:33066", "root", "", "majomacontrolhospitalario");
    if ($mysqli->connect_errno) {
        echo "Falló la conexión a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    $i=0;

    $resultado = array();
// $resultado = $mysqli->multi_query("CALL prueba");

    if (!$mysqli->multi_query("CALL spConsultar1RegistroAdmision(91261)")) {
        echo "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;
    }

    /*Ahora con este bucle recogemos los resultados y los recorremos*/
    do {
        /*En el if recogemos una fila de la tabla*/
        if ($res = $mysqli->store_result()) {
            /*Imprimimos el resultado de la fila y debajo un salto de línea*/

            array_push($resultado,$res->fetch_all());
            //print_r($res->fetch_all());
           // printf("\n");
            //echo "<br>";
            $i = $i +1;
            echo $i;

            /*La llamada a free() no es obligatoria, pero si recomendable para aligerar memoria y para evitar problemas si después hacemos una llamada a otro procedimiento*/
            $res->free();
        } else {
            if ($mysqli->errno) {
                echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
            }
        }
    } while ($mysqli->more_results() && $mysqli->next_result());
    /*El bucle se ejecuta mientras haya más resultados y se pueda saltar al siguiente*/

    print_r($resultado);
    $primer_select = $resultado[0]; // from tb1RegistroAdmision
    $segundo_select = $resultado[1]; // from tb1RegistroAdmision c, tbPaciente p
    //$tercer_select = $resultado[2];
    //$cuarto_select = $resultado[3];

    if($resultado[0] !== []) {
        $id = $resultado[0][0][0];
        $numero_atencion= $resultado[0][0][0];
        $afiliado = $resultado[0][0][0];
        $paciente = $resultado[0][0][0];
        $fecha = $resultado[0][0][0];
        $hora = $resultado[0][0][0];
        $responsable = $resultado[0][0][0];
        $observacion = $resultado[0][0][0];
        $doctor = $resultado[0][0][0];
        $des_campo1 = $resultado[0][0][0];
        $des_campo2 = $resultado[0][0][0];
        $des_campo3 = $resultado[0][0][0];
    }

    //echo  $resultado[0][0][1];
    //$resultado = object($resultado);
    //dd($resultado[0][0]);
    //dd($resultado[1][0]);
    //dd($resultado[2]);


    //print_r($resultado->fetch_assoc());

})->name('prueba');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');
route::get('/admision',function (){
    return view('admision');
})->name('estadisticas.admision');
route::get('/diseño',function (){
   // DB::table('users')->where('id',24)
     //               ->update([
       //                 'password' => bcrypt("12345678")
         //           ]);
    
    return view('diseño');
});
route::post('/diseño',function(Illuminate\Http\Request $request){
    $cantidad = count($_FILES["uploadedfile"]["tmp_name"]);
    $cantidad2 = count($request->input('documentos'));
    //dd($request->input());
    $documentos = $request->input('documentos');
    //$foto = addslashes(file_get_contents($_FILES['uploadedfile']['tmp_name']));

    for ($i=0; $i<$cantidad; $i++) {
        DB::table('tbimagenes')->insert(['documento'=> $documentos[$i]  , 'descripcion' => $request->input('observacion') ,'imagen'=> addslashes(file_get_contents($_FILES['uploadedfile']['tmp_name'][$i]))]);
    }


    //DB::table('tbimagenes')->insert(['documento'=>1 , 'descripcion' => $request->input('observacion') ,'imagen'=>$foto]);

});
route::get('/aceptar/{id}/{token}',function ($id,$token){

    $result = DB::table('tbtokenemail')
                    ->select('id')
                    ->where('id',$id)
                    ->where('token',$token)
                    ->where('estado',0)
                    ->get();
    if( isset($result[0]->id))
    {
        DB::table('tbtokenemail')
            ->where('id',$id)
            ->update(['estado' => 1]);

        $user = Auth::user();
        $usuario_ingreso = $user->id;
        \DB::select('Call SpUpdateMedicollamado(?,?,?,?)',array($id,1,'aceptado',$usuario_ingreso));
        // pasar notificacion

        $fecha = getdate();

        $day = $fecha['mday'];
        if($fecha['mon'] < 10){
            $mes = "0".$fecha['mon'];
        } else {
            $mes = $fecha['mon'];
        }
        $fecha_ingreso = $fecha['year']."/".$mes."/".$day;
        $rol = \DB::table('role_user')->select('role_id')->where('user_id',$user->id)->get();

        //$rol = (object) $rol;
        //echo $rol[0]->role_id;
        //dd($rol);

            $notificacion_id =\DB::table('tbnotificaciones')->insertGetId(
                ['rol_id' => $rol[0]->role_id,
                    'titulo' => 'Medicos al Llamado',
                    'descripcion' => 'El medico ha responido al llamado' ,
                    'icono' => 1,
                    'estado' => 1,
                    'fecha_ingreso' => $fecha_ingreso,
                    'usuario_ingreso' => $usuario_ingreso,
                    'fecha_modificacion' => $fecha_ingreso,
                    'usuario_modificacion' => $usuario_ingreso]
            );
            // guardamos en un array todos los los usuaios que tengan el rol de ingreso
            $user_rol = \DB::table('role_user')->select('user_id')->where('role_id',$rol[0]->role_id)->get();


           foreach ($user_rol as $users )
            {

                \DB::table('tbuser_notification')->insert(
                    [   'user_id' => $users->user_id,
                        'notification_id' => $notificacion_id,
                        'estado' => 1,
                        'fecha_ingreso' => "2018/07/19",
                        'usuario_ingreso' => $usuario_ingreso
                    ]
                );
            }

        return view('mail.respuestas.aceptarMedicoLLamado');
        } else {
        return view('mail.respuestas.ExpiradaMedicoLLamado');
    }




});
route::get('/declinar/{id}/{token}',function ($id,$token){
    $result = DB::table('tbtokenemail')
        ->select('id')
        ->where('id',$id)
        ->where('token',$token)
        ->where('estado',0)
        ->get();


    if( isset($result[0]->id) == true )
    {
        $user = \Auth::user();
        $usuario = $user->id;
        \DB::select('Call SpUpdateMedicollamado(?,?,?,?)',array($id,0,'rechazado',$usuario ));
        \DB::table('tbtokenemail')
            ->where('id', $id)
            ->update(['estado' => 1]);
        return view('mail.respuestas.declinarMedicoLLamado');
    } else {
        return view('mail.respuestas.ExpiradaMedicoLLamado');
    }
});
route::get('/notificacion','NotificacionesController@viewNotificaciones');
route::get('/total/notificaciones','Administracion\NotificacionesController@notificacionesUsuario');
route::get('/notificacionusuario','Administracion\NotificacionesController@historialNotificaciones')->name('historico.notificaciones');
route::get('/notificaciones/show/all',function (){

    $user = \Auth::user();
    $usuario = $user->id;
    $resultado = \DB::select('Call spNotificacionesUsuario(?)',array($usuario));

    return view('modulos.otros.notificacionesShow',compact('resultado'));


})->name('notificaciones.all');
route::get('/notificacion/leida/{id_u}/{id_n}',function ($id_u,$id_n){

    \DB::table('tbuser_notification')
        ->where([
            ['user_id', $id_u],['notification_id', $id_n]
        ])
        ->update(['estado' => 0]);

   return redirect()->route('notificaciones.all');

    //return view('modulos.otros.notificacionesShow',compact('resultado'));
})->name('notificacion.leida');
route::get('/indicadores/administracion','HomeController@IndicadoresAdministracion')->name('home.administracion');
route::get('/indicadores/admision','HomeController@IndicadoresAdmision')->name('home.admision');
route::get('/lista/camas','HomeController@estadoCamasSala')->name('home.camas');
route::get('/lista/cunas','HomeController@estadoCunasSala')->name('home.cunas');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

?>